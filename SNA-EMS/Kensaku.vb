﻿' 2016-04-26 YAMAOKA EDIT 設備一覧の表示構成を変更
' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更
' 2016-05-09 YAMAOKA EDIT HT取込データ管理リスト作成 

Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.IO

Imports Oracle.ManagedDataAccess.Client
Imports System.Text.RegularExpressions

'
'   クラス：Kensaku
'   処理　：設備管理の検索画面を構成し、検索から一覧表示、個別情報の編集まで対応する
'
Public Class Kensaku
    Public kanriNo As String = ""
    Dim str As String = ""
    Dim search_model_text As String = ""
    Dim search_kanri_no_text As String = ""
    Dim search_name As String = ""
    Private haita_flag As Boolean = False
    Private insert_flag As Boolean = False

    Private cacheMain As Dictionary(Of String, Main) = New Dictionary(Of String, Main)
    ' 2016-05-09 YAMAOKA EDIT HT取込データ管理リスト作成 START
    Private cacheHT As Dictionary(Of String, LendingStatus) = New Dictionary(Of String, LendingStatus)
    ' 2016-05-09 YAMAOKA EDIT HT取込データ管理リスト作成 END


    Public tabPageList As List(Of TabPage) = Nothing
    Private sourceTabsList As List(Of String) = Nothing
    Private showTabList As List(Of String) = Nothing

    Public gid As String = Nothing
    Private kousei As Kousei

    ' 2016-04-07 YAMAOKA EDIT 印刷前チェック START
    Private exsType As Integer = -1   '2015/09/11 taskplan ADD 前回表示種
    ' 2016-04-07 YAMAOKA EDIT 印刷前チェック END

    '
    '   クラスの新規生成処理
    '   
    Public Sub New(ByVal loginForm As Login)
        loginForm.Visible = False
        InitializeComponent()
        Me.Text = Ini.ReadValue(Ini.Config.UX, "Setting", "SYSNAME")

        '   ユーザー権限の検証
        If LoginRoler.Employee Is Nothing Then
            Me.Close()
            Application.ExitThread()
            Return
        End If
        SetUserRole()
    End Sub

    '
    '   画面情報の初期化処理
    '   
    Private Sub Kensaku_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Helper.SetAllControlText(Me)
            SetDataGridViewText(Me)
            M_HEADER_DataGridView.AutoGenerateColumns = False

            search_KUBUN.DataSource = ParameterRepository.GetParameters("SEARCH_TYPE", Nothing)
            search_KUBUN.DisplayMember = "PARAMETER_VALUE"
            search_KUBUN.ValueMember = "SEQ_NO"

            search_BASE.DataSource = ParameterRepository.GetParameters("BASE", Nothing)
            search_BASE.DisplayMember = "PARAMETER_VALUE"
            search_BASE.ValueMember = "PARAMETER_VALUE"

            ' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 START
            search_SEARCH_SELECT.DataSource = ParameterRepository.GetParameters("SEARCH_SELECT", Nothing)
            search_SEARCH_SELECT.DisplayMember = "PARAMETER_VALUE"
            search_SEARCH_SELECT.ValueMember = "PARAMETER_VALUE"

            search_HAICHI_KIJYUN.DataSource = ParameterRepository.GetParameters("HAICHI_KIJYUN", Nothing)
            search_HAICHI_KIJYUN.DisplayMember = "PARAMETER_VALUE"
            search_HAICHI_KIJYUN.ValueMember = "PARAMETER_VALUE"

            SetSearchLocked(0)

            ' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 END

            SearchData(False)

            '2018/09/11 無条件で非表示にしておく
            'If LoginRoler.Employee.AX_MST <> "E" Then
            '    btn_DelTana.Visible = False
            'Else
            '    btn_DelTana.Visible = True
            'End If



        Catch ex As Exception
            Helper.Log("Kensaku_Load", ex)
        End Try
    End Sub


    '
    '   システムの終了処理
    '   
    Private Sub Kensaku_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'LoginOut Log
        EmployeeRepository.AddLoginLog(LoginLog.LOGIN_OUT)

        Me.Dispose()
        Application.ExitThread()
    End Sub


    '
    '   システムの終了前処理
    '   
    Private Sub Kensaku_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02002"), MsgBoxStyle.YesNo, "Warning")
        If Rtn = 6 Then
            e.Cancel = False
            Return
        End If

        e.Cancel = True
        haita_flag = False
        Me.Cursor = Cursors.Default
    End Sub

    '
    '   画面の終了処理   
    '   
    Private Sub btn_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Close.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        If kousei IsNot Nothing Then
            kousei.Close()
            kousei.Dispose()
        End If

        Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02002"), MsgBoxStyle.YesNo, "Warning")
        If Rtn = 6 Then
            Me.Close()
            Me.Dispose()
            Application.ExitThread()
            Return
        End If

        haita_flag = False
        Me.Cursor = Cursors.Default
    End Sub

    '
    '   設備の検索処理の呼び出し  
    '   
    Private Sub Search_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Search.Click
        If search_WORD.Text.Length > 0 And search_SEARCH_SELECT.SelectedIndex = 0 Then
            MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02141"), MsgBoxStyle.Information, "Warning")
            Exit Sub
        End If
        SearchData(True)

    End Sub

    '
    '   設備の検索処理
    '   
    Public Sub SearchData(isRefreshDetail As Boolean, Optional KeepRow As Boolean = False)
        'Static exsType As String = ""   '2015/09/11 taskplan ADD 前回表示種

        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True

            Me.Cursor = Cursors.WaitCursor


            '/* 2015/09/15 taskplan ADD カーソル位置の保持
            Dim currow As Integer = -1

            If KeepRow = True Then
                If Not M_HEADER_DataGridView.CurrentRow Is Nothing Then
                    Try
                        currow = M_HEADER_DataGridView.CurrentRow.Index
                    Catch ex As Exception

                    End Try
                End If
            Else
                currow = -1
            End If
            ' 2015/09/15 taskplan ADD */

            ' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 START
            'Dim sType As String = search_KUBUN.SelectedValue
            'Dim sBase As String = ""
            'If Not search_BASE.SelectedValue Is DBNull.Value Then
            '    sBase = search_BASE.SelectedValue
            'End If

            'Dim headerTable As DataTable = MasterRepository.GetHeaderByModel(search_MODEL.Text, search_KANRI_NO.Text, search_MEISYOU.Text, sType, search_DUE_DATE.Text, sBase, search_BUNRUI_01.Text)

            Dim kubun_index As Integer = search_KUBUN.SelectedIndex
            Dim select_index As Integer = search_SEARCH_SELECT.SelectedIndex

            Dim sBase As String = ""
            If Not search_BASE.SelectedValue Is DBNull.Value Then
                sBase = search_BASE.SelectedValue
            End If

            Dim sHaichi As String = ""
            If Not search_HAICHI_KIJYUN.SelectedValue Is DBNull.Value Then
                sHaichi = search_HAICHI_KIJYUN.SelectedValue
            End If

            Dim headerTable As DataTable = MasterRepository.GetHeaderByModel(kubun_index, search_WORD.Text, select_index, sBase, sHaichi, search_END_CHECK_DATE.Text, search_DUE_DATE.Text, 0, "", "", "")
            ' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 END


            '設備一覧
            M_HEADER_DataGridView.DataSource = headerTable
            HideDG(kubun_index)

            If headerTable Is Nothing Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02103"), MsgBoxStyle.Information, "Warning")
            Else
                If headerTable.Rows.Count = 0 Then
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02103"), MsgBoxStyle.Information, "Warning")
                End If
            End If

            '/* 2015/09/15 taskplan ADD
            If exsType <> kubun_index Then currow = -1

            If KeepRow = True Then
                If currow > -1 AndAlso Me.M_HEADER_DataGridView.RowCount - 1 >= currow Then
                    Try
                        Me.M_HEADER_DataGridView.CurrentCell = Me.M_HEADER_DataGridView.Item(0, currow)
                    Catch ex As Exception

                    End Try
                Else
                    '                Me.M_HEADER_DataGridView.CurrentCell = Nothing 'クリア表示にカレント行を未選択にしたい場合は有効化
                End If
            End If
            exsType = kubun_index

            ' 2015/09/15 taskplan ADD */

        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub HideDG(ByVal type As Integer)

        ' 2016-04-26 YAMAOKA EDIT 設備一覧の表示構成を変更 START
        'DG_RENTAL_FLG.Visible = False
        'DG_SHISAN_FLG.Visible = False
        'DG_LEASE_FLG.Visible = False
        'DG_UN_NO.Visible = False
        'DG_MFG.Visible = False
        'DG_SPL.Visible = False


        'DG_CHECK_INTERVAL.Visible = False
        'DG_CHECK_DATE.Visible = False
        'DG_NEXT_CHECK_DATE.Visible = False
        'DG_END_CHECK_DATE.Visible = False
        'DG_CHECK_COMPANY.Visible = False

        'DG_KOUSEI_INTERVAL.Visible = False
        'DG_KOUSEI_COMPANY.Visible = False
        'DG_KOUSEI_BY_COMPANY.Visible = False
        'DG_KOUSEI_DATE.Visible = False
        'DG_DUE_DATE.Visible = False

        'If sType = "3" Or sType = "4" Then
        '    DG_KOUSEI_COMPANY.Visible = True '校正会社
        '    DG_KOUSEI_DATE.Visible = True '最終校正日
        '    DG_DUE_DATE.Visible = True '有効期限
        '    DG_KOUSEI_INTERVAL.Visible = True '校正間隔
        '    DG_REMARKS.Visible = True '備考

        '    DG_CREATED_DATE.Visible = False '登録日
        '    DG_LOCATION.Visible = False 'ロケーション
        '    DG_KUBUN_01.Visible = False '区分
        'Else
        '    DG_CREATED_DATE.Visible = True '登録日
        '    DG_LOCATION.Visible = True 'ロケーション
        '    DG_KUBUN_01.Visible = True '区分
        '    DG_STATUS.Visible = True 'STATUS

        '    DG_KOUSEI_COMPANY.Visible = False '校正会社
        '    DG_KOUSEI_DATE.Visible = False '最終校正日
        '    DG_DUE_DATE.Visible = False '有効期限
        '    DG_KOUSEI_INTERVAL.Visible = False '校正間隔
        '    DG_REMARKS.Visible = False '備考
        'End If

        Select Case type
            '2018/09/11 車両管理用に表示ヘッダーを変更
            Case 0 '未選択
                No.Visible = False                       'No
                DG_KANRI_NO.Visible = True              '管理番号
                DG_BUNRUI_01.Visible = True             '保有区分
                DG_MEISYOU.Visible = True               '用途
                DG_MODEL.Visible = True                 '車体番号
                DG_KIKAKU.Visible = True                '車種
                DG_MFG_NO.Visible = True                'ナンバー
                DG_CREATED_DATE.Visible = True          '登録日
                DG_BASE.Visible = True                  '配置基地
                DG_LOCATION.Visible = True              '定位置
                DG_HAICHI_KIJYUN.Visible = False         '配置基準
                DG_KUBUN_01.Visible = True              '設備区分
                DG_RENTAL_FLG.Visible = False           '借用設備
                DG_MFG.Visible = False                  '製造会社
                DG_SPL.Visible = False                  '購入会社
                DG_CHECK_INTERVAL.Visible = False       '点検間隔
                DG_CHECK_DATE.Visible = False           '点検実施日
                DG_NEXT_CHECK_DATE.Visible = False      '次回点検月
                DG_END_CHECK_DATE.Visible = False       '点検期限
                DG_CHECK_COMPANY.Visible = False        '点検会社
                DG_KOUSEI_INTERVAL.Visible = False      '校正間隔
                DG_KOUSEI_COMPANY.Visible = False       '校正会社
                DG_KOUSEI_BY_COMPANY.Visible = False    '校正仲介会社
                DG_KOUSEI_DATE.Visible = False          '最終校正日
                DG_DUE_DATE.Visible = False             '有効期限
                DG_STATUS.Visible = False                'STATUS
                DG_SHISAN_FLG.Visible = False           '固定資産
                DG_LEASE_FLG.Visible = False            'リース
                DG_UN_NO.Visible = False                '危険物
                DG_REMARKS.Visible = False              '備考
            Case 1 '施設設備管理台帳
                No.Visible = False                       'No
                DG_KANRI_NO.Visible = True              '管理番号
                DG_BUNRUI_01.Visible = True             '分類番号
                DG_MEISYOU.Visible = True               '名称
                DG_MODEL.Visible = True                 '型式
                DG_KIKAKU.Visible = True                '規格
                DG_MFG_NO.Visible = True                '製造番号
                DG_CREATED_DATE.Visible = True          '登録日
                DG_BASE.Visible = True                  '設置場所
                DG_LOCATION.Visible = True              'ロケーション
                DG_HAICHI_KIJYUN.Visible = True         '配置基準
                DG_KUBUN_01.Visible = True              '区分
                DG_RENTAL_FLG.Visible = False           '借用設備
                DG_MFG.Visible = False                  '製造会社
                DG_SPL.Visible = False                  '購入会社
                DG_CHECK_INTERVAL.Visible = False       '点検間隔
                DG_CHECK_DATE.Visible = False           '点検実施日
                DG_NEXT_CHECK_DATE.Visible = False      '次回点検月
                DG_END_CHECK_DATE.Visible = False       '点検期限
                DG_CHECK_COMPANY.Visible = False        '点検会社
                DG_KOUSEI_INTERVAL.Visible = False      '校正間隔
                DG_KOUSEI_COMPANY.Visible = False       '校正会社
                DG_KOUSEI_BY_COMPANY.Visible = False    '校正仲介会社
                DG_KOUSEI_DATE.Visible = False          '最終校正日
                DG_DUE_DATE.Visible = False             '有効期限
                DG_STATUS.Visible = True                'STATUS

                DG_SHISAN_FLG.Visible = True            '固定資産
                DG_LEASE_FLG.Visible = True             'リース
                DG_UN_NO.Visible = True                 '危険物
                DG_REMARKS.Visible = False              '備考

                DG_KANRI_NO.Width = 100              '管理番号
                DG_BUNRUI_01.Width = 100             '分類番号
                DG_MEISYOU.Width = 100               '名称
                DG_MODEL.Width = 100                 '型式
                DG_KIKAKU.Width = 100                '規格
                DG_MFG_NO.Width = 100                '製造番号
                DG_CREATED_DATE.Width = 100          '登録日
                DG_BASE.Width = 100                  '設置場所
                DG_LOCATION.Width = 100              'ロケーション
                DG_HAICHI_KIJYUN.Width = 100         '配置基準
                DG_KUBUN_01.Width = 100              '区分
                DG_STATUS.Width = 100                'STATUS
                DG_SHISAN_FLG.Width = 100            '固定資産
                DG_LEASE_FLG.Width = 100             'リース
                DG_UN_NO.Width = 100                 '危険物


            Case 2 '設備管理台帳
                No.Visible = False                       'No
                DG_KANRI_NO.Visible = True              '管理番号
                DG_BUNRUI_01.Visible = True             '分類番号
                DG_MEISYOU.Visible = True               '名称
                DG_MODEL.Visible = True                 '型式
                DG_KIKAKU.Visible = True                '規格
                DG_MFG_NO.Visible = True                '製造番号
                DG_CREATED_DATE.Visible = False         '登録日
                DG_BASE.Visible = True                  '設置場所
                DG_LOCATION.Visible = True              'ロケーション
                DG_HAICHI_KIJYUN.Visible = False        '配置基準
                DG_KUBUN_01.Visible = False             '区分
                DG_RENTAL_FLG.Visible = False           '借用設備
                DG_MFG.Visible = False                  '製造会社
                DG_SPL.Visible = False                  '購入会社
                DG_CHECK_INTERVAL.Visible = True        '点検間隔
                DG_CHECK_DATE.Visible = True            '点検実施日
                DG_NEXT_CHECK_DATE.Visible = True       '次回点検月
                DG_END_CHECK_DATE.Visible = True        '点検期限
                DG_CHECK_COMPANY.Visible = False        '点検会社
                DG_KOUSEI_INTERVAL.Visible = False      '校正間隔
                DG_KOUSEI_COMPANY.Visible = False       '校正会社
                DG_KOUSEI_BY_COMPANY.Visible = False    '校正仲介会社
                DG_KOUSEI_DATE.Visible = False          '最終校正日
                DG_DUE_DATE.Visible = False             '有効期限
                DG_STATUS.Visible = True                'STATUS
                DG_SHISAN_FLG.Visible = False           '固定資産
                DG_LEASE_FLG.Visible = False            'リース
                DG_UN_NO.Visible = False                '危険物
                DG_REMARKS.Visible = False              '備考

                DG_KANRI_NO.Width = 100              '管理番号
                DG_BUNRUI_01.Width = 100             '分類番号
                DG_MEISYOU.Width = 100               '名称
                DG_MODEL.Width = 100                 '型式
                DG_KIKAKU.Width = 100                '規格
                DG_MFG_NO.Width = 100                '製造番号
                DG_BASE.Width = 100                  '設置場所
                DG_LOCATION.Width = 100              'ロケーション
                DG_CHECK_INTERVAL.Width = 100        '点検間隔
                DG_CHECK_DATE.Width = 100            '点検実施日
                DG_NEXT_CHECK_DATE.Width = 100       '次回点検月
                DG_END_CHECK_DATE.Width = 100        '点検期限
                DG_STATUS.Width = 100                'STATUS

            Case 3, 4 '計測器管理台帳
                No.Visible = False                       'No
                DG_KANRI_NO.Visible = True              '管理番号
                DG_BUNRUI_01.Visible = True             '分類番号
                DG_MEISYOU.Visible = True               '名称
                DG_MODEL.Visible = True                 '型式
                DG_KIKAKU.Visible = True                '規格
                DG_MFG_NO.Visible = True                '製造番号
                DG_CREATED_DATE.Visible = False         '登録日
                DG_BASE.Visible = True                  '設置場所
                DG_LOCATION.Visible = False             'ロケーション
                DG_HAICHI_KIJYUN.Visible = True         '配置基準
                DG_KUBUN_01.Visible = False             '区分
                DG_RENTAL_FLG.Visible = False           '借用設備
                DG_MFG.Visible = False                  '製造会社
                DG_SPL.Visible = False                  '購入会社
                DG_CHECK_INTERVAL.Visible = False       '点検間隔
                DG_CHECK_DATE.Visible = False           '点検実施日
                DG_NEXT_CHECK_DATE.Visible = False      '次回点検月
                DG_END_CHECK_DATE.Visible = False       '点検期限
                DG_CHECK_COMPANY.Visible = False        '点検会社
                DG_KOUSEI_INTERVAL.Visible = True       '校正間隔
                DG_KOUSEI_COMPANY.Visible = True        '校正会社
                DG_KOUSEI_BY_COMPANY.Visible = False    '校正仲介会社
                DG_KOUSEI_DATE.Visible = True           '最終校正日
                DG_DUE_DATE.Visible = True              '有効期限
                DG_STATUS.Visible = True                'STATUS
                DG_SHISAN_FLG.Visible = False           '固定資産
                DG_LEASE_FLG.Visible = False            'リース
                DG_UN_NO.Visible = False                '危険物
                DG_REMARKS.Visible = True               '備考

                DG_KANRI_NO.Width = 100              '管理番号
                DG_BUNRUI_01.Width = 100             '分類番号
                DG_MEISYOU.Width = 100               '名称
                DG_MODEL.Width = 100                 '型式
                DG_KIKAKU.Width = 100                '規格
                DG_MFG_NO.Width = 100                '製造番号
                DG_BASE.Width = 100                  '設置場所
                DG_HAICHI_KIJYUN.Width = 100         '配置基準
                DG_KOUSEI_INTERVAL.Width = 100       '校正間隔
                DG_KOUSEI_COMPANY.Width = 100        '校正会社
                DG_KOUSEI_DATE.Width = 100           '最終校正日
                DG_DUE_DATE.Width = 100              '有効期限
                DG_STATUS.Width = 100                'STATUS
                DG_REMARKS.Width = 300               '備考


        End Select

        ' 2016-04-26 YAMAOKA EDIT 設備一覧の表示構成を変更 END

    End Sub

    Private Sub M_HEADER_DataGridView_CellContentDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles M_HEADER_DataGridView.CellDoubleClick
        Try
            If haita_flag Then
                Return
            End If

            If (e.RowIndex = -1) Then
                Return
            End If

            Dim main As Main = Nothing
            Dim isNew As Boolean = False
            kanriNo = M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_KANRI_NO").Value.ToString()
            If (cacheMain.ContainsKey(kanriNo)) Then
                main = CType(cacheMain.Item(kanriNo), Main)
            End If
            If main Is Nothing Then
                isNew = True
            Else
                If main.IsDisposed Then
                    cacheMain.Remove(kanriNo)
                    isNew = True
                End If
            End If
            If isNew Then
                main = New Main(kanriNo)
                cacheMain.Add(kanriNo, main)
            End If
            main.TopMost = True
            main.Show()
            main.TopMost = False

        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
        End Try
    End Sub

    '
    '   新規設備の生成処理
    '   
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_New.Click
        If haita_flag Then
            Return
        End If
        Try

            'Open main UI
            Dim main As Main = New Main(Nothing)
            main.Show()

        Catch ex As Exception
            Helper.Log("btnNew_Click", ex)
        Finally
        End Try
    End Sub

    '
    '   一覧中の情報の出力処理
    ' 
    Private Sub _Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Print.Click
        Try
            If haita_flag Then
                Return
            End If

            ' 2016-04-07 YAMAOKA EDIT 印刷前チェック START
            Dim sType As String = search_KUBUN.SelectedValue

            If exsType <> sType And (Not (exsType = 0 And sType = 1)) And (Not (exsType = 1 And sType = 0)) Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02140"), MsgBoxStyle.Information, "Warning")
                Return
            End If
            ' 2016-04-07 YAMAOKA EDIT 印刷前チェック END

            haita_flag = True
            Me.Cursor = Cursors.WaitCursor

            Dim exp As New ExportLedgerExcel(Me)
            exp.Export()
        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Function SetDataGridViewText(ByRef kensaku As Kensaku)
        For index = 0 To kensaku.M_HEADER_DataGridView.Columns.Count - 1
            Dim val = Ini.ReadValue(Ini.Config.UX, "Setting", Mid(Kensaku.M_HEADER_DataGridView.Columns(index).Name, "DG_".Length + 1))
            If val <> "" Then
                kensaku.M_HEADER_DataGridView.Columns(index).HeaderText = val
            End If
        Next
        Return Nothing
    End Function

    Private Sub SetUserRole()
        If LoginRoler.Employee.AX_MST <> "E" Then
            'View role, hide  create/update/delete
            btn_New.Visible = False
        ElseIf LoginRoler.Employee.AX_MST = "E" Then
            btn_New.Visible = True
        End If
    End Sub

    '
    '   検索条件のクリア処理
    '   
    Private Sub btn_Clear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Clear.Click
        Try
            ' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 START
            'search_MODEL.Clear()
            'search_KANRI_NO.Clear()
            'search_MEISYOU.Clear()
            search_KUBUN.SelectedIndex = 0
            search_BASE.SelectedIndex = 0
            search_DUE_DATE.Clear()
            'search_BUNRUI_01.Clear()

            search_WORD.Clear()
            search_END_CHECK_DATE.Clear()
            search_HAICHI_KIJYUN.SelectedIndex = 0
            search_SEARCH_SELECT.SelectedIndex = 0
            ' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 END

            SearchData(False)
        Catch ex As Exception
            Helper.Log("btn_Clear_Click", ex)
        End Try

    End Sub

    Private Sub M_HEADER_DataGridView_RowPrePaint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowPrePaintEventArgs) Handles M_HEADER_DataGridView.RowPrePaint
        Try
            If (e.RowIndex >= M_HEADER_DataGridView.Rows.Count - 1) Then
                Return
            End If
            Dim due_date As Object = M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_DUE_DATE").Value
            If due_date Is Nothing Or due_date Is DBNull.Value Then
                Return
            End If

            Dim due_before As Date = DateAdd("m", -1, due_date)
            If due_date >= DateTime.Today And due_before <= DateTime.Today Then
                M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_DUE_DATE").Style.BackColor = Color.Yellow
            ElseIf due_date < DateTime.Today Then
                M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_DUE_DATE").Style.BackColor = Color.Red
            Else
                M_HEADER_DataGridView.Rows(e.RowIndex).Cells("DG_DUE_DATE").Style.BackColor = Color.White
            End If
        Catch ex As Exception
            Helper.Log("M_HEADER_DataGridView_RowPrePaint", ex)
        End Try
    End Sub

    ' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 START
    Private Sub search_KUBUN_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles search_KUBUN.SelectedIndexChanged
        SetSearchLocked(search_KUBUN.SelectedIndex)
    End Sub

    ' 検索項目入力有無
    Private Sub SetSearchLocked(Index As Integer)

        search_END_CHECK_DATE.ReadOnly = True
        search_DUE_DATE.ReadOnly = True

        Select Case Index
            Case 2 '設備管理台帳
                search_END_CHECK_DATE.ReadOnly = False
            Case 3 '計測器管理台帳
                search_DUE_DATE.ReadOnly = False
        End Select
    End Sub

	' 2016-04-26 YAMAOKA EDIT 検索条件の構成を変更 END




	' 2016-05-09 YAMAOKA EDIT HT取込データ管理リスト作成 START
	Private Sub btn_HT_List_Click_1(sender As System.Object, e As System.EventArgs) Handles btn_HT_List.Click
		Try
			If haita_flag Then
				Return
			End If

			Dim ht As LendingStatus = Nothing

			ht = New LendingStatus()

			ht.TopMost = False
			'ht.ShowDialog()
			ht.Show()
			'ht.TopMost = False

		Catch ex As Exception
			Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
		Finally
		End Try
	End Sub
	' 2016-05-09 YAMAOKA EDIT HT取込データ管理リスト作成 END

	Private Sub btn_History_Click(sender As Object, e As EventArgs)
		Dim lsh As LendingStatusHistory = New LendingStatusHistory
		lsh.Show()
	End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_Tana.Click

        Dim headerTable As DataTable = MasterRepository.GetTabaList()

        DataGridView1.DataSource = headerTable

        Dim objExcel As Excel.Application = Nothing
        Dim objWorkBook As Excel.Workbook = Nothing
        objExcel = New Excel.Application
        objWorkBook = objExcel.Workbooks.Add

        ' DataGridViewのセルのデータ取得
        Dim v As String(,) = New String(
            DataGridView1.Rows.Count - 1, DataGridView1.Columns.Count - 1) {}
        For r As Integer = 0 To DataGridView1.Rows.Count - 1
            For c As Integer = 0 To DataGridView1.Columns.Count - 1
                Dim dt As String = ""
                If DataGridView1.Rows(r).Cells(c).Value _
                    Is Nothing = False Then
                    dt = DataGridView1.Rows(r).Cells(c).Value.ToString()
                End If
                v(r, c) = dt
            Next
        Next
        ' EXCELにデータ転送
        Dim ran As String = "A2:" &
            Chr(Asc("A") + DataGridView1.Columns.Count - 1) & DataGridView1.Rows.Count
        objWorkBook.Sheets(1).Range(ran) = v

        Dim header() As String = New String(19) {"判定", "管理番号", "分類番号", "名称", "型式", "規格", "製造番号", "登録日", "設置場所", "ロケーション", "配置基準", "区分", "危険物", "製造会社", "購入会社", "STATUS", "棚卸ロケーション", "棚卸数", "棚卸者", "棚卸日"}
        objWorkBook.Sheets(1).Range("A1:T1") = header

        ' エクセル表示
        objExcel.Visible = True

        ' EXCEL解放
        Marshal.ReleaseComObject(objWorkBook)
        Marshal.ReleaseComObject(objExcel)
        objWorkBook = Nothing
        objExcel = Nothing

    End Sub

    Private Sub btn_DelTana_Click(sender As Object, e As EventArgs) Handles btn_DelTana.Click
        'メッセージボックスを表示する 
        Dim result As DialogResult = MessageBox.Show("棚卸データを削除します。よろしいですか？",
                                                     "",
                                                     MessageBoxButtons.YesNo,
                                                     MessageBoxIcon.Exclamation,
                                                     MessageBoxDefaultButton.Button2)

        '何が選択されたか調べる 
        If result = DialogResult.Yes Then
            Dim updateqry As String = ""
            '「はい」が選択された時 
            updateqry = "TRUNCATE TABLE T_TANA"
            MainHelper.TrancateT_TANA()

        ElseIf result = DialogResult.No Then
            '「いいえ」が選択された時 
            Console.WriteLine("「いいえ」が選択されました")
        ElseIf result = DialogResult.Cancel Then
            '「キャンセル」が選択された時 
            Console.WriteLine("「キャンセル」が選択されました")
        End If
    End Sub




End Class
