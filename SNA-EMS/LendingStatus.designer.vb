﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class LendingStatus
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LendingStatus))
        Me.btn_Search = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btn_Close = New System.Windows.Forms.Button()
        Me.OK = New System.Windows.Forms.Button()
        Me.btn_Clear = New System.Windows.Forms.Button()
        Me.pgs = New System.Windows.Forms.ProgressBar()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.lbl_List = New System.Windows.Forms.TabPage()
        Me.M_HEADER_DataGridView = New System.Windows.Forms.DataGridView()
        Me.DG_REASE_DATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LEND_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_KANRI_NO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_BUNRUI_01 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_MEISYOU = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_MODEL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_KIKAKU = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_BASE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_LOCATION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACNO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_STATUS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_REASE_FLG = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.DG_REASE_KIND = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.DG_USAGE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DG_SYAIN_CD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.search_BASE = New System.Windows.Forms.ComboBox()
        Me.lbl_SEARCH_BASE = New System.Windows.Forms.Label()
        Me.btn_GoBack = New System.Windows.Forms.Button()
        Me.btn_Update = New System.Windows.Forms.Button()
        Me.btn_Del = New System.Windows.Forms.Button()
        Me.lbl_STATUS = New System.Windows.Forms.Label()
        Me.search_STATUS = New System.Windows.Forms.ComboBox()
        Me.lbl_SYAIN_CD = New System.Windows.Forms.Label()
        Me.search_SYAIN_CD = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_LendHistory = New System.Windows.Forms.Button()
        Me.lblAcNo = New System.Windows.Forms.Label()
        Me.search_AcNo = New System.Windows.Forms.TextBox()
        Me.btn_Save = New System.Windows.Forms.Button()
        Me.btn_RowAdd = New System.Windows.Forms.Button()
        Me.TabControl2.SuspendLayout()
        Me.lbl_List.SuspendLayout()
        CType(Me.M_HEADER_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_Search
        '
        Me.btn_Search.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Search.Location = New System.Drawing.Point(1164, 10)
        Me.btn_Search.Name = "btn_Search"
        Me.btn_Search.Size = New System.Drawing.Size(58, 21)
        Me.btn_Search.TabIndex = 7
        Me.btn_Search.Text = "検索"
        Me.btn_Search.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label6.Location = New System.Drawing.Point(0, 555)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(2450, 3)
        Me.Label6.TabIndex = 0
        '
        'btn_Close
        '
        Me.btn_Close.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_Close.Location = New System.Drawing.Point(12, 563)
        Me.btn_Close.Name = "btn_Close"
        Me.btn_Close.Size = New System.Drawing.Size(65, 21)
        Me.btn_Close.TabIndex = 11
        Me.btn_Close.Text = "閉じる"
        '
        'OK
        '
        Me.OK.Location = New System.Drawing.Point(554, 10)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(58, 21)
        Me.OK.TabIndex = 3
        Me.OK.Text = "検索"
        '
        'btn_Clear
        '
        Me.btn_Clear.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Clear.Location = New System.Drawing.Point(1225, 10)
        Me.btn_Clear.Name = "btn_Clear"
        Me.btn_Clear.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btn_Clear.Size = New System.Drawing.Size(58, 21)
        Me.btn_Clear.TabIndex = 8
        Me.btn_Clear.Text = "クリア"
        Me.btn_Clear.UseVisualStyleBackColor = False
        '
        'pgs
        '
        Me.pgs.Location = New System.Drawing.Point(240, 277)
        Me.pgs.MarqueeAnimationSpeed = 1
        Me.pgs.Name = "pgs"
        Me.pgs.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.pgs.Size = New System.Drawing.Size(242, 23)
        Me.pgs.Step = 1
        Me.pgs.TabIndex = 1015
        Me.pgs.Value = 1
        Me.pgs.Visible = False
        '
        'TabControl2
        '
        Me.TabControl2.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl2.Controls.Add(Me.lbl_List)
        Me.TabControl2.Location = New System.Drawing.Point(12, 36)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(1382, 470)
        Me.TabControl2.TabIndex = 8
        '
        'lbl_List
        '
        Me.lbl_List.AutoScroll = True
        Me.lbl_List.Controls.Add(Me.M_HEADER_DataGridView)
        Me.lbl_List.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lbl_List.Location = New System.Drawing.Point(4, 4)
        Me.lbl_List.Name = "lbl_List"
        Me.lbl_List.Padding = New System.Windows.Forms.Padding(3)
        Me.lbl_List.Size = New System.Drawing.Size(1374, 444)
        Me.lbl_List.TabIndex = 0
        Me.lbl_List.Text = "  設備一覧  "
        Me.lbl_List.UseVisualStyleBackColor = True
        '
        'M_HEADER_DataGridView
        '
        Me.M_HEADER_DataGridView.AllowUserToAddRows = False
        Me.M_HEADER_DataGridView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_HEADER_DataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.M_HEADER_DataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DG_REASE_DATE, Me.LEND_NO, Me.DG_KANRI_NO, Me.DG_BUNRUI_01, Me.DG_MEISYOU, Me.DG_MODEL, Me.DG_KIKAKU, Me.DG_BASE, Me.DG_LOCATION, Me.ACNO, Me.DG_STATUS, Me.DG_REASE_FLG, Me.DG_REASE_KIND, Me.DG_USAGE, Me.DG_SYAIN_CD})
        Me.M_HEADER_DataGridView.Location = New System.Drawing.Point(0, 2)
        Me.M_HEADER_DataGridView.MultiSelect = False
        Me.M_HEADER_DataGridView.Name = "M_HEADER_DataGridView"
        Me.M_HEADER_DataGridView.RowHeadersVisible = False
        Me.M_HEADER_DataGridView.RowTemplate.Height = 23
        Me.M_HEADER_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.M_HEADER_DataGridView.Size = New System.Drawing.Size(1368, 437)
        Me.M_HEADER_DataGridView.TabIndex = 0
        '
        'DG_REASE_DATE
        '
        Me.DG_REASE_DATE.DataPropertyName = "LENT_DATE"
        Me.DG_REASE_DATE.HeaderText = "貸出日"
        Me.DG_REASE_DATE.MaxInputLength = 16
        Me.DG_REASE_DATE.Name = "DG_REASE_DATE"
        Me.DG_REASE_DATE.ReadOnly = True
        '
        'LEND_NO
        '
        Me.LEND_NO.DataPropertyName = "LEND_NO"
        Me.LEND_NO.HeaderText = "LEND_NO"
        Me.LEND_NO.Name = "LEND_NO"
        Me.LEND_NO.Visible = False
        '
        'DG_KANRI_NO
        '
        Me.DG_KANRI_NO.DataPropertyName = "KANRI_NO"
        Me.DG_KANRI_NO.HeaderText = "管理番号"
        Me.DG_KANRI_NO.Name = "DG_KANRI_NO"
        Me.DG_KANRI_NO.ReadOnly = True
        Me.DG_KANRI_NO.Width = 60
        '
        'DG_BUNRUI_01
        '
        Me.DG_BUNRUI_01.DataPropertyName = "BUNRUI_01"
        Me.DG_BUNRUI_01.HeaderText = "分類番号"
        Me.DG_BUNRUI_01.Name = "DG_BUNRUI_01"
        Me.DG_BUNRUI_01.ReadOnly = True
        Me.DG_BUNRUI_01.Width = 60
        '
        'DG_MEISYOU
        '
        Me.DG_MEISYOU.DataPropertyName = "MEISYOU"
        Me.DG_MEISYOU.HeaderText = "名称"
        Me.DG_MEISYOU.Name = "DG_MEISYOU"
        Me.DG_MEISYOU.ReadOnly = True
        Me.DG_MEISYOU.Width = 400
        '
        'DG_MODEL
        '
        Me.DG_MODEL.DataPropertyName = "MODEL"
        Me.DG_MODEL.HeaderText = "型式"
        Me.DG_MODEL.Name = "DG_MODEL"
        Me.DG_MODEL.ReadOnly = True
        Me.DG_MODEL.Width = 120
        '
        'DG_KIKAKU
        '
        Me.DG_KIKAKU.DataPropertyName = "KIKAKU"
        Me.DG_KIKAKU.HeaderText = "規格"
        Me.DG_KIKAKU.Name = "DG_KIKAKU"
        Me.DG_KIKAKU.ReadOnly = True
        Me.DG_KIKAKU.Width = 120
        '
        'DG_BASE
        '
        Me.DG_BASE.DataPropertyName = "BASE"
        Me.DG_BASE.HeaderText = "設置場所"
        Me.DG_BASE.Name = "DG_BASE"
        Me.DG_BASE.ReadOnly = True
        Me.DG_BASE.Width = 60
        '
        'DG_LOCATION
        '
        Me.DG_LOCATION.DataPropertyName = "LOCATION"
        Me.DG_LOCATION.HeaderText = "ロケーション"
        Me.DG_LOCATION.Name = "DG_LOCATION"
        Me.DG_LOCATION.ReadOnly = True
        Me.DG_LOCATION.Width = 90
        '
        'ACNO
        '
        Me.ACNO.DataPropertyName = "AC_NO"
        Me.ACNO.HeaderText = "使用機番"
        Me.ACNO.Name = "ACNO"
        Me.ACNO.Visible = False
        '
        'DG_STATUS
        '
        Me.DG_STATUS.DataPropertyName = "STATUS"
        Me.DG_STATUS.HeaderText = "STATUS"
        Me.DG_STATUS.Name = "DG_STATUS"
        Me.DG_STATUS.ReadOnly = True
        Me.DG_STATUS.Visible = False
        Me.DG_STATUS.Width = 70
        '
        'DG_REASE_FLG
        '
        Me.DG_REASE_FLG.DataPropertyName = "REASE_FLG"
        Me.DG_REASE_FLG.HeaderText = "貸出状態"
        Me.DG_REASE_FLG.Items.AddRange(New Object() {"", "貸出", "返却"})
        Me.DG_REASE_FLG.MaxDropDownItems = 3
        Me.DG_REASE_FLG.Name = "DG_REASE_FLG"
        Me.DG_REASE_FLG.Visible = False
        Me.DG_REASE_FLG.Width = 70
        '
        'DG_REASE_KIND
        '
        Me.DG_REASE_KIND.DataPropertyName = "LENT_KIND"
        Me.DG_REASE_KIND.HeaderText = "貸出種類"
        Me.DG_REASE_KIND.Items.AddRange(New Object() {"", "通常", "校正", "羽田貸出", "宮崎貸出", "熊本貸出", "長崎貸出", "鹿児島貸出", "沖縄貸出", "大分貸出", "神戸貸出", "石垣貸出", "中部貸出", "その他貸出"})
        Me.DG_REASE_KIND.Name = "DG_REASE_KIND"
        Me.DG_REASE_KIND.Visible = False
        '
        'DG_USAGE
        '
        Me.DG_USAGE.DataPropertyName = "USAGE"
        Me.DG_USAGE.HeaderText = "STATUS"
        Me.DG_USAGE.Name = "DG_USAGE"
        '
        'DG_SYAIN_CD
        '
        Me.DG_SYAIN_CD.DataPropertyName = "LENT_BY"
        Me.DG_SYAIN_CD.HeaderText = "貸出者"
        Me.DG_SYAIN_CD.MaxInputLength = 5
        Me.DG_SYAIN_CD.Name = "DG_SYAIN_CD"
        Me.DG_SYAIN_CD.ReadOnly = True
        Me.DG_SYAIN_CD.Width = 70
        '
        'search_BASE
        '
        Me.search_BASE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.search_BASE.FormattingEnabled = True
        Me.search_BASE.Location = New System.Drawing.Point(76, 9)
        Me.search_BASE.Name = "search_BASE"
        Me.search_BASE.Size = New System.Drawing.Size(66, 20)
        Me.search_BASE.TabIndex = 4
        '
        'lbl_SEARCH_BASE
        '
        Me.lbl_SEARCH_BASE.Location = New System.Drawing.Point(15, 9)
        Me.lbl_SEARCH_BASE.Name = "lbl_SEARCH_BASE"
        Me.lbl_SEARCH_BASE.Size = New System.Drawing.Size(59, 23)
        Me.lbl_SEARCH_BASE.TabIndex = 1019
        Me.lbl_SEARCH_BASE.Text = "配置基地"
        Me.lbl_SEARCH_BASE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btn_GoBack
        '
        Me.btn_GoBack.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_GoBack.BackColor = System.Drawing.SystemColors.Control
        Me.btn_GoBack.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_GoBack.Location = New System.Drawing.Point(12, 515)
        Me.btn_GoBack.Name = "btn_GoBack"
        Me.btn_GoBack.Size = New System.Drawing.Size(65, 21)
        Me.btn_GoBack.TabIndex = 1199
        Me.btn_GoBack.Text = "戻る"
        Me.btn_GoBack.UseVisualStyleBackColor = False
        '
        'btn_Update
        '
        Me.btn_Update.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_Update.Location = New System.Drawing.Point(1301, 516)
        Me.btn_Update.Name = "btn_Update"
        Me.btn_Update.Size = New System.Drawing.Size(19, 21)
        Me.btn_Update.TabIndex = 1200
        Me.btn_Update.Text = "更新"
        Me.btn_Update.Visible = False
        '
        'btn_Del
        '
        Me.btn_Del.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_Del.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Del.Location = New System.Drawing.Point(209, 515)
        Me.btn_Del.Name = "btn_Del"
        Me.btn_Del.Size = New System.Drawing.Size(65, 21)
        Me.btn_Del.TabIndex = 1201
        Me.btn_Del.Text = "削除"
        Me.btn_Del.UseVisualStyleBackColor = False
        '
        'lbl_STATUS
        '
        Me.lbl_STATUS.Location = New System.Drawing.Point(148, 7)
        Me.lbl_STATUS.Name = "lbl_STATUS"
        Me.lbl_STATUS.Size = New System.Drawing.Size(56, 23)
        Me.lbl_STATUS.TabIndex = 1202
        Me.lbl_STATUS.Text = "STATUS"
        Me.lbl_STATUS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'search_STATUS
        '
        Me.search_STATUS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.search_STATUS.FormattingEnabled = True
        Me.search_STATUS.Location = New System.Drawing.Point(210, 6)
        Me.search_STATUS.Name = "search_STATUS"
        Me.search_STATUS.Size = New System.Drawing.Size(107, 20)
        Me.search_STATUS.TabIndex = 1203
        '
        'lbl_SYAIN_CD
        '
        Me.lbl_SYAIN_CD.Location = New System.Drawing.Point(323, 6)
        Me.lbl_SYAIN_CD.Name = "lbl_SYAIN_CD"
        Me.lbl_SYAIN_CD.Size = New System.Drawing.Size(57, 23)
        Me.lbl_SYAIN_CD.TabIndex = 1208
        Me.lbl_SYAIN_CD.Text = "貸出者"
        Me.lbl_SYAIN_CD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'search_SYAIN_CD
        '
        Me.search_SYAIN_CD.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.search_SYAIN_CD.Location = New System.Drawing.Point(380, 7)
        Me.search_SYAIN_CD.MaxLength = 5
        Me.search_SYAIN_CD.Name = "search_SYAIN_CD"
        Me.search_SYAIN_CD.Size = New System.Drawing.Size(67, 19)
        Me.search_SYAIN_CD.TabIndex = 1209
        Me.search_SYAIN_CD.Tag = ""
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label1.Location = New System.Drawing.Point(7, 509)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(1883, 3)
        Me.Label1.TabIndex = 1211
        '
        'btn_LendHistory
        '
        Me.btn_LendHistory.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_LendHistory.BackColor = System.Drawing.SystemColors.Control
        Me.btn_LendHistory.ForeColor = System.Drawing.Color.Black
        Me.btn_LendHistory.Location = New System.Drawing.Point(304, 516)
        Me.btn_LendHistory.Name = "btn_LendHistory"
        Me.btn_LendHistory.Size = New System.Drawing.Size(103, 20)
        Me.btn_LendHistory.TabIndex = 1213
        Me.btn_LendHistory.Text = "貸出履歴"
        Me.btn_LendHistory.UseVisualStyleBackColor = False
        '
        'lblAcNo
        '
        Me.lblAcNo.Location = New System.Drawing.Point(633, 5)
        Me.lblAcNo.Name = "lblAcNo"
        Me.lblAcNo.Size = New System.Drawing.Size(59, 23)
        Me.lblAcNo.TabIndex = 1019
        Me.lblAcNo.Text = "使用機番"
        Me.lblAcNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblAcNo.Visible = False
        '
        'search_AcNo
        '
        Me.search_AcNo.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.search_AcNo.Location = New System.Drawing.Point(692, 7)
        Me.search_AcNo.MaxLength = 5
        Me.search_AcNo.Name = "search_AcNo"
        Me.search_AcNo.Size = New System.Drawing.Size(79, 19)
        Me.search_AcNo.TabIndex = 1209
        Me.search_AcNo.Tag = ""
        Me.search_AcNo.Visible = False
        '
        'btn_Save
        '
        Me.btn_Save.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_Save.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Save.Location = New System.Drawing.Point(513, 516)
        Me.btn_Save.Name = "btn_Save"
        Me.btn_Save.Size = New System.Drawing.Size(73, 21)
        Me.btn_Save.TabIndex = 1214
        Me.btn_Save.Text = "保存"
        Me.btn_Save.UseVisualStyleBackColor = False
        Me.btn_Save.Visible = False
        '
        'btn_RowAdd
        '
        Me.btn_RowAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btn_RowAdd.BackColor = System.Drawing.SystemColors.Control
        Me.btn_RowAdd.Location = New System.Drawing.Point(422, 516)
        Me.btn_RowAdd.Name = "btn_RowAdd"
        Me.btn_RowAdd.Size = New System.Drawing.Size(76, 21)
        Me.btn_RowAdd.TabIndex = 1215
        Me.btn_RowAdd.Text = "行追加"
        Me.btn_RowAdd.UseVisualStyleBackColor = False
        Me.btn_RowAdd.Visible = False
        '
        'LendingStatus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(151, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1284, 545)
        Me.Controls.Add(Me.btn_RowAdd)
        Me.Controls.Add(Me.btn_Save)
        Me.Controls.Add(Me.btn_LendHistory)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.search_AcNo)
        Me.Controls.Add(Me.search_SYAIN_CD)
        Me.Controls.Add(Me.lbl_SYAIN_CD)
        Me.Controls.Add(Me.search_STATUS)
        Me.Controls.Add(Me.lbl_STATUS)
        Me.Controls.Add(Me.btn_Del)
        Me.Controls.Add(Me.btn_Update)
        Me.Controls.Add(Me.btn_GoBack)
        Me.Controls.Add(Me.search_BASE)
        Me.Controls.Add(Me.lblAcNo)
        Me.Controls.Add(Me.lbl_SEARCH_BASE)
        Me.Controls.Add(Me.pgs)
        Me.Controls.Add(Me.btn_Clear)
        Me.Controls.Add(Me.TabControl2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btn_Close)
        Me.Controls.Add(Me.btn_Search)
        Me.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 9.0!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(902, 400)
        Me.Name = "LendingStatus"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "HT取込データ確認リスト"
        Me.TabControl2.ResumeLayout(False)
        Me.lbl_List.ResumeLayout(False)
        CType(Me.M_HEADER_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_Search As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btn_Close As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents btn_Clear As System.Windows.Forms.Button

    Friend WithEvents pgs As System.Windows.Forms.ProgressBar
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents search_BASE As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_SEARCH_BASE As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_DataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents btn_GoBack As System.Windows.Forms.Button
    Friend WithEvents btn_Update As System.Windows.Forms.Button
    Friend WithEvents btn_Del As System.Windows.Forms.Button
    Friend WithEvents lbl_STATUS As System.Windows.Forms.Label
    Friend WithEvents search_STATUS As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_SYAIN_CD As System.Windows.Forms.Label
    Friend WithEvents search_SYAIN_CD As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents btn_LendHistory As System.Windows.Forms.Button
    Friend WithEvents lbl_List As TabPage
    Friend WithEvents lblAcNo As Label
    Friend WithEvents search_AcNo As TextBox
    Friend WithEvents btn_Save As Button
    Friend WithEvents btn_RowAdd As Button
    Friend WithEvents DG_REASE_DATE As DataGridViewTextBoxColumn
    Friend WithEvents LEND_NO As DataGridViewTextBoxColumn
    Friend WithEvents DG_KANRI_NO As DataGridViewTextBoxColumn
    Friend WithEvents DG_BUNRUI_01 As DataGridViewTextBoxColumn
    Friend WithEvents DG_MEISYOU As DataGridViewTextBoxColumn
    Friend WithEvents DG_MODEL As DataGridViewTextBoxColumn
    Friend WithEvents DG_KIKAKU As DataGridViewTextBoxColumn
    Friend WithEvents DG_BASE As DataGridViewTextBoxColumn
    Friend WithEvents DG_LOCATION As DataGridViewTextBoxColumn
    Friend WithEvents ACNO As DataGridViewTextBoxColumn
    Friend WithEvents DG_STATUS As DataGridViewTextBoxColumn
    Friend WithEvents DG_REASE_FLG As DataGridViewComboBoxColumn
    Friend WithEvents DG_REASE_KIND As DataGridViewComboBoxColumn
    Friend WithEvents DG_USAGE As DataGridViewTextBoxColumn
    Friend WithEvents DG_SYAIN_CD As DataGridViewTextBoxColumn
End Class
