﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.lbl_M_SHISAN = New System.Windows.Forms.TabPage()
        Me.eleven_ref_M_HEADER_PARENT_NO = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.eleven_ref_M_HEADER_MODEL = New System.Windows.Forms.TextBox()
        Me.elevent_lbl_MODEL = New System.Windows.Forms.Label()
        Me.eleven_lbl_BUNRUI_01 = New System.Windows.Forms.Label()
        Me.eleven_ref_M_HEADER_KANRI_NO = New System.Windows.Forms.TextBox()
        Me.eleven_ref_M_HEADER_BUNRUI_01 = New System.Windows.Forms.TextBox()
        Me.eleven_lbl_KANRI_NO = New System.Windows.Forms.Label()
        Me.M_SHISAN_TAIYOU = New System.Windows.Forms.TextBox()
        Me.eleven_ref_M_HEADER_CREATED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.eleven_lbl_CREATED_DATE = New System.Windows.Forms.Label()
        Me.eleven_ref_M_HEADER_KIKAKU = New System.Windows.Forms.TextBox()
        Me.eleven_lbl_LOCATION = New System.Windows.Forms.Label()
        Me.eleven_lbl_BASE = New System.Windows.Forms.Label()
        Me.eleven_lbl_MFG = New System.Windows.Forms.Label()
        Me.eleven_ref_M_HEADER_LOCATION = New System.Windows.Forms.TextBox()
        Me.eleven_lbl_TAIYOU = New System.Windows.Forms.Label()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.eleven_ref_M_HEADER_IMAGE = New System.Windows.Forms.PictureBox()
        Me.eleven_lbl_IMAGE = New System.Windows.Forms.Label()
        Me.M_SHISAN_SHISAN_NO = New System.Windows.Forms.TextBox()
        Me.eleven_lbl_SHISAN_NO = New System.Windows.Forms.Label()
        Me.eleven_ref_M_HEADER_BASE = New System.Windows.Forms.ComboBox()
        Me.eleven_lbl_MFG_NO = New System.Windows.Forms.Label()
        Me.eleven_lbl_KIKAKU = New System.Windows.Forms.Label()
        Me.eleven_lbl_MEISYOU = New System.Windows.Forms.Label()
        Me.eleven_ref_M_HEADER_MEISYOU = New System.Windows.Forms.TextBox()
        Me.eleven_ref_M_HEADER_MFG = New System.Windows.Forms.TextBox()
        Me.eleven_ref_M_HEADER_SPL = New System.Windows.Forms.TextBox()
        Me.eleven_lbl_SPL = New System.Windows.Forms.Label()
        Me.eleven_ref_M_HEADER_MFG_NO = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btn_New = New System.Windows.Forms.Button()
        Me.btn_Close = New System.Windows.Forms.Button()
        Me.btn_Update = New System.Windows.Forms.Button()
        Me.btn_Del = New System.Windows.Forms.Button()
        Me.btn_Export = New System.Windows.Forms.Button()
        Me.OK = New System.Windows.Forms.Button()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.lbl_Detail = New System.Windows.Forms.TabPage()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.lbl_M_HEADER = New System.Windows.Forms.TabPage()
        Me.M_HEADER_PARENT_NO = New System.Windows.Forms.TextBox()
        Me.lbl_PARENT_NO = New System.Windows.Forms.Label()
        Me.btn_ImageDownload = New System.Windows.Forms.Button()
        Me.btn_FileDownload = New System.Windows.Forms.Button()
        Me.gpx_ADDITIONAL_GROUP = New System.Windows.Forms.GroupBox()
        Me.M_HEADER_SHISAN_FLG = New System.Windows.Forms.CheckBox()
        Me.M_HEADER_LEASE_FLG = New System.Windows.Forms.CheckBox()
        Me.one_ref_STATUS = New System.Windows.Forms.ComboBox()
        Me.M_HEADER_HAIKI_DATE = New System.Windows.Forms.DateTimePicker()
        Me.M_HEADER_HAIKI_FLG = New System.Windows.Forms.CheckBox()
        Me.lbl_HAIKI_DATE = New System.Windows.Forms.Label()
        Me.M_HEADER_UN_NO = New System.Windows.Forms.TextBox()
        Me.lbl_UN_NO = New System.Windows.Forms.Label()
        Me.M_HEADER_SPL = New System.Windows.Forms.TextBox()
        Me.lbl_SPL = New System.Windows.Forms.Label()
        Me.M_HEADER_MFG = New System.Windows.Forms.TextBox()
        Me.lbl_MFG = New System.Windows.Forms.Label()
        Me.M_HEADER_BUNRUI_01 = New System.Windows.Forms.TextBox()
        Me.lbl_BUNRUI_01 = New System.Windows.Forms.Label()
        Me.M_HEADER_KANRI_NO = New System.Windows.Forms.TextBox()
        Me.one_lbl_KANRI_NO = New System.Windows.Forms.Label()
        Me.M_HEADER_KUBUN_01 = New System.Windows.Forms.ComboBox()
        Me.lbl_KUBUN_01 = New System.Windows.Forms.Label()
        Me.M_HEADER_MODEL = New System.Windows.Forms.TextBox()
        Me.one_lbl_MODEL = New System.Windows.Forms.Label()
        Me.btn_FileClear = New System.Windows.Forms.Button()
        Me.M_HEADER_REASON = New System.Windows.Forms.TextBox()
        Me.lbl_REASON = New System.Windows.Forms.Label()
        Me.M_HEADER_DOUTOUSEI = New System.Windows.Forms.TextBox()
        Me.lbl_DOUTOUSEI = New System.Windows.Forms.Label()
        Me.one_lbl_STATUS = New System.Windows.Forms.Label()
        Me.M_HEADER_REFERENCE = New System.Windows.Forms.TextBox()
        Me.btn_File = New System.Windows.Forms.Button()
        Me.lbl_Reference = New System.Windows.Forms.Label()
        Me.M_HEADER_CERTIFICATE_NAME = New System.Windows.Forms.TextBox()
        Me.M_HEADER_MFG_NO = New System.Windows.Forms.TextBox()
        Me.one_lbl_MEISYOU = New System.Windows.Forms.Label()
        Me.M_HEADER_MEISYOU = New System.Windows.Forms.TextBox()
        Me.M_HEADER_MODIFIED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lbl_MODIFIED_DATE = New System.Windows.Forms.Label()
        Me.M_HEADER_MODIFIED_BY = New System.Windows.Forms.TextBox()
        Me.lbl_MODIFIED_BY = New System.Windows.Forms.Label()
        Me.M_HEADER_CREATED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lbl_CREATED_DATE = New System.Windows.Forms.Label()
        Me.M_HEADER_CREATED_BY = New System.Windows.Forms.TextBox()
        Me.lbl_CREATED_BY = New System.Windows.Forms.Label()
        Me.lbl_MFG_NO = New System.Windows.Forms.Label()
        Me.btn_ImageClear = New System.Windows.Forms.Button()
        Me.btn_ImageSel = New System.Windows.Forms.Button()
        Me.lbl_IMAGE = New System.Windows.Forms.Label()
        Me.M_HEADER_REMARKS = New System.Windows.Forms.TextBox()
        Me.lbl_Certificate = New System.Windows.Forms.Label()
        Me.lbl_REMARKS = New System.Windows.Forms.Label()
        Me.M_HEADER_LOCATION = New System.Windows.Forms.TextBox()
        Me.lbl_LOCATION = New System.Windows.Forms.Label()
        Me.M_HEADER_BASE = New System.Windows.Forms.ComboBox()
        Me.lbl_BASE = New System.Windows.Forms.Label()
        Me.M_HEADER_KIKAKU = New System.Windows.Forms.TextBox()
        Me.lbl_KIKAKU = New System.Windows.Forms.Label()
        Me.M_HEADER_HAICHI_KIJYUN = New System.Windows.Forms.ComboBox()
        Me.lbl_HAICHI_KIJYUN = New System.Windows.Forms.Label()
        Me.gpx_HOYU_GROUP = New System.Windows.Forms.GroupBox()
        Me.M_HEADER_RENTAL_FLG = New System.Windows.Forms.CheckBox()
        Me.M_HEADER_HOYU_FLG = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.M_HEADER_IMAGE = New System.Windows.Forms.PictureBox()
        Me.gpx_RENTAL_GROUP = New System.Windows.Forms.GroupBox()
        Me.one_ref_M_RENTAL_TEKIGOUSEI = New System.Windows.Forms.DateTimePicker()
        Me.lbl_TEKIGOUSEI = New System.Windows.Forms.Label()
        Me.one_ref_M_RENTAL_NOURYOKU = New System.Windows.Forms.ComboBox()
        Me.lbl_NOURYOKU = New System.Windows.Forms.Label()
        Me.lbl_RENTAL_CONPANY = New System.Windows.Forms.Label()
        Me.one_ref_M_RENTAL_LEASE_CONPANY = New System.Windows.Forms.TextBox()
        Me.gpx_CATEGORY_GROUP = New System.Windows.Forms.GroupBox()
        Me.M_HEADER_EQUIPMENT_FLG = New System.Windows.Forms.CheckBox()
        Me.M_HEADER_HOZEN_FLG = New System.Windows.Forms.CheckBox()
        Me.M_HEADER_KOUSEI_FLG = New System.Windows.Forms.CheckBox()
        Me.M_HEADER_NOT_KOUSEI_FLG = New System.Windows.Forms.CheckBox()
        Me.lbl_M_TOOL = New System.Windows.Forms.TabPage()
        Me.two_ref_M_HEADER_PARENT_NO = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.two_ref_M_HEADER_KANRI_NO = New System.Windows.Forms.TextBox()
        Me.two_lbl_KANRI_NO = New System.Windows.Forms.Label()
        Me.two_ref_M_HEADER_MODEL = New System.Windows.Forms.TextBox()
        Me.two_lbl_MODEL = New System.Windows.Forms.Label()
        Me.two_lbl_BUNRUI_01 = New System.Windows.Forms.Label()
        Me.two_ref_M_HEADER_BUNRUI_01 = New System.Windows.Forms.TextBox()
        Me.two_lbl_MEISYOU = New System.Windows.Forms.Label()
        Me.two_ref_M_HEADER_MEISYOU = New System.Windows.Forms.TextBox()
        Me.two_ref_M_HEADER_UN_NO = New System.Windows.Forms.TextBox()
        Me.two_lbl_UN_NO = New System.Windows.Forms.Label()
        Me.two_ref_M_HEADER_MFG = New System.Windows.Forms.TextBox()
        Me.two_lbl_MFG = New System.Windows.Forms.Label()
        Me.two_ref_M_HEADER_KIKAKU = New System.Windows.Forms.TextBox()
        Me.two_lbl_KIKAKU = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.two_ref_M_HEADER_IMAGE = New System.Windows.Forms.PictureBox()
        Me.two_ref_M_HEADER_CREATED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.two_lbl_CREATED_DATE = New System.Windows.Forms.Label()
        Me.two_lbl_ATA = New System.Windows.Forms.Label()
        Me.M_TOOL_ATA = New System.Windows.Forms.TextBox()
        Me.two_ref_M_HEADER_MFG_NO = New System.Windows.Forms.TextBox()
        Me.two_lbl_MFG_NO = New System.Windows.Forms.Label()
        Me.two_lbl_IMAGE = New System.Windows.Forms.Label()
        Me.two_ref_M_HEADER_LOCATION = New System.Windows.Forms.TextBox()
        Me.two_lbl_LOCATION = New System.Windows.Forms.Label()
        Me.two_ref_M_HEADER_BASE = New System.Windows.Forms.ComboBox()
        Me.two_lbl_BASE = New System.Windows.Forms.Label()
        Me.two_ref_M_HEADER_HAICHI_KIJYUN = New System.Windows.Forms.ComboBox()
        Me.two_lbl_HAICHI_KIJYUN = New System.Windows.Forms.Label()
        Me.M_TOOL_ALLOW_FLG = New System.Windows.Forms.CheckBox()
        Me.M_TOOL_STATUS = New System.Windows.Forms.ComboBox()
        Me.lbl_STATUS = New System.Windows.Forms.Label()
        Me.lbl_M_HOZEN = New System.Windows.Forms.TabPage()
        Me.three_ref_M_HEADER_PARENT_NO = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.M_HOZEN_ATA = New System.Windows.Forms.TextBox()
        Me.three_ref_M_HEADER_KANRI_NO = New System.Windows.Forms.TextBox()
        Me.three_lbl_KANRI_NO = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_MODEL = New System.Windows.Forms.TextBox()
        Me.three_lbl_MODEL = New System.Windows.Forms.Label()
        Me.three_lbl_BUNRUI_01 = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_MODIFIED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.three_ref_M_HEADER_BUNRUI_01 = New System.Windows.Forms.TextBox()
        Me.three_lbl_MODIFIED_DATE = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_MODIFIED_BY = New System.Windows.Forms.TextBox()
        Me.three_lbl_MODIFIED_BY = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_CREATED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.three_lbl_CREATED_DATE = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_SPL = New System.Windows.Forms.TextBox()
        Me.three_lbl_SPL = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_MFG_NO = New System.Windows.Forms.TextBox()
        Me.three_lbl_MEISYOU = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_MEISYOU = New System.Windows.Forms.TextBox()
        Me.three_lbl_MFG_NO = New System.Windows.Forms.Label()
        Me.three_lbl_IMAGE = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_BASE = New System.Windows.Forms.ComboBox()
        Me.three_lbl_BASE = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_UN_NO = New System.Windows.Forms.TextBox()
        Me.three_lbl_UN_NO = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_MFG = New System.Windows.Forms.TextBox()
        Me.three_lbl_MFG = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_KIKAKU = New System.Windows.Forms.TextBox()
        Me.three_lbl_KIKAKU = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_HAICHI_KIJYUN = New System.Windows.Forms.ComboBox()
        Me.three_lbl_HAICHI_KIJYUN = New System.Windows.Forms.Label()
        Me.gpx_INSPECTION_PROGRAM = New System.Windows.Forms.GroupBox()
        Me.M_HOZEN_END_CHECK_DATE = New System.Windows.Forms.DateTimePicker()
        Me.three_lbl_END_CHECK_DATE = New System.Windows.Forms.Label()
        Me.three_ref_M_HEADER_LOCATION = New System.Windows.Forms.TextBox()
        Me.three_lbl_LOCATION = New System.Windows.Forms.Label()
        Me.M_HOZEN_CHECK_COMPANY = New System.Windows.Forms.TextBox()
        Me.three_lbl_CHECK_COMPANY = New System.Windows.Forms.Label()
        Me.M_HOZEN_CHECK_INTERVAL = New System.Windows.Forms.ComboBox()
        Me.three_lbl_HOZEN_INTERVAL = New System.Windows.Forms.Label()
        Me.M_HOZEN_NEXT_CHECK_DATE = New System.Windows.Forms.DateTimePicker()
        Me.three_lbl_NEXT_CHECK_DATE = New System.Windows.Forms.Label()
        Me.M_HOZEN_REASON = New System.Windows.Forms.TextBox()
        Me.three_lbl_REASON = New System.Windows.Forms.Label()
        Me.M_HOZEN_REMARKS = New System.Windows.Forms.TextBox()
        Me.three_lbl_REMARKS2 = New System.Windows.Forms.Label()
        Me.M_HOZEN_ALLOW_FLG = New System.Windows.Forms.CheckBox()
        Me.M_HOZEN_STATUS = New System.Windows.Forms.ComboBox()
        Me.three_lbl_STATUS = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.three_ref_M_HEADER_IMAGE = New System.Windows.Forms.PictureBox()
        Me.gpx_CHECK_RECORDS = New System.Windows.Forms.GroupBox()
        Me.M_HOZEN_CHECK_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lbl_CHECK_BY = New System.Windows.Forms.Label()
        Me.M_HOZEN_CHECK_BY = New System.Windows.Forms.TextBox()
        Me.lbl_CHECK_DATE = New System.Windows.Forms.Label()
        Me.lbl_M_PME = New System.Windows.Forms.TabPage()
        Me.four_ref_M_HEADER_PARENT_NO = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_MODEL = New System.Windows.Forms.TextBox()
        Me.four_lbl_MODEL = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_KIKAKU = New System.Windows.Forms.TextBox()
        Me.four_lbl_KIKAKU = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_KANRI_NO = New System.Windows.Forms.TextBox()
        Me.four_lbl_KANRI_NO = New System.Windows.Forms.Label()
        Me.four_lbl_BUNRUI_01 = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_BUNRUI_01 = New System.Windows.Forms.TextBox()
        Me.four_gpx_KOUSEI_GROUP = New System.Windows.Forms.GroupBox()
        Me.M_PME_KOUSEI_INTERVAL = New System.Windows.Forms.ComboBox()
        Me.btn_Kousei = New System.Windows.Forms.Button()
        Me.M_PME_KOUSEI_SEIDO = New System.Windows.Forms.TextBox()
        Me.lbl_KOUSEI_SEIDO = New System.Windows.Forms.Label()
        Me.four_lbl_KOUSEI_INTERVAL = New System.Windows.Forms.Label()
        Me.lbl_KEKKA = New System.Windows.Forms.Label()
        Me.M_PME_KEKKA = New System.Windows.Forms.TextBox()
        Me.M_PME_KOUSEI_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lbl_KOUSEI_DATE = New System.Windows.Forms.Label()
        Me.M_PME_DUE_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lbl_DUE_DATE = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_MODIFIED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.four_lbl_MODIFIED_DATE = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_MODIFIED_BY = New System.Windows.Forms.TextBox()
        Me.four_lbl_MODIFIED_BY = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_CREATED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.four_lbl_CREATED_DATE = New System.Windows.Forms.Label()
        Me.four_lbl_IMAGE = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_SPL = New System.Windows.Forms.TextBox()
        Me.four_lbl_SPL = New System.Windows.Forms.Label()
        Me.four_lbl_ATA = New System.Windows.Forms.Label()
        Me.M_PME_ATA = New System.Windows.Forms.TextBox()
        Me.four_ref_M_HEADER_MFG_NO = New System.Windows.Forms.TextBox()
        Me.four_lbl_MEISYOU = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_MEISYOU = New System.Windows.Forms.TextBox()
        Me.four_lbl_MFG_NO = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_BASE = New System.Windows.Forms.ComboBox()
        Me.four_lbl_BASE = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_UN_NO = New System.Windows.Forms.TextBox()
        Me.four_lbl_UN_NO = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_MFG = New System.Windows.Forms.TextBox()
        Me.four_lbl_MFG = New System.Windows.Forms.Label()
        Me.four_ref_M_HEADER_HAICHI_KIJYUN = New System.Windows.Forms.ComboBox()
        Me.four_lbl_HAICHI_KIJYUN = New System.Windows.Forms.Label()
        Me.gpx_M_PME_MFG_NO = New System.Windows.Forms.GroupBox()
        Me.M_PME_NOURYOKU = New System.Windows.Forms.ComboBox()
        Me.pgs = New System.Windows.Forms.ProgressBar()
        Me.four_lbl_M_PME_NOURYOKU = New System.Windows.Forms.Label()
        Me.M_PME_KOUSEI_COMPANY = New System.Windows.Forms.TextBox()
        Me.lbl_KOUSEI_COMPANY = New System.Windows.Forms.Label()
        Me.lbl_KOUSEI_BY_COMPANY = New System.Windows.Forms.Label()
        Me.M_PME_KOUSEI_BY_COMPANY = New System.Windows.Forms.TextBox()
        Me.M_PME_KOUSEI_KIKAN = New System.Windows.Forms.TextBox()
        Me.four_lbl_KOUSEI_KIKAN = New System.Windows.Forms.Label()
        Me.M_PME_REASON = New System.Windows.Forms.TextBox()
        Me.four_lbl_REASON = New System.Windows.Forms.Label()
        Me.M_PME_REMARKS = New System.Windows.Forms.TextBox()
        Me.four_lbl_REMARKS2 = New System.Windows.Forms.Label()
        Me.M_PME_ALLOW_FLG = New System.Windows.Forms.CheckBox()
        Me.M_PME_STATUS = New System.Windows.Forms.ComboBox()
        Me.four_lbl_STATUS = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.four_ref_M_HEADER_IMAGE = New System.Windows.Forms.PictureBox()
        Me.lbl_M_RENTAL = New System.Windows.Forms.TabPage()
        Me.six_ref_M_HEADER_PARENT_NO = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_KANRI_NO = New System.Windows.Forms.TextBox()
        Me.six_lbl_KANRI_NO = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_MODEL = New System.Windows.Forms.TextBox()
        Me.six_lbl_MODEL = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_KUBUN_01 = New System.Windows.Forms.ComboBox()
        Me.six_lbl_KUBUN_01 = New System.Windows.Forms.Label()
        Me.six_lbl_BUNRUI_01 = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_DOUTOUSEI = New System.Windows.Forms.TextBox()
        Me.six_ref_M_HEADER_BUNRUI_01 = New System.Windows.Forms.TextBox()
        Me.six_lbl_DOUTOUSEI = New System.Windows.Forms.Label()
        Me.M_RENTAL_TEKIGOUSEI = New System.Windows.Forms.DateTimePicker()
        Me.six_lbl_TEKIGOUSEI = New System.Windows.Forms.Label()
        Me.M_RENTAL_NOURYOKU = New System.Windows.Forms.ComboBox()
        Me.six_lbl_NOURYOKU = New System.Windows.Forms.Label()
        Me.M_RENTAL_LEASE_CONPANY = New System.Windows.Forms.TextBox()
        Me.six_lbl_RENTAL_CONPANY = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_KIKAKU = New System.Windows.Forms.TextBox()
        Me.six_lbl_KIKAKU = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_MODIFIED_BY = New System.Windows.Forms.TextBox()
        Me.six_lbl_MODIFIED_BY = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_CREATED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.six_lbl_CREATED_DATE = New System.Windows.Forms.Label()
        Me.six_lbl_IMAGE = New System.Windows.Forms.Label()
        Me.six_lbl_ATA = New System.Windows.Forms.Label()
        Me.M_RENTAL_ATA = New System.Windows.Forms.TextBox()
        Me.six_ref_M_HEADER_MFG_NO = New System.Windows.Forms.TextBox()
        Me.six_lbl_MEISYOU = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_MEISYOU = New System.Windows.Forms.TextBox()
        Me.six_lbl_MFG_NO = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_BASE = New System.Windows.Forms.ComboBox()
        Me.six_lbl_BASE = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_UN_NO = New System.Windows.Forms.TextBox()
        Me.six_lbl_UN_NO = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_MFG = New System.Windows.Forms.TextBox()
        Me.six_lbl_MFG = New System.Windows.Forms.Label()
        Me.six_ref_M_HEADER_HAICHI_KIJYUN = New System.Windows.Forms.ComboBox()
        Me.six_lbl_HAICHI_KIJYUN = New System.Windows.Forms.Label()
        Me.M_RENTAL_REASON = New System.Windows.Forms.TextBox()
        Me.six_lbl_REASON = New System.Windows.Forms.Label()
        Me.M_RENTAL_REMARKS = New System.Windows.Forms.TextBox()
        Me.six_lbl_REMARKS2 = New System.Windows.Forms.Label()
        Me.M_RENTAL_ALLOW_FLG = New System.Windows.Forms.CheckBox()
        Me.M_RENTAL_DUE_DATE = New System.Windows.Forms.DateTimePicker()
        Me.six_lbl_DUE_DATE = New System.Windows.Forms.Label()
        Me.M_RENTAL_KAKUNIN_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lbl_KAKUNIN_DATE = New System.Windows.Forms.Label()
        Me.M_RENTAL_LEASE_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lbl_LEASE_DATE = New System.Windows.Forms.Label()
        Me.M_RENTAL_KEKKA = New System.Windows.Forms.TextBox()
        Me.six_lbl_KEKKA = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.six_ref_M_HEADER_IMAGE = New System.Windows.Forms.PictureBox()
        Me.lbl_M_LEASE = New System.Windows.Forms.TabPage()
        Me.eight_ref_M_HEADER_PARENT_NO = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.eight_ref_M_HEADER_MODEL = New System.Windows.Forms.TextBox()
        Me.eightt_lbl_MODEL = New System.Windows.Forms.Label()
        Me.eight_ref_M_HEADER_KANRI_NO = New System.Windows.Forms.TextBox()
        Me.eight_lbl_KANRI_NO = New System.Windows.Forms.Label()
        Me.eight_lbl_BUNRUI_01 = New System.Windows.Forms.Label()
        Me.eight_ref_M_HEADER_KIKAKU = New System.Windows.Forms.TextBox()
        Me.eight_ref_M_HEADER_BUNRUI_01 = New System.Windows.Forms.TextBox()
        Me.eight_lbl_KIKAKU = New System.Windows.Forms.Label()
        Me.eight_ref_M_HEADER_MODIFIED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.eight_lbl_MODIFIED_DATE = New System.Windows.Forms.Label()
        Me.eight_ref_M_HEADER_MODIFIED_BY = New System.Windows.Forms.TextBox()
        Me.eight_lbl_MODIFIED_BY = New System.Windows.Forms.Label()
        Me.eight_ref_M_HEADER_CREATED_DATE = New System.Windows.Forms.DateTimePicker()
        Me.eight_lbl_CREATED_DATE = New System.Windows.Forms.Label()
        Me.eight_lbl_IMAGE = New System.Windows.Forms.Label()
        Me.eight_ref_M_HEADER_MFG_NO = New System.Windows.Forms.TextBox()
        Me.eight_lbl_MEISYOU = New System.Windows.Forms.Label()
        Me.eight_ref_M_HEADER_MEISYOU = New System.Windows.Forms.TextBox()
        Me.eight_lbl_MFG_NO = New System.Windows.Forms.Label()
        Me.eight_ref_M_HEADER_MFG = New System.Windows.Forms.TextBox()
        Me.eight_lbl_MFG = New System.Windows.Forms.Label()
        Me.M_LEASE_REASON = New System.Windows.Forms.TextBox()
        Me.eight_lbl_REASON = New System.Windows.Forms.Label()
        Me.M_LEASE_REMARKS = New System.Windows.Forms.TextBox()
        Me.eight_lbl_REMARKS2 = New System.Windows.Forms.Label()
        Me.M_LEASE_LEASE_END_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lbl_LEASE_END_DATE = New System.Windows.Forms.Label()
        Me.M_LEASE_LEASE_START_DATE = New System.Windows.Forms.DateTimePicker()
        Me.lbl_LEASE_START_DATE = New System.Windows.Forms.Label()
        Me.M_LEASE_LEASE_COUNT = New System.Windows.Forms.ComboBox()
        Me.lbl_LEASE_COUNT = New System.Windows.Forms.Label()
        Me.M_LEASE_FIRST_LOCATION = New System.Windows.Forms.ComboBox()
        Me.lbl_FIRST_LOCATION = New System.Windows.Forms.Label()
        Me.M_LEASE_LEASE_NO = New System.Windows.Forms.TextBox()
        Me.lbl_LEASE_NO = New System.Windows.Forms.Label()
        Me.M_LEASE_LEASE_COMPANY = New System.Windows.Forms.TextBox()
        Me.lbl_LEASE_COMPANY = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.eight_ref_M_HEADER_IMAGE = New System.Windows.Forms.PictureBox()
        Me.btn_Cancel = New System.Windows.Forms.Button()
        Me.btn_Insert = New System.Windows.Forms.Button()
        Me.btn_History = New System.Windows.Forms.Button()
        Me.btn_Copy = New System.Windows.Forms.Button()
        Me.lbl_M_SHISAN.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        CType(Me.eleven_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl2.SuspendLayout()
        Me.lbl_Detail.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.lbl_M_HEADER.SuspendLayout()
        Me.gpx_ADDITIONAL_GROUP.SuspendLayout()
        Me.gpx_HOYU_GROUP.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpx_RENTAL_GROUP.SuspendLayout()
        Me.gpx_CATEGORY_GROUP.SuspendLayout()
        Me.lbl_M_TOOL.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.two_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.lbl_M_HOZEN.SuspendLayout()
        Me.gpx_INSPECTION_PROGRAM.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.three_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gpx_CHECK_RECORDS.SuspendLayout()
        Me.lbl_M_PME.SuspendLayout()
        Me.four_gpx_KOUSEI_GROUP.SuspendLayout()
        Me.gpx_M_PME_MFG_NO.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.four_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.lbl_M_RENTAL.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.six_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.lbl_M_LEASE.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.eight_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lbl_M_SHISAN
        '
        Me.lbl_M_SHISAN.BackColor = System.Drawing.SystemColors.Control
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_PARENT_NO)
        Me.lbl_M_SHISAN.Controls.Add(Me.Label5)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_MODEL)
        Me.lbl_M_SHISAN.Controls.Add(Me.elevent_lbl_MODEL)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_BUNRUI_01)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_KANRI_NO)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_BUNRUI_01)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_KANRI_NO)
        Me.lbl_M_SHISAN.Controls.Add(Me.M_SHISAN_TAIYOU)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_CREATED_DATE)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_CREATED_DATE)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_KIKAKU)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_LOCATION)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_BASE)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_MFG)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_LOCATION)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_TAIYOU)
        Me.lbl_M_SHISAN.Controls.Add(Me.GroupBox16)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_IMAGE)
        Me.lbl_M_SHISAN.Controls.Add(Me.M_SHISAN_SHISAN_NO)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_SHISAN_NO)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_BASE)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_MFG_NO)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_KIKAKU)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_MEISYOU)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_MEISYOU)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_MFG)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_SPL)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_lbl_SPL)
        Me.lbl_M_SHISAN.Controls.Add(Me.eleven_ref_M_HEADER_MFG_NO)
        Me.lbl_M_SHISAN.Location = New System.Drawing.Point(4, 22)
        Me.lbl_M_SHISAN.Name = "lbl_M_SHISAN"
        Me.lbl_M_SHISAN.Size = New System.Drawing.Size(703, 576)
        Me.lbl_M_SHISAN.TabIndex = 7
        Me.lbl_M_SHISAN.Tag = "M_SHISAN"
        Me.lbl_M_SHISAN.Text = "固定資産"
        '
        'eleven_ref_M_HEADER_PARENT_NO
        '
        Me.eleven_ref_M_HEADER_PARENT_NO.Enabled = False
        Me.eleven_ref_M_HEADER_PARENT_NO.Location = New System.Drawing.Point(68, 154)
        Me.eleven_ref_M_HEADER_PARENT_NO.MaxLength = 20
        Me.eleven_ref_M_HEADER_PARENT_NO.Name = "eleven_ref_M_HEADER_PARENT_NO"
        Me.eleven_ref_M_HEADER_PARENT_NO.Size = New System.Drawing.Size(379, 19)
        Me.eleven_ref_M_HEADER_PARENT_NO.TabIndex = 1279
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(10, 153)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 23)
        Me.Label5.TabIndex = 1280
        Me.Label5.Text = "親番号"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_ref_M_HEADER_MODEL
        '
        Me.eleven_ref_M_HEADER_MODEL.Enabled = False
        Me.eleven_ref_M_HEADER_MODEL.Location = New System.Drawing.Point(68, 57)
        Me.eleven_ref_M_HEADER_MODEL.MaxLength = 30
        Me.eleven_ref_M_HEADER_MODEL.Name = "eleven_ref_M_HEADER_MODEL"
        Me.eleven_ref_M_HEADER_MODEL.Size = New System.Drawing.Size(607, 19)
        Me.eleven_ref_M_HEADER_MODEL.TabIndex = 1278
        '
        'elevent_lbl_MODEL
        '
        Me.elevent_lbl_MODEL.Location = New System.Drawing.Point(30, 54)
        Me.elevent_lbl_MODEL.Name = "elevent_lbl_MODEL"
        Me.elevent_lbl_MODEL.Size = New System.Drawing.Size(34, 23)
        Me.elevent_lbl_MODEL.TabIndex = 1277
        Me.elevent_lbl_MODEL.Text = "型式"
        Me.elevent_lbl_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_lbl_BUNRUI_01
        '
        Me.eleven_lbl_BUNRUI_01.Location = New System.Drawing.Point(342, 10)
        Me.eleven_lbl_BUNRUI_01.Name = "eleven_lbl_BUNRUI_01"
        Me.eleven_lbl_BUNRUI_01.Size = New System.Drawing.Size(56, 16)
        Me.eleven_lbl_BUNRUI_01.TabIndex = 214
        Me.eleven_lbl_BUNRUI_01.Text = "分類番号"
        Me.eleven_lbl_BUNRUI_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_ref_M_HEADER_KANRI_NO
        '
        Me.eleven_ref_M_HEADER_KANRI_NO.Enabled = False
        Me.eleven_ref_M_HEADER_KANRI_NO.Location = New System.Drawing.Point(68, 9)
        Me.eleven_ref_M_HEADER_KANRI_NO.MaxLength = 100
        Me.eleven_ref_M_HEADER_KANRI_NO.Name = "eleven_ref_M_HEADER_KANRI_NO"
        Me.eleven_ref_M_HEADER_KANRI_NO.Size = New System.Drawing.Size(268, 19)
        Me.eleven_ref_M_HEADER_KANRI_NO.TabIndex = 1275
        Me.eleven_ref_M_HEADER_KANRI_NO.Tag = "KANRI_NO"
        '
        'eleven_ref_M_HEADER_BUNRUI_01
        '
        Me.eleven_ref_M_HEADER_BUNRUI_01.Enabled = False
        Me.eleven_ref_M_HEADER_BUNRUI_01.Location = New System.Drawing.Point(407, 9)
        Me.eleven_ref_M_HEADER_BUNRUI_01.MaxLength = 20
        Me.eleven_ref_M_HEADER_BUNRUI_01.Name = "eleven_ref_M_HEADER_BUNRUI_01"
        Me.eleven_ref_M_HEADER_BUNRUI_01.Size = New System.Drawing.Size(268, 19)
        Me.eleven_ref_M_HEADER_BUNRUI_01.TabIndex = 204
        Me.eleven_ref_M_HEADER_BUNRUI_01.Tag = "BUNRUI_01"
        '
        'eleven_lbl_KANRI_NO
        '
        Me.eleven_lbl_KANRI_NO.Location = New System.Drawing.Point(7, 9)
        Me.eleven_lbl_KANRI_NO.Name = "eleven_lbl_KANRI_NO"
        Me.eleven_lbl_KANRI_NO.Size = New System.Drawing.Size(57, 23)
        Me.eleven_lbl_KANRI_NO.TabIndex = 1276
        Me.eleven_lbl_KANRI_NO.Text = "管理番号"
        Me.eleven_lbl_KANRI_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_SHISAN_TAIYOU
        '
        Me.M_SHISAN_TAIYOU.Location = New System.Drawing.Point(69, 204)
        Me.M_SHISAN_TAIYOU.MaxLength = 20
        Me.M_SHISAN_TAIYOU.Name = "M_SHISAN_TAIYOU"
        Me.M_SHISAN_TAIYOU.Size = New System.Drawing.Size(133, 19)
        Me.M_SHISAN_TAIYOU.TabIndex = 1
        '
        'eleven_ref_M_HEADER_CREATED_DATE
        '
        Me.eleven_ref_M_HEADER_CREATED_DATE.Enabled = False
        Me.eleven_ref_M_HEADER_CREATED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.eleven_ref_M_HEADER_CREATED_DATE.Location = New System.Drawing.Point(68, 180)
        Me.eleven_ref_M_HEADER_CREATED_DATE.Name = "eleven_ref_M_HEADER_CREATED_DATE"
        Me.eleven_ref_M_HEADER_CREATED_DATE.Size = New System.Drawing.Size(133, 19)
        Me.eleven_ref_M_HEADER_CREATED_DATE.TabIndex = 1266
        '
        'eleven_lbl_CREATED_DATE
        '
        Me.eleven_lbl_CREATED_DATE.Location = New System.Drawing.Point(13, 178)
        Me.eleven_lbl_CREATED_DATE.Name = "eleven_lbl_CREATED_DATE"
        Me.eleven_lbl_CREATED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.eleven_lbl_CREATED_DATE.TabIndex = 1265
        Me.eleven_lbl_CREATED_DATE.Text = "登録日"
        Me.eleven_lbl_CREATED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_ref_M_HEADER_KIKAKU
        '
        Me.eleven_ref_M_HEADER_KIKAKU.Enabled = False
        Me.eleven_ref_M_HEADER_KIKAKU.Location = New System.Drawing.Point(68, 81)
        Me.eleven_ref_M_HEADER_KIKAKU.MaxLength = 50
        Me.eleven_ref_M_HEADER_KIKAKU.Name = "eleven_ref_M_HEADER_KIKAKU"
        Me.eleven_ref_M_HEADER_KIKAKU.Size = New System.Drawing.Size(607, 19)
        Me.eleven_ref_M_HEADER_KIKAKU.TabIndex = 1254
        '
        'eleven_lbl_LOCATION
        '
        Me.eleven_lbl_LOCATION.Location = New System.Drawing.Point(468, 153)
        Me.eleven_lbl_LOCATION.Name = "eleven_lbl_LOCATION"
        Me.eleven_lbl_LOCATION.Size = New System.Drawing.Size(71, 23)
        Me.eleven_lbl_LOCATION.TabIndex = 1263
        Me.eleven_lbl_LOCATION.Text = "ロケーション"
        Me.eleven_lbl_LOCATION.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_lbl_BASE
        '
        Me.eleven_lbl_BASE.Location = New System.Drawing.Point(479, 128)
        Me.eleven_lbl_BASE.Name = "eleven_lbl_BASE"
        Me.eleven_lbl_BASE.Size = New System.Drawing.Size(58, 23)
        Me.eleven_lbl_BASE.TabIndex = 1257
        Me.eleven_lbl_BASE.Text = "配置基地"
        Me.eleven_lbl_BASE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_lbl_MFG
        '
        Me.eleven_lbl_MFG.Location = New System.Drawing.Point(5, 102)
        Me.eleven_lbl_MFG.Name = "eleven_lbl_MFG"
        Me.eleven_lbl_MFG.Size = New System.Drawing.Size(59, 23)
        Me.eleven_lbl_MFG.TabIndex = 1256
        Me.eleven_lbl_MFG.Text = "製造会社"
        Me.eleven_lbl_MFG.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_ref_M_HEADER_LOCATION
        '
        Me.eleven_ref_M_HEADER_LOCATION.Enabled = False
        Me.eleven_ref_M_HEADER_LOCATION.Location = New System.Drawing.Point(542, 155)
        Me.eleven_ref_M_HEADER_LOCATION.MaxLength = 20
        Me.eleven_ref_M_HEADER_LOCATION.Name = "eleven_ref_M_HEADER_LOCATION"
        Me.eleven_ref_M_HEADER_LOCATION.Size = New System.Drawing.Size(133, 19)
        Me.eleven_ref_M_HEADER_LOCATION.TabIndex = 1264
        '
        'eleven_lbl_TAIYOU
        '
        Me.eleven_lbl_TAIYOU.Location = New System.Drawing.Point(5, 204)
        Me.eleven_lbl_TAIYOU.Name = "eleven_lbl_TAIYOU"
        Me.eleven_lbl_TAIYOU.Size = New System.Drawing.Size(59, 20)
        Me.eleven_lbl_TAIYOU.TabIndex = 1268
        Me.eleven_lbl_TAIYOU.Text = "耐用年数"
        Me.eleven_lbl_TAIYOU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox16
        '
        Me.GroupBox16.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox16.Controls.Add(Me.eleven_ref_M_HEADER_IMAGE)
        Me.GroupBox16.Location = New System.Drawing.Point(507, 207)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(168, 126)
        Me.GroupBox16.TabIndex = 1272
        Me.GroupBox16.TabStop = False
        '
        'eleven_ref_M_HEADER_IMAGE
        '
        Me.eleven_ref_M_HEADER_IMAGE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.eleven_ref_M_HEADER_IMAGE.Location = New System.Drawing.Point(2, 9)
        Me.eleven_ref_M_HEADER_IMAGE.Name = "eleven_ref_M_HEADER_IMAGE"
        Me.eleven_ref_M_HEADER_IMAGE.Size = New System.Drawing.Size(163, 115)
        Me.eleven_ref_M_HEADER_IMAGE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.eleven_ref_M_HEADER_IMAGE.TabIndex = 84
        Me.eleven_ref_M_HEADER_IMAGE.TabStop = False
        '
        'eleven_lbl_IMAGE
        '
        Me.eleven_lbl_IMAGE.Location = New System.Drawing.Point(492, 187)
        Me.eleven_lbl_IMAGE.Name = "eleven_lbl_IMAGE"
        Me.eleven_lbl_IMAGE.Size = New System.Drawing.Size(44, 17)
        Me.eleven_lbl_IMAGE.TabIndex = 1271
        Me.eleven_lbl_IMAGE.Text = "写真"
        Me.eleven_lbl_IMAGE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_SHISAN_SHISAN_NO
        '
        Me.M_SHISAN_SHISAN_NO.Location = New System.Drawing.Point(315, 180)
        Me.M_SHISAN_SHISAN_NO.MaxLength = 20
        Me.M_SHISAN_SHISAN_NO.Name = "M_SHISAN_SHISAN_NO"
        Me.M_SHISAN_SHISAN_NO.Size = New System.Drawing.Size(132, 19)
        Me.M_SHISAN_SHISAN_NO.TabIndex = 0
        Me.M_SHISAN_SHISAN_NO.Tag = ""
        Me.M_SHISAN_SHISAN_NO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'eleven_lbl_SHISAN_NO
        '
        Me.eleven_lbl_SHISAN_NO.Location = New System.Drawing.Point(238, 179)
        Me.eleven_lbl_SHISAN_NO.Name = "eleven_lbl_SHISAN_NO"
        Me.eleven_lbl_SHISAN_NO.Size = New System.Drawing.Size(78, 20)
        Me.eleven_lbl_SHISAN_NO.TabIndex = 1270
        Me.eleven_lbl_SHISAN_NO.Text = "固定資産番号"
        Me.eleven_lbl_SHISAN_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_ref_M_HEADER_BASE
        '
        Me.eleven_ref_M_HEADER_BASE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.eleven_ref_M_HEADER_BASE.Enabled = False
        Me.eleven_ref_M_HEADER_BASE.FormattingEnabled = True
        Me.eleven_ref_M_HEADER_BASE.Location = New System.Drawing.Point(542, 128)
        Me.eleven_ref_M_HEADER_BASE.Name = "eleven_ref_M_HEADER_BASE"
        Me.eleven_ref_M_HEADER_BASE.Size = New System.Drawing.Size(133, 20)
        Me.eleven_ref_M_HEADER_BASE.TabIndex = 1259
        '
        'eleven_lbl_MFG_NO
        '
        Me.eleven_lbl_MFG_NO.Location = New System.Drawing.Point(481, 102)
        Me.eleven_lbl_MFG_NO.Name = "eleven_lbl_MFG_NO"
        Me.eleven_lbl_MFG_NO.Size = New System.Drawing.Size(56, 23)
        Me.eleven_lbl_MFG_NO.TabIndex = 1255
        Me.eleven_lbl_MFG_NO.Text = "製造番号"
        Me.eleven_lbl_MFG_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_lbl_KIKAKU
        '
        Me.eleven_lbl_KIKAKU.Location = New System.Drawing.Point(12, 81)
        Me.eleven_lbl_KIKAKU.Name = "eleven_lbl_KIKAKU"
        Me.eleven_lbl_KIKAKU.Size = New System.Drawing.Size(52, 16)
        Me.eleven_lbl_KIKAKU.TabIndex = 1253
        Me.eleven_lbl_KIKAKU.Text = "規格"
        Me.eleven_lbl_KIKAKU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_lbl_MEISYOU
        '
        Me.eleven_lbl_MEISYOU.Location = New System.Drawing.Point(30, 31)
        Me.eleven_lbl_MEISYOU.Name = "eleven_lbl_MEISYOU"
        Me.eleven_lbl_MEISYOU.Size = New System.Drawing.Size(34, 23)
        Me.eleven_lbl_MEISYOU.TabIndex = 1250
        Me.eleven_lbl_MEISYOU.Text = "名称"
        Me.eleven_lbl_MEISYOU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_ref_M_HEADER_MEISYOU
        '
        Me.eleven_ref_M_HEADER_MEISYOU.Enabled = False
        Me.eleven_ref_M_HEADER_MEISYOU.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.eleven_ref_M_HEADER_MEISYOU.Location = New System.Drawing.Point(68, 33)
        Me.eleven_ref_M_HEADER_MEISYOU.MaxLength = 100
        Me.eleven_ref_M_HEADER_MEISYOU.Name = "eleven_ref_M_HEADER_MEISYOU"
        Me.eleven_ref_M_HEADER_MEISYOU.Size = New System.Drawing.Size(607, 19)
        Me.eleven_ref_M_HEADER_MEISYOU.TabIndex = 1251
        Me.eleven_ref_M_HEADER_MEISYOU.Tag = "MEISYOU"
        '
        'eleven_ref_M_HEADER_MFG
        '
        Me.eleven_ref_M_HEADER_MFG.Enabled = False
        Me.eleven_ref_M_HEADER_MFG.Location = New System.Drawing.Point(68, 105)
        Me.eleven_ref_M_HEADER_MFG.MaxLength = 50
        Me.eleven_ref_M_HEADER_MFG.Name = "eleven_ref_M_HEADER_MFG"
        Me.eleven_ref_M_HEADER_MFG.Size = New System.Drawing.Size(379, 19)
        Me.eleven_ref_M_HEADER_MFG.TabIndex = 1262
        '
        'eleven_ref_M_HEADER_SPL
        '
        Me.eleven_ref_M_HEADER_SPL.Enabled = False
        Me.eleven_ref_M_HEADER_SPL.Location = New System.Drawing.Point(68, 129)
        Me.eleven_ref_M_HEADER_SPL.MaxLength = 50
        Me.eleven_ref_M_HEADER_SPL.Name = "eleven_ref_M_HEADER_SPL"
        Me.eleven_ref_M_HEADER_SPL.Size = New System.Drawing.Size(379, 19)
        Me.eleven_ref_M_HEADER_SPL.TabIndex = 1261
        '
        'eleven_lbl_SPL
        '
        Me.eleven_lbl_SPL.Location = New System.Drawing.Point(5, 126)
        Me.eleven_lbl_SPL.Name = "eleven_lbl_SPL"
        Me.eleven_lbl_SPL.Size = New System.Drawing.Size(59, 23)
        Me.eleven_lbl_SPL.TabIndex = 1260
        Me.eleven_lbl_SPL.Text = "購入会社"
        Me.eleven_lbl_SPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eleven_ref_M_HEADER_MFG_NO
        '
        Me.eleven_ref_M_HEADER_MFG_NO.Enabled = False
        Me.eleven_ref_M_HEADER_MFG_NO.Location = New System.Drawing.Point(542, 104)
        Me.eleven_ref_M_HEADER_MFG_NO.MaxLength = 100
        Me.eleven_ref_M_HEADER_MFG_NO.Name = "eleven_ref_M_HEADER_MFG_NO"
        Me.eleven_ref_M_HEADER_MFG_NO.Size = New System.Drawing.Size(133, 19)
        Me.eleven_ref_M_HEADER_MFG_NO.TabIndex = 1258
        Me.eleven_ref_M_HEADER_MFG_NO.Tag = "BUNRUI_02"
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label6.Location = New System.Drawing.Point(0, 628)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(740, 3)
        Me.Label6.TabIndex = 0
        '
        'btn_New
        '
        Me.btn_New.BackColor = System.Drawing.SystemColors.Control
        Me.btn_New.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_New.Location = New System.Drawing.Point(359, 636)
        Me.btn_New.Name = "btn_New"
        Me.btn_New.Size = New System.Drawing.Size(65, 21)
        Me.btn_New.TabIndex = 6
        Me.btn_New.Text = "新規"
        Me.btn_New.UseVisualStyleBackColor = False
        Me.btn_New.Visible = False
        '
        'btn_Close
        '
        Me.btn_Close.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Close.Location = New System.Drawing.Point(12, 636)
        Me.btn_Close.Name = "btn_Close"
        Me.btn_Close.Size = New System.Drawing.Size(65, 21)
        Me.btn_Close.TabIndex = 14
        Me.btn_Close.Text = "閉じる"
        Me.btn_Close.UseVisualStyleBackColor = False
        '
        'btn_Update
        '
        Me.btn_Update.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Update.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Update.Location = New System.Drawing.Point(431, 636)
        Me.btn_Update.Name = "btn_Update"
        Me.btn_Update.Size = New System.Drawing.Size(65, 21)
        Me.btn_Update.TabIndex = 7
        Me.btn_Update.Text = "更新"
        Me.btn_Update.UseVisualStyleBackColor = False
        Me.btn_Update.Visible = False
        '
        'btn_Del
        '
        Me.btn_Del.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Del.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Del.Location = New System.Drawing.Point(502, 636)
        Me.btn_Del.Name = "btn_Del"
        Me.btn_Del.Size = New System.Drawing.Size(65, 21)
        Me.btn_Del.TabIndex = 8
        Me.btn_Del.Text = "削除"
        Me.btn_Del.UseVisualStyleBackColor = False
        Me.btn_Del.Visible = False
        '
        'btn_Export
        '
        Me.btn_Export.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Export.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Export.Location = New System.Drawing.Point(573, 636)
        Me.btn_Export.Name = "btn_Export"
        Me.btn_Export.Size = New System.Drawing.Size(65, 21)
        Me.btn_Export.TabIndex = 9
        Me.btn_Export.Text = "一覧表"
        Me.btn_Export.UseVisualStyleBackColor = False
        Me.btn_Export.Visible = False
        '
        'OK
        '
        Me.OK.Location = New System.Drawing.Point(554, 10)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(58, 21)
        Me.OK.TabIndex = 3
        Me.OK.Text = "検索"
        '
        'TabControl2
        '
        Me.TabControl2.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.TabControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl2.Controls.Add(Me.lbl_Detail)
        Me.TabControl2.Location = New System.Drawing.Point(12, 6)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(698, 617)
        Me.TabControl2.TabIndex = 6
        '
        'lbl_Detail
        '
        Me.lbl_Detail.BackColor = System.Drawing.Color.Transparent
        Me.lbl_Detail.Controls.Add(Me.TabControl1)
        Me.lbl_Detail.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lbl_Detail.Location = New System.Drawing.Point(4, 4)
        Me.lbl_Detail.Name = "lbl_Detail"
        Me.lbl_Detail.Padding = New System.Windows.Forms.Padding(3)
        Me.lbl_Detail.Size = New System.Drawing.Size(690, 591)
        Me.lbl_Detail.TabIndex = 1
        Me.lbl_Detail.Text = "  編集画面  "
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.lbl_M_HEADER)
        Me.TabControl1.Controls.Add(Me.lbl_M_TOOL)
        Me.TabControl1.Controls.Add(Me.lbl_M_HOZEN)
        Me.TabControl1.Controls.Add(Me.lbl_M_PME)
        Me.TabControl1.Controls.Add(Me.lbl_M_RENTAL)
        Me.TabControl1.Controls.Add(Me.lbl_M_SHISAN)
        Me.TabControl1.Controls.Add(Me.lbl_M_LEASE)
        Me.TabControl1.Location = New System.Drawing.Point(-3, 1)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.RightToLeftLayout = True
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(711, 602)
        Me.TabControl1.TabIndex = 0
        Me.TabControl1.Tag = "M_TOOL"
        '
        'lbl_M_HEADER
        '
        Me.lbl_M_HEADER.BackColor = System.Drawing.Color.FromArgb(CType(CType(151, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_PARENT_NO)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_PARENT_NO)
        Me.lbl_M_HEADER.Controls.Add(Me.btn_ImageDownload)
        Me.lbl_M_HEADER.Controls.Add(Me.btn_FileDownload)
        Me.lbl_M_HEADER.Controls.Add(Me.gpx_ADDITIONAL_GROUP)
        Me.lbl_M_HEADER.Controls.Add(Me.one_ref_STATUS)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_HAIKI_DATE)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_HAIKI_FLG)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_HAIKI_DATE)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_UN_NO)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_UN_NO)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_SPL)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_SPL)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_MFG)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_MFG)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_BUNRUI_01)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_BUNRUI_01)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_KANRI_NO)
        Me.lbl_M_HEADER.Controls.Add(Me.one_lbl_KANRI_NO)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_KUBUN_01)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_KUBUN_01)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_MODEL)
        Me.lbl_M_HEADER.Controls.Add(Me.one_lbl_MODEL)
        Me.lbl_M_HEADER.Controls.Add(Me.btn_FileClear)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_REASON)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_REASON)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_DOUTOUSEI)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_DOUTOUSEI)
        Me.lbl_M_HEADER.Controls.Add(Me.one_lbl_STATUS)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_REFERENCE)
        Me.lbl_M_HEADER.Controls.Add(Me.btn_File)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_Reference)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_CERTIFICATE_NAME)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_MFG_NO)
        Me.lbl_M_HEADER.Controls.Add(Me.one_lbl_MEISYOU)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_MEISYOU)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_MODIFIED_DATE)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_MODIFIED_DATE)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_MODIFIED_BY)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_MODIFIED_BY)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_CREATED_DATE)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_CREATED_DATE)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_CREATED_BY)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_CREATED_BY)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_MFG_NO)
        Me.lbl_M_HEADER.Controls.Add(Me.btn_ImageClear)
        Me.lbl_M_HEADER.Controls.Add(Me.btn_ImageSel)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_IMAGE)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_REMARKS)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_Certificate)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_REMARKS)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_LOCATION)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_LOCATION)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_BASE)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_BASE)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_KIKAKU)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_KIKAKU)
        Me.lbl_M_HEADER.Controls.Add(Me.M_HEADER_HAICHI_KIJYUN)
        Me.lbl_M_HEADER.Controls.Add(Me.lbl_HAICHI_KIJYUN)
        Me.lbl_M_HEADER.Controls.Add(Me.gpx_HOYU_GROUP)
        Me.lbl_M_HEADER.Controls.Add(Me.GroupBox1)
        Me.lbl_M_HEADER.Controls.Add(Me.gpx_RENTAL_GROUP)
        Me.lbl_M_HEADER.Controls.Add(Me.gpx_CATEGORY_GROUP)
        Me.lbl_M_HEADER.Location = New System.Drawing.Point(4, 22)
        Me.lbl_M_HEADER.Name = "lbl_M_HEADER"
        Me.lbl_M_HEADER.Padding = New System.Windows.Forms.Padding(3)
        Me.lbl_M_HEADER.Size = New System.Drawing.Size(703, 576)
        Me.lbl_M_HEADER.TabIndex = 0
        Me.lbl_M_HEADER.Tag = "M_HEADER"
        '
        'M_HEADER_PARENT_NO
        '
        Me.M_HEADER_PARENT_NO.Location = New System.Drawing.Point(67, 170)
        Me.M_HEADER_PARENT_NO.MaxLength = 20
        Me.M_HEADER_PARENT_NO.Name = "M_HEADER_PARENT_NO"
        Me.M_HEADER_PARENT_NO.Size = New System.Drawing.Size(394, 19)
        Me.M_HEADER_PARENT_NO.TabIndex = 1032
        '
        'lbl_PARENT_NO
        '
        Me.lbl_PARENT_NO.Location = New System.Drawing.Point(11, 169)
        Me.lbl_PARENT_NO.Name = "lbl_PARENT_NO"
        Me.lbl_PARENT_NO.Size = New System.Drawing.Size(52, 23)
        Me.lbl_PARENT_NO.TabIndex = 1033
        Me.lbl_PARENT_NO.Text = "親番号"
        Me.lbl_PARENT_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btn_ImageDownload
        '
        Me.btn_ImageDownload.BackColor = System.Drawing.SystemColors.Control
        Me.btn_ImageDownload.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_ImageDownload.Location = New System.Drawing.Point(538, 283)
        Me.btn_ImageDownload.Name = "btn_ImageDownload"
        Me.btn_ImageDownload.Size = New System.Drawing.Size(134, 21)
        Me.btn_ImageDownload.TabIndex = 1031
        Me.btn_ImageDownload.Text = "ダウンロード"
        Me.btn_ImageDownload.UseVisualStyleBackColor = False
        '
        'btn_FileDownload
        '
        Me.btn_FileDownload.BackColor = System.Drawing.SystemColors.Control
        Me.btn_FileDownload.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_FileDownload.Location = New System.Drawing.Point(402, 458)
        Me.btn_FileDownload.Name = "btn_FileDownload"
        Me.btn_FileDownload.Size = New System.Drawing.Size(78, 21)
        Me.btn_FileDownload.TabIndex = 1030
        Me.btn_FileDownload.Text = "ダウンロード"
        Me.btn_FileDownload.UseVisualStyleBackColor = False
        Me.btn_FileDownload.Visible = False
        '
        'gpx_ADDITIONAL_GROUP
        '
        Me.gpx_ADDITIONAL_GROUP.Controls.Add(Me.M_HEADER_SHISAN_FLG)
        Me.gpx_ADDITIONAL_GROUP.Controls.Add(Me.M_HEADER_LEASE_FLG)
        Me.gpx_ADDITIONAL_GROUP.Location = New System.Drawing.Point(212, 203)
        Me.gpx_ADDITIONAL_GROUP.Name = "gpx_ADDITIONAL_GROUP"
        Me.gpx_ADDITIONAL_GROUP.Size = New System.Drawing.Size(247, 41)
        Me.gpx_ADDITIONAL_GROUP.TabIndex = 12
        Me.gpx_ADDITIONAL_GROUP.TabStop = False
        Me.gpx_ADDITIONAL_GROUP.Text = "追加情報"
        Me.gpx_ADDITIONAL_GROUP.Visible = False
        '
        'M_HEADER_SHISAN_FLG
        '
        Me.M_HEADER_SHISAN_FLG.AutoSize = True
        Me.M_HEADER_SHISAN_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_HEADER_SHISAN_FLG.Location = New System.Drawing.Point(14, 18)
        Me.M_HEADER_SHISAN_FLG.Name = "M_HEADER_SHISAN_FLG"
        Me.M_HEADER_SHISAN_FLG.Size = New System.Drawing.Size(72, 16)
        Me.M_HEADER_SHISAN_FLG.TabIndex = 0
        Me.M_HEADER_SHISAN_FLG.Text = "固定資産"
        Me.M_HEADER_SHISAN_FLG.UseVisualStyleBackColor = True
        '
        'M_HEADER_LEASE_FLG
        '
        Me.M_HEADER_LEASE_FLG.AutoSize = True
        Me.M_HEADER_LEASE_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_HEADER_LEASE_FLG.Location = New System.Drawing.Point(108, 20)
        Me.M_HEADER_LEASE_FLG.Name = "M_HEADER_LEASE_FLG"
        Me.M_HEADER_LEASE_FLG.Size = New System.Drawing.Size(56, 16)
        Me.M_HEADER_LEASE_FLG.TabIndex = 1
        Me.M_HEADER_LEASE_FLG.Text = "リース"
        Me.M_HEADER_LEASE_FLG.UseVisualStyleBackColor = True
        '
        'one_ref_STATUS
        '
        Me.one_ref_STATUS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.one_ref_STATUS.Enabled = False
        Me.one_ref_STATUS.FormattingEnabled = True
        Me.one_ref_STATUS.Location = New System.Drawing.Point(539, 231)
        Me.one_ref_STATUS.Name = "one_ref_STATUS"
        Me.one_ref_STATUS.Size = New System.Drawing.Size(133, 20)
        Me.one_ref_STATUS.TabIndex = 15
        Me.one_ref_STATUS.Visible = False
        '
        'M_HEADER_HAIKI_DATE
        '
        Me.M_HEADER_HAIKI_DATE.Checked = False
        Me.M_HEADER_HAIKI_DATE.CustomFormat = ""
        Me.M_HEADER_HAIKI_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_HEADER_HAIKI_DATE.Location = New System.Drawing.Point(536, 457)
        Me.M_HEADER_HAIKI_DATE.Name = "M_HEADER_HAIKI_DATE"
        Me.M_HEADER_HAIKI_DATE.ShowCheckBox = True
        Me.M_HEADER_HAIKI_DATE.Size = New System.Drawing.Size(133, 19)
        Me.M_HEADER_HAIKI_DATE.TabIndex = 24
        '
        'M_HEADER_HAIKI_FLG
        '
        Me.M_HEADER_HAIKI_FLG.AutoSize = True
        Me.M_HEADER_HAIKI_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_HEADER_HAIKI_FLG.Location = New System.Drawing.Point(505, 436)
        Me.M_HEADER_HAIKI_FLG.Name = "M_HEADER_HAIKI_FLG"
        Me.M_HEADER_HAIKI_FLG.Size = New System.Drawing.Size(48, 16)
        Me.M_HEADER_HAIKI_FLG.TabIndex = 21
        Me.M_HEADER_HAIKI_FLG.Text = "廃棄"
        Me.M_HEADER_HAIKI_FLG.UseVisualStyleBackColor = True
        '
        'lbl_HAIKI_DATE
        '
        Me.lbl_HAIKI_DATE.Location = New System.Drawing.Point(470, 454)
        Me.lbl_HAIKI_DATE.Name = "lbl_HAIKI_DATE"
        Me.lbl_HAIKI_DATE.Size = New System.Drawing.Size(66, 23)
        Me.lbl_HAIKI_DATE.TabIndex = 23
        Me.lbl_HAIKI_DATE.Text = "廃棄日"
        Me.lbl_HAIKI_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_UN_NO
        '
        Me.M_HEADER_UN_NO.Location = New System.Drawing.Point(67, 145)
        Me.M_HEADER_UN_NO.MaxLength = 50
        Me.M_HEADER_UN_NO.Name = "M_HEADER_UN_NO"
        Me.M_HEADER_UN_NO.Size = New System.Drawing.Size(394, 19)
        Me.M_HEADER_UN_NO.TabIndex = 7
        Me.M_HEADER_UN_NO.Visible = False
        '
        'lbl_UN_NO
        '
        Me.lbl_UN_NO.Location = New System.Drawing.Point(11, 144)
        Me.lbl_UN_NO.Name = "lbl_UN_NO"
        Me.lbl_UN_NO.Size = New System.Drawing.Size(52, 23)
        Me.lbl_UN_NO.TabIndex = 40
        Me.lbl_UN_NO.Text = "危険物"
        Me.lbl_UN_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbl_UN_NO.Visible = False
        '
        'M_HEADER_SPL
        '
        Me.M_HEADER_SPL.Location = New System.Drawing.Point(111, 122)
        Me.M_HEADER_SPL.MaxLength = 50
        Me.M_HEADER_SPL.Name = "M_HEADER_SPL"
        Me.M_HEADER_SPL.Size = New System.Drawing.Size(350, 19)
        Me.M_HEADER_SPL.TabIndex = 6
        '
        'lbl_SPL
        '
        Me.lbl_SPL.Location = New System.Drawing.Point(3, 118)
        Me.lbl_SPL.Name = "lbl_SPL"
        Me.lbl_SPL.Size = New System.Drawing.Size(102, 23)
        Me.lbl_SPL.TabIndex = 38
        Me.lbl_SPL.Text = "購入会社"
        Me.lbl_SPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_MFG
        '
        Me.M_HEADER_MFG.Location = New System.Drawing.Point(111, 99)
        Me.M_HEADER_MFG.MaxLength = 50
        Me.M_HEADER_MFG.Name = "M_HEADER_MFG"
        Me.M_HEADER_MFG.Size = New System.Drawing.Size(350, 19)
        Me.M_HEADER_MFG.TabIndex = 5
        '
        'lbl_MFG
        '
        Me.lbl_MFG.Location = New System.Drawing.Point(3, 97)
        Me.lbl_MFG.Name = "lbl_MFG"
        Me.lbl_MFG.Size = New System.Drawing.Size(102, 23)
        Me.lbl_MFG.TabIndex = 36
        Me.lbl_MFG.Text = "製造会社"
        Me.lbl_MFG.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_BUNRUI_01
        '
        Me.M_HEADER_BUNRUI_01.Location = New System.Drawing.Point(403, 7)
        Me.M_HEADER_BUNRUI_01.MaxLength = 20
        Me.M_HEADER_BUNRUI_01.Name = "M_HEADER_BUNRUI_01"
        Me.M_HEADER_BUNRUI_01.Size = New System.Drawing.Size(267, 19)
        Me.M_HEADER_BUNRUI_01.TabIndex = 1
        Me.M_HEADER_BUNRUI_01.Tag = "BUNRUI_01"
        '
        'lbl_BUNRUI_01
        '
        Me.lbl_BUNRUI_01.Location = New System.Drawing.Point(341, 7)
        Me.lbl_BUNRUI_01.Name = "lbl_BUNRUI_01"
        Me.lbl_BUNRUI_01.Size = New System.Drawing.Size(56, 19)
        Me.lbl_BUNRUI_01.TabIndex = 214
        Me.lbl_BUNRUI_01.Text = "分類番号"
        Me.lbl_BUNRUI_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_KANRI_NO
        '
        Me.M_HEADER_KANRI_NO.Location = New System.Drawing.Point(67, 7)
        Me.M_HEADER_KANRI_NO.MaxLength = 20
        Me.M_HEADER_KANRI_NO.Name = "M_HEADER_KANRI_NO"
        Me.M_HEADER_KANRI_NO.Size = New System.Drawing.Size(268, 19)
        Me.M_HEADER_KANRI_NO.TabIndex = 0
        Me.M_HEADER_KANRI_NO.Tag = "KANRI_NO"
        '
        'one_lbl_KANRI_NO
        '
        Me.one_lbl_KANRI_NO.Location = New System.Drawing.Point(6, 7)
        Me.one_lbl_KANRI_NO.Name = "one_lbl_KANRI_NO"
        Me.one_lbl_KANRI_NO.Size = New System.Drawing.Size(57, 19)
        Me.one_lbl_KANRI_NO.TabIndex = 295
        Me.one_lbl_KANRI_NO.Text = "管理番号"
        Me.one_lbl_KANRI_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_KUBUN_01
        '
        Me.M_HEADER_KUBUN_01.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_HEADER_KUBUN_01.FormattingEnabled = True
        Me.M_HEADER_KUBUN_01.Location = New System.Drawing.Point(538, 146)
        Me.M_HEADER_KUBUN_01.Name = "M_HEADER_KUBUN_01"
        Me.M_HEADER_KUBUN_01.Size = New System.Drawing.Size(134, 20)
        Me.M_HEADER_KUBUN_01.TabIndex = 10
        '
        'lbl_KUBUN_01
        '
        Me.lbl_KUBUN_01.Location = New System.Drawing.Point(476, 150)
        Me.lbl_KUBUN_01.Name = "lbl_KUBUN_01"
        Me.lbl_KUBUN_01.Size = New System.Drawing.Size(59, 16)
        Me.lbl_KUBUN_01.TabIndex = 42
        Me.lbl_KUBUN_01.Text = "設備区分"
        Me.lbl_KUBUN_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_MODEL
        '
        Me.M_HEADER_MODEL.Location = New System.Drawing.Point(67, 53)
        Me.M_HEADER_MODEL.MaxLength = 30
        Me.M_HEADER_MODEL.Name = "M_HEADER_MODEL"
        Me.M_HEADER_MODEL.Size = New System.Drawing.Size(603, 19)
        Me.M_HEADER_MODEL.TabIndex = 3
        '
        'one_lbl_MODEL
        '
        Me.one_lbl_MODEL.Location = New System.Drawing.Point(29, 52)
        Me.one_lbl_MODEL.Name = "one_lbl_MODEL"
        Me.one_lbl_MODEL.Size = New System.Drawing.Size(34, 23)
        Me.one_lbl_MODEL.TabIndex = 292
        Me.one_lbl_MODEL.Text = "型式"
        Me.one_lbl_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btn_FileClear
        '
        Me.btn_FileClear.BackColor = System.Drawing.SystemColors.Control
        Me.btn_FileClear.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_FileClear.Location = New System.Drawing.Point(346, 458)
        Me.btn_FileClear.Name = "btn_FileClear"
        Me.btn_FileClear.Size = New System.Drawing.Size(55, 21)
        Me.btn_FileClear.TabIndex = 22
        Me.btn_FileClear.Text = "クリア"
        Me.btn_FileClear.UseVisualStyleBackColor = False
        Me.btn_FileClear.Visible = False
        '
        'M_HEADER_REASON
        '
        Me.M_HEADER_REASON.Location = New System.Drawing.Point(65, 513)
        Me.M_HEADER_REASON.MaxLength = 1000
        Me.M_HEADER_REASON.Multiline = True
        Me.M_HEADER_REASON.Name = "M_HEADER_REASON"
        Me.M_HEADER_REASON.Size = New System.Drawing.Size(607, 32)
        Me.M_HEADER_REASON.TabIndex = 26
        '
        'lbl_REASON
        '
        Me.lbl_REASON.Location = New System.Drawing.Point(3, 513)
        Me.lbl_REASON.Name = "lbl_REASON"
        Me.lbl_REASON.Size = New System.Drawing.Size(58, 23)
        Me.lbl_REASON.TabIndex = 274
        Me.lbl_REASON.Text = "更新理由"
        Me.lbl_REASON.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_DOUTOUSEI
        '
        Me.M_HEADER_DOUTOUSEI.Location = New System.Drawing.Point(65, 368)
        Me.M_HEADER_DOUTOUSEI.MaxLength = 20
        Me.M_HEADER_DOUTOUSEI.Name = "M_HEADER_DOUTOUSEI"
        Me.M_HEADER_DOUTOUSEI.Size = New System.Drawing.Size(133, 19)
        Me.M_HEADER_DOUTOUSEI.TabIndex = 20
        Me.M_HEADER_DOUTOUSEI.Visible = False
        '
        'lbl_DOUTOUSEI
        '
        Me.lbl_DOUTOUSEI.Location = New System.Drawing.Point(-6, 366)
        Me.lbl_DOUTOUSEI.Name = "lbl_DOUTOUSEI"
        Me.lbl_DOUTOUSEI.Size = New System.Drawing.Size(72, 23)
        Me.lbl_DOUTOUSEI.TabIndex = 270
        Me.lbl_DOUTOUSEI.Text = "同等性確認"
        Me.lbl_DOUTOUSEI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbl_DOUTOUSEI.Visible = False
        '
        'one_lbl_STATUS
        '
        Me.one_lbl_STATUS.Location = New System.Drawing.Point(483, 229)
        Me.one_lbl_STATUS.Name = "one_lbl_STATUS"
        Me.one_lbl_STATUS.Size = New System.Drawing.Size(52, 23)
        Me.one_lbl_STATUS.TabIndex = 34
        Me.one_lbl_STATUS.Text = "STATUS"
        Me.one_lbl_STATUS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.one_lbl_STATUS.Visible = False
        '
        'M_HEADER_REFERENCE
        '
        Me.M_HEADER_REFERENCE.Location = New System.Drawing.Point(65, 393)
        Me.M_HEADER_REFERENCE.MaxLength = 100
        Me.M_HEADER_REFERENCE.Multiline = True
        Me.M_HEADER_REFERENCE.Name = "M_HEADER_REFERENCE"
        Me.M_HEADER_REFERENCE.Size = New System.Drawing.Size(414, 58)
        Me.M_HEADER_REFERENCE.TabIndex = 19
        Me.M_HEADER_REFERENCE.Visible = False
        '
        'btn_File
        '
        Me.btn_File.BackColor = System.Drawing.SystemColors.Control
        Me.btn_File.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_File.Location = New System.Drawing.Point(263, 458)
        Me.btn_File.Name = "btn_File"
        Me.btn_File.Size = New System.Drawing.Size(82, 21)
        Me.btn_File.TabIndex = 21
        Me.btn_File.Text = "ファイル選択"
        Me.btn_File.UseVisualStyleBackColor = False
        Me.btn_File.Visible = False
        '
        'lbl_Reference
        '
        Me.lbl_Reference.Location = New System.Drawing.Point(5, 389)
        Me.lbl_Reference.Name = "lbl_Reference"
        Me.lbl_Reference.Size = New System.Drawing.Size(58, 23)
        Me.lbl_Reference.TabIndex = 214
        Me.lbl_Reference.Text = "Reference"
        Me.lbl_Reference.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbl_Reference.Visible = False
        '
        'M_HEADER_CERTIFICATE_NAME
        '
        Me.M_HEADER_CERTIFICATE_NAME.Enabled = False
        Me.M_HEADER_CERTIFICATE_NAME.Location = New System.Drawing.Point(65, 459)
        Me.M_HEADER_CERTIFICATE_NAME.Name = "M_HEADER_CERTIFICATE_NAME"
        Me.M_HEADER_CERTIFICATE_NAME.Size = New System.Drawing.Size(196, 19)
        Me.M_HEADER_CERTIFICATE_NAME.TabIndex = 1
        Me.M_HEADER_CERTIFICATE_NAME.Visible = False
        '
        'M_HEADER_MFG_NO
        '
        Me.M_HEADER_MFG_NO.Location = New System.Drawing.Point(538, 124)
        Me.M_HEADER_MFG_NO.MaxLength = 100
        Me.M_HEADER_MFG_NO.Name = "M_HEADER_MFG_NO"
        Me.M_HEADER_MFG_NO.Size = New System.Drawing.Size(135, 19)
        Me.M_HEADER_MFG_NO.TabIndex = 9
        Me.M_HEADER_MFG_NO.Tag = "BUNRUI_02"
        '
        'one_lbl_MEISYOU
        '
        Me.one_lbl_MEISYOU.Location = New System.Drawing.Point(29, 29)
        Me.one_lbl_MEISYOU.Name = "one_lbl_MEISYOU"
        Me.one_lbl_MEISYOU.Size = New System.Drawing.Size(34, 23)
        Me.one_lbl_MEISYOU.TabIndex = 112
        Me.one_lbl_MEISYOU.Text = "名称"
        Me.one_lbl_MEISYOU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_MEISYOU
        '
        Me.M_HEADER_MEISYOU.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.M_HEADER_MEISYOU.Location = New System.Drawing.Point(67, 30)
        Me.M_HEADER_MEISYOU.MaxLength = 100
        Me.M_HEADER_MEISYOU.Name = "M_HEADER_MEISYOU"
        Me.M_HEADER_MEISYOU.Size = New System.Drawing.Size(603, 19)
        Me.M_HEADER_MEISYOU.TabIndex = 2
        Me.M_HEADER_MEISYOU.Tag = "MEISYOU"
        '
        'M_HEADER_MODIFIED_DATE
        '
        Me.M_HEADER_MODIFIED_DATE.Enabled = False
        Me.M_HEADER_MODIFIED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_HEADER_MODIFIED_DATE.Location = New System.Drawing.Point(587, 546)
        Me.M_HEADER_MODIFIED_DATE.Name = "M_HEADER_MODIFIED_DATE"
        Me.M_HEADER_MODIFIED_DATE.Size = New System.Drawing.Size(85, 19)
        Me.M_HEADER_MODIFIED_DATE.TabIndex = 259
        '
        'lbl_MODIFIED_DATE
        '
        Me.lbl_MODIFIED_DATE.Location = New System.Drawing.Point(532, 545)
        Me.lbl_MODIFIED_DATE.Name = "lbl_MODIFIED_DATE"
        Me.lbl_MODIFIED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.lbl_MODIFIED_DATE.TabIndex = 107
        Me.lbl_MODIFIED_DATE.Text = "更新日"
        Me.lbl_MODIFIED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_MODIFIED_BY
        '
        Me.M_HEADER_MODIFIED_BY.Enabled = False
        Me.M_HEADER_MODIFIED_BY.Location = New System.Drawing.Point(421, 547)
        Me.M_HEADER_MODIFIED_BY.Name = "M_HEADER_MODIFIED_BY"
        Me.M_HEADER_MODIFIED_BY.Size = New System.Drawing.Size(110, 19)
        Me.M_HEADER_MODIFIED_BY.TabIndex = 258
        '
        'lbl_MODIFIED_BY
        '
        Me.lbl_MODIFIED_BY.Location = New System.Drawing.Point(370, 546)
        Me.lbl_MODIFIED_BY.Name = "lbl_MODIFIED_BY"
        Me.lbl_MODIFIED_BY.Size = New System.Drawing.Size(43, 23)
        Me.lbl_MODIFIED_BY.TabIndex = 105
        Me.lbl_MODIFIED_BY.Text = "更新者"
        Me.lbl_MODIFIED_BY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_CREATED_DATE
        '
        Me.M_HEADER_CREATED_DATE.Enabled = False
        Me.M_HEADER_CREATED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_HEADER_CREATED_DATE.Location = New System.Drawing.Point(235, 547)
        Me.M_HEADER_CREATED_DATE.Name = "M_HEADER_CREATED_DATE"
        Me.M_HEADER_CREATED_DATE.Size = New System.Drawing.Size(85, 19)
        Me.M_HEADER_CREATED_DATE.TabIndex = 257
        '
        'lbl_CREATED_DATE
        '
        Me.lbl_CREATED_DATE.Location = New System.Drawing.Point(180, 546)
        Me.lbl_CREATED_DATE.Name = "lbl_CREATED_DATE"
        Me.lbl_CREATED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.lbl_CREATED_DATE.TabIndex = 104
        Me.lbl_CREATED_DATE.Text = "登録日"
        Me.lbl_CREATED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_CREATED_BY
        '
        Me.M_HEADER_CREATED_BY.Enabled = False
        Me.M_HEADER_CREATED_BY.Location = New System.Drawing.Point(65, 547)
        Me.M_HEADER_CREATED_BY.Name = "M_HEADER_CREATED_BY"
        Me.M_HEADER_CREATED_BY.Size = New System.Drawing.Size(110, 19)
        Me.M_HEADER_CREATED_BY.TabIndex = 256
        '
        'lbl_CREATED_BY
        '
        Me.lbl_CREATED_BY.Location = New System.Drawing.Point(9, 546)
        Me.lbl_CREATED_BY.Name = "lbl_CREATED_BY"
        Me.lbl_CREATED_BY.Size = New System.Drawing.Size(52, 23)
        Me.lbl_CREATED_BY.TabIndex = 103
        Me.lbl_CREATED_BY.Text = "登録者"
        Me.lbl_CREATED_BY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_MFG_NO
        '
        Me.lbl_MFG_NO.Location = New System.Drawing.Point(479, 123)
        Me.lbl_MFG_NO.Name = "lbl_MFG_NO"
        Me.lbl_MFG_NO.Size = New System.Drawing.Size(56, 23)
        Me.lbl_MFG_NO.TabIndex = 4
        Me.lbl_MFG_NO.Text = "製造番号"
        Me.lbl_MFG_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btn_ImageClear
        '
        Me.btn_ImageClear.BackColor = System.Drawing.SystemColors.Control
        Me.btn_ImageClear.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_ImageClear.Location = New System.Drawing.Point(608, 260)
        Me.btn_ImageClear.Name = "btn_ImageClear"
        Me.btn_ImageClear.Size = New System.Drawing.Size(65, 21)
        Me.btn_ImageClear.TabIndex = 18
        Me.btn_ImageClear.Text = "クリア"
        Me.btn_ImageClear.UseVisualStyleBackColor = False
        '
        'btn_ImageSel
        '
        Me.btn_ImageSel.BackColor = System.Drawing.SystemColors.Control
        Me.btn_ImageSel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_ImageSel.Location = New System.Drawing.Point(538, 260)
        Me.btn_ImageSel.Name = "btn_ImageSel"
        Me.btn_ImageSel.Size = New System.Drawing.Size(65, 21)
        Me.btn_ImageSel.TabIndex = 17
        Me.btn_ImageSel.Text = "選択"
        Me.btn_ImageSel.UseVisualStyleBackColor = False
        '
        'lbl_IMAGE
        '
        Me.lbl_IMAGE.Location = New System.Drawing.Point(491, 260)
        Me.lbl_IMAGE.Name = "lbl_IMAGE"
        Me.lbl_IMAGE.Size = New System.Drawing.Size(44, 23)
        Me.lbl_IMAGE.TabIndex = 82
        Me.lbl_IMAGE.Text = "写真"
        Me.lbl_IMAGE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_REMARKS
        '
        Me.M_HEADER_REMARKS.Location = New System.Drawing.Point(65, 480)
        Me.M_HEADER_REMARKS.MaxLength = 500
        Me.M_HEADER_REMARKS.Multiline = True
        Me.M_HEADER_REMARKS.Name = "M_HEADER_REMARKS"
        Me.M_HEADER_REMARKS.Size = New System.Drawing.Size(607, 32)
        Me.M_HEADER_REMARKS.TabIndex = 25
        '
        'lbl_Certificate
        '
        Me.lbl_Certificate.Location = New System.Drawing.Point(0, 457)
        Me.lbl_Certificate.Name = "lbl_Certificate"
        Me.lbl_Certificate.Size = New System.Drawing.Size(61, 23)
        Me.lbl_Certificate.TabIndex = 80
        Me.lbl_Certificate.Text = "Certificate"
        Me.lbl_Certificate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbl_Certificate.Visible = False
        '
        'lbl_REMARKS
        '
        Me.lbl_REMARKS.Location = New System.Drawing.Point(15, 477)
        Me.lbl_REMARKS.Name = "lbl_REMARKS"
        Me.lbl_REMARKS.Size = New System.Drawing.Size(46, 23)
        Me.lbl_REMARKS.TabIndex = 80
        Me.lbl_REMARKS.Text = "備考"
        Me.lbl_REMARKS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_LOCATION
        '
        Me.M_HEADER_LOCATION.Location = New System.Drawing.Point(539, 208)
        Me.M_HEADER_LOCATION.MaxLength = 20
        Me.M_HEADER_LOCATION.Name = "M_HEADER_LOCATION"
        Me.M_HEADER_LOCATION.Size = New System.Drawing.Size(133, 19)
        Me.M_HEADER_LOCATION.TabIndex = 14
        '
        'lbl_LOCATION
        '
        Me.lbl_LOCATION.Location = New System.Drawing.Point(460, 206)
        Me.lbl_LOCATION.Name = "lbl_LOCATION"
        Me.lbl_LOCATION.Size = New System.Drawing.Size(75, 23)
        Me.lbl_LOCATION.TabIndex = 48
        Me.lbl_LOCATION.Text = "ロケーション"
        Me.lbl_LOCATION.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_BASE
        '
        Me.M_HEADER_BASE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_HEADER_BASE.FormattingEnabled = True
        Me.M_HEADER_BASE.Location = New System.Drawing.Point(539, 185)
        Me.M_HEADER_BASE.Name = "M_HEADER_BASE"
        Me.M_HEADER_BASE.Size = New System.Drawing.Size(133, 20)
        Me.M_HEADER_BASE.TabIndex = 13
        '
        'lbl_BASE
        '
        Me.lbl_BASE.Location = New System.Drawing.Point(477, 184)
        Me.lbl_BASE.Name = "lbl_BASE"
        Me.lbl_BASE.Size = New System.Drawing.Size(58, 23)
        Me.lbl_BASE.TabIndex = 46
        Me.lbl_BASE.Text = "配置基地"
        Me.lbl_BASE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_KIKAKU
        '
        Me.M_HEADER_KIKAKU.Location = New System.Drawing.Point(67, 76)
        Me.M_HEADER_KIKAKU.MaxLength = 50
        Me.M_HEADER_KIKAKU.Name = "M_HEADER_KIKAKU"
        Me.M_HEADER_KIKAKU.Size = New System.Drawing.Size(603, 19)
        Me.M_HEADER_KIKAKU.TabIndex = 4
        '
        'lbl_KIKAKU
        '
        Me.lbl_KIKAKU.Location = New System.Drawing.Point(11, 78)
        Me.lbl_KIKAKU.Name = "lbl_KIKAKU"
        Me.lbl_KIKAKU.Size = New System.Drawing.Size(52, 16)
        Me.lbl_KIKAKU.TabIndex = 32
        Me.lbl_KIKAKU.Text = "規格"
        Me.lbl_KIKAKU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HEADER_HAICHI_KIJYUN
        '
        Me.M_HEADER_HAICHI_KIJYUN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_HEADER_HAICHI_KIJYUN.FormattingEnabled = True
        Me.M_HEADER_HAICHI_KIJYUN.Location = New System.Drawing.Point(538, 99)
        Me.M_HEADER_HAICHI_KIJYUN.Name = "M_HEADER_HAICHI_KIJYUN"
        Me.M_HEADER_HAICHI_KIJYUN.Size = New System.Drawing.Size(133, 20)
        Me.M_HEADER_HAICHI_KIJYUN.TabIndex = 8
        Me.M_HEADER_HAICHI_KIJYUN.Tag = "HAICHI_KIJYUN"
        Me.M_HEADER_HAICHI_KIJYUN.Visible = False
        '
        'lbl_HAICHI_KIJYUN
        '
        Me.lbl_HAICHI_KIJYUN.Location = New System.Drawing.Point(479, 103)
        Me.lbl_HAICHI_KIJYUN.Name = "lbl_HAICHI_KIJYUN"
        Me.lbl_HAICHI_KIJYUN.Size = New System.Drawing.Size(56, 16)
        Me.lbl_HAICHI_KIJYUN.TabIndex = 6
        Me.lbl_HAICHI_KIJYUN.Text = "配置基準"
        Me.lbl_HAICHI_KIJYUN.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lbl_HAICHI_KIJYUN.Visible = False
        '
        'gpx_HOYU_GROUP
        '
        Me.gpx_HOYU_GROUP.Controls.Add(Me.M_HEADER_RENTAL_FLG)
        Me.gpx_HOYU_GROUP.Controls.Add(Me.M_HEADER_HOYU_FLG)
        Me.gpx_HOYU_GROUP.Location = New System.Drawing.Point(5, 203)
        Me.gpx_HOYU_GROUP.Name = "gpx_HOYU_GROUP"
        Me.gpx_HOYU_GROUP.Size = New System.Drawing.Size(193, 41)
        Me.gpx_HOYU_GROUP.TabIndex = 11
        Me.gpx_HOYU_GROUP.TabStop = False
        Me.gpx_HOYU_GROUP.Text = "保有区分"
        Me.gpx_HOYU_GROUP.Visible = False
        '
        'M_HEADER_RENTAL_FLG
        '
        Me.M_HEADER_RENTAL_FLG.AutoSize = True
        Me.M_HEADER_RENTAL_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_HEADER_RENTAL_FLG.Location = New System.Drawing.Point(86, 18)
        Me.M_HEADER_RENTAL_FLG.Name = "M_HEADER_RENTAL_FLG"
        Me.M_HEADER_RENTAL_FLG.Size = New System.Drawing.Size(48, 16)
        Me.M_HEADER_RENTAL_FLG.TabIndex = 1
        Me.M_HEADER_RENTAL_FLG.Text = "借用"
        Me.M_HEADER_RENTAL_FLG.UseVisualStyleBackColor = True
        '
        'M_HEADER_HOYU_FLG
        '
        Me.M_HEADER_HOYU_FLG.AutoSize = True
        Me.M_HEADER_HOYU_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_HEADER_HOYU_FLG.Checked = True
        Me.M_HEADER_HOYU_FLG.CheckState = System.Windows.Forms.CheckState.Checked
        Me.M_HEADER_HOYU_FLG.Location = New System.Drawing.Point(19, 18)
        Me.M_HEADER_HOYU_FLG.Name = "M_HEADER_HOYU_FLG"
        Me.M_HEADER_HOYU_FLG.Size = New System.Drawing.Size(48, 16)
        Me.M_HEADER_HOYU_FLG.TabIndex = 0
        Me.M_HEADER_HOYU_FLG.Text = "保有"
        Me.M_HEADER_HOYU_FLG.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.M_HEADER_IMAGE)
        Me.GroupBox1.Location = New System.Drawing.Point(507, 298)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(168, 126)
        Me.GroupBox1.TabIndex = 286
        Me.GroupBox1.TabStop = False
        '
        'M_HEADER_IMAGE
        '
        Me.M_HEADER_IMAGE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.M_HEADER_IMAGE.Location = New System.Drawing.Point(2, 9)
        Me.M_HEADER_IMAGE.Name = "M_HEADER_IMAGE"
        Me.M_HEADER_IMAGE.Size = New System.Drawing.Size(163, 115)
        Me.M_HEADER_IMAGE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.M_HEADER_IMAGE.TabIndex = 84
        Me.M_HEADER_IMAGE.TabStop = False
        '
        'gpx_RENTAL_GROUP
        '
        Me.gpx_RENTAL_GROUP.BackColor = System.Drawing.Color.Transparent
        Me.gpx_RENTAL_GROUP.Controls.Add(Me.one_ref_M_RENTAL_TEKIGOUSEI)
        Me.gpx_RENTAL_GROUP.Controls.Add(Me.lbl_TEKIGOUSEI)
        Me.gpx_RENTAL_GROUP.Controls.Add(Me.one_ref_M_RENTAL_NOURYOKU)
        Me.gpx_RENTAL_GROUP.Controls.Add(Me.lbl_NOURYOKU)
        Me.gpx_RENTAL_GROUP.Controls.Add(Me.lbl_RENTAL_CONPANY)
        Me.gpx_RENTAL_GROUP.Controls.Add(Me.one_ref_M_RENTAL_LEASE_CONPANY)
        Me.gpx_RENTAL_GROUP.Location = New System.Drawing.Point(212, 252)
        Me.gpx_RENTAL_GROUP.Name = "gpx_RENTAL_GROUP"
        Me.gpx_RENTAL_GROUP.Size = New System.Drawing.Size(247, 111)
        Me.gpx_RENTAL_GROUP.TabIndex = 17
        Me.gpx_RENTAL_GROUP.TabStop = False
        Me.gpx_RENTAL_GROUP.Text = "借用情報"
        Me.gpx_RENTAL_GROUP.Visible = False
        '
        'one_ref_M_RENTAL_TEKIGOUSEI
        '
        Me.one_ref_M_RENTAL_TEKIGOUSEI.Enabled = False
        Me.one_ref_M_RENTAL_TEKIGOUSEI.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.one_ref_M_RENTAL_TEKIGOUSEI.Location = New System.Drawing.Point(90, 76)
        Me.one_ref_M_RENTAL_TEKIGOUSEI.Name = "one_ref_M_RENTAL_TEKIGOUSEI"
        Me.one_ref_M_RENTAL_TEKIGOUSEI.Size = New System.Drawing.Size(133, 19)
        Me.one_ref_M_RENTAL_TEKIGOUSEI.TabIndex = 249
        '
        'lbl_TEKIGOUSEI
        '
        Me.lbl_TEKIGOUSEI.Location = New System.Drawing.Point(21, 72)
        Me.lbl_TEKIGOUSEI.Name = "lbl_TEKIGOUSEI"
        Me.lbl_TEKIGOUSEI.Size = New System.Drawing.Size(66, 23)
        Me.lbl_TEKIGOUSEI.TabIndex = 269
        Me.lbl_TEKIGOUSEI.Text = "適合性確認"
        Me.lbl_TEKIGOUSEI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'one_ref_M_RENTAL_NOURYOKU
        '
        Me.one_ref_M_RENTAL_NOURYOKU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.one_ref_M_RENTAL_NOURYOKU.Enabled = False
        Me.one_ref_M_RENTAL_NOURYOKU.FormattingEnabled = True
        Me.one_ref_M_RENTAL_NOURYOKU.Location = New System.Drawing.Point(90, 45)
        Me.one_ref_M_RENTAL_NOURYOKU.Name = "one_ref_M_RENTAL_NOURYOKU"
        Me.one_ref_M_RENTAL_NOURYOKU.Size = New System.Drawing.Size(133, 20)
        Me.one_ref_M_RENTAL_NOURYOKU.TabIndex = 247
        '
        'lbl_NOURYOKU
        '
        Me.lbl_NOURYOKU.Location = New System.Drawing.Point(30, 44)
        Me.lbl_NOURYOKU.Name = "lbl_NOURYOKU"
        Me.lbl_NOURYOKU.Size = New System.Drawing.Size(58, 23)
        Me.lbl_NOURYOKU.TabIndex = 268
        Me.lbl_NOURYOKU.Text = "能力審査"
        Me.lbl_NOURYOKU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_RENTAL_CONPANY
        '
        Me.lbl_RENTAL_CONPANY.Location = New System.Drawing.Point(37, 16)
        Me.lbl_RENTAL_CONPANY.Name = "lbl_RENTAL_CONPANY"
        Me.lbl_RENTAL_CONPANY.Size = New System.Drawing.Size(51, 23)
        Me.lbl_RENTAL_CONPANY.TabIndex = 61
        Me.lbl_RENTAL_CONPANY.Text = "借用先"
        Me.lbl_RENTAL_CONPANY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'one_ref_M_RENTAL_LEASE_CONPANY
        '
        Me.one_ref_M_RENTAL_LEASE_CONPANY.Enabled = False
        Me.one_ref_M_RENTAL_LEASE_CONPANY.Location = New System.Drawing.Point(90, 17)
        Me.one_ref_M_RENTAL_LEASE_CONPANY.MaxLength = 20
        Me.one_ref_M_RENTAL_LEASE_CONPANY.Name = "one_ref_M_RENTAL_LEASE_CONPANY"
        Me.one_ref_M_RENTAL_LEASE_CONPANY.Size = New System.Drawing.Size(133, 19)
        Me.one_ref_M_RENTAL_LEASE_CONPANY.TabIndex = 230
        '
        'gpx_CATEGORY_GROUP
        '
        Me.gpx_CATEGORY_GROUP.BackColor = System.Drawing.Color.Transparent
        Me.gpx_CATEGORY_GROUP.Controls.Add(Me.M_HEADER_EQUIPMENT_FLG)
        Me.gpx_CATEGORY_GROUP.Controls.Add(Me.M_HEADER_HOZEN_FLG)
        Me.gpx_CATEGORY_GROUP.Controls.Add(Me.M_HEADER_KOUSEI_FLG)
        Me.gpx_CATEGORY_GROUP.Controls.Add(Me.M_HEADER_NOT_KOUSEI_FLG)
        Me.gpx_CATEGORY_GROUP.Location = New System.Drawing.Point(5, 251)
        Me.gpx_CATEGORY_GROUP.Name = "gpx_CATEGORY_GROUP"
        Me.gpx_CATEGORY_GROUP.Size = New System.Drawing.Size(193, 112)
        Me.gpx_CATEGORY_GROUP.TabIndex = 16
        Me.gpx_CATEGORY_GROUP.TabStop = False
        Me.gpx_CATEGORY_GROUP.Text = "分類"
        Me.gpx_CATEGORY_GROUP.Visible = False
        '
        'M_HEADER_EQUIPMENT_FLG
        '
        Me.M_HEADER_EQUIPMENT_FLG.AutoSize = True
        Me.M_HEADER_EQUIPMENT_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_HEADER_EQUIPMENT_FLG.Location = New System.Drawing.Point(92, 17)
        Me.M_HEADER_EQUIPMENT_FLG.Name = "M_HEADER_EQUIPMENT_FLG"
        Me.M_HEADER_EQUIPMENT_FLG.Size = New System.Drawing.Size(82, 16)
        Me.M_HEADER_EQUIPMENT_FLG.TabIndex = 0
        Me.M_HEADER_EQUIPMENT_FLG.Text = "工具・ 器具"
        Me.M_HEADER_EQUIPMENT_FLG.UseVisualStyleBackColor = True
        '
        'M_HEADER_HOZEN_FLG
        '
        Me.M_HEADER_HOZEN_FLG.AutoSize = True
        Me.M_HEADER_HOZEN_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_HEADER_HOZEN_FLG.Location = New System.Drawing.Point(102, 40)
        Me.M_HEADER_HOZEN_FLG.Name = "M_HEADER_HOZEN_FLG"
        Me.M_HEADER_HOZEN_FLG.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.M_HEADER_HOZEN_FLG.Size = New System.Drawing.Size(72, 16)
        Me.M_HEADER_HOZEN_FLG.TabIndex = 1
        Me.M_HEADER_HOZEN_FLG.Text = "保全設備"
        Me.M_HEADER_HOZEN_FLG.UseVisualStyleBackColor = True
        '
        'M_HEADER_KOUSEI_FLG
        '
        Me.M_HEADER_KOUSEI_FLG.AutoSize = True
        Me.M_HEADER_KOUSEI_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_HEADER_KOUSEI_FLG.Location = New System.Drawing.Point(66, 64)
        Me.M_HEADER_KOUSEI_FLG.Name = "M_HEADER_KOUSEI_FLG"
        Me.M_HEADER_KOUSEI_FLG.Size = New System.Drawing.Size(108, 16)
        Me.M_HEADER_KOUSEI_FLG.TabIndex = 2
        Me.M_HEADER_KOUSEI_FLG.Text = "定期校正計測器"
        Me.M_HEADER_KOUSEI_FLG.UseVisualStyleBackColor = True
        '
        'M_HEADER_NOT_KOUSEI_FLG
        '
        Me.M_HEADER_NOT_KOUSEI_FLG.AutoSize = True
        Me.M_HEADER_NOT_KOUSEI_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_HEADER_NOT_KOUSEI_FLG.Location = New System.Drawing.Point(6, 86)
        Me.M_HEADER_NOT_KOUSEI_FLG.Name = "M_HEADER_NOT_KOUSEI_FLG"
        Me.M_HEADER_NOT_KOUSEI_FLG.Size = New System.Drawing.Size(168, 16)
        Me.M_HEADER_NOT_KOUSEI_FLG.TabIndex = 3
        Me.M_HEADER_NOT_KOUSEI_FLG.Text = "定期校正管理対象外計測器"
        Me.M_HEADER_NOT_KOUSEI_FLG.UseVisualStyleBackColor = True
        Me.M_HEADER_NOT_KOUSEI_FLG.Visible = False
        '
        'lbl_M_TOOL
        '
        Me.lbl_M_TOOL.BackColor = System.Drawing.SystemColors.Control
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_PARENT_NO)
        Me.lbl_M_TOOL.Controls.Add(Me.Label1)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_KANRI_NO)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_KANRI_NO)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_MODEL)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_MODEL)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_BUNRUI_01)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_BUNRUI_01)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_MEISYOU)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_MEISYOU)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_UN_NO)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_UN_NO)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_MFG)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_MFG)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_KIKAKU)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_KIKAKU)
        Me.lbl_M_TOOL.Controls.Add(Me.GroupBox2)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_CREATED_DATE)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_CREATED_DATE)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_ATA)
        Me.lbl_M_TOOL.Controls.Add(Me.M_TOOL_ATA)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_MFG_NO)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_MFG_NO)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_IMAGE)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_LOCATION)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_LOCATION)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_BASE)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_BASE)
        Me.lbl_M_TOOL.Controls.Add(Me.two_ref_M_HEADER_HAICHI_KIJYUN)
        Me.lbl_M_TOOL.Controls.Add(Me.two_lbl_HAICHI_KIJYUN)
        Me.lbl_M_TOOL.Controls.Add(Me.M_TOOL_ALLOW_FLG)
        Me.lbl_M_TOOL.Controls.Add(Me.M_TOOL_STATUS)
        Me.lbl_M_TOOL.Controls.Add(Me.lbl_STATUS)
        Me.lbl_M_TOOL.Location = New System.Drawing.Point(4, 22)
        Me.lbl_M_TOOL.Name = "lbl_M_TOOL"
        Me.lbl_M_TOOL.Padding = New System.Windows.Forms.Padding(3)
        Me.lbl_M_TOOL.Size = New System.Drawing.Size(703, 576)
        Me.lbl_M_TOOL.TabIndex = 1
        Me.lbl_M_TOOL.Tag = "M_TOOL"
        Me.lbl_M_TOOL.Text = "工具・器具"
        '
        'two_ref_M_HEADER_PARENT_NO
        '
        Me.two_ref_M_HEADER_PARENT_NO.Enabled = False
        Me.two_ref_M_HEADER_PARENT_NO.Location = New System.Drawing.Point(67, 151)
        Me.two_ref_M_HEADER_PARENT_NO.MaxLength = 20
        Me.two_ref_M_HEADER_PARENT_NO.Name = "two_ref_M_HEADER_PARENT_NO"
        Me.two_ref_M_HEADER_PARENT_NO.Size = New System.Drawing.Size(381, 19)
        Me.two_ref_M_HEADER_PARENT_NO.TabIndex = 1228
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(7, 153)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 23)
        Me.Label1.TabIndex = 1229
        Me.Label1.Text = "親番号"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_ref_M_HEADER_KANRI_NO
        '
        Me.two_ref_M_HEADER_KANRI_NO.Enabled = False
        Me.two_ref_M_HEADER_KANRI_NO.Location = New System.Drawing.Point(70, 11)
        Me.two_ref_M_HEADER_KANRI_NO.MaxLength = 100
        Me.two_ref_M_HEADER_KANRI_NO.Name = "two_ref_M_HEADER_KANRI_NO"
        Me.two_ref_M_HEADER_KANRI_NO.Size = New System.Drawing.Size(268, 19)
        Me.two_ref_M_HEADER_KANRI_NO.TabIndex = 1226
        Me.two_ref_M_HEADER_KANRI_NO.Tag = "KANRI_NO"
        '
        'two_lbl_KANRI_NO
        '
        Me.two_lbl_KANRI_NO.Location = New System.Drawing.Point(11, 12)
        Me.two_lbl_KANRI_NO.Name = "two_lbl_KANRI_NO"
        Me.two_lbl_KANRI_NO.Size = New System.Drawing.Size(57, 16)
        Me.two_lbl_KANRI_NO.TabIndex = 1227
        Me.two_lbl_KANRI_NO.Text = "管理番号"
        Me.two_lbl_KANRI_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_ref_M_HEADER_MODEL
        '
        Me.two_ref_M_HEADER_MODEL.Enabled = False
        Me.two_ref_M_HEADER_MODEL.Location = New System.Drawing.Point(69, 57)
        Me.two_ref_M_HEADER_MODEL.MaxLength = 30
        Me.two_ref_M_HEADER_MODEL.Name = "two_ref_M_HEADER_MODEL"
        Me.two_ref_M_HEADER_MODEL.Size = New System.Drawing.Size(607, 19)
        Me.two_ref_M_HEADER_MODEL.TabIndex = 1225
        '
        'two_lbl_MODEL
        '
        Me.two_lbl_MODEL.Location = New System.Drawing.Point(32, 56)
        Me.two_lbl_MODEL.Name = "two_lbl_MODEL"
        Me.two_lbl_MODEL.Size = New System.Drawing.Size(34, 23)
        Me.two_lbl_MODEL.TabIndex = 1224
        Me.two_lbl_MODEL.Text = "型式"
        Me.two_lbl_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_lbl_BUNRUI_01
        '
        Me.two_lbl_BUNRUI_01.Location = New System.Drawing.Point(344, 12)
        Me.two_lbl_BUNRUI_01.Name = "two_lbl_BUNRUI_01"
        Me.two_lbl_BUNRUI_01.Size = New System.Drawing.Size(59, 16)
        Me.two_lbl_BUNRUI_01.TabIndex = 1200
        Me.two_lbl_BUNRUI_01.Text = "分類番号"
        Me.two_lbl_BUNRUI_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_ref_M_HEADER_BUNRUI_01
        '
        Me.two_ref_M_HEADER_BUNRUI_01.Enabled = False
        Me.two_ref_M_HEADER_BUNRUI_01.Location = New System.Drawing.Point(407, 11)
        Me.two_ref_M_HEADER_BUNRUI_01.MaxLength = 20
        Me.two_ref_M_HEADER_BUNRUI_01.Name = "two_ref_M_HEADER_BUNRUI_01"
        Me.two_ref_M_HEADER_BUNRUI_01.Size = New System.Drawing.Size(269, 19)
        Me.two_ref_M_HEADER_BUNRUI_01.TabIndex = 1199
        Me.two_ref_M_HEADER_BUNRUI_01.Tag = "BUNRUI_01"
        '
        'two_lbl_MEISYOU
        '
        Me.two_lbl_MEISYOU.Location = New System.Drawing.Point(33, 34)
        Me.two_lbl_MEISYOU.Name = "two_lbl_MEISYOU"
        Me.two_lbl_MEISYOU.Size = New System.Drawing.Size(34, 23)
        Me.two_lbl_MEISYOU.TabIndex = 1208
        Me.two_lbl_MEISYOU.Text = "名称"
        Me.two_lbl_MEISYOU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_ref_M_HEADER_MEISYOU
        '
        Me.two_ref_M_HEADER_MEISYOU.Enabled = False
        Me.two_ref_M_HEADER_MEISYOU.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.two_ref_M_HEADER_MEISYOU.Location = New System.Drawing.Point(69, 34)
        Me.two_ref_M_HEADER_MEISYOU.MaxLength = 100
        Me.two_ref_M_HEADER_MEISYOU.Name = "two_ref_M_HEADER_MEISYOU"
        Me.two_ref_M_HEADER_MEISYOU.Size = New System.Drawing.Size(607, 19)
        Me.two_ref_M_HEADER_MEISYOU.TabIndex = 1209
        Me.two_ref_M_HEADER_MEISYOU.Tag = "MEISYOU"
        '
        'two_ref_M_HEADER_UN_NO
        '
        Me.two_ref_M_HEADER_UN_NO.Enabled = False
        Me.two_ref_M_HEADER_UN_NO.Location = New System.Drawing.Point(68, 126)
        Me.two_ref_M_HEADER_UN_NO.MaxLength = 50
        Me.two_ref_M_HEADER_UN_NO.Name = "two_ref_M_HEADER_UN_NO"
        Me.two_ref_M_HEADER_UN_NO.Size = New System.Drawing.Size(379, 19)
        Me.two_ref_M_HEADER_UN_NO.TabIndex = 1214
        '
        'two_lbl_UN_NO
        '
        Me.two_lbl_UN_NO.Location = New System.Drawing.Point(5, 126)
        Me.two_lbl_UN_NO.Name = "two_lbl_UN_NO"
        Me.two_lbl_UN_NO.Size = New System.Drawing.Size(62, 23)
        Me.two_lbl_UN_NO.TabIndex = 1205
        Me.two_lbl_UN_NO.Text = "危険物"
        Me.two_lbl_UN_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_ref_M_HEADER_MFG
        '
        Me.two_ref_M_HEADER_MFG.Enabled = False
        Me.two_ref_M_HEADER_MFG.Location = New System.Drawing.Point(69, 103)
        Me.two_ref_M_HEADER_MFG.MaxLength = 50
        Me.two_ref_M_HEADER_MFG.Name = "two_ref_M_HEADER_MFG"
        Me.two_ref_M_HEADER_MFG.Size = New System.Drawing.Size(379, 19)
        Me.two_ref_M_HEADER_MFG.TabIndex = 1213
        '
        'two_lbl_MFG
        '
        Me.two_lbl_MFG.Location = New System.Drawing.Point(5, 103)
        Me.two_lbl_MFG.Name = "two_lbl_MFG"
        Me.two_lbl_MFG.Size = New System.Drawing.Size(62, 23)
        Me.two_lbl_MFG.TabIndex = 1204
        Me.two_lbl_MFG.Text = "製造会社"
        Me.two_lbl_MFG.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_ref_M_HEADER_KIKAKU
        '
        Me.two_ref_M_HEADER_KIKAKU.Enabled = False
        Me.two_ref_M_HEADER_KIKAKU.Location = New System.Drawing.Point(69, 80)
        Me.two_ref_M_HEADER_KIKAKU.MaxLength = 100
        Me.two_ref_M_HEADER_KIKAKU.Name = "two_ref_M_HEADER_KIKAKU"
        Me.two_ref_M_HEADER_KIKAKU.Size = New System.Drawing.Size(607, 19)
        Me.two_ref_M_HEADER_KIKAKU.TabIndex = 1210
        '
        'two_lbl_KIKAKU
        '
        Me.two_lbl_KIKAKU.Location = New System.Drawing.Point(16, 82)
        Me.two_lbl_KIKAKU.Name = "two_lbl_KIKAKU"
        Me.two_lbl_KIKAKU.Size = New System.Drawing.Size(52, 16)
        Me.two_lbl_KIKAKU.TabIndex = 1203
        Me.two_lbl_KIKAKU.Text = "規格"
        Me.two_lbl_KIKAKU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.two_ref_M_HEADER_IMAGE)
        Me.GroupBox2.Location = New System.Drawing.Point(507, 212)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(168, 126)
        Me.GroupBox2.TabIndex = 1157
        Me.GroupBox2.TabStop = False
        '
        'two_ref_M_HEADER_IMAGE
        '
        Me.two_ref_M_HEADER_IMAGE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.two_ref_M_HEADER_IMAGE.Location = New System.Drawing.Point(2, 9)
        Me.two_ref_M_HEADER_IMAGE.Name = "two_ref_M_HEADER_IMAGE"
        Me.two_ref_M_HEADER_IMAGE.Size = New System.Drawing.Size(163, 115)
        Me.two_ref_M_HEADER_IMAGE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.two_ref_M_HEADER_IMAGE.TabIndex = 84
        Me.two_ref_M_HEADER_IMAGE.TabStop = False
        '
        'two_ref_M_HEADER_CREATED_DATE
        '
        Me.two_ref_M_HEADER_CREATED_DATE.Enabled = False
        Me.two_ref_M_HEADER_CREATED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.two_ref_M_HEADER_CREATED_DATE.Location = New System.Drawing.Point(542, 177)
        Me.two_ref_M_HEADER_CREATED_DATE.Name = "two_ref_M_HEADER_CREATED_DATE"
        Me.two_ref_M_HEADER_CREATED_DATE.Size = New System.Drawing.Size(133, 19)
        Me.two_ref_M_HEADER_CREATED_DATE.TabIndex = 1156
        '
        'two_lbl_CREATED_DATE
        '
        Me.two_lbl_CREATED_DATE.Location = New System.Drawing.Point(481, 175)
        Me.two_lbl_CREATED_DATE.Name = "two_lbl_CREATED_DATE"
        Me.two_lbl_CREATED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.two_lbl_CREATED_DATE.TabIndex = 1155
        Me.two_lbl_CREATED_DATE.Text = "登録日"
        Me.two_lbl_CREATED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_lbl_ATA
        '
        Me.two_lbl_ATA.Location = New System.Drawing.Point(27, 196)
        Me.two_lbl_ATA.Name = "two_lbl_ATA"
        Me.two_lbl_ATA.Size = New System.Drawing.Size(35, 23)
        Me.two_lbl_ATA.TabIndex = 0
        Me.two_lbl_ATA.Text = "ATA"
        Me.two_lbl_ATA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_TOOL_ATA
        '
        Me.M_TOOL_ATA.Location = New System.Drawing.Point(67, 196)
        Me.M_TOOL_ATA.MaxLength = 2
        Me.M_TOOL_ATA.Name = "M_TOOL_ATA"
        Me.M_TOOL_ATA.Size = New System.Drawing.Size(45, 19)
        Me.M_TOOL_ATA.TabIndex = 0
        Me.M_TOOL_ATA.Tag = "NUMBER"
        Me.M_TOOL_ATA.Text = "0"
        Me.M_TOOL_ATA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'two_ref_M_HEADER_MFG_NO
        '
        Me.two_ref_M_HEADER_MFG_NO.Enabled = False
        Me.two_ref_M_HEADER_MFG_NO.Location = New System.Drawing.Point(542, 103)
        Me.two_ref_M_HEADER_MFG_NO.MaxLength = 100
        Me.two_ref_M_HEADER_MFG_NO.Name = "two_ref_M_HEADER_MFG_NO"
        Me.two_ref_M_HEADER_MFG_NO.Size = New System.Drawing.Size(133, 19)
        Me.two_ref_M_HEADER_MFG_NO.TabIndex = 1049
        Me.two_ref_M_HEADER_MFG_NO.Tag = "BUNRUI_02"
        '
        'two_lbl_MFG_NO
        '
        Me.two_lbl_MFG_NO.Location = New System.Drawing.Point(476, 101)
        Me.two_lbl_MFG_NO.Name = "two_lbl_MFG_NO"
        Me.two_lbl_MFG_NO.Size = New System.Drawing.Size(56, 23)
        Me.two_lbl_MFG_NO.TabIndex = 1034
        Me.two_lbl_MFG_NO.Text = "製造番号"
        Me.two_lbl_MFG_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_lbl_IMAGE
        '
        Me.two_lbl_IMAGE.Location = New System.Drawing.Point(488, 198)
        Me.two_lbl_IMAGE.Name = "two_lbl_IMAGE"
        Me.two_lbl_IMAGE.Size = New System.Drawing.Size(44, 17)
        Me.two_lbl_IMAGE.TabIndex = 1043
        Me.two_lbl_IMAGE.Text = "写真"
        Me.two_lbl_IMAGE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_ref_M_HEADER_LOCATION
        '
        Me.two_ref_M_HEADER_LOCATION.Enabled = False
        Me.two_ref_M_HEADER_LOCATION.Location = New System.Drawing.Point(315, 196)
        Me.two_ref_M_HEADER_LOCATION.MaxLength = 20
        Me.two_ref_M_HEADER_LOCATION.Name = "two_ref_M_HEADER_LOCATION"
        Me.two_ref_M_HEADER_LOCATION.Size = New System.Drawing.Size(133, 19)
        Me.two_ref_M_HEADER_LOCATION.TabIndex = 1055
        '
        'two_lbl_LOCATION
        '
        Me.two_lbl_LOCATION.Location = New System.Drawing.Point(222, 200)
        Me.two_lbl_LOCATION.Name = "two_lbl_LOCATION"
        Me.two_lbl_LOCATION.Size = New System.Drawing.Size(85, 23)
        Me.two_lbl_LOCATION.TabIndex = 1041
        Me.two_lbl_LOCATION.Text = "ロケーション"
        Me.two_lbl_LOCATION.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_ref_M_HEADER_BASE
        '
        Me.two_ref_M_HEADER_BASE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.two_ref_M_HEADER_BASE.Enabled = False
        Me.two_ref_M_HEADER_BASE.FormattingEnabled = True
        Me.two_ref_M_HEADER_BASE.Location = New System.Drawing.Point(542, 148)
        Me.two_ref_M_HEADER_BASE.Name = "two_ref_M_HEADER_BASE"
        Me.two_ref_M_HEADER_BASE.Size = New System.Drawing.Size(133, 20)
        Me.two_ref_M_HEADER_BASE.TabIndex = 1054
        '
        'two_lbl_BASE
        '
        Me.two_lbl_BASE.Location = New System.Drawing.Point(474, 148)
        Me.two_lbl_BASE.Name = "two_lbl_BASE"
        Me.two_lbl_BASE.Size = New System.Drawing.Size(58, 23)
        Me.two_lbl_BASE.TabIndex = 1040
        Me.two_lbl_BASE.Text = "配置基地"
        Me.two_lbl_BASE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'two_ref_M_HEADER_HAICHI_KIJYUN
        '
        Me.two_ref_M_HEADER_HAICHI_KIJYUN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.two_ref_M_HEADER_HAICHI_KIJYUN.Enabled = False
        Me.two_ref_M_HEADER_HAICHI_KIJYUN.FormattingEnabled = True
        Me.two_ref_M_HEADER_HAICHI_KIJYUN.Location = New System.Drawing.Point(543, 125)
        Me.two_ref_M_HEADER_HAICHI_KIJYUN.Name = "two_ref_M_HEADER_HAICHI_KIJYUN"
        Me.two_ref_M_HEADER_HAICHI_KIJYUN.Size = New System.Drawing.Size(133, 20)
        Me.two_ref_M_HEADER_HAICHI_KIJYUN.TabIndex = 1051
        Me.two_ref_M_HEADER_HAICHI_KIJYUN.Tag = "HAICHI_KIJYUN"
        '
        'two_lbl_HAICHI_KIJYUN
        '
        Me.two_lbl_HAICHI_KIJYUN.Location = New System.Drawing.Point(477, 127)
        Me.two_lbl_HAICHI_KIJYUN.Name = "two_lbl_HAICHI_KIJYUN"
        Me.two_lbl_HAICHI_KIJYUN.Size = New System.Drawing.Size(56, 16)
        Me.two_lbl_HAICHI_KIJYUN.TabIndex = 1035
        Me.two_lbl_HAICHI_KIJYUN.Text = "配置基準"
        Me.two_lbl_HAICHI_KIJYUN.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_TOOL_ALLOW_FLG
        '
        Me.M_TOOL_ALLOW_FLG.AutoSize = True
        Me.M_TOOL_ALLOW_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_TOOL_ALLOW_FLG.Location = New System.Drawing.Point(9, 221)
        Me.M_TOOL_ALLOW_FLG.Name = "M_TOOL_ALLOW_FLG"
        Me.M_TOOL_ALLOW_FLG.Size = New System.Drawing.Size(72, 16)
        Me.M_TOOL_ALLOW_FLG.TabIndex = 1
        Me.M_TOOL_ALLOW_FLG.Text = "閲覧許可"
        Me.M_TOOL_ALLOW_FLG.UseVisualStyleBackColor = True
        '
        'M_TOOL_STATUS
        '
        Me.M_TOOL_STATUS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_TOOL_STATUS.FormattingEnabled = True
        Me.M_TOOL_STATUS.Location = New System.Drawing.Point(315, 219)
        Me.M_TOOL_STATUS.Name = "M_TOOL_STATUS"
        Me.M_TOOL_STATUS.Size = New System.Drawing.Size(133, 20)
        Me.M_TOOL_STATUS.TabIndex = 2
        '
        'lbl_STATUS
        '
        Me.lbl_STATUS.Location = New System.Drawing.Point(257, 224)
        Me.lbl_STATUS.Name = "lbl_STATUS"
        Me.lbl_STATUS.Size = New System.Drawing.Size(52, 23)
        Me.lbl_STATUS.TabIndex = 10
        Me.lbl_STATUS.Text = "STATUS"
        Me.lbl_STATUS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_M_HOZEN
        '
        Me.lbl_M_HOZEN.BackColor = System.Drawing.SystemColors.Control
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_PARENT_NO)
        Me.lbl_M_HOZEN.Controls.Add(Me.Label2)
        Me.lbl_M_HOZEN.Controls.Add(Me.Label7)
        Me.lbl_M_HOZEN.Controls.Add(Me.M_HOZEN_ATA)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_KANRI_NO)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_KANRI_NO)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_MODEL)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_MODEL)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_BUNRUI_01)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_MODIFIED_DATE)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_BUNRUI_01)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_MODIFIED_DATE)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_MODIFIED_BY)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_MODIFIED_BY)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_CREATED_DATE)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_CREATED_DATE)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_SPL)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_SPL)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_MFG_NO)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_MEISYOU)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_MEISYOU)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_MFG_NO)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_IMAGE)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_BASE)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_BASE)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_UN_NO)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_UN_NO)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_MFG)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_MFG)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_KIKAKU)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_KIKAKU)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_ref_M_HEADER_HAICHI_KIJYUN)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_HAICHI_KIJYUN)
        Me.lbl_M_HOZEN.Controls.Add(Me.gpx_INSPECTION_PROGRAM)
        Me.lbl_M_HOZEN.Controls.Add(Me.M_HOZEN_REASON)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_REASON)
        Me.lbl_M_HOZEN.Controls.Add(Me.M_HOZEN_REMARKS)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_REMARKS2)
        Me.lbl_M_HOZEN.Controls.Add(Me.M_HOZEN_ALLOW_FLG)
        Me.lbl_M_HOZEN.Controls.Add(Me.M_HOZEN_STATUS)
        Me.lbl_M_HOZEN.Controls.Add(Me.three_lbl_STATUS)
        Me.lbl_M_HOZEN.Controls.Add(Me.GroupBox3)
        Me.lbl_M_HOZEN.Controls.Add(Me.gpx_CHECK_RECORDS)
        Me.lbl_M_HOZEN.Location = New System.Drawing.Point(4, 22)
        Me.lbl_M_HOZEN.Name = "lbl_M_HOZEN"
        Me.lbl_M_HOZEN.Size = New System.Drawing.Size(703, 576)
        Me.lbl_M_HOZEN.TabIndex = 2
        Me.lbl_M_HOZEN.Tag = "M_HOZEN"
        Me.lbl_M_HOZEN.Text = "保全設備"
        '
        'three_ref_M_HEADER_PARENT_NO
        '
        Me.three_ref_M_HEADER_PARENT_NO.Enabled = False
        Me.three_ref_M_HEADER_PARENT_NO.Location = New System.Drawing.Point(69, 165)
        Me.three_ref_M_HEADER_PARENT_NO.MaxLength = 20
        Me.three_ref_M_HEADER_PARENT_NO.Name = "three_ref_M_HEADER_PARENT_NO"
        Me.three_ref_M_HEADER_PARENT_NO.Size = New System.Drawing.Size(381, 19)
        Me.three_ref_M_HEADER_PARENT_NO.TabIndex = 1230
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(5, 165)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 23)
        Me.Label2.TabIndex = 1231
        Me.Label2.Text = "親番号"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(589, 343)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 23)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "ATA"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HOZEN_ATA
        '
        Me.M_HOZEN_ATA.Location = New System.Drawing.Point(627, 344)
        Me.M_HOZEN_ATA.MaxLength = 2
        Me.M_HOZEN_ATA.Name = "M_HOZEN_ATA"
        Me.M_HOZEN_ATA.Size = New System.Drawing.Size(45, 19)
        Me.M_HOZEN_ATA.TabIndex = 3
        Me.M_HOZEN_ATA.Tag = "NUMBER"
        Me.M_HOZEN_ATA.Text = "0"
        Me.M_HOZEN_ATA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'three_ref_M_HEADER_KANRI_NO
        '
        Me.three_ref_M_HEADER_KANRI_NO.Enabled = False
        Me.three_ref_M_HEADER_KANRI_NO.Location = New System.Drawing.Point(69, 8)
        Me.three_ref_M_HEADER_KANRI_NO.MaxLength = 100
        Me.three_ref_M_HEADER_KANRI_NO.Name = "three_ref_M_HEADER_KANRI_NO"
        Me.three_ref_M_HEADER_KANRI_NO.Size = New System.Drawing.Size(268, 19)
        Me.three_ref_M_HEADER_KANRI_NO.TabIndex = 1197
        Me.three_ref_M_HEADER_KANRI_NO.Tag = "KANRI_NO"
        '
        'three_lbl_KANRI_NO
        '
        Me.three_lbl_KANRI_NO.Location = New System.Drawing.Point(7, 6)
        Me.three_lbl_KANRI_NO.Name = "three_lbl_KANRI_NO"
        Me.three_lbl_KANRI_NO.Size = New System.Drawing.Size(57, 23)
        Me.three_lbl_KANRI_NO.TabIndex = 1198
        Me.three_lbl_KANRI_NO.Text = "管理番号"
        Me.three_lbl_KANRI_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_MODEL
        '
        Me.three_ref_M_HEADER_MODEL.Enabled = False
        Me.three_ref_M_HEADER_MODEL.Location = New System.Drawing.Point(69, 52)
        Me.three_ref_M_HEADER_MODEL.MaxLength = 30
        Me.three_ref_M_HEADER_MODEL.Name = "three_ref_M_HEADER_MODEL"
        Me.three_ref_M_HEADER_MODEL.Size = New System.Drawing.Size(607, 19)
        Me.three_ref_M_HEADER_MODEL.TabIndex = 1196
        '
        'three_lbl_MODEL
        '
        Me.three_lbl_MODEL.Location = New System.Drawing.Point(29, 52)
        Me.three_lbl_MODEL.Name = "three_lbl_MODEL"
        Me.three_lbl_MODEL.Size = New System.Drawing.Size(34, 23)
        Me.three_lbl_MODEL.TabIndex = 1195
        Me.three_lbl_MODEL.Text = "型式"
        Me.three_lbl_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_lbl_BUNRUI_01
        '
        Me.three_lbl_BUNRUI_01.Location = New System.Drawing.Point(343, 9)
        Me.three_lbl_BUNRUI_01.Name = "three_lbl_BUNRUI_01"
        Me.three_lbl_BUNRUI_01.Size = New System.Drawing.Size(60, 16)
        Me.three_lbl_BUNRUI_01.TabIndex = 214
        Me.three_lbl_BUNRUI_01.Text = "分類番号"
        Me.three_lbl_BUNRUI_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_MODIFIED_DATE
        '
        Me.three_ref_M_HEADER_MODIFIED_DATE.Enabled = False
        Me.three_ref_M_HEADER_MODIFIED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.three_ref_M_HEADER_MODIFIED_DATE.Location = New System.Drawing.Point(314, 190)
        Me.three_ref_M_HEADER_MODIFIED_DATE.Name = "three_ref_M_HEADER_MODIFIED_DATE"
        Me.three_ref_M_HEADER_MODIFIED_DATE.Size = New System.Drawing.Size(133, 19)
        Me.three_ref_M_HEADER_MODIFIED_DATE.TabIndex = 1193
        '
        'three_ref_M_HEADER_BUNRUI_01
        '
        Me.three_ref_M_HEADER_BUNRUI_01.Enabled = False
        Me.three_ref_M_HEADER_BUNRUI_01.Location = New System.Drawing.Point(407, 8)
        Me.three_ref_M_HEADER_BUNRUI_01.MaxLength = 20
        Me.three_ref_M_HEADER_BUNRUI_01.Name = "three_ref_M_HEADER_BUNRUI_01"
        Me.three_ref_M_HEADER_BUNRUI_01.Size = New System.Drawing.Size(268, 19)
        Me.three_ref_M_HEADER_BUNRUI_01.TabIndex = 204
        Me.three_ref_M_HEADER_BUNRUI_01.Tag = "BUNRUI_01"
        '
        'three_lbl_MODIFIED_DATE
        '
        Me.three_lbl_MODIFIED_DATE.Location = New System.Drawing.Point(255, 189)
        Me.three_lbl_MODIFIED_DATE.Name = "three_lbl_MODIFIED_DATE"
        Me.three_lbl_MODIFIED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.three_lbl_MODIFIED_DATE.TabIndex = 1191
        Me.three_lbl_MODIFIED_DATE.Text = "更新日"
        Me.three_lbl_MODIFIED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_MODIFIED_BY
        '
        Me.three_ref_M_HEADER_MODIFIED_BY.Enabled = False
        Me.three_ref_M_HEADER_MODIFIED_BY.Location = New System.Drawing.Point(70, 188)
        Me.three_ref_M_HEADER_MODIFIED_BY.Name = "three_ref_M_HEADER_MODIFIED_BY"
        Me.three_ref_M_HEADER_MODIFIED_BY.Size = New System.Drawing.Size(131, 19)
        Me.three_ref_M_HEADER_MODIFIED_BY.TabIndex = 1192
        '
        'three_lbl_MODIFIED_BY
        '
        Me.three_lbl_MODIFIED_BY.Location = New System.Drawing.Point(22, 187)
        Me.three_lbl_MODIFIED_BY.Name = "three_lbl_MODIFIED_BY"
        Me.three_lbl_MODIFIED_BY.Size = New System.Drawing.Size(43, 23)
        Me.three_lbl_MODIFIED_BY.TabIndex = 1190
        Me.three_lbl_MODIFIED_BY.Text = "更新者"
        Me.three_lbl_MODIFIED_BY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_CREATED_DATE
        '
        Me.three_ref_M_HEADER_CREATED_DATE.Enabled = False
        Me.three_ref_M_HEADER_CREATED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.three_ref_M_HEADER_CREATED_DATE.Location = New System.Drawing.Point(70, 214)
        Me.three_ref_M_HEADER_CREATED_DATE.Name = "three_ref_M_HEADER_CREATED_DATE"
        Me.three_ref_M_HEADER_CREATED_DATE.Size = New System.Drawing.Size(131, 19)
        Me.three_ref_M_HEADER_CREATED_DATE.TabIndex = 1156
        '
        'three_lbl_CREATED_DATE
        '
        Me.three_lbl_CREATED_DATE.Location = New System.Drawing.Point(14, 212)
        Me.three_lbl_CREATED_DATE.Name = "three_lbl_CREATED_DATE"
        Me.three_lbl_CREATED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.three_lbl_CREATED_DATE.TabIndex = 1155
        Me.three_lbl_CREATED_DATE.Text = "登録日"
        Me.three_lbl_CREATED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_SPL
        '
        Me.three_ref_M_HEADER_SPL.Enabled = False
        Me.three_ref_M_HEADER_SPL.Location = New System.Drawing.Point(69, 118)
        Me.three_ref_M_HEADER_SPL.MaxLength = 50
        Me.three_ref_M_HEADER_SPL.Name = "three_ref_M_HEADER_SPL"
        Me.three_ref_M_HEADER_SPL.Size = New System.Drawing.Size(378, 19)
        Me.three_ref_M_HEADER_SPL.TabIndex = 1127
        '
        'three_lbl_SPL
        '
        Me.three_lbl_SPL.Location = New System.Drawing.Point(2, 121)
        Me.three_lbl_SPL.Name = "three_lbl_SPL"
        Me.three_lbl_SPL.Size = New System.Drawing.Size(62, 23)
        Me.three_lbl_SPL.TabIndex = 1126
        Me.three_lbl_SPL.Text = "購入会社"
        Me.three_lbl_SPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_MFG_NO
        '
        Me.three_ref_M_HEADER_MFG_NO.Enabled = False
        Me.three_ref_M_HEADER_MFG_NO.Location = New System.Drawing.Point(543, 98)
        Me.three_ref_M_HEADER_MFG_NO.MaxLength = 100
        Me.three_ref_M_HEADER_MFG_NO.Name = "three_ref_M_HEADER_MFG_NO"
        Me.three_ref_M_HEADER_MFG_NO.Size = New System.Drawing.Size(133, 19)
        Me.three_ref_M_HEADER_MFG_NO.TabIndex = 1118
        Me.three_ref_M_HEADER_MFG_NO.Tag = "BUNRUI_02"
        '
        'three_lbl_MEISYOU
        '
        Me.three_lbl_MEISYOU.Location = New System.Drawing.Point(30, 29)
        Me.three_lbl_MEISYOU.Name = "three_lbl_MEISYOU"
        Me.three_lbl_MEISYOU.Size = New System.Drawing.Size(34, 23)
        Me.three_lbl_MEISYOU.TabIndex = 1114
        Me.three_lbl_MEISYOU.Text = "名称"
        Me.three_lbl_MEISYOU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_MEISYOU
        '
        Me.three_ref_M_HEADER_MEISYOU.Enabled = False
        Me.three_ref_M_HEADER_MEISYOU.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.three_ref_M_HEADER_MEISYOU.Location = New System.Drawing.Point(69, 30)
        Me.three_ref_M_HEADER_MEISYOU.MaxLength = 100
        Me.three_ref_M_HEADER_MEISYOU.Name = "three_ref_M_HEADER_MEISYOU"
        Me.three_ref_M_HEADER_MEISYOU.Size = New System.Drawing.Size(606, 19)
        Me.three_ref_M_HEADER_MEISYOU.TabIndex = 1115
        Me.three_ref_M_HEADER_MEISYOU.Tag = "MEISYOU"
        '
        'three_lbl_MFG_NO
        '
        Me.three_lbl_MFG_NO.Location = New System.Drawing.Point(476, 96)
        Me.three_lbl_MFG_NO.Name = "three_lbl_MFG_NO"
        Me.three_lbl_MFG_NO.Size = New System.Drawing.Size(56, 23)
        Me.three_lbl_MFG_NO.TabIndex = 1103
        Me.three_lbl_MFG_NO.Text = "製造番号"
        Me.three_lbl_MFG_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_lbl_IMAGE
        '
        Me.three_lbl_IMAGE.Location = New System.Drawing.Point(488, 191)
        Me.three_lbl_IMAGE.Name = "three_lbl_IMAGE"
        Me.three_lbl_IMAGE.Size = New System.Drawing.Size(44, 17)
        Me.three_lbl_IMAGE.TabIndex = 1112
        Me.three_lbl_IMAGE.Text = "写真"
        Me.three_lbl_IMAGE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_BASE
        '
        Me.three_ref_M_HEADER_BASE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.three_ref_M_HEADER_BASE.Enabled = False
        Me.three_ref_M_HEADER_BASE.FormattingEnabled = True
        Me.three_ref_M_HEADER_BASE.Location = New System.Drawing.Point(543, 146)
        Me.three_ref_M_HEADER_BASE.Name = "three_ref_M_HEADER_BASE"
        Me.three_ref_M_HEADER_BASE.Size = New System.Drawing.Size(133, 20)
        Me.three_ref_M_HEADER_BASE.TabIndex = 1123
        '
        'three_lbl_BASE
        '
        Me.three_lbl_BASE.Location = New System.Drawing.Point(474, 146)
        Me.three_lbl_BASE.Name = "three_lbl_BASE"
        Me.three_lbl_BASE.Size = New System.Drawing.Size(58, 23)
        Me.three_lbl_BASE.TabIndex = 1109
        Me.three_lbl_BASE.Text = "配置基地"
        Me.three_lbl_BASE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_UN_NO
        '
        Me.three_ref_M_HEADER_UN_NO.Enabled = False
        Me.three_ref_M_HEADER_UN_NO.Location = New System.Drawing.Point(69, 140)
        Me.three_ref_M_HEADER_UN_NO.MaxLength = 50
        Me.three_ref_M_HEADER_UN_NO.Name = "three_ref_M_HEADER_UN_NO"
        Me.three_ref_M_HEADER_UN_NO.Size = New System.Drawing.Size(378, 19)
        Me.three_ref_M_HEADER_UN_NO.TabIndex = 1122
        '
        'three_lbl_UN_NO
        '
        Me.three_lbl_UN_NO.Location = New System.Drawing.Point(2, 144)
        Me.three_lbl_UN_NO.Name = "three_lbl_UN_NO"
        Me.three_lbl_UN_NO.Size = New System.Drawing.Size(62, 23)
        Me.three_lbl_UN_NO.TabIndex = 1108
        Me.three_lbl_UN_NO.Text = "危険物"
        Me.three_lbl_UN_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_MFG
        '
        Me.three_ref_M_HEADER_MFG.Enabled = False
        Me.three_ref_M_HEADER_MFG.Location = New System.Drawing.Point(69, 96)
        Me.three_ref_M_HEADER_MFG.MaxLength = 50
        Me.three_ref_M_HEADER_MFG.Name = "three_ref_M_HEADER_MFG"
        Me.three_ref_M_HEADER_MFG.Size = New System.Drawing.Size(378, 19)
        Me.three_ref_M_HEADER_MFG.TabIndex = 1121
        '
        'three_lbl_MFG
        '
        Me.three_lbl_MFG.Location = New System.Drawing.Point(2, 98)
        Me.three_lbl_MFG.Name = "three_lbl_MFG"
        Me.three_lbl_MFG.Size = New System.Drawing.Size(62, 23)
        Me.three_lbl_MFG.TabIndex = 1107
        Me.three_lbl_MFG.Text = "製造会社"
        Me.three_lbl_MFG.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_KIKAKU
        '
        Me.three_ref_M_HEADER_KIKAKU.Enabled = False
        Me.three_ref_M_HEADER_KIKAKU.Location = New System.Drawing.Point(69, 74)
        Me.three_ref_M_HEADER_KIKAKU.MaxLength = 50
        Me.three_ref_M_HEADER_KIKAKU.Name = "three_ref_M_HEADER_KIKAKU"
        Me.three_ref_M_HEADER_KIKAKU.Size = New System.Drawing.Size(607, 19)
        Me.three_ref_M_HEADER_KIKAKU.TabIndex = 1116
        '
        'three_lbl_KIKAKU
        '
        Me.three_lbl_KIKAKU.Location = New System.Drawing.Point(13, 75)
        Me.three_lbl_KIKAKU.Name = "three_lbl_KIKAKU"
        Me.three_lbl_KIKAKU.Size = New System.Drawing.Size(52, 23)
        Me.three_lbl_KIKAKU.TabIndex = 1105
        Me.three_lbl_KIKAKU.Text = "規格"
        Me.three_lbl_KIKAKU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_HAICHI_KIJYUN
        '
        Me.three_ref_M_HEADER_HAICHI_KIJYUN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.three_ref_M_HEADER_HAICHI_KIJYUN.Enabled = False
        Me.three_ref_M_HEADER_HAICHI_KIJYUN.FormattingEnabled = True
        Me.three_ref_M_HEADER_HAICHI_KIJYUN.Location = New System.Drawing.Point(542, 121)
        Me.three_ref_M_HEADER_HAICHI_KIJYUN.Name = "three_ref_M_HEADER_HAICHI_KIJYUN"
        Me.three_ref_M_HEADER_HAICHI_KIJYUN.Size = New System.Drawing.Size(133, 20)
        Me.three_ref_M_HEADER_HAICHI_KIJYUN.TabIndex = 1120
        Me.three_ref_M_HEADER_HAICHI_KIJYUN.Tag = "HAICHI_KIJYUN"
        '
        'three_lbl_HAICHI_KIJYUN
        '
        Me.three_lbl_HAICHI_KIJYUN.Location = New System.Drawing.Point(476, 123)
        Me.three_lbl_HAICHI_KIJYUN.Name = "three_lbl_HAICHI_KIJYUN"
        Me.three_lbl_HAICHI_KIJYUN.Size = New System.Drawing.Size(56, 16)
        Me.three_lbl_HAICHI_KIJYUN.TabIndex = 1104
        Me.three_lbl_HAICHI_KIJYUN.Text = "配置基準"
        Me.three_lbl_HAICHI_KIJYUN.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpx_INSPECTION_PROGRAM
        '
        Me.gpx_INSPECTION_PROGRAM.Controls.Add(Me.M_HOZEN_END_CHECK_DATE)
        Me.gpx_INSPECTION_PROGRAM.Controls.Add(Me.three_lbl_END_CHECK_DATE)
        Me.gpx_INSPECTION_PROGRAM.Controls.Add(Me.three_ref_M_HEADER_LOCATION)
        Me.gpx_INSPECTION_PROGRAM.Controls.Add(Me.three_lbl_LOCATION)
        Me.gpx_INSPECTION_PROGRAM.Controls.Add(Me.M_HOZEN_CHECK_COMPANY)
        Me.gpx_INSPECTION_PROGRAM.Controls.Add(Me.three_lbl_CHECK_COMPANY)
        Me.gpx_INSPECTION_PROGRAM.Controls.Add(Me.M_HOZEN_CHECK_INTERVAL)
        Me.gpx_INSPECTION_PROGRAM.Controls.Add(Me.three_lbl_HOZEN_INTERVAL)
        Me.gpx_INSPECTION_PROGRAM.Controls.Add(Me.M_HOZEN_NEXT_CHECK_DATE)
        Me.gpx_INSPECTION_PROGRAM.Controls.Add(Me.three_lbl_NEXT_CHECK_DATE)
        Me.gpx_INSPECTION_PROGRAM.Location = New System.Drawing.Point(10, 238)
        Me.gpx_INSPECTION_PROGRAM.Name = "gpx_INSPECTION_PROGRAM"
        Me.gpx_INSPECTION_PROGRAM.Size = New System.Drawing.Size(455, 99)
        Me.gpx_INSPECTION_PROGRAM.TabIndex = 0
        Me.gpx_INSPECTION_PROGRAM.TabStop = False
        Me.gpx_INSPECTION_PROGRAM.Text = "点検計画"
        '
        'M_HOZEN_END_CHECK_DATE
        '
        Me.M_HOZEN_END_CHECK_DATE.Enabled = False
        Me.M_HOZEN_END_CHECK_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_HOZEN_END_CHECK_DATE.Location = New System.Drawing.Point(314, 68)
        Me.M_HOZEN_END_CHECK_DATE.Name = "M_HOZEN_END_CHECK_DATE"
        Me.M_HOZEN_END_CHECK_DATE.Size = New System.Drawing.Size(133, 19)
        Me.M_HOZEN_END_CHECK_DATE.TabIndex = 1131
        Me.M_HOZEN_END_CHECK_DATE.Tag = ""
        '
        'three_lbl_END_CHECK_DATE
        '
        Me.three_lbl_END_CHECK_DATE.Location = New System.Drawing.Point(227, 67)
        Me.three_lbl_END_CHECK_DATE.Name = "three_lbl_END_CHECK_DATE"
        Me.three_lbl_END_CHECK_DATE.Size = New System.Drawing.Size(79, 20)
        Me.three_lbl_END_CHECK_DATE.TabIndex = 1130
        Me.three_lbl_END_CHECK_DATE.Text = "点検期限"
        Me.three_lbl_END_CHECK_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'three_ref_M_HEADER_LOCATION
        '
        Me.three_ref_M_HEADER_LOCATION.Enabled = False
        Me.three_ref_M_HEADER_LOCATION.Location = New System.Drawing.Point(314, 18)
        Me.three_ref_M_HEADER_LOCATION.MaxLength = 20
        Me.three_ref_M_HEADER_LOCATION.Name = "three_ref_M_HEADER_LOCATION"
        Me.three_ref_M_HEADER_LOCATION.Size = New System.Drawing.Size(133, 19)
        Me.three_ref_M_HEADER_LOCATION.TabIndex = 1124
        '
        'three_lbl_LOCATION
        '
        Me.three_lbl_LOCATION.Location = New System.Drawing.Point(221, 16)
        Me.three_lbl_LOCATION.Name = "three_lbl_LOCATION"
        Me.three_lbl_LOCATION.Size = New System.Drawing.Size(85, 23)
        Me.three_lbl_LOCATION.TabIndex = 1110
        Me.three_lbl_LOCATION.Text = "ロケーション"
        Me.three_lbl_LOCATION.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HOZEN_CHECK_COMPANY
        '
        Me.M_HOZEN_CHECK_COMPANY.Location = New System.Drawing.Point(88, 42)
        Me.M_HOZEN_CHECK_COMPANY.MaxLength = 50
        Me.M_HOZEN_CHECK_COMPANY.Name = "M_HOZEN_CHECK_COMPANY"
        Me.M_HOZEN_CHECK_COMPANY.Size = New System.Drawing.Size(358, 19)
        Me.M_HOZEN_CHECK_COMPANY.TabIndex = 1
        '
        'three_lbl_CHECK_COMPANY
        '
        Me.three_lbl_CHECK_COMPANY.Location = New System.Drawing.Point(4, 40)
        Me.three_lbl_CHECK_COMPANY.Name = "three_lbl_CHECK_COMPANY"
        Me.three_lbl_CHECK_COMPANY.Size = New System.Drawing.Size(80, 23)
        Me.three_lbl_CHECK_COMPANY.TabIndex = 11
        Me.three_lbl_CHECK_COMPANY.Text = "点検実施会社"
        Me.three_lbl_CHECK_COMPANY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HOZEN_CHECK_INTERVAL
        '
        Me.M_HOZEN_CHECK_INTERVAL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_HOZEN_CHECK_INTERVAL.FormattingEnabled = True
        Me.M_HOZEN_CHECK_INTERVAL.Location = New System.Drawing.Point(88, 16)
        Me.M_HOZEN_CHECK_INTERVAL.Name = "M_HOZEN_CHECK_INTERVAL"
        Me.M_HOZEN_CHECK_INTERVAL.Size = New System.Drawing.Size(129, 20)
        Me.M_HOZEN_CHECK_INTERVAL.TabIndex = 0
        Me.M_HOZEN_CHECK_INTERVAL.Tag = "INTERVAL"
        '
        'three_lbl_HOZEN_INTERVAL
        '
        Me.three_lbl_HOZEN_INTERVAL.Location = New System.Drawing.Point(22, 16)
        Me.three_lbl_HOZEN_INTERVAL.Name = "three_lbl_HOZEN_INTERVAL"
        Me.three_lbl_HOZEN_INTERVAL.Size = New System.Drawing.Size(61, 21)
        Me.three_lbl_HOZEN_INTERVAL.TabIndex = 1128
        Me.three_lbl_HOZEN_INTERVAL.Text = "点検間隔"
        Me.three_lbl_HOZEN_INTERVAL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HOZEN_NEXT_CHECK_DATE
        '
        Me.M_HOZEN_NEXT_CHECK_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_HOZEN_NEXT_CHECK_DATE.Location = New System.Drawing.Point(86, 68)
        Me.M_HOZEN_NEXT_CHECK_DATE.Name = "M_HOZEN_NEXT_CHECK_DATE"
        Me.M_HOZEN_NEXT_CHECK_DATE.ShowCheckBox = True
        Me.M_HOZEN_NEXT_CHECK_DATE.Size = New System.Drawing.Size(133, 19)
        Me.M_HOZEN_NEXT_CHECK_DATE.TabIndex = 2
        '
        'three_lbl_NEXT_CHECK_DATE
        '
        Me.three_lbl_NEXT_CHECK_DATE.Location = New System.Drawing.Point(18, 66)
        Me.three_lbl_NEXT_CHECK_DATE.Name = "three_lbl_NEXT_CHECK_DATE"
        Me.three_lbl_NEXT_CHECK_DATE.Size = New System.Drawing.Size(67, 23)
        Me.three_lbl_NEXT_CHECK_DATE.TabIndex = 257
        Me.three_lbl_NEXT_CHECK_DATE.Text = "次回点検日"
        Me.three_lbl_NEXT_CHECK_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HOZEN_REASON
        '
        Me.M_HOZEN_REASON.Location = New System.Drawing.Point(66, 471)
        Me.M_HOZEN_REASON.MaxLength = 1000
        Me.M_HOZEN_REASON.Multiline = True
        Me.M_HOZEN_REASON.Name = "M_HOZEN_REASON"
        Me.M_HOZEN_REASON.Size = New System.Drawing.Size(607, 32)
        Me.M_HOZEN_REASON.TabIndex = 4
        '
        'three_lbl_REASON
        '
        Me.three_lbl_REASON.Location = New System.Drawing.Point(2, 471)
        Me.three_lbl_REASON.Name = "three_lbl_REASON"
        Me.three_lbl_REASON.Size = New System.Drawing.Size(60, 23)
        Me.three_lbl_REASON.TabIndex = 409
        Me.three_lbl_REASON.Text = "更新理由"
        Me.three_lbl_REASON.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HOZEN_REMARKS
        '
        Me.M_HOZEN_REMARKS.Location = New System.Drawing.Point(66, 435)
        Me.M_HOZEN_REMARKS.MaxLength = 500
        Me.M_HOZEN_REMARKS.Multiline = True
        Me.M_HOZEN_REMARKS.Name = "M_HOZEN_REMARKS"
        Me.M_HOZEN_REMARKS.Size = New System.Drawing.Size(607, 32)
        Me.M_HOZEN_REMARKS.TabIndex = 3
        '
        'three_lbl_REMARKS2
        '
        Me.three_lbl_REMARKS2.Location = New System.Drawing.Point(16, 432)
        Me.three_lbl_REMARKS2.Name = "three_lbl_REMARKS2"
        Me.three_lbl_REMARKS2.Size = New System.Drawing.Size(46, 23)
        Me.three_lbl_REMARKS2.TabIndex = 264
        Me.three_lbl_REMARKS2.Text = "備考"
        Me.three_lbl_REMARKS2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HOZEN_ALLOW_FLG
        '
        Me.M_HOZEN_ALLOW_FLG.AutoSize = True
        Me.M_HOZEN_ALLOW_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_HOZEN_ALLOW_FLG.Location = New System.Drawing.Point(485, 345)
        Me.M_HOZEN_ALLOW_FLG.Name = "M_HOZEN_ALLOW_FLG"
        Me.M_HOZEN_ALLOW_FLG.Size = New System.Drawing.Size(72, 16)
        Me.M_HOZEN_ALLOW_FLG.TabIndex = 2
        Me.M_HOZEN_ALLOW_FLG.Text = "閲覧許可"
        Me.M_HOZEN_ALLOW_FLG.UseVisualStyleBackColor = True
        '
        'M_HOZEN_STATUS
        '
        Me.M_HOZEN_STATUS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_HOZEN_STATUS.FormattingEnabled = True
        Me.M_HOZEN_STATUS.Location = New System.Drawing.Point(540, 378)
        Me.M_HOZEN_STATUS.Name = "M_HOZEN_STATUS"
        Me.M_HOZEN_STATUS.Size = New System.Drawing.Size(133, 20)
        Me.M_HOZEN_STATUS.TabIndex = 4
        '
        'three_lbl_STATUS
        '
        Me.three_lbl_STATUS.Location = New System.Drawing.Point(480, 376)
        Me.three_lbl_STATUS.Name = "three_lbl_STATUS"
        Me.three_lbl_STATUS.Size = New System.Drawing.Size(52, 23)
        Me.three_lbl_STATUS.TabIndex = 262
        Me.three_lbl_STATUS.Text = "STATUS"
        Me.three_lbl_STATUS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.three_ref_M_HEADER_IMAGE)
        Me.GroupBox3.Location = New System.Drawing.Point(507, 202)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(168, 126)
        Me.GroupBox3.TabIndex = 1194
        Me.GroupBox3.TabStop = False
        '
        'three_ref_M_HEADER_IMAGE
        '
        Me.three_ref_M_HEADER_IMAGE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.three_ref_M_HEADER_IMAGE.Location = New System.Drawing.Point(2, 9)
        Me.three_ref_M_HEADER_IMAGE.Name = "three_ref_M_HEADER_IMAGE"
        Me.three_ref_M_HEADER_IMAGE.Size = New System.Drawing.Size(163, 115)
        Me.three_ref_M_HEADER_IMAGE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.three_ref_M_HEADER_IMAGE.TabIndex = 84
        Me.three_ref_M_HEADER_IMAGE.TabStop = False
        '
        'gpx_CHECK_RECORDS
        '
        Me.gpx_CHECK_RECORDS.Controls.Add(Me.M_HOZEN_CHECK_DATE)
        Me.gpx_CHECK_RECORDS.Controls.Add(Me.lbl_CHECK_BY)
        Me.gpx_CHECK_RECORDS.Controls.Add(Me.M_HOZEN_CHECK_BY)
        Me.gpx_CHECK_RECORDS.Controls.Add(Me.lbl_CHECK_DATE)
        Me.gpx_CHECK_RECORDS.Location = New System.Drawing.Point(15, 347)
        Me.gpx_CHECK_RECORDS.Name = "gpx_CHECK_RECORDS"
        Me.gpx_CHECK_RECORDS.Size = New System.Drawing.Size(406, 77)
        Me.gpx_CHECK_RECORDS.TabIndex = 1
        Me.gpx_CHECK_RECORDS.TabStop = False
        Me.gpx_CHECK_RECORDS.Text = "点検記録"
        '
        'M_HOZEN_CHECK_DATE
        '
        Me.M_HOZEN_CHECK_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_HOZEN_CHECK_DATE.Location = New System.Drawing.Point(79, 44)
        Me.M_HOZEN_CHECK_DATE.Name = "M_HOZEN_CHECK_DATE"
        Me.M_HOZEN_CHECK_DATE.ShowCheckBox = True
        Me.M_HOZEN_CHECK_DATE.Size = New System.Drawing.Size(145, 19)
        Me.M_HOZEN_CHECK_DATE.TabIndex = 1
        Me.M_HOZEN_CHECK_DATE.Tag = ""
        '
        'lbl_CHECK_BY
        '
        Me.lbl_CHECK_BY.Location = New System.Drawing.Point(7, 20)
        Me.lbl_CHECK_BY.Name = "lbl_CHECK_BY"
        Me.lbl_CHECK_BY.Size = New System.Drawing.Size(68, 23)
        Me.lbl_CHECK_BY.TabIndex = 253
        Me.lbl_CHECK_BY.Text = "点検実施者"
        Me.lbl_CHECK_BY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_HOZEN_CHECK_BY
        '
        Me.M_HOZEN_CHECK_BY.Location = New System.Drawing.Point(79, 21)
        Me.M_HOZEN_CHECK_BY.MaxLength = 20
        Me.M_HOZEN_CHECK_BY.Name = "M_HOZEN_CHECK_BY"
        Me.M_HOZEN_CHECK_BY.Size = New System.Drawing.Size(145, 19)
        Me.M_HOZEN_CHECK_BY.TabIndex = 0
        '
        'lbl_CHECK_DATE
        '
        Me.lbl_CHECK_DATE.Location = New System.Drawing.Point(6, 43)
        Me.lbl_CHECK_DATE.Name = "lbl_CHECK_DATE"
        Me.lbl_CHECK_DATE.Size = New System.Drawing.Size(69, 23)
        Me.lbl_CHECK_DATE.TabIndex = 255
        Me.lbl_CHECK_DATE.Text = "点検実施日"
        Me.lbl_CHECK_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_M_PME
        '
        Me.lbl_M_PME.BackColor = System.Drawing.SystemColors.Control
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_PARENT_NO)
        Me.lbl_M_PME.Controls.Add(Me.Label3)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_MODEL)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_MODEL)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_KIKAKU)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_KIKAKU)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_KANRI_NO)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_KANRI_NO)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_BUNRUI_01)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_BUNRUI_01)
        Me.lbl_M_PME.Controls.Add(Me.four_gpx_KOUSEI_GROUP)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_MODIFIED_DATE)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_MODIFIED_DATE)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_MODIFIED_BY)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_MODIFIED_BY)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_CREATED_DATE)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_CREATED_DATE)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_IMAGE)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_SPL)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_SPL)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_ATA)
        Me.lbl_M_PME.Controls.Add(Me.M_PME_ATA)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_MFG_NO)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_MEISYOU)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_MEISYOU)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_MFG_NO)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_BASE)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_BASE)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_UN_NO)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_UN_NO)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_MFG)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_MFG)
        Me.lbl_M_PME.Controls.Add(Me.four_ref_M_HEADER_HAICHI_KIJYUN)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_HAICHI_KIJYUN)
        Me.lbl_M_PME.Controls.Add(Me.gpx_M_PME_MFG_NO)
        Me.lbl_M_PME.Controls.Add(Me.M_PME_REASON)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_REASON)
        Me.lbl_M_PME.Controls.Add(Me.M_PME_REMARKS)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_REMARKS2)
        Me.lbl_M_PME.Controls.Add(Me.M_PME_ALLOW_FLG)
        Me.lbl_M_PME.Controls.Add(Me.M_PME_STATUS)
        Me.lbl_M_PME.Controls.Add(Me.four_lbl_STATUS)
        Me.lbl_M_PME.Controls.Add(Me.GroupBox4)
        Me.lbl_M_PME.Location = New System.Drawing.Point(4, 22)
        Me.lbl_M_PME.Name = "lbl_M_PME"
        Me.lbl_M_PME.Size = New System.Drawing.Size(703, 576)
        Me.lbl_M_PME.TabIndex = 3
        Me.lbl_M_PME.Tag = "M_PME"
        Me.lbl_M_PME.Text = "計測器"
        '
        'four_ref_M_HEADER_PARENT_NO
        '
        Me.four_ref_M_HEADER_PARENT_NO.Enabled = False
        Me.four_ref_M_HEADER_PARENT_NO.Location = New System.Drawing.Point(63, 169)
        Me.four_ref_M_HEADER_PARENT_NO.MaxLength = 20
        Me.four_ref_M_HEADER_PARENT_NO.Name = "four_ref_M_HEADER_PARENT_NO"
        Me.four_ref_M_HEADER_PARENT_NO.Size = New System.Drawing.Size(405, 19)
        Me.four_ref_M_HEADER_PARENT_NO.TabIndex = 1232
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 167)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 23)
        Me.Label3.TabIndex = 1233
        Me.Label3.Text = "親番号"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_MODEL
        '
        Me.four_ref_M_HEADER_MODEL.Enabled = False
        Me.four_ref_M_HEADER_MODEL.Location = New System.Drawing.Point(63, 54)
        Me.four_ref_M_HEADER_MODEL.MaxLength = 30
        Me.four_ref_M_HEADER_MODEL.Name = "four_ref_M_HEADER_MODEL"
        Me.four_ref_M_HEADER_MODEL.Size = New System.Drawing.Size(613, 19)
        Me.four_ref_M_HEADER_MODEL.TabIndex = 1206
        '
        'four_lbl_MODEL
        '
        Me.four_lbl_MODEL.Location = New System.Drawing.Point(26, 55)
        Me.four_lbl_MODEL.Name = "four_lbl_MODEL"
        Me.four_lbl_MODEL.Size = New System.Drawing.Size(34, 23)
        Me.four_lbl_MODEL.TabIndex = 1205
        Me.four_lbl_MODEL.Text = "型式"
        Me.four_lbl_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_KIKAKU
        '
        Me.four_ref_M_HEADER_KIKAKU.Enabled = False
        Me.four_ref_M_HEADER_KIKAKU.Location = New System.Drawing.Point(64, 77)
        Me.four_ref_M_HEADER_KIKAKU.MaxLength = 50
        Me.four_ref_M_HEADER_KIKAKU.Name = "four_ref_M_HEADER_KIKAKU"
        Me.four_ref_M_HEADER_KIKAKU.Size = New System.Drawing.Size(613, 19)
        Me.four_ref_M_HEADER_KIKAKU.TabIndex = 1204
        '
        'four_lbl_KIKAKU
        '
        Me.four_lbl_KIKAKU.Location = New System.Drawing.Point(10, 78)
        Me.four_lbl_KIKAKU.Name = "four_lbl_KIKAKU"
        Me.four_lbl_KIKAKU.Size = New System.Drawing.Size(52, 23)
        Me.four_lbl_KIKAKU.TabIndex = 1203
        Me.four_lbl_KIKAKU.Text = "規格"
        Me.four_lbl_KIKAKU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_KANRI_NO
        '
        Me.four_ref_M_HEADER_KANRI_NO.Enabled = False
        Me.four_ref_M_HEADER_KANRI_NO.Location = New System.Drawing.Point(64, 9)
        Me.four_ref_M_HEADER_KANRI_NO.MaxLength = 100
        Me.four_ref_M_HEADER_KANRI_NO.Name = "four_ref_M_HEADER_KANRI_NO"
        Me.four_ref_M_HEADER_KANRI_NO.Size = New System.Drawing.Size(268, 19)
        Me.four_ref_M_HEADER_KANRI_NO.TabIndex = 1201
        Me.four_ref_M_HEADER_KANRI_NO.Tag = "KANRI_NO"
        '
        'four_lbl_KANRI_NO
        '
        Me.four_lbl_KANRI_NO.Location = New System.Drawing.Point(2, 8)
        Me.four_lbl_KANRI_NO.Name = "four_lbl_KANRI_NO"
        Me.four_lbl_KANRI_NO.Size = New System.Drawing.Size(57, 23)
        Me.four_lbl_KANRI_NO.TabIndex = 1202
        Me.four_lbl_KANRI_NO.Text = "管理番号"
        Me.four_lbl_KANRI_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_lbl_BUNRUI_01
        '
        Me.four_lbl_BUNRUI_01.Location = New System.Drawing.Point(342, 10)
        Me.four_lbl_BUNRUI_01.Name = "four_lbl_BUNRUI_01"
        Me.four_lbl_BUNRUI_01.Size = New System.Drawing.Size(62, 16)
        Me.four_lbl_BUNRUI_01.TabIndex = 1200
        Me.four_lbl_BUNRUI_01.Text = "分類番号"
        Me.four_lbl_BUNRUI_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_BUNRUI_01
        '
        Me.four_ref_M_HEADER_BUNRUI_01.Enabled = False
        Me.four_ref_M_HEADER_BUNRUI_01.Location = New System.Drawing.Point(408, 9)
        Me.four_ref_M_HEADER_BUNRUI_01.MaxLength = 20
        Me.four_ref_M_HEADER_BUNRUI_01.Name = "four_ref_M_HEADER_BUNRUI_01"
        Me.four_ref_M_HEADER_BUNRUI_01.Size = New System.Drawing.Size(268, 19)
        Me.four_ref_M_HEADER_BUNRUI_01.TabIndex = 1199
        Me.four_ref_M_HEADER_BUNRUI_01.Tag = "BUNRUI_01"
        '
        'four_gpx_KOUSEI_GROUP
        '
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.M_PME_KOUSEI_INTERVAL)
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.btn_Kousei)
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.M_PME_KOUSEI_SEIDO)
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.lbl_KOUSEI_SEIDO)
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.four_lbl_KOUSEI_INTERVAL)
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.lbl_KEKKA)
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.M_PME_KEKKA)
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.M_PME_KOUSEI_DATE)
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.lbl_KOUSEI_DATE)
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.M_PME_DUE_DATE)
        Me.four_gpx_KOUSEI_GROUP.Controls.Add(Me.lbl_DUE_DATE)
        Me.four_gpx_KOUSEI_GROUP.Location = New System.Drawing.Point(12, 198)
        Me.four_gpx_KOUSEI_GROUP.Name = "four_gpx_KOUSEI_GROUP"
        Me.four_gpx_KOUSEI_GROUP.Size = New System.Drawing.Size(210, 186)
        Me.four_gpx_KOUSEI_GROUP.TabIndex = 0
        Me.four_gpx_KOUSEI_GROUP.TabStop = False
        Me.four_gpx_KOUSEI_GROUP.Text = "校正計測器情報"
        '
        'M_PME_KOUSEI_INTERVAL
        '
        Me.M_PME_KOUSEI_INTERVAL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_PME_KOUSEI_INTERVAL.FormattingEnabled = True
        Me.M_PME_KOUSEI_INTERVAL.Location = New System.Drawing.Point(77, 40)
        Me.M_PME_KOUSEI_INTERVAL.Name = "M_PME_KOUSEI_INTERVAL"
        Me.M_PME_KOUSEI_INTERVAL.Size = New System.Drawing.Size(121, 20)
        Me.M_PME_KOUSEI_INTERVAL.TabIndex = 1232
        '
        'btn_Kousei
        '
        Me.btn_Kousei.Location = New System.Drawing.Point(79, 153)
        Me.btn_Kousei.Name = "btn_Kousei"
        Me.btn_Kousei.Size = New System.Drawing.Size(119, 23)
        Me.btn_Kousei.TabIndex = 5
        Me.btn_Kousei.Text = "校正計測器管理表"
        Me.btn_Kousei.UseVisualStyleBackColor = True
        '
        'M_PME_KOUSEI_SEIDO
        '
        Me.M_PME_KOUSEI_SEIDO.Location = New System.Drawing.Point(78, 18)
        Me.M_PME_KOUSEI_SEIDO.MaxLength = 20
        Me.M_PME_KOUSEI_SEIDO.Name = "M_PME_KOUSEI_SEIDO"
        Me.M_PME_KOUSEI_SEIDO.Size = New System.Drawing.Size(120, 19)
        Me.M_PME_KOUSEI_SEIDO.TabIndex = 0
        Me.M_PME_KOUSEI_SEIDO.Tag = ""
        Me.M_PME_KOUSEI_SEIDO.Text = "123.45"
        Me.M_PME_KOUSEI_SEIDO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbl_KOUSEI_SEIDO
        '
        Me.lbl_KOUSEI_SEIDO.Location = New System.Drawing.Point(7, 16)
        Me.lbl_KOUSEI_SEIDO.Name = "lbl_KOUSEI_SEIDO"
        Me.lbl_KOUSEI_SEIDO.Size = New System.Drawing.Size(64, 23)
        Me.lbl_KOUSEI_SEIDO.TabIndex = 0
        Me.lbl_KOUSEI_SEIDO.Text = "精度範囲"
        Me.lbl_KOUSEI_SEIDO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_lbl_KOUSEI_INTERVAL
        '
        Me.four_lbl_KOUSEI_INTERVAL.Location = New System.Drawing.Point(8, 41)
        Me.four_lbl_KOUSEI_INTERVAL.Name = "four_lbl_KOUSEI_INTERVAL"
        Me.four_lbl_KOUSEI_INTERVAL.Size = New System.Drawing.Size(63, 23)
        Me.four_lbl_KOUSEI_INTERVAL.TabIndex = 1174
        Me.four_lbl_KOUSEI_INTERVAL.Text = "校正間隔"
        Me.four_lbl_KOUSEI_INTERVAL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_KEKKA
        '
        Me.lbl_KEKKA.Location = New System.Drawing.Point(31, 87)
        Me.lbl_KEKKA.Name = "lbl_KEKKA"
        Me.lbl_KEKKA.Size = New System.Drawing.Size(39, 23)
        Me.lbl_KEKKA.TabIndex = 406
        Me.lbl_KEKKA.Text = "合否"
        Me.lbl_KEKKA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_KEKKA
        '
        Me.M_PME_KEKKA.Enabled = False
        Me.M_PME_KEKKA.Location = New System.Drawing.Point(77, 90)
        Me.M_PME_KEKKA.MaxLength = 20
        Me.M_PME_KEKKA.Name = "M_PME_KEKKA"
        Me.M_PME_KEKKA.Size = New System.Drawing.Size(121, 19)
        Me.M_PME_KEKKA.TabIndex = 3
        '
        'M_PME_KOUSEI_DATE
        '
        Me.M_PME_KOUSEI_DATE.Enabled = False
        Me.M_PME_KOUSEI_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_PME_KOUSEI_DATE.Location = New System.Drawing.Point(77, 66)
        Me.M_PME_KOUSEI_DATE.Name = "M_PME_KOUSEI_DATE"
        Me.M_PME_KOUSEI_DATE.Size = New System.Drawing.Size(121, 19)
        Me.M_PME_KOUSEI_DATE.TabIndex = 2
        '
        'lbl_KOUSEI_DATE
        '
        Me.lbl_KOUSEI_DATE.Location = New System.Drawing.Point(1, 67)
        Me.lbl_KOUSEI_DATE.Name = "lbl_KOUSEI_DATE"
        Me.lbl_KOUSEI_DATE.Size = New System.Drawing.Size(70, 23)
        Me.lbl_KOUSEI_DATE.TabIndex = 411
        Me.lbl_KOUSEI_DATE.Text = "校正実施日"
        Me.lbl_KOUSEI_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_DUE_DATE
        '
        Me.M_PME_DUE_DATE.Enabled = False
        Me.M_PME_DUE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_PME_DUE_DATE.Location = New System.Drawing.Point(77, 114)
        Me.M_PME_DUE_DATE.Name = "M_PME_DUE_DATE"
        Me.M_PME_DUE_DATE.Size = New System.Drawing.Size(121, 19)
        Me.M_PME_DUE_DATE.TabIndex = 4
        '
        'lbl_DUE_DATE
        '
        Me.lbl_DUE_DATE.Location = New System.Drawing.Point(10, 109)
        Me.lbl_DUE_DATE.Name = "lbl_DUE_DATE"
        Me.lbl_DUE_DATE.Size = New System.Drawing.Size(61, 23)
        Me.lbl_DUE_DATE.TabIndex = 412
        Me.lbl_DUE_DATE.Text = "有効期限"
        Me.lbl_DUE_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_MODIFIED_DATE
        '
        Me.four_ref_M_HEADER_MODIFIED_DATE.Enabled = False
        Me.four_ref_M_HEADER_MODIFIED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.four_ref_M_HEADER_MODIFIED_DATE.Location = New System.Drawing.Point(473, 403)
        Me.four_ref_M_HEADER_MODIFIED_DATE.Name = "four_ref_M_HEADER_MODIFIED_DATE"
        Me.four_ref_M_HEADER_MODIFIED_DATE.Size = New System.Drawing.Size(133, 19)
        Me.four_ref_M_HEADER_MODIFIED_DATE.TabIndex = 1185
        '
        'four_lbl_MODIFIED_DATE
        '
        Me.four_lbl_MODIFIED_DATE.Location = New System.Drawing.Point(414, 402)
        Me.four_lbl_MODIFIED_DATE.Name = "four_lbl_MODIFIED_DATE"
        Me.four_lbl_MODIFIED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.four_lbl_MODIFIED_DATE.TabIndex = 1183
        Me.four_lbl_MODIFIED_DATE.Text = "更新日"
        Me.four_lbl_MODIFIED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_MODIFIED_BY
        '
        Me.four_ref_M_HEADER_MODIFIED_BY.Enabled = False
        Me.four_ref_M_HEADER_MODIFIED_BY.Location = New System.Drawing.Point(268, 403)
        Me.four_ref_M_HEADER_MODIFIED_BY.Name = "four_ref_M_HEADER_MODIFIED_BY"
        Me.four_ref_M_HEADER_MODIFIED_BY.Size = New System.Drawing.Size(139, 19)
        Me.four_ref_M_HEADER_MODIFIED_BY.TabIndex = 1184
        '
        'four_lbl_MODIFIED_BY
        '
        Me.four_lbl_MODIFIED_BY.Location = New System.Drawing.Point(222, 402)
        Me.four_lbl_MODIFIED_BY.Name = "four_lbl_MODIFIED_BY"
        Me.four_lbl_MODIFIED_BY.Size = New System.Drawing.Size(43, 23)
        Me.four_lbl_MODIFIED_BY.TabIndex = 1182
        Me.four_lbl_MODIFIED_BY.Text = "更新者"
        Me.four_lbl_MODIFIED_BY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_CREATED_DATE
        '
        Me.four_ref_M_HEADER_CREATED_DATE.Enabled = False
        Me.four_ref_M_HEADER_CREATED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.four_ref_M_HEADER_CREATED_DATE.Location = New System.Drawing.Point(64, 403)
        Me.four_ref_M_HEADER_CREATED_DATE.Name = "four_ref_M_HEADER_CREATED_DATE"
        Me.four_ref_M_HEADER_CREATED_DATE.Size = New System.Drawing.Size(139, 19)
        Me.four_ref_M_HEADER_CREATED_DATE.TabIndex = 1181
        '
        'four_lbl_CREATED_DATE
        '
        Me.four_lbl_CREATED_DATE.Location = New System.Drawing.Point(9, 401)
        Me.four_lbl_CREATED_DATE.Name = "four_lbl_CREATED_DATE"
        Me.four_lbl_CREATED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.four_lbl_CREATED_DATE.TabIndex = 1180
        Me.four_lbl_CREATED_DATE.Text = "登録日"
        Me.four_lbl_CREATED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_lbl_IMAGE
        '
        Me.four_lbl_IMAGE.Location = New System.Drawing.Point(492, 177)
        Me.four_lbl_IMAGE.Name = "four_lbl_IMAGE"
        Me.four_lbl_IMAGE.Size = New System.Drawing.Size(44, 17)
        Me.four_lbl_IMAGE.TabIndex = 1178
        Me.four_lbl_IMAGE.Text = "写真"
        Me.four_lbl_IMAGE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_SPL
        '
        Me.four_ref_M_HEADER_SPL.Enabled = False
        Me.four_ref_M_HEADER_SPL.Location = New System.Drawing.Point(64, 123)
        Me.four_ref_M_HEADER_SPL.MaxLength = 50
        Me.four_ref_M_HEADER_SPL.Name = "four_ref_M_HEADER_SPL"
        Me.four_ref_M_HEADER_SPL.Size = New System.Drawing.Size(404, 19)
        Me.four_ref_M_HEADER_SPL.TabIndex = 1173
        '
        'four_lbl_SPL
        '
        Me.four_lbl_SPL.Location = New System.Drawing.Point(2, 123)
        Me.four_lbl_SPL.Name = "four_lbl_SPL"
        Me.four_lbl_SPL.Size = New System.Drawing.Size(58, 23)
        Me.four_lbl_SPL.TabIndex = 1172
        Me.four_lbl_SPL.Text = "購入会社"
        Me.four_lbl_SPL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_lbl_ATA
        '
        Me.four_lbl_ATA.Location = New System.Drawing.Point(500, 317)
        Me.four_lbl_ATA.Name = "four_lbl_ATA"
        Me.four_lbl_ATA.Size = New System.Drawing.Size(35, 23)
        Me.four_lbl_ATA.TabIndex = 1157
        Me.four_lbl_ATA.Text = "ATA"
        Me.four_lbl_ATA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_ATA
        '
        Me.M_PME_ATA.Location = New System.Drawing.Point(538, 319)
        Me.M_PME_ATA.MaxLength = 2
        Me.M_PME_ATA.Name = "M_PME_ATA"
        Me.M_PME_ATA.Size = New System.Drawing.Size(45, 19)
        Me.M_PME_ATA.TabIndex = 2
        Me.M_PME_ATA.Tag = "NUMBER"
        Me.M_PME_ATA.Text = "0"
        Me.M_PME_ATA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'four_ref_M_HEADER_MFG_NO
        '
        Me.four_ref_M_HEADER_MFG_NO.Enabled = False
        Me.four_ref_M_HEADER_MFG_NO.Location = New System.Drawing.Point(543, 101)
        Me.four_ref_M_HEADER_MFG_NO.MaxLength = 100
        Me.four_ref_M_HEADER_MFG_NO.Name = "four_ref_M_HEADER_MFG_NO"
        Me.four_ref_M_HEADER_MFG_NO.Size = New System.Drawing.Size(133, 19)
        Me.four_ref_M_HEADER_MFG_NO.TabIndex = 1165
        Me.four_ref_M_HEADER_MFG_NO.Tag = "BUNRUI_02"
        '
        'four_lbl_MEISYOU
        '
        Me.four_lbl_MEISYOU.Location = New System.Drawing.Point(26, 33)
        Me.four_lbl_MEISYOU.Name = "four_lbl_MEISYOU"
        Me.four_lbl_MEISYOU.Size = New System.Drawing.Size(34, 23)
        Me.four_lbl_MEISYOU.TabIndex = 1162
        Me.four_lbl_MEISYOU.Text = "名称"
        Me.four_lbl_MEISYOU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_MEISYOU
        '
        Me.four_ref_M_HEADER_MEISYOU.Enabled = False
        Me.four_ref_M_HEADER_MEISYOU.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.four_ref_M_HEADER_MEISYOU.Location = New System.Drawing.Point(64, 31)
        Me.four_ref_M_HEADER_MEISYOU.MaxLength = 100
        Me.four_ref_M_HEADER_MEISYOU.Name = "four_ref_M_HEADER_MEISYOU"
        Me.four_ref_M_HEADER_MEISYOU.Size = New System.Drawing.Size(612, 19)
        Me.four_ref_M_HEADER_MEISYOU.TabIndex = 1163
        Me.four_ref_M_HEADER_MEISYOU.Tag = "MEISYOU"
        '
        'four_lbl_MFG_NO
        '
        Me.four_lbl_MFG_NO.Location = New System.Drawing.Point(477, 99)
        Me.four_lbl_MFG_NO.Name = "four_lbl_MFG_NO"
        Me.four_lbl_MFG_NO.Size = New System.Drawing.Size(56, 23)
        Me.four_lbl_MFG_NO.TabIndex = 1155
        Me.four_lbl_MFG_NO.Text = "製造番号"
        Me.four_lbl_MFG_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_BASE
        '
        Me.four_ref_M_HEADER_BASE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.four_ref_M_HEADER_BASE.Enabled = False
        Me.four_ref_M_HEADER_BASE.FormattingEnabled = True
        Me.four_ref_M_HEADER_BASE.Location = New System.Drawing.Point(543, 146)
        Me.four_ref_M_HEADER_BASE.Name = "four_ref_M_HEADER_BASE"
        Me.four_ref_M_HEADER_BASE.Size = New System.Drawing.Size(133, 20)
        Me.four_ref_M_HEADER_BASE.TabIndex = 1170
        '
        'four_lbl_BASE
        '
        Me.four_lbl_BASE.Location = New System.Drawing.Point(475, 145)
        Me.four_lbl_BASE.Name = "four_lbl_BASE"
        Me.four_lbl_BASE.Size = New System.Drawing.Size(58, 23)
        Me.four_lbl_BASE.TabIndex = 1160
        Me.four_lbl_BASE.Text = "配置基地"
        Me.four_lbl_BASE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_UN_NO
        '
        Me.four_ref_M_HEADER_UN_NO.Enabled = False
        Me.four_ref_M_HEADER_UN_NO.Location = New System.Drawing.Point(64, 145)
        Me.four_ref_M_HEADER_UN_NO.MaxLength = 50
        Me.four_ref_M_HEADER_UN_NO.Name = "four_ref_M_HEADER_UN_NO"
        Me.four_ref_M_HEADER_UN_NO.Size = New System.Drawing.Size(404, 19)
        Me.four_ref_M_HEADER_UN_NO.TabIndex = 1169
        '
        'four_lbl_UN_NO
        '
        Me.four_lbl_UN_NO.Location = New System.Drawing.Point(7, 144)
        Me.four_lbl_UN_NO.Name = "four_lbl_UN_NO"
        Me.four_lbl_UN_NO.Size = New System.Drawing.Size(53, 23)
        Me.four_lbl_UN_NO.TabIndex = 1159
        Me.four_lbl_UN_NO.Text = "危険物"
        Me.four_lbl_UN_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_MFG
        '
        Me.four_ref_M_HEADER_MFG.Enabled = False
        Me.four_ref_M_HEADER_MFG.Location = New System.Drawing.Point(64, 101)
        Me.four_ref_M_HEADER_MFG.MaxLength = 50
        Me.four_ref_M_HEADER_MFG.Name = "four_ref_M_HEADER_MFG"
        Me.four_ref_M_HEADER_MFG.Size = New System.Drawing.Size(404, 19)
        Me.four_ref_M_HEADER_MFG.TabIndex = 1168
        '
        'four_lbl_MFG
        '
        Me.four_lbl_MFG.Location = New System.Drawing.Point(3, 102)
        Me.four_lbl_MFG.Name = "four_lbl_MFG"
        Me.four_lbl_MFG.Size = New System.Drawing.Size(57, 23)
        Me.four_lbl_MFG.TabIndex = 1158
        Me.four_lbl_MFG.Text = "製造会社"
        Me.four_lbl_MFG.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'four_ref_M_HEADER_HAICHI_KIJYUN
        '
        Me.four_ref_M_HEADER_HAICHI_KIJYUN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.four_ref_M_HEADER_HAICHI_KIJYUN.Enabled = False
        Me.four_ref_M_HEADER_HAICHI_KIJYUN.FormattingEnabled = True
        Me.four_ref_M_HEADER_HAICHI_KIJYUN.Location = New System.Drawing.Point(543, 123)
        Me.four_ref_M_HEADER_HAICHI_KIJYUN.Name = "four_ref_M_HEADER_HAICHI_KIJYUN"
        Me.four_ref_M_HEADER_HAICHI_KIJYUN.Size = New System.Drawing.Size(133, 20)
        Me.four_ref_M_HEADER_HAICHI_KIJYUN.TabIndex = 1167
        Me.four_ref_M_HEADER_HAICHI_KIJYUN.Tag = "HAICHI_KIJYUN"
        '
        'four_lbl_HAICHI_KIJYUN
        '
        Me.four_lbl_HAICHI_KIJYUN.Location = New System.Drawing.Point(477, 125)
        Me.four_lbl_HAICHI_KIJYUN.Name = "four_lbl_HAICHI_KIJYUN"
        Me.four_lbl_HAICHI_KIJYUN.Size = New System.Drawing.Size(56, 16)
        Me.four_lbl_HAICHI_KIJYUN.TabIndex = 1156
        Me.four_lbl_HAICHI_KIJYUN.Text = "配置基準"
        Me.four_lbl_HAICHI_KIJYUN.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'gpx_M_PME_MFG_NO
        '
        Me.gpx_M_PME_MFG_NO.Controls.Add(Me.M_PME_NOURYOKU)
        Me.gpx_M_PME_MFG_NO.Controls.Add(Me.pgs)
        Me.gpx_M_PME_MFG_NO.Controls.Add(Me.four_lbl_M_PME_NOURYOKU)
        Me.gpx_M_PME_MFG_NO.Controls.Add(Me.M_PME_KOUSEI_COMPANY)
        Me.gpx_M_PME_MFG_NO.Controls.Add(Me.lbl_KOUSEI_COMPANY)
        Me.gpx_M_PME_MFG_NO.Controls.Add(Me.lbl_KOUSEI_BY_COMPANY)
        Me.gpx_M_PME_MFG_NO.Controls.Add(Me.M_PME_KOUSEI_BY_COMPANY)
        Me.gpx_M_PME_MFG_NO.Controls.Add(Me.M_PME_KOUSEI_KIKAN)
        Me.gpx_M_PME_MFG_NO.Controls.Add(Me.four_lbl_KOUSEI_KIKAN)
        Me.gpx_M_PME_MFG_NO.Location = New System.Drawing.Point(228, 198)
        Me.gpx_M_PME_MFG_NO.Name = "gpx_M_PME_MFG_NO"
        Me.gpx_M_PME_MFG_NO.Size = New System.Drawing.Size(258, 188)
        Me.gpx_M_PME_MFG_NO.TabIndex = 1
        Me.gpx_M_PME_MFG_NO.TabStop = False
        Me.gpx_M_PME_MFG_NO.Text = "校正先情報"
        '
        'M_PME_NOURYOKU
        '
        Me.M_PME_NOURYOKU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_PME_NOURYOKU.FormattingEnabled = True
        Me.M_PME_NOURYOKU.Location = New System.Drawing.Point(87, 94)
        Me.M_PME_NOURYOKU.Name = "M_PME_NOURYOKU"
        Me.M_PME_NOURYOKU.Size = New System.Drawing.Size(159, 20)
        Me.M_PME_NOURYOKU.TabIndex = 3
        '
        'pgs
        '
        Me.pgs.Location = New System.Drawing.Point(-7, 119)
        Me.pgs.MarqueeAnimationSpeed = 1
        Me.pgs.Name = "pgs"
        Me.pgs.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.pgs.Size = New System.Drawing.Size(242, 23)
        Me.pgs.Step = 1
        Me.pgs.TabIndex = 1015
        Me.pgs.Value = 1
        Me.pgs.Visible = False
        '
        'four_lbl_M_PME_NOURYOKU
        '
        Me.four_lbl_M_PME_NOURYOKU.Location = New System.Drawing.Point(14, 98)
        Me.four_lbl_M_PME_NOURYOKU.Name = "four_lbl_M_PME_NOURYOKU"
        Me.four_lbl_M_PME_NOURYOKU.Size = New System.Drawing.Size(66, 21)
        Me.four_lbl_M_PME_NOURYOKU.TabIndex = 1231
        Me.four_lbl_M_PME_NOURYOKU.Text = "委託先区分"
        Me.four_lbl_M_PME_NOURYOKU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_KOUSEI_COMPANY
        '
        Me.M_PME_KOUSEI_COMPANY.Location = New System.Drawing.Point(87, 20)
        Me.M_PME_KOUSEI_COMPANY.MaxLength = 50
        Me.M_PME_KOUSEI_COMPANY.Name = "M_PME_KOUSEI_COMPANY"
        Me.M_PME_KOUSEI_COMPANY.Size = New System.Drawing.Size(160, 19)
        Me.M_PME_KOUSEI_COMPANY.TabIndex = 0
        '
        'lbl_KOUSEI_COMPANY
        '
        Me.lbl_KOUSEI_COMPANY.Location = New System.Drawing.Point(12, 18)
        Me.lbl_KOUSEI_COMPANY.Name = "lbl_KOUSEI_COMPANY"
        Me.lbl_KOUSEI_COMPANY.Size = New System.Drawing.Size(68, 23)
        Me.lbl_KOUSEI_COMPANY.TabIndex = 402
        Me.lbl_KOUSEI_COMPANY.Text = "校正会社"
        Me.lbl_KOUSEI_COMPANY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbl_KOUSEI_BY_COMPANY
        '
        Me.lbl_KOUSEI_BY_COMPANY.Location = New System.Drawing.Point(2, 43)
        Me.lbl_KOUSEI_BY_COMPANY.Name = "lbl_KOUSEI_BY_COMPANY"
        Me.lbl_KOUSEI_BY_COMPANY.Size = New System.Drawing.Size(78, 23)
        Me.lbl_KOUSEI_BY_COMPANY.TabIndex = 404
        Me.lbl_KOUSEI_BY_COMPANY.Text = "校正仲介会社"
        Me.lbl_KOUSEI_BY_COMPANY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_KOUSEI_BY_COMPANY
        '
        Me.M_PME_KOUSEI_BY_COMPANY.Location = New System.Drawing.Point(87, 45)
        Me.M_PME_KOUSEI_BY_COMPANY.MaxLength = 50
        Me.M_PME_KOUSEI_BY_COMPANY.Name = "M_PME_KOUSEI_BY_COMPANY"
        Me.M_PME_KOUSEI_BY_COMPANY.Size = New System.Drawing.Size(159, 19)
        Me.M_PME_KOUSEI_BY_COMPANY.TabIndex = 1
        '
        'M_PME_KOUSEI_KIKAN
        '
        Me.M_PME_KOUSEI_KIKAN.Location = New System.Drawing.Point(87, 70)
        Me.M_PME_KOUSEI_KIKAN.MaxLength = 20
        Me.M_PME_KOUSEI_KIKAN.Name = "M_PME_KOUSEI_KIKAN"
        Me.M_PME_KOUSEI_KIKAN.Size = New System.Drawing.Size(160, 19)
        Me.M_PME_KOUSEI_KIKAN.TabIndex = 2
        Me.M_PME_KOUSEI_KIKAN.Tag = "NUMBER"
        Me.M_PME_KOUSEI_KIKAN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'four_lbl_KOUSEI_KIKAN
        '
        Me.four_lbl_KOUSEI_KIKAN.Location = New System.Drawing.Point(3, 68)
        Me.four_lbl_KOUSEI_KIKAN.Name = "four_lbl_KOUSEI_KIKAN"
        Me.four_lbl_KOUSEI_KIKAN.Size = New System.Drawing.Size(77, 23)
        Me.four_lbl_KOUSEI_KIKAN.TabIndex = 1161
        Me.four_lbl_KOUSEI_KIKAN.Text = "校正作業期間"
        Me.four_lbl_KOUSEI_KIKAN.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_REASON
        '
        Me.M_PME_REASON.Location = New System.Drawing.Point(64, 462)
        Me.M_PME_REASON.MaxLength = 1000
        Me.M_PME_REASON.Multiline = True
        Me.M_PME_REASON.Name = "M_PME_REASON"
        Me.M_PME_REASON.Size = New System.Drawing.Size(599, 32)
        Me.M_PME_REASON.TabIndex = 6
        '
        'four_lbl_REASON
        '
        Me.four_lbl_REASON.Location = New System.Drawing.Point(3, 462)
        Me.four_lbl_REASON.Name = "four_lbl_REASON"
        Me.four_lbl_REASON.Size = New System.Drawing.Size(64, 23)
        Me.four_lbl_REASON.TabIndex = 510
        Me.four_lbl_REASON.Text = "更新理由"
        Me.four_lbl_REASON.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_REMARKS
        '
        Me.M_PME_REMARKS.Location = New System.Drawing.Point(64, 428)
        Me.M_PME_REMARKS.MaxLength = 500
        Me.M_PME_REMARKS.Multiline = True
        Me.M_PME_REMARKS.Name = "M_PME_REMARKS"
        Me.M_PME_REMARKS.Size = New System.Drawing.Size(599, 32)
        Me.M_PME_REMARKS.TabIndex = 5
        '
        'four_lbl_REMARKS2
        '
        Me.four_lbl_REMARKS2.Location = New System.Drawing.Point(29, 425)
        Me.four_lbl_REMARKS2.Name = "four_lbl_REMARKS2"
        Me.four_lbl_REMARKS2.Size = New System.Drawing.Size(38, 23)
        Me.four_lbl_REMARKS2.TabIndex = 419
        Me.four_lbl_REMARKS2.Text = "備考"
        Me.four_lbl_REMARKS2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_PME_ALLOW_FLG
        '
        Me.M_PME_ALLOW_FLG.AutoSize = True
        Me.M_PME_ALLOW_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_PME_ALLOW_FLG.Location = New System.Drawing.Point(605, 320)
        Me.M_PME_ALLOW_FLG.Name = "M_PME_ALLOW_FLG"
        Me.M_PME_ALLOW_FLG.Size = New System.Drawing.Size(72, 16)
        Me.M_PME_ALLOW_FLG.TabIndex = 3
        Me.M_PME_ALLOW_FLG.Text = "閲覧許可"
        Me.M_PME_ALLOW_FLG.UseVisualStyleBackColor = True
        '
        'M_PME_STATUS
        '
        Me.M_PME_STATUS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_PME_STATUS.FormattingEnabled = True
        Me.M_PME_STATUS.Location = New System.Drawing.Point(546, 342)
        Me.M_PME_STATUS.Name = "M_PME_STATUS"
        Me.M_PME_STATUS.Size = New System.Drawing.Size(133, 20)
        Me.M_PME_STATUS.TabIndex = 4
        '
        'four_lbl_STATUS
        '
        Me.four_lbl_STATUS.Location = New System.Drawing.Point(488, 341)
        Me.four_lbl_STATUS.Name = "four_lbl_STATUS"
        Me.four_lbl_STATUS.Size = New System.Drawing.Size(52, 23)
        Me.four_lbl_STATUS.TabIndex = 407
        Me.four_lbl_STATUS.Text = "STATUS"
        Me.four_lbl_STATUS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox4.Controls.Add(Me.four_ref_M_HEADER_IMAGE)
        Me.GroupBox4.Location = New System.Drawing.Point(511, 189)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(168, 126)
        Me.GroupBox4.TabIndex = 1186
        Me.GroupBox4.TabStop = False
        '
        'four_ref_M_HEADER_IMAGE
        '
        Me.four_ref_M_HEADER_IMAGE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.four_ref_M_HEADER_IMAGE.Location = New System.Drawing.Point(2, 9)
        Me.four_ref_M_HEADER_IMAGE.Name = "four_ref_M_HEADER_IMAGE"
        Me.four_ref_M_HEADER_IMAGE.Size = New System.Drawing.Size(163, 115)
        Me.four_ref_M_HEADER_IMAGE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.four_ref_M_HEADER_IMAGE.TabIndex = 84
        Me.four_ref_M_HEADER_IMAGE.TabStop = False
        '
        'lbl_M_RENTAL
        '
        Me.lbl_M_RENTAL.BackColor = System.Drawing.SystemColors.Control
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_PARENT_NO)
        Me.lbl_M_RENTAL.Controls.Add(Me.Label4)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_KANRI_NO)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_KANRI_NO)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_MODEL)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_MODEL)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_KUBUN_01)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_KUBUN_01)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_BUNRUI_01)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_DOUTOUSEI)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_BUNRUI_01)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_DOUTOUSEI)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_TEKIGOUSEI)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_TEKIGOUSEI)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_NOURYOKU)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_NOURYOKU)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_LEASE_CONPANY)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_RENTAL_CONPANY)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_KIKAKU)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_KIKAKU)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_MODIFIED_BY)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_MODIFIED_BY)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_CREATED_DATE)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_CREATED_DATE)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_IMAGE)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_ATA)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_ATA)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_MFG_NO)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_MEISYOU)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_MEISYOU)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_MFG_NO)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_BASE)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_BASE)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_UN_NO)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_UN_NO)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_MFG)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_MFG)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_ref_M_HEADER_HAICHI_KIJYUN)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_HAICHI_KIJYUN)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_REASON)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_REASON)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_REMARKS)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_REMARKS2)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_ALLOW_FLG)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_DUE_DATE)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_DUE_DATE)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_KAKUNIN_DATE)
        Me.lbl_M_RENTAL.Controls.Add(Me.lbl_KAKUNIN_DATE)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_LEASE_DATE)
        Me.lbl_M_RENTAL.Controls.Add(Me.lbl_LEASE_DATE)
        Me.lbl_M_RENTAL.Controls.Add(Me.M_RENTAL_KEKKA)
        Me.lbl_M_RENTAL.Controls.Add(Me.six_lbl_KEKKA)
        Me.lbl_M_RENTAL.Controls.Add(Me.GroupBox6)
        Me.lbl_M_RENTAL.Location = New System.Drawing.Point(4, 22)
        Me.lbl_M_RENTAL.Name = "lbl_M_RENTAL"
        Me.lbl_M_RENTAL.Size = New System.Drawing.Size(703, 576)
        Me.lbl_M_RENTAL.TabIndex = 5
        Me.lbl_M_RENTAL.Tag = "M_RENTAL"
        Me.lbl_M_RENTAL.Text = "借用設備"
        '
        'six_ref_M_HEADER_PARENT_NO
        '
        Me.six_ref_M_HEADER_PARENT_NO.Enabled = False
        Me.six_ref_M_HEADER_PARENT_NO.Location = New System.Drawing.Point(74, 148)
        Me.six_ref_M_HEADER_PARENT_NO.MaxLength = 20
        Me.six_ref_M_HEADER_PARENT_NO.Name = "six_ref_M_HEADER_PARENT_NO"
        Me.six_ref_M_HEADER_PARENT_NO.Size = New System.Drawing.Size(372, 19)
        Me.six_ref_M_HEADER_PARENT_NO.TabIndex = 1240
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(19, 148)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 23)
        Me.Label4.TabIndex = 1241
        Me.Label4.Text = "親番号"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_KANRI_NO
        '
        Me.six_ref_M_HEADER_KANRI_NO.Enabled = False
        Me.six_ref_M_HEADER_KANRI_NO.Location = New System.Drawing.Point(74, 8)
        Me.six_ref_M_HEADER_KANRI_NO.MaxLength = 100
        Me.six_ref_M_HEADER_KANRI_NO.Name = "six_ref_M_HEADER_KANRI_NO"
        Me.six_ref_M_HEADER_KANRI_NO.Size = New System.Drawing.Size(268, 19)
        Me.six_ref_M_HEADER_KANRI_NO.TabIndex = 1238
        Me.six_ref_M_HEADER_KANRI_NO.Tag = "KANRI_NO"
        '
        'six_lbl_KANRI_NO
        '
        Me.six_lbl_KANRI_NO.Location = New System.Drawing.Point(14, 8)
        Me.six_lbl_KANRI_NO.Name = "six_lbl_KANRI_NO"
        Me.six_lbl_KANRI_NO.Size = New System.Drawing.Size(57, 18)
        Me.six_lbl_KANRI_NO.TabIndex = 1239
        Me.six_lbl_KANRI_NO.Text = "管理番号"
        Me.six_lbl_KANRI_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_MODEL
        '
        Me.six_ref_M_HEADER_MODEL.Enabled = False
        Me.six_ref_M_HEADER_MODEL.Location = New System.Drawing.Point(74, 53)
        Me.six_ref_M_HEADER_MODEL.MaxLength = 50
        Me.six_ref_M_HEADER_MODEL.Name = "six_ref_M_HEADER_MODEL"
        Me.six_ref_M_HEADER_MODEL.Size = New System.Drawing.Size(601, 19)
        Me.six_ref_M_HEADER_MODEL.TabIndex = 1237
        '
        'six_lbl_MODEL
        '
        Me.six_lbl_MODEL.Location = New System.Drawing.Point(37, 49)
        Me.six_lbl_MODEL.Name = "six_lbl_MODEL"
        Me.six_lbl_MODEL.Size = New System.Drawing.Size(34, 23)
        Me.six_lbl_MODEL.TabIndex = 1236
        Me.six_lbl_MODEL.Text = "型式"
        Me.six_lbl_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_KUBUN_01
        '
        Me.six_ref_M_HEADER_KUBUN_01.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.six_ref_M_HEADER_KUBUN_01.Enabled = False
        Me.six_ref_M_HEADER_KUBUN_01.FormattingEnabled = True
        Me.six_ref_M_HEADER_KUBUN_01.Location = New System.Drawing.Point(542, 143)
        Me.six_ref_M_HEADER_KUBUN_01.Name = "six_ref_M_HEADER_KUBUN_01"
        Me.six_ref_M_HEADER_KUBUN_01.Size = New System.Drawing.Size(132, 20)
        Me.six_ref_M_HEADER_KUBUN_01.TabIndex = 218
        '
        'six_lbl_KUBUN_01
        '
        Me.six_lbl_KUBUN_01.Location = New System.Drawing.Point(479, 144)
        Me.six_lbl_KUBUN_01.Name = "six_lbl_KUBUN_01"
        Me.six_lbl_KUBUN_01.Size = New System.Drawing.Size(56, 16)
        Me.six_lbl_KUBUN_01.TabIndex = 42
        Me.six_lbl_KUBUN_01.Text = "設備区分"
        Me.six_lbl_KUBUN_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_lbl_BUNRUI_01
        '
        Me.six_lbl_BUNRUI_01.Location = New System.Drawing.Point(344, 9)
        Me.six_lbl_BUNRUI_01.Name = "six_lbl_BUNRUI_01"
        Me.six_lbl_BUNRUI_01.Size = New System.Drawing.Size(58, 18)
        Me.six_lbl_BUNRUI_01.TabIndex = 214
        Me.six_lbl_BUNRUI_01.Text = "分類番号"
        Me.six_lbl_BUNRUI_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_DOUTOUSEI
        '
        Me.six_ref_M_HEADER_DOUTOUSEI.Enabled = False
        Me.six_ref_M_HEADER_DOUTOUSEI.Location = New System.Drawing.Point(74, 258)
        Me.six_ref_M_HEADER_DOUTOUSEI.MaxLength = 20
        Me.six_ref_M_HEADER_DOUTOUSEI.Name = "six_ref_M_HEADER_DOUTOUSEI"
        Me.six_ref_M_HEADER_DOUTOUSEI.Size = New System.Drawing.Size(133, 19)
        Me.six_ref_M_HEADER_DOUTOUSEI.TabIndex = 3
        '
        'six_ref_M_HEADER_BUNRUI_01
        '
        Me.six_ref_M_HEADER_BUNRUI_01.Enabled = False
        Me.six_ref_M_HEADER_BUNRUI_01.Location = New System.Drawing.Point(406, 8)
        Me.six_ref_M_HEADER_BUNRUI_01.MaxLength = 20
        Me.six_ref_M_HEADER_BUNRUI_01.Name = "six_ref_M_HEADER_BUNRUI_01"
        Me.six_ref_M_HEADER_BUNRUI_01.Size = New System.Drawing.Size(268, 19)
        Me.six_ref_M_HEADER_BUNRUI_01.TabIndex = 204
        Me.six_ref_M_HEADER_BUNRUI_01.Tag = "BUNRUI_01"
        '
        'six_lbl_DOUTOUSEI
        '
        Me.six_lbl_DOUTOUSEI.Location = New System.Drawing.Point(3, 256)
        Me.six_lbl_DOUTOUSEI.Name = "six_lbl_DOUTOUSEI"
        Me.six_lbl_DOUTOUSEI.Size = New System.Drawing.Size(68, 23)
        Me.six_lbl_DOUTOUSEI.TabIndex = 1232
        Me.six_lbl_DOUTOUSEI.Text = "同等性確認"
        Me.six_lbl_DOUTOUSEI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_RENTAL_TEKIGOUSEI
        '
        Me.M_RENTAL_TEKIGOUSEI.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_RENTAL_TEKIGOUSEI.Location = New System.Drawing.Point(317, 258)
        Me.M_RENTAL_TEKIGOUSEI.Name = "M_RENTAL_TEKIGOUSEI"
        Me.M_RENTAL_TEKIGOUSEI.ShowCheckBox = True
        Me.M_RENTAL_TEKIGOUSEI.Size = New System.Drawing.Size(137, 19)
        Me.M_RENTAL_TEKIGOUSEI.TabIndex = 4
        '
        'six_lbl_TEKIGOUSEI
        '
        Me.six_lbl_TEKIGOUSEI.Location = New System.Drawing.Point(245, 256)
        Me.six_lbl_TEKIGOUSEI.Name = "six_lbl_TEKIGOUSEI"
        Me.six_lbl_TEKIGOUSEI.Size = New System.Drawing.Size(66, 21)
        Me.six_lbl_TEKIGOUSEI.TabIndex = 1231
        Me.six_lbl_TEKIGOUSEI.Text = "適合性確認"
        Me.six_lbl_TEKIGOUSEI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_RENTAL_NOURYOKU
        '
        Me.M_RENTAL_NOURYOKU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_RENTAL_NOURYOKU.FormattingEnabled = True
        Me.M_RENTAL_NOURYOKU.Location = New System.Drawing.Point(317, 234)
        Me.M_RENTAL_NOURYOKU.Name = "M_RENTAL_NOURYOKU"
        Me.M_RENTAL_NOURYOKU.Size = New System.Drawing.Size(137, 20)
        Me.M_RENTAL_NOURYOKU.TabIndex = 3
        '
        'six_lbl_NOURYOKU
        '
        Me.six_lbl_NOURYOKU.Location = New System.Drawing.Point(253, 233)
        Me.six_lbl_NOURYOKU.Name = "six_lbl_NOURYOKU"
        Me.six_lbl_NOURYOKU.Size = New System.Drawing.Size(58, 23)
        Me.six_lbl_NOURYOKU.TabIndex = 1229
        Me.six_lbl_NOURYOKU.Text = "能力審査"
        Me.six_lbl_NOURYOKU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_RENTAL_LEASE_CONPANY
        '
        Me.M_RENTAL_LEASE_CONPANY.Location = New System.Drawing.Point(74, 235)
        Me.M_RENTAL_LEASE_CONPANY.MaxLength = 20
        Me.M_RENTAL_LEASE_CONPANY.Name = "M_RENTAL_LEASE_CONPANY"
        Me.M_RENTAL_LEASE_CONPANY.Size = New System.Drawing.Size(133, 19)
        Me.M_RENTAL_LEASE_CONPANY.TabIndex = 2
        '
        'six_lbl_RENTAL_CONPANY
        '
        Me.six_lbl_RENTAL_CONPANY.Location = New System.Drawing.Point(10, 233)
        Me.six_lbl_RENTAL_CONPANY.Name = "six_lbl_RENTAL_CONPANY"
        Me.six_lbl_RENTAL_CONPANY.Size = New System.Drawing.Size(61, 23)
        Me.six_lbl_RENTAL_CONPANY.TabIndex = 1227
        Me.six_lbl_RENTAL_CONPANY.Text = "借用先"
        Me.six_lbl_RENTAL_CONPANY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_KIKAKU
        '
        Me.six_ref_M_HEADER_KIKAKU.Enabled = False
        Me.six_ref_M_HEADER_KIKAKU.Location = New System.Drawing.Point(74, 76)
        Me.six_ref_M_HEADER_KIKAKU.MaxLength = 50
        Me.six_ref_M_HEADER_KIKAKU.Name = "six_ref_M_HEADER_KIKAKU"
        Me.six_ref_M_HEADER_KIKAKU.Size = New System.Drawing.Size(601, 19)
        Me.six_ref_M_HEADER_KIKAKU.TabIndex = 1225
        '
        'six_lbl_KIKAKU
        '
        Me.six_lbl_KIKAKU.Location = New System.Drawing.Point(19, 71)
        Me.six_lbl_KIKAKU.Name = "six_lbl_KIKAKU"
        Me.six_lbl_KIKAKU.Size = New System.Drawing.Size(52, 23)
        Me.six_lbl_KIKAKU.TabIndex = 1224
        Me.six_lbl_KIKAKU.Text = "規格"
        Me.six_lbl_KIKAKU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_MODIFIED_BY
        '
        Me.six_ref_M_HEADER_MODIFIED_BY.Enabled = False
        Me.six_ref_M_HEADER_MODIFIED_BY.Location = New System.Drawing.Point(317, 279)
        Me.six_ref_M_HEADER_MODIFIED_BY.Name = "six_ref_M_HEADER_MODIFIED_BY"
        Me.six_ref_M_HEADER_MODIFIED_BY.Size = New System.Drawing.Size(137, 19)
        Me.six_ref_M_HEADER_MODIFIED_BY.TabIndex = 1223
        '
        'six_lbl_MODIFIED_BY
        '
        Me.six_lbl_MODIFIED_BY.Location = New System.Drawing.Point(268, 277)
        Me.six_lbl_MODIFIED_BY.Name = "six_lbl_MODIFIED_BY"
        Me.six_lbl_MODIFIED_BY.Size = New System.Drawing.Size(43, 23)
        Me.six_lbl_MODIFIED_BY.TabIndex = 1222
        Me.six_lbl_MODIFIED_BY.Text = "更新者"
        Me.six_lbl_MODIFIED_BY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_CREATED_DATE
        '
        Me.six_ref_M_HEADER_CREATED_DATE.Enabled = False
        Me.six_ref_M_HEADER_CREATED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.six_ref_M_HEADER_CREATED_DATE.Location = New System.Drawing.Point(74, 279)
        Me.six_ref_M_HEADER_CREATED_DATE.Name = "six_ref_M_HEADER_CREATED_DATE"
        Me.six_ref_M_HEADER_CREATED_DATE.Size = New System.Drawing.Size(133, 19)
        Me.six_ref_M_HEADER_CREATED_DATE.TabIndex = 1221
        '
        'six_lbl_CREATED_DATE
        '
        Me.six_lbl_CREATED_DATE.Location = New System.Drawing.Point(20, 277)
        Me.six_lbl_CREATED_DATE.Name = "six_lbl_CREATED_DATE"
        Me.six_lbl_CREATED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.six_lbl_CREATED_DATE.TabIndex = 1220
        Me.six_lbl_CREATED_DATE.Text = "登録日"
        Me.six_lbl_CREATED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_lbl_IMAGE
        '
        Me.six_lbl_IMAGE.Location = New System.Drawing.Point(485, 182)
        Me.six_lbl_IMAGE.Name = "six_lbl_IMAGE"
        Me.six_lbl_IMAGE.Size = New System.Drawing.Size(44, 17)
        Me.six_lbl_IMAGE.TabIndex = 1218
        Me.six_lbl_IMAGE.Text = "写真"
        Me.six_lbl_IMAGE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_lbl_ATA
        '
        Me.six_lbl_ATA.Location = New System.Drawing.Point(369, 213)
        Me.six_lbl_ATA.Name = "six_lbl_ATA"
        Me.six_lbl_ATA.Size = New System.Drawing.Size(35, 23)
        Me.six_lbl_ATA.TabIndex = 1203
        Me.six_lbl_ATA.Text = "ATA"
        Me.six_lbl_ATA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_RENTAL_ATA
        '
        Me.M_RENTAL_ATA.Location = New System.Drawing.Point(409, 213)
        Me.M_RENTAL_ATA.MaxLength = 2
        Me.M_RENTAL_ATA.Name = "M_RENTAL_ATA"
        Me.M_RENTAL_ATA.Size = New System.Drawing.Size(45, 19)
        Me.M_RENTAL_ATA.TabIndex = 1
        Me.M_RENTAL_ATA.Tag = "NUMBER"
        Me.M_RENTAL_ATA.Text = "0"
        Me.M_RENTAL_ATA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'six_ref_M_HEADER_MFG_NO
        '
        Me.six_ref_M_HEADER_MFG_NO.Enabled = False
        Me.six_ref_M_HEADER_MFG_NO.Location = New System.Drawing.Point(74, 213)
        Me.six_ref_M_HEADER_MFG_NO.MaxLength = 30
        Me.six_ref_M_HEADER_MFG_NO.Name = "six_ref_M_HEADER_MFG_NO"
        Me.six_ref_M_HEADER_MFG_NO.Size = New System.Drawing.Size(133, 19)
        Me.six_ref_M_HEADER_MFG_NO.TabIndex = 0
        Me.six_ref_M_HEADER_MFG_NO.Tag = "BUNRUI_02"
        '
        'six_lbl_MEISYOU
        '
        Me.six_lbl_MEISYOU.Location = New System.Drawing.Point(37, 27)
        Me.six_lbl_MEISYOU.Name = "six_lbl_MEISYOU"
        Me.six_lbl_MEISYOU.Size = New System.Drawing.Size(34, 23)
        Me.six_lbl_MEISYOU.TabIndex = 1207
        Me.six_lbl_MEISYOU.Text = "名称"
        Me.six_lbl_MEISYOU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_MEISYOU
        '
        Me.six_ref_M_HEADER_MEISYOU.Enabled = False
        Me.six_ref_M_HEADER_MEISYOU.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.six_ref_M_HEADER_MEISYOU.Location = New System.Drawing.Point(74, 30)
        Me.six_ref_M_HEADER_MEISYOU.MaxLength = 100
        Me.six_ref_M_HEADER_MEISYOU.Name = "six_ref_M_HEADER_MEISYOU"
        Me.six_ref_M_HEADER_MEISYOU.Size = New System.Drawing.Size(600, 19)
        Me.six_ref_M_HEADER_MEISYOU.TabIndex = 1208
        Me.six_ref_M_HEADER_MEISYOU.Tag = "MEISYOU"
        '
        'six_lbl_MFG_NO
        '
        Me.six_lbl_MFG_NO.Location = New System.Drawing.Point(15, 211)
        Me.six_lbl_MFG_NO.Name = "six_lbl_MFG_NO"
        Me.six_lbl_MFG_NO.Size = New System.Drawing.Size(56, 23)
        Me.six_lbl_MFG_NO.TabIndex = 1201
        Me.six_lbl_MFG_NO.Text = "製造番号"
        Me.six_lbl_MFG_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_BASE
        '
        Me.six_ref_M_HEADER_BASE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.six_ref_M_HEADER_BASE.Enabled = False
        Me.six_ref_M_HEADER_BASE.FormattingEnabled = True
        Me.six_ref_M_HEADER_BASE.Location = New System.Drawing.Point(542, 121)
        Me.six_ref_M_HEADER_BASE.Name = "six_ref_M_HEADER_BASE"
        Me.six_ref_M_HEADER_BASE.Size = New System.Drawing.Size(133, 20)
        Me.six_ref_M_HEADER_BASE.TabIndex = 1215
        '
        'six_lbl_BASE
        '
        Me.six_lbl_BASE.Location = New System.Drawing.Point(478, 120)
        Me.six_lbl_BASE.Name = "six_lbl_BASE"
        Me.six_lbl_BASE.Size = New System.Drawing.Size(58, 23)
        Me.six_lbl_BASE.TabIndex = 1206
        Me.six_lbl_BASE.Text = "配置基地"
        Me.six_lbl_BASE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_UN_NO
        '
        Me.six_ref_M_HEADER_UN_NO.Enabled = False
        Me.six_ref_M_HEADER_UN_NO.Location = New System.Drawing.Point(74, 123)
        Me.six_ref_M_HEADER_UN_NO.MaxLength = 50
        Me.six_ref_M_HEADER_UN_NO.Name = "six_ref_M_HEADER_UN_NO"
        Me.six_ref_M_HEADER_UN_NO.Size = New System.Drawing.Size(372, 19)
        Me.six_ref_M_HEADER_UN_NO.TabIndex = 1214
        '
        'six_lbl_UN_NO
        '
        Me.six_lbl_UN_NO.Location = New System.Drawing.Point(18, 116)
        Me.six_lbl_UN_NO.Name = "six_lbl_UN_NO"
        Me.six_lbl_UN_NO.Size = New System.Drawing.Size(53, 23)
        Me.six_lbl_UN_NO.TabIndex = 1205
        Me.six_lbl_UN_NO.Text = "危険物"
        Me.six_lbl_UN_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_MFG
        '
        Me.six_ref_M_HEADER_MFG.Enabled = False
        Me.six_ref_M_HEADER_MFG.Location = New System.Drawing.Point(74, 99)
        Me.six_ref_M_HEADER_MFG.MaxLength = 50
        Me.six_ref_M_HEADER_MFG.Name = "six_ref_M_HEADER_MFG"
        Me.six_ref_M_HEADER_MFG.Size = New System.Drawing.Size(372, 19)
        Me.six_ref_M_HEADER_MFG.TabIndex = 1213
        '
        'six_lbl_MFG
        '
        Me.six_lbl_MFG.Location = New System.Drawing.Point(17, 93)
        Me.six_lbl_MFG.Name = "six_lbl_MFG"
        Me.six_lbl_MFG.Size = New System.Drawing.Size(54, 23)
        Me.six_lbl_MFG.TabIndex = 1204
        Me.six_lbl_MFG.Text = "製造会社"
        Me.six_lbl_MFG.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'six_ref_M_HEADER_HAICHI_KIJYUN
        '
        Me.six_ref_M_HEADER_HAICHI_KIJYUN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.six_ref_M_HEADER_HAICHI_KIJYUN.Enabled = False
        Me.six_ref_M_HEADER_HAICHI_KIJYUN.FormattingEnabled = True
        Me.six_ref_M_HEADER_HAICHI_KIJYUN.Location = New System.Drawing.Point(542, 99)
        Me.six_ref_M_HEADER_HAICHI_KIJYUN.Name = "six_ref_M_HEADER_HAICHI_KIJYUN"
        Me.six_ref_M_HEADER_HAICHI_KIJYUN.Size = New System.Drawing.Size(133, 20)
        Me.six_ref_M_HEADER_HAICHI_KIJYUN.TabIndex = 1212
        Me.six_ref_M_HEADER_HAICHI_KIJYUN.Tag = "HAICHI_KIJYUN"
        '
        'six_lbl_HAICHI_KIJYUN
        '
        Me.six_lbl_HAICHI_KIJYUN.Location = New System.Drawing.Point(479, 101)
        Me.six_lbl_HAICHI_KIJYUN.Name = "six_lbl_HAICHI_KIJYUN"
        Me.six_lbl_HAICHI_KIJYUN.Size = New System.Drawing.Size(56, 16)
        Me.six_lbl_HAICHI_KIJYUN.TabIndex = 1202
        Me.six_lbl_HAICHI_KIJYUN.Text = "配置基準"
        Me.six_lbl_HAICHI_KIJYUN.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_RENTAL_REASON
        '
        Me.M_RENTAL_REASON.Location = New System.Drawing.Point(74, 389)
        Me.M_RENTAL_REASON.MaxLength = 1000
        Me.M_RENTAL_REASON.Multiline = True
        Me.M_RENTAL_REASON.Name = "M_RENTAL_REASON"
        Me.M_RENTAL_REASON.Size = New System.Drawing.Size(607, 32)
        Me.M_RENTAL_REASON.TabIndex = 11
        '
        'six_lbl_REASON
        '
        Me.six_lbl_REASON.Location = New System.Drawing.Point(9, 389)
        Me.six_lbl_REASON.Name = "six_lbl_REASON"
        Me.six_lbl_REASON.Size = New System.Drawing.Size(62, 23)
        Me.six_lbl_REASON.TabIndex = 707
        Me.six_lbl_REASON.Text = "更新理由"
        Me.six_lbl_REASON.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_RENTAL_REMARKS
        '
        Me.M_RENTAL_REMARKS.Location = New System.Drawing.Point(74, 354)
        Me.M_RENTAL_REMARKS.MaxLength = 500
        Me.M_RENTAL_REMARKS.Multiline = True
        Me.M_RENTAL_REMARKS.Name = "M_RENTAL_REMARKS"
        Me.M_RENTAL_REMARKS.Size = New System.Drawing.Size(607, 32)
        Me.M_RENTAL_REMARKS.TabIndex = 10
        '
        'six_lbl_REMARKS2
        '
        Me.six_lbl_REMARKS2.Location = New System.Drawing.Point(25, 351)
        Me.six_lbl_REMARKS2.Name = "six_lbl_REMARKS2"
        Me.six_lbl_REMARKS2.Size = New System.Drawing.Size(46, 23)
        Me.six_lbl_REMARKS2.TabIndex = 432
        Me.six_lbl_REMARKS2.Text = "備考2"
        Me.six_lbl_REMARKS2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_RENTAL_ALLOW_FLG
        '
        Me.M_RENTAL_ALLOW_FLG.AutoSize = True
        Me.M_RENTAL_ALLOW_FLG.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.M_RENTAL_ALLOW_FLG.Location = New System.Drawing.Point(17, 304)
        Me.M_RENTAL_ALLOW_FLG.Name = "M_RENTAL_ALLOW_FLG"
        Me.M_RENTAL_ALLOW_FLG.Size = New System.Drawing.Size(72, 16)
        Me.M_RENTAL_ALLOW_FLG.TabIndex = 5
        Me.M_RENTAL_ALLOW_FLG.Text = "閲覧許可"
        Me.M_RENTAL_ALLOW_FLG.UseVisualStyleBackColor = True
        '
        'M_RENTAL_DUE_DATE
        '
        Me.M_RENTAL_DUE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_RENTAL_DUE_DATE.Location = New System.Drawing.Point(549, 332)
        Me.M_RENTAL_DUE_DATE.Name = "M_RENTAL_DUE_DATE"
        Me.M_RENTAL_DUE_DATE.ShowCheckBox = True
        Me.M_RENTAL_DUE_DATE.Size = New System.Drawing.Size(133, 19)
        Me.M_RENTAL_DUE_DATE.TabIndex = 9
        '
        'six_lbl_DUE_DATE
        '
        Me.six_lbl_DUE_DATE.Location = New System.Drawing.Point(456, 331)
        Me.six_lbl_DUE_DATE.Name = "six_lbl_DUE_DATE"
        Me.six_lbl_DUE_DATE.Size = New System.Drawing.Size(83, 23)
        Me.six_lbl_DUE_DATE.TabIndex = 430
        Me.six_lbl_DUE_DATE.Text = "有効期限"
        Me.six_lbl_DUE_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_RENTAL_KAKUNIN_DATE
        '
        Me.M_RENTAL_KAKUNIN_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_RENTAL_KAKUNIN_DATE.Location = New System.Drawing.Point(317, 325)
        Me.M_RENTAL_KAKUNIN_DATE.Name = "M_RENTAL_KAKUNIN_DATE"
        Me.M_RENTAL_KAKUNIN_DATE.ShowCheckBox = True
        Me.M_RENTAL_KAKUNIN_DATE.Size = New System.Drawing.Size(137, 19)
        Me.M_RENTAL_KAKUNIN_DATE.TabIndex = 8
        '
        'lbl_KAKUNIN_DATE
        '
        Me.lbl_KAKUNIN_DATE.Location = New System.Drawing.Point(211, 324)
        Me.lbl_KAKUNIN_DATE.Name = "lbl_KAKUNIN_DATE"
        Me.lbl_KAKUNIN_DATE.Size = New System.Drawing.Size(100, 23)
        Me.lbl_KAKUNIN_DATE.TabIndex = 429
        Me.lbl_KAKUNIN_DATE.Text = "校正結果確認日"
        Me.lbl_KAKUNIN_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_RENTAL_LEASE_DATE
        '
        Me.M_RENTAL_LEASE_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_RENTAL_LEASE_DATE.Location = New System.Drawing.Point(70, 325)
        Me.M_RENTAL_LEASE_DATE.Name = "M_RENTAL_LEASE_DATE"
        Me.M_RENTAL_LEASE_DATE.ShowCheckBox = True
        Me.M_RENTAL_LEASE_DATE.Size = New System.Drawing.Size(133, 19)
        Me.M_RENTAL_LEASE_DATE.TabIndex = 7
        '
        'lbl_LEASE_DATE
        '
        Me.lbl_LEASE_DATE.Location = New System.Drawing.Point(18, 324)
        Me.lbl_LEASE_DATE.Name = "lbl_LEASE_DATE"
        Me.lbl_LEASE_DATE.Size = New System.Drawing.Size(53, 23)
        Me.lbl_LEASE_DATE.TabIndex = 428
        Me.lbl_LEASE_DATE.Text = "借用日"
        Me.lbl_LEASE_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_RENTAL_KEKKA
        '
        Me.M_RENTAL_KEKKA.Location = New System.Drawing.Point(317, 302)
        Me.M_RENTAL_KEKKA.MaxLength = 20
        Me.M_RENTAL_KEKKA.Name = "M_RENTAL_KEKKA"
        Me.M_RENTAL_KEKKA.Size = New System.Drawing.Size(137, 19)
        Me.M_RENTAL_KEKKA.TabIndex = 6
        '
        'six_lbl_KEKKA
        '
        Me.six_lbl_KEKKA.Location = New System.Drawing.Point(258, 300)
        Me.six_lbl_KEKKA.Name = "six_lbl_KEKKA"
        Me.six_lbl_KEKKA.Size = New System.Drawing.Size(53, 23)
        Me.six_lbl_KEKKA.TabIndex = 427
        Me.six_lbl_KEKKA.Text = "合否"
        Me.six_lbl_KEKKA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.six_ref_M_HEADER_IMAGE)
        Me.GroupBox6.Location = New System.Drawing.Point(514, 199)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(168, 126)
        Me.GroupBox6.TabIndex = 1235
        Me.GroupBox6.TabStop = False
        '
        'six_ref_M_HEADER_IMAGE
        '
        Me.six_ref_M_HEADER_IMAGE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.six_ref_M_HEADER_IMAGE.Location = New System.Drawing.Point(2, 9)
        Me.six_ref_M_HEADER_IMAGE.Name = "six_ref_M_HEADER_IMAGE"
        Me.six_ref_M_HEADER_IMAGE.Size = New System.Drawing.Size(163, 115)
        Me.six_ref_M_HEADER_IMAGE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.six_ref_M_HEADER_IMAGE.TabIndex = 84
        Me.six_ref_M_HEADER_IMAGE.TabStop = False
        '
        'lbl_M_LEASE
        '
        Me.lbl_M_LEASE.BackColor = System.Drawing.SystemColors.Control
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_PARENT_NO)
        Me.lbl_M_LEASE.Controls.Add(Me.Label8)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_MODEL)
        Me.lbl_M_LEASE.Controls.Add(Me.eightt_lbl_MODEL)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_KANRI_NO)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_KANRI_NO)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_BUNRUI_01)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_KIKAKU)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_BUNRUI_01)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_KIKAKU)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_MODIFIED_DATE)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_MODIFIED_DATE)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_MODIFIED_BY)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_MODIFIED_BY)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_CREATED_DATE)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_CREATED_DATE)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_IMAGE)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_MFG_NO)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_MEISYOU)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_MEISYOU)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_MFG_NO)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_ref_M_HEADER_MFG)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_MFG)
        Me.lbl_M_LEASE.Controls.Add(Me.M_LEASE_REASON)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_REASON)
        Me.lbl_M_LEASE.Controls.Add(Me.M_LEASE_REMARKS)
        Me.lbl_M_LEASE.Controls.Add(Me.eight_lbl_REMARKS2)
        Me.lbl_M_LEASE.Controls.Add(Me.M_LEASE_LEASE_END_DATE)
        Me.lbl_M_LEASE.Controls.Add(Me.lbl_LEASE_END_DATE)
        Me.lbl_M_LEASE.Controls.Add(Me.M_LEASE_LEASE_START_DATE)
        Me.lbl_M_LEASE.Controls.Add(Me.lbl_LEASE_START_DATE)
        Me.lbl_M_LEASE.Controls.Add(Me.M_LEASE_LEASE_COUNT)
        Me.lbl_M_LEASE.Controls.Add(Me.lbl_LEASE_COUNT)
        Me.lbl_M_LEASE.Controls.Add(Me.M_LEASE_FIRST_LOCATION)
        Me.lbl_M_LEASE.Controls.Add(Me.lbl_FIRST_LOCATION)
        Me.lbl_M_LEASE.Controls.Add(Me.M_LEASE_LEASE_NO)
        Me.lbl_M_LEASE.Controls.Add(Me.lbl_LEASE_NO)
        Me.lbl_M_LEASE.Controls.Add(Me.M_LEASE_LEASE_COMPANY)
        Me.lbl_M_LEASE.Controls.Add(Me.lbl_LEASE_COMPANY)
        Me.lbl_M_LEASE.Controls.Add(Me.GroupBox8)
        Me.lbl_M_LEASE.Location = New System.Drawing.Point(4, 22)
        Me.lbl_M_LEASE.Name = "lbl_M_LEASE"
        Me.lbl_M_LEASE.Size = New System.Drawing.Size(703, 576)
        Me.lbl_M_LEASE.TabIndex = 8
        Me.lbl_M_LEASE.Tag = "M_LEASE"
        Me.lbl_M_LEASE.Text = "リース"
        '
        'eight_ref_M_HEADER_PARENT_NO
        '
        Me.eight_ref_M_HEADER_PARENT_NO.Enabled = False
        Me.eight_ref_M_HEADER_PARENT_NO.Location = New System.Drawing.Point(68, 129)
        Me.eight_ref_M_HEADER_PARENT_NO.MaxLength = 20
        Me.eight_ref_M_HEADER_PARENT_NO.Name = "eight_ref_M_HEADER_PARENT_NO"
        Me.eight_ref_M_HEADER_PARENT_NO.Size = New System.Drawing.Size(379, 19)
        Me.eight_ref_M_HEADER_PARENT_NO.TabIndex = 1283
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(10, 129)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 23)
        Me.Label8.TabIndex = 1284
        Me.Label8.Text = "親番号"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_ref_M_HEADER_MODEL
        '
        Me.eight_ref_M_HEADER_MODEL.Enabled = False
        Me.eight_ref_M_HEADER_MODEL.Location = New System.Drawing.Point(68, 56)
        Me.eight_ref_M_HEADER_MODEL.MaxLength = 50
        Me.eight_ref_M_HEADER_MODEL.Name = "eight_ref_M_HEADER_MODEL"
        Me.eight_ref_M_HEADER_MODEL.Size = New System.Drawing.Size(595, 19)
        Me.eight_ref_M_HEADER_MODEL.TabIndex = 1282
        '
        'eightt_lbl_MODEL
        '
        Me.eightt_lbl_MODEL.Location = New System.Drawing.Point(27, 55)
        Me.eightt_lbl_MODEL.Name = "eightt_lbl_MODEL"
        Me.eightt_lbl_MODEL.Size = New System.Drawing.Size(34, 23)
        Me.eightt_lbl_MODEL.TabIndex = 1281
        Me.eightt_lbl_MODEL.Text = "型式"
        Me.eightt_lbl_MODEL.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_ref_M_HEADER_KANRI_NO
        '
        Me.eight_ref_M_HEADER_KANRI_NO.Enabled = False
        Me.eight_ref_M_HEADER_KANRI_NO.Location = New System.Drawing.Point(68, 8)
        Me.eight_ref_M_HEADER_KANRI_NO.MaxLength = 100
        Me.eight_ref_M_HEADER_KANRI_NO.Name = "eight_ref_M_HEADER_KANRI_NO"
        Me.eight_ref_M_HEADER_KANRI_NO.Size = New System.Drawing.Size(268, 19)
        Me.eight_ref_M_HEADER_KANRI_NO.TabIndex = 1279
        Me.eight_ref_M_HEADER_KANRI_NO.Tag = "KANRI_NO"
        '
        'eight_lbl_KANRI_NO
        '
        Me.eight_lbl_KANRI_NO.Location = New System.Drawing.Point(6, 8)
        Me.eight_lbl_KANRI_NO.Name = "eight_lbl_KANRI_NO"
        Me.eight_lbl_KANRI_NO.Size = New System.Drawing.Size(57, 23)
        Me.eight_lbl_KANRI_NO.TabIndex = 1280
        Me.eight_lbl_KANRI_NO.Text = "管理番号"
        Me.eight_lbl_KANRI_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_lbl_BUNRUI_01
        '
        Me.eight_lbl_BUNRUI_01.Location = New System.Drawing.Point(342, 11)
        Me.eight_lbl_BUNRUI_01.Name = "eight_lbl_BUNRUI_01"
        Me.eight_lbl_BUNRUI_01.Size = New System.Drawing.Size(61, 16)
        Me.eight_lbl_BUNRUI_01.TabIndex = 214
        Me.eight_lbl_BUNRUI_01.Text = "分類番号"
        Me.eight_lbl_BUNRUI_01.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_ref_M_HEADER_KIKAKU
        '
        Me.eight_ref_M_HEADER_KIKAKU.Enabled = False
        Me.eight_ref_M_HEADER_KIKAKU.Location = New System.Drawing.Point(68, 80)
        Me.eight_ref_M_HEADER_KIKAKU.MaxLength = 50
        Me.eight_ref_M_HEADER_KIKAKU.Name = "eight_ref_M_HEADER_KIKAKU"
        Me.eight_ref_M_HEADER_KIKAKU.Size = New System.Drawing.Size(595, 19)
        Me.eight_ref_M_HEADER_KIKAKU.TabIndex = 1229
        '
        'eight_ref_M_HEADER_BUNRUI_01
        '
        Me.eight_ref_M_HEADER_BUNRUI_01.Enabled = False
        Me.eight_ref_M_HEADER_BUNRUI_01.Location = New System.Drawing.Point(407, 7)
        Me.eight_ref_M_HEADER_BUNRUI_01.MaxLength = 20
        Me.eight_ref_M_HEADER_BUNRUI_01.Name = "eight_ref_M_HEADER_BUNRUI_01"
        Me.eight_ref_M_HEADER_BUNRUI_01.Size = New System.Drawing.Size(268, 19)
        Me.eight_ref_M_HEADER_BUNRUI_01.TabIndex = 204
        Me.eight_ref_M_HEADER_BUNRUI_01.Tag = "BUNRUI_01"
        '
        'eight_lbl_KIKAKU
        '
        Me.eight_lbl_KIKAKU.Location = New System.Drawing.Point(23, 81)
        Me.eight_lbl_KIKAKU.Name = "eight_lbl_KIKAKU"
        Me.eight_lbl_KIKAKU.Size = New System.Drawing.Size(39, 16)
        Me.eight_lbl_KIKAKU.TabIndex = 1228
        Me.eight_lbl_KIKAKU.Text = "規格"
        Me.eight_lbl_KIKAKU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_ref_M_HEADER_MODIFIED_DATE
        '
        Me.eight_ref_M_HEADER_MODIFIED_DATE.Enabled = False
        Me.eight_ref_M_HEADER_MODIFIED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.eight_ref_M_HEADER_MODIFIED_DATE.Location = New System.Drawing.Point(332, 241)
        Me.eight_ref_M_HEADER_MODIFIED_DATE.Name = "eight_ref_M_HEADER_MODIFIED_DATE"
        Me.eight_ref_M_HEADER_MODIFIED_DATE.Size = New System.Drawing.Size(133, 19)
        Me.eight_ref_M_HEADER_MODIFIED_DATE.TabIndex = 1227
        '
        'eight_lbl_MODIFIED_DATE
        '
        Me.eight_lbl_MODIFIED_DATE.Location = New System.Drawing.Point(274, 239)
        Me.eight_lbl_MODIFIED_DATE.Name = "eight_lbl_MODIFIED_DATE"
        Me.eight_lbl_MODIFIED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.eight_lbl_MODIFIED_DATE.TabIndex = 1225
        Me.eight_lbl_MODIFIED_DATE.Text = "更新日"
        Me.eight_lbl_MODIFIED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_ref_M_HEADER_MODIFIED_BY
        '
        Me.eight_ref_M_HEADER_MODIFIED_BY.Enabled = False
        Me.eight_ref_M_HEADER_MODIFIED_BY.Location = New System.Drawing.Point(332, 217)
        Me.eight_ref_M_HEADER_MODIFIED_BY.Name = "eight_ref_M_HEADER_MODIFIED_BY"
        Me.eight_ref_M_HEADER_MODIFIED_BY.Size = New System.Drawing.Size(133, 19)
        Me.eight_ref_M_HEADER_MODIFIED_BY.TabIndex = 1226
        '
        'eight_lbl_MODIFIED_BY
        '
        Me.eight_lbl_MODIFIED_BY.Location = New System.Drawing.Point(285, 215)
        Me.eight_lbl_MODIFIED_BY.Name = "eight_lbl_MODIFIED_BY"
        Me.eight_lbl_MODIFIED_BY.Size = New System.Drawing.Size(43, 23)
        Me.eight_lbl_MODIFIED_BY.TabIndex = 1224
        Me.eight_lbl_MODIFIED_BY.Text = "更新者"
        Me.eight_lbl_MODIFIED_BY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_ref_M_HEADER_CREATED_DATE
        '
        Me.eight_ref_M_HEADER_CREATED_DATE.Enabled = False
        Me.eight_ref_M_HEADER_CREATED_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.eight_ref_M_HEADER_CREATED_DATE.Location = New System.Drawing.Point(85, 241)
        Me.eight_ref_M_HEADER_CREATED_DATE.Name = "eight_ref_M_HEADER_CREATED_DATE"
        Me.eight_ref_M_HEADER_CREATED_DATE.Size = New System.Drawing.Size(133, 19)
        Me.eight_ref_M_HEADER_CREATED_DATE.TabIndex = 1223
        '
        'eight_lbl_CREATED_DATE
        '
        Me.eight_lbl_CREATED_DATE.Location = New System.Drawing.Point(34, 239)
        Me.eight_lbl_CREATED_DATE.Name = "eight_lbl_CREATED_DATE"
        Me.eight_lbl_CREATED_DATE.Size = New System.Drawing.Size(51, 23)
        Me.eight_lbl_CREATED_DATE.TabIndex = 1222
        Me.eight_lbl_CREATED_DATE.Text = "登録日"
        Me.eight_lbl_CREATED_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_lbl_IMAGE
        '
        Me.eight_lbl_IMAGE.Location = New System.Drawing.Point(496, 112)
        Me.eight_lbl_IMAGE.Name = "eight_lbl_IMAGE"
        Me.eight_lbl_IMAGE.Size = New System.Drawing.Size(44, 17)
        Me.eight_lbl_IMAGE.TabIndex = 1220
        Me.eight_lbl_IMAGE.Text = "写真"
        Me.eight_lbl_IMAGE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_ref_M_HEADER_MFG_NO
        '
        Me.eight_ref_M_HEADER_MFG_NO.Enabled = False
        Me.eight_ref_M_HEADER_MFG_NO.Location = New System.Drawing.Point(85, 217)
        Me.eight_ref_M_HEADER_MFG_NO.MaxLength = 100
        Me.eight_ref_M_HEADER_MFG_NO.Name = "eight_ref_M_HEADER_MFG_NO"
        Me.eight_ref_M_HEADER_MFG_NO.Size = New System.Drawing.Size(133, 19)
        Me.eight_ref_M_HEADER_MFG_NO.TabIndex = 1216
        Me.eight_ref_M_HEADER_MFG_NO.Tag = "BUNRUI_02"
        '
        'eight_lbl_MEISYOU
        '
        Me.eight_lbl_MEISYOU.Location = New System.Drawing.Point(28, 31)
        Me.eight_lbl_MEISYOU.Name = "eight_lbl_MEISYOU"
        Me.eight_lbl_MEISYOU.Size = New System.Drawing.Size(34, 23)
        Me.eight_lbl_MEISYOU.TabIndex = 1213
        Me.eight_lbl_MEISYOU.Text = "名称"
        Me.eight_lbl_MEISYOU.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_ref_M_HEADER_MEISYOU
        '
        Me.eight_ref_M_HEADER_MEISYOU.Enabled = False
        Me.eight_ref_M_HEADER_MEISYOU.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.eight_ref_M_HEADER_MEISYOU.Location = New System.Drawing.Point(68, 32)
        Me.eight_ref_M_HEADER_MEISYOU.MaxLength = 100
        Me.eight_ref_M_HEADER_MEISYOU.Name = "eight_ref_M_HEADER_MEISYOU"
        Me.eight_ref_M_HEADER_MEISYOU.Size = New System.Drawing.Size(607, 19)
        Me.eight_ref_M_HEADER_MEISYOU.TabIndex = 1214
        Me.eight_ref_M_HEADER_MEISYOU.Tag = "MEISYOU"
        '
        'eight_lbl_MFG_NO
        '
        Me.eight_lbl_MFG_NO.Location = New System.Drawing.Point(29, 215)
        Me.eight_lbl_MFG_NO.Name = "eight_lbl_MFG_NO"
        Me.eight_lbl_MFG_NO.Size = New System.Drawing.Size(56, 23)
        Me.eight_lbl_MFG_NO.TabIndex = 1211
        Me.eight_lbl_MFG_NO.Text = "製造番号"
        Me.eight_lbl_MFG_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'eight_ref_M_HEADER_MFG
        '
        Me.eight_ref_M_HEADER_MFG.Enabled = False
        Me.eight_ref_M_HEADER_MFG.Location = New System.Drawing.Point(68, 104)
        Me.eight_ref_M_HEADER_MFG.MaxLength = 50
        Me.eight_ref_M_HEADER_MFG.Name = "eight_ref_M_HEADER_MFG"
        Me.eight_ref_M_HEADER_MFG.Size = New System.Drawing.Size(379, 19)
        Me.eight_ref_M_HEADER_MFG.TabIndex = 1217
        '
        'eight_lbl_MFG
        '
        Me.eight_lbl_MFG.Location = New System.Drawing.Point(3, 103)
        Me.eight_lbl_MFG.Name = "eight_lbl_MFG"
        Me.eight_lbl_MFG.Size = New System.Drawing.Size(60, 23)
        Me.eight_lbl_MFG.TabIndex = 1212
        Me.eight_lbl_MFG.Text = "製造会社"
        Me.eight_lbl_MFG.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_LEASE_REASON
        '
        Me.M_LEASE_REASON.Location = New System.Drawing.Point(85, 396)
        Me.M_LEASE_REASON.MaxLength = 1000
        Me.M_LEASE_REASON.Multiline = True
        Me.M_LEASE_REASON.Name = "M_LEASE_REASON"
        Me.M_LEASE_REASON.Size = New System.Drawing.Size(595, 32)
        Me.M_LEASE_REASON.TabIndex = 909
        '
        'eight_lbl_REASON
        '
        Me.eight_lbl_REASON.Location = New System.Drawing.Point(16, 396)
        Me.eight_lbl_REASON.Name = "eight_lbl_REASON"
        Me.eight_lbl_REASON.Size = New System.Drawing.Size(68, 23)
        Me.eight_lbl_REASON.TabIndex = 908
        Me.eight_lbl_REASON.Text = "更新理由"
        Me.eight_lbl_REASON.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_LEASE_REMARKS
        '
        Me.M_LEASE_REMARKS.Location = New System.Drawing.Point(85, 360)
        Me.M_LEASE_REMARKS.MaxLength = 500
        Me.M_LEASE_REMARKS.Multiline = True
        Me.M_LEASE_REMARKS.Name = "M_LEASE_REMARKS"
        Me.M_LEASE_REMARKS.Size = New System.Drawing.Size(595, 32)
        Me.M_LEASE_REMARKS.TabIndex = 907
        '
        'eight_lbl_REMARKS2
        '
        Me.eight_lbl_REMARKS2.Location = New System.Drawing.Point(38, 360)
        Me.eight_lbl_REMARKS2.Name = "eight_lbl_REMARKS2"
        Me.eight_lbl_REMARKS2.Size = New System.Drawing.Size(46, 23)
        Me.eight_lbl_REMARKS2.TabIndex = 515
        Me.eight_lbl_REMARKS2.Text = "備考2"
        Me.eight_lbl_REMARKS2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_LEASE_LEASE_END_DATE
        '
        Me.M_LEASE_LEASE_END_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_LEASE_LEASE_END_DATE.Location = New System.Drawing.Point(331, 338)
        Me.M_LEASE_LEASE_END_DATE.Name = "M_LEASE_LEASE_END_DATE"
        Me.M_LEASE_LEASE_END_DATE.ShowCheckBox = True
        Me.M_LEASE_LEASE_END_DATE.Size = New System.Drawing.Size(133, 19)
        Me.M_LEASE_LEASE_END_DATE.TabIndex = 906
        '
        'lbl_LEASE_END_DATE
        '
        Me.lbl_LEASE_END_DATE.Location = New System.Drawing.Point(248, 338)
        Me.lbl_LEASE_END_DATE.Name = "lbl_LEASE_END_DATE"
        Me.lbl_LEASE_END_DATE.Size = New System.Drawing.Size(79, 23)
        Me.lbl_LEASE_END_DATE.TabIndex = 512
        Me.lbl_LEASE_END_DATE.Text = "リース満了日"
        Me.lbl_LEASE_END_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_LEASE_LEASE_START_DATE
        '
        Me.M_LEASE_LEASE_START_DATE.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.M_LEASE_LEASE_START_DATE.Location = New System.Drawing.Point(86, 339)
        Me.M_LEASE_LEASE_START_DATE.Name = "M_LEASE_LEASE_START_DATE"
        Me.M_LEASE_LEASE_START_DATE.ShowCheckBox = True
        Me.M_LEASE_LEASE_START_DATE.Size = New System.Drawing.Size(133, 19)
        Me.M_LEASE_LEASE_START_DATE.TabIndex = 905
        '
        'lbl_LEASE_START_DATE
        '
        Me.lbl_LEASE_START_DATE.Location = New System.Drawing.Point(5, 338)
        Me.lbl_LEASE_START_DATE.Name = "lbl_LEASE_START_DATE"
        Me.lbl_LEASE_START_DATE.Size = New System.Drawing.Size(79, 23)
        Me.lbl_LEASE_START_DATE.TabIndex = 511
        Me.lbl_LEASE_START_DATE.Text = "リース開始日"
        Me.lbl_LEASE_START_DATE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_LEASE_LEASE_COUNT
        '
        Me.M_LEASE_LEASE_COUNT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_LEASE_LEASE_COUNT.FormattingEnabled = True
        Me.M_LEASE_LEASE_COUNT.Location = New System.Drawing.Point(331, 312)
        Me.M_LEASE_LEASE_COUNT.Name = "M_LEASE_LEASE_COUNT"
        Me.M_LEASE_LEASE_COUNT.Size = New System.Drawing.Size(133, 20)
        Me.M_LEASE_LEASE_COUNT.TabIndex = 904
        '
        'lbl_LEASE_COUNT
        '
        Me.lbl_LEASE_COUNT.Location = New System.Drawing.Point(244, 313)
        Me.lbl_LEASE_COUNT.Name = "lbl_LEASE_COUNT"
        Me.lbl_LEASE_COUNT.Size = New System.Drawing.Size(83, 23)
        Me.lbl_LEASE_COUNT.TabIndex = 509
        Me.lbl_LEASE_COUNT.Text = "リース回数"
        Me.lbl_LEASE_COUNT.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_LEASE_FIRST_LOCATION
        '
        Me.M_LEASE_FIRST_LOCATION.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.M_LEASE_FIRST_LOCATION.FormattingEnabled = True
        Me.M_LEASE_FIRST_LOCATION.Location = New System.Drawing.Point(86, 313)
        Me.M_LEASE_FIRST_LOCATION.Name = "M_LEASE_FIRST_LOCATION"
        Me.M_LEASE_FIRST_LOCATION.Size = New System.Drawing.Size(133, 20)
        Me.M_LEASE_FIRST_LOCATION.TabIndex = 903
        '
        'lbl_FIRST_LOCATION
        '
        Me.lbl_FIRST_LOCATION.Location = New System.Drawing.Point(3, 312)
        Me.lbl_FIRST_LOCATION.Name = "lbl_FIRST_LOCATION"
        Me.lbl_FIRST_LOCATION.Size = New System.Drawing.Size(83, 23)
        Me.lbl_FIRST_LOCATION.TabIndex = 507
        Me.lbl_FIRST_LOCATION.Text = "初回配置基地"
        Me.lbl_FIRST_LOCATION.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_LEASE_LEASE_NO
        '
        Me.M_LEASE_LEASE_NO.Location = New System.Drawing.Point(85, 288)
        Me.M_LEASE_LEASE_NO.MaxLength = 50
        Me.M_LEASE_LEASE_NO.Name = "M_LEASE_LEASE_NO"
        Me.M_LEASE_LEASE_NO.Size = New System.Drawing.Size(595, 19)
        Me.M_LEASE_LEASE_NO.TabIndex = 902
        '
        'lbl_LEASE_NO
        '
        Me.lbl_LEASE_NO.Location = New System.Drawing.Point(-7, 286)
        Me.lbl_LEASE_NO.Name = "lbl_LEASE_NO"
        Me.lbl_LEASE_NO.Size = New System.Drawing.Size(92, 23)
        Me.lbl_LEASE_NO.TabIndex = 504
        Me.lbl_LEASE_NO.Text = "リース契約番号"
        Me.lbl_LEASE_NO.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'M_LEASE_LEASE_COMPANY
        '
        Me.M_LEASE_LEASE_COMPANY.Location = New System.Drawing.Point(85, 264)
        Me.M_LEASE_LEASE_COMPANY.MaxLength = 20
        Me.M_LEASE_LEASE_COMPANY.Name = "M_LEASE_LEASE_COMPANY"
        Me.M_LEASE_LEASE_COMPANY.Size = New System.Drawing.Size(595, 19)
        Me.M_LEASE_LEASE_COMPANY.TabIndex = 505
        Me.M_LEASE_LEASE_COMPANY.Tag = "901"
        '
        'lbl_LEASE_COMPANY
        '
        Me.lbl_LEASE_COMPANY.Location = New System.Drawing.Point(9, 261)
        Me.lbl_LEASE_COMPANY.Name = "lbl_LEASE_COMPANY"
        Me.lbl_LEASE_COMPANY.Size = New System.Drawing.Size(76, 23)
        Me.lbl_LEASE_COMPANY.TabIndex = 503
        Me.lbl_LEASE_COMPANY.Text = "リース会社"
        Me.lbl_LEASE_COMPANY.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox8
        '
        Me.GroupBox8.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox8.Controls.Add(Me.eight_ref_M_HEADER_IMAGE)
        Me.GroupBox8.Location = New System.Drawing.Point(512, 132)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(168, 126)
        Me.GroupBox8.TabIndex = 1230
        Me.GroupBox8.TabStop = False
        '
        'eight_ref_M_HEADER_IMAGE
        '
        Me.eight_ref_M_HEADER_IMAGE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.eight_ref_M_HEADER_IMAGE.Location = New System.Drawing.Point(2, 9)
        Me.eight_ref_M_HEADER_IMAGE.Name = "eight_ref_M_HEADER_IMAGE"
        Me.eight_ref_M_HEADER_IMAGE.Size = New System.Drawing.Size(163, 115)
        Me.eight_ref_M_HEADER_IMAGE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.eight_ref_M_HEADER_IMAGE.TabIndex = 84
        Me.eight_ref_M_HEADER_IMAGE.TabStop = False
        '
        'btn_Cancel
        '
        Me.btn_Cancel.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Cancel.Location = New System.Drawing.Point(596, 636)
        Me.btn_Cancel.Name = "btn_Cancel"
        Me.btn_Cancel.Size = New System.Drawing.Size(79, 21)
        Me.btn_Cancel.TabIndex = 12
        Me.btn_Cancel.Text = "キャンセル"
        Me.btn_Cancel.UseVisualStyleBackColor = False
        Me.btn_Cancel.Visible = False
        '
        'btn_Insert
        '
        Me.btn_Insert.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Insert.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Insert.Location = New System.Drawing.Point(523, 636)
        Me.btn_Insert.Name = "btn_Insert"
        Me.btn_Insert.Size = New System.Drawing.Size(65, 21)
        Me.btn_Insert.TabIndex = 11
        Me.btn_Insert.Text = "登録"
        Me.btn_Insert.UseVisualStyleBackColor = False
        Me.btn_Insert.Visible = False
        '
        'btn_History
        '
        Me.btn_History.BackColor = System.Drawing.SystemColors.Control
        Me.btn_History.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_History.Location = New System.Drawing.Point(644, 636)
        Me.btn_History.Name = "btn_History"
        Me.btn_History.Size = New System.Drawing.Size(65, 21)
        Me.btn_History.TabIndex = 10
        Me.btn_History.Text = "更新履歴"
        Me.btn_History.UseVisualStyleBackColor = False
        Me.btn_History.Visible = False
        '
        'btn_Copy
        '
        Me.btn_Copy.BackColor = System.Drawing.SystemColors.Control
        Me.btn_Copy.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_Copy.Location = New System.Drawing.Point(287, 636)
        Me.btn_Copy.Name = "btn_Copy"
        Me.btn_Copy.Size = New System.Drawing.Size(65, 21)
        Me.btn_Copy.TabIndex = 1016
        Me.btn_Copy.Text = "複写"
        Me.btn_Copy.UseVisualStyleBackColor = False
        Me.btn_Copy.Visible = False
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(151, Byte), Integer), CType(CType(185, Byte), Integer), CType(CType(141, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(722, 664)
        Me.Controls.Add(Me.btn_Copy)
        Me.Controls.Add(Me.btn_History)
        Me.Controls.Add(Me.btn_Cancel)
        Me.Controls.Add(Me.btn_Insert)
        Me.Controls.Add(Me.TabControl2)
        Me.Controls.Add(Me.btn_Export)
        Me.Controls.Add(Me.btn_Del)
        Me.Controls.Add(Me.btn_Update)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btn_New)
        Me.Controls.Add(Me.btn_Close)
        Me.Font = New System.Drawing.Font("ＭＳ Ｐゴシック", 9.0!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(738, 703)
        Me.MinimumSize = New System.Drawing.Size(738, 703)
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "設備管理システム"
        Me.lbl_M_SHISAN.ResumeLayout(False)
        Me.lbl_M_SHISAN.PerformLayout()
        Me.GroupBox16.ResumeLayout(False)
        CType(Me.eleven_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl2.ResumeLayout(False)
        Me.lbl_Detail.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.lbl_M_HEADER.ResumeLayout(False)
        Me.lbl_M_HEADER.PerformLayout()
        Me.gpx_ADDITIONAL_GROUP.ResumeLayout(False)
        Me.gpx_ADDITIONAL_GROUP.PerformLayout()
        Me.gpx_HOYU_GROUP.ResumeLayout(False)
        Me.gpx_HOYU_GROUP.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpx_RENTAL_GROUP.ResumeLayout(False)
        Me.gpx_RENTAL_GROUP.PerformLayout()
        Me.gpx_CATEGORY_GROUP.ResumeLayout(False)
        Me.gpx_CATEGORY_GROUP.PerformLayout()
        Me.lbl_M_TOOL.ResumeLayout(False)
        Me.lbl_M_TOOL.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.two_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.lbl_M_HOZEN.ResumeLayout(False)
        Me.lbl_M_HOZEN.PerformLayout()
        Me.gpx_INSPECTION_PROGRAM.ResumeLayout(False)
        Me.gpx_INSPECTION_PROGRAM.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.three_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gpx_CHECK_RECORDS.ResumeLayout(False)
        Me.gpx_CHECK_RECORDS.PerformLayout()
        Me.lbl_M_PME.ResumeLayout(False)
        Me.lbl_M_PME.PerformLayout()
        Me.four_gpx_KOUSEI_GROUP.ResumeLayout(False)
        Me.four_gpx_KOUSEI_GROUP.PerformLayout()
        Me.gpx_M_PME_MFG_NO.ResumeLayout(False)
        Me.gpx_M_PME_MFG_NO.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.four_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.lbl_M_RENTAL.ResumeLayout(False)
        Me.lbl_M_RENTAL.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.six_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.lbl_M_LEASE.ResumeLayout(False)
        Me.lbl_M_LEASE.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        CType(Me.eight_ref_M_HEADER_IMAGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btn_New As System.Windows.Forms.Button
    Friend WithEvents btn_Close As System.Windows.Forms.Button
    Friend WithEvents btn_Update As System.Windows.Forms.Button
    Friend WithEvents btn_Del As System.Windows.Forms.Button
    Friend WithEvents btn_Export As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents lbl_Detail As System.Windows.Forms.TabPage
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents lbl_M_HEADER As System.Windows.Forms.TabPage
    Friend WithEvents M_HEADER_MODIFIED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_MODIFIED_DATE As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_MODIFIED_BY As System.Windows.Forms.TextBox
    Friend WithEvents lbl_MODIFIED_BY As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_CREATED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_CREATED_DATE As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_CREATED_BY As System.Windows.Forms.TextBox
    Friend WithEvents lbl_CREATED_BY As System.Windows.Forms.Label
    Friend WithEvents one_ref_M_RENTAL_LEASE_CONPANY As System.Windows.Forms.TextBox
    Friend WithEvents M_HEADER_HOYU_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents lbl_HAIKI_DATE As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_HAIKI_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents lbl_MFG_NO As System.Windows.Forms.Label
    Friend WithEvents btn_ImageClear As System.Windows.Forms.Button
    Friend WithEvents btn_ImageSel As System.Windows.Forms.Button
    Friend WithEvents lbl_IMAGE As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_REMARKS As System.Windows.Forms.TextBox
    Friend WithEvents lbl_REMARKS As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_LEASE_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents lbl_RENTAL_CONPANY As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_RENTAL_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents M_HEADER_HOZEN_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents M_HEADER_LOCATION As System.Windows.Forms.TextBox
    Friend WithEvents lbl_LOCATION As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_BASE As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_BASE As System.Windows.Forms.Label
    Friend WithEvents lbl_KUBUN_01 As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_UN_NO As System.Windows.Forms.TextBox
    Friend WithEvents lbl_UN_NO As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_SPL As System.Windows.Forms.TextBox
    Friend WithEvents lbl_SPL As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_MFG As System.Windows.Forms.TextBox
    Friend WithEvents lbl_MFG As System.Windows.Forms.Label
    Friend WithEvents one_lbl_STATUS As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_KIKAKU As System.Windows.Forms.TextBox
    Friend WithEvents lbl_KIKAKU As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_HAICHI_KIJYUN As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_HAICHI_KIJYUN As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_KUBUN_01 As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_M_TOOL As System.Windows.Forms.TabPage
    Friend WithEvents M_TOOL_ALLOW_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents M_TOOL_STATUS As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_STATUS As System.Windows.Forms.Label
    Friend WithEvents one_lbl_MEISYOU As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_MEISYOU As System.Windows.Forms.TextBox
    Friend WithEvents M_HEADER_MFG_NO As System.Windows.Forms.TextBox
    Friend WithEvents lbl_M_HOZEN As System.Windows.Forms.TabPage
    Friend WithEvents lbl_M_PME As System.Windows.Forms.TabPage
    Friend WithEvents lbl_M_RENTAL As System.Windows.Forms.TabPage
    Friend WithEvents lbl_M_LEASE As System.Windows.Forms.TabPage
    Friend WithEvents three_lbl_CHECK_COMPANY As System.Windows.Forms.Label
    Friend WithEvents M_HOZEN_CHECK_COMPANY As System.Windows.Forms.TextBox
    Friend WithEvents M_HOZEN_CHECK_BY As System.Windows.Forms.TextBox
    Friend WithEvents lbl_CHECK_BY As System.Windows.Forms.Label
    Friend WithEvents M_HOZEN_CHECK_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_CHECK_DATE As System.Windows.Forms.Label
    Friend WithEvents M_HOZEN_ALLOW_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents M_HOZEN_STATUS As System.Windows.Forms.ComboBox
    Friend WithEvents three_lbl_STATUS As System.Windows.Forms.Label
    Friend WithEvents M_HOZEN_REMARKS As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_REMARKS2 As System.Windows.Forms.Label
    Friend WithEvents M_PME_KOUSEI_COMPANY As System.Windows.Forms.TextBox
    Friend WithEvents lbl_KOUSEI_COMPANY As System.Windows.Forms.Label
    Friend WithEvents M_PME_KOUSEI_BY_COMPANY As System.Windows.Forms.TextBox
    Friend WithEvents lbl_KOUSEI_BY_COMPANY As System.Windows.Forms.Label
    Friend WithEvents M_PME_ALLOW_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents M_PME_STATUS As System.Windows.Forms.ComboBox
    Friend WithEvents four_lbl_STATUS As System.Windows.Forms.Label
    Friend WithEvents M_PME_DUE_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_DUE_DATE As System.Windows.Forms.Label
    Friend WithEvents M_PME_KOUSEI_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_KOUSEI_DATE As System.Windows.Forms.Label
    Friend WithEvents M_PME_KOUSEI_SEIDO As System.Windows.Forms.TextBox
    Friend WithEvents lbl_KOUSEI_SEIDO As System.Windows.Forms.Label
    Friend WithEvents M_PME_REMARKS As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_REMARKS2 As System.Windows.Forms.Label
    Friend WithEvents M_HOZEN_NEXT_CHECK_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents three_lbl_NEXT_CHECK_DATE As System.Windows.Forms.Label
    Friend WithEvents M_RENTAL_REMARKS As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_REMARKS2 As System.Windows.Forms.Label
    Friend WithEvents M_RENTAL_ALLOW_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents M_RENTAL_DUE_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents six_lbl_DUE_DATE As System.Windows.Forms.Label
    Friend WithEvents M_RENTAL_KAKUNIN_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_KAKUNIN_DATE As System.Windows.Forms.Label
    Friend WithEvents M_RENTAL_LEASE_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_LEASE_DATE As System.Windows.Forms.Label
    Friend WithEvents M_RENTAL_KEKKA As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_KEKKA As System.Windows.Forms.Label
    Friend WithEvents M_LEASE_LEASE_NO As System.Windows.Forms.TextBox
    Friend WithEvents lbl_LEASE_NO As System.Windows.Forms.Label
    Friend WithEvents M_LEASE_LEASE_COMPANY As System.Windows.Forms.TextBox
    Friend WithEvents lbl_LEASE_COMPANY As System.Windows.Forms.Label
    Friend WithEvents M_LEASE_FIRST_LOCATION As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_FIRST_LOCATION As System.Windows.Forms.Label
    Friend WithEvents M_LEASE_LEASE_COUNT As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_LEASE_COUNT As System.Windows.Forms.Label
    Friend WithEvents M_LEASE_LEASE_END_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_LEASE_END_DATE As System.Windows.Forms.Label
    Friend WithEvents M_LEASE_LEASE_START_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_LEASE_START_DATE As System.Windows.Forms.Label
    Friend WithEvents M_LEASE_REMARKS As System.Windows.Forms.TextBox
    Friend WithEvents eight_lbl_REMARKS2 As System.Windows.Forms.Label
    Friend WithEvents lbl_Certificate As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_CERTIFICATE_NAME As System.Windows.Forms.TextBox
    Friend WithEvents btn_File As System.Windows.Forms.Button
    Friend WithEvents M_HEADER_REFERENCE As System.Windows.Forms.TextBox
    Friend WithEvents lbl_Reference As System.Windows.Forms.Label
    Friend WithEvents lbl_M_SHISAN As System.Windows.Forms.TabPage
    Friend WithEvents M_HEADER_EQUIPMENT_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents btn_Cancel As System.Windows.Forms.Button
    Friend WithEvents btn_Insert As System.Windows.Forms.Button
    Friend WithEvents btn_History As System.Windows.Forms.Button
    Friend WithEvents M_HEADER_REASON As System.Windows.Forms.TextBox
    Friend WithEvents lbl_REASON As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_DOUTOUSEI As System.Windows.Forms.TextBox
    Friend WithEvents lbl_DOUTOUSEI As System.Windows.Forms.Label
    Friend WithEvents one_ref_M_RENTAL_TEKIGOUSEI As System.Windows.Forms.DateTimePicker
    Friend WithEvents lbl_TEKIGOUSEI As System.Windows.Forms.Label
    Friend WithEvents one_ref_M_RENTAL_NOURYOKU As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_NOURYOKU As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_SHISAN_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents M_HEADER_NOT_KOUSEI_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents M_HEADER_KOUSEI_FLG As System.Windows.Forms.CheckBox
    Friend WithEvents M_HOZEN_REASON As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_REASON As System.Windows.Forms.Label
    Friend WithEvents M_PME_REASON As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_REASON As System.Windows.Forms.Label
    Friend WithEvents M_RENTAL_REASON As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_REASON As System.Windows.Forms.Label
    Friend WithEvents M_LEASE_REASON As System.Windows.Forms.TextBox
    Friend WithEvents eight_lbl_REASON As System.Windows.Forms.Label
    Friend WithEvents gpx_M_PME_MFG_NO As System.Windows.Forms.GroupBox
    Friend WithEvents two_lbl_ATA As System.Windows.Forms.Label
    Friend WithEvents M_TOOL_ATA As System.Windows.Forms.TextBox
    Friend WithEvents two_ref_M_HEADER_MFG_NO As System.Windows.Forms.TextBox
    Friend WithEvents two_lbl_MFG_NO As System.Windows.Forms.Label
    Friend WithEvents two_lbl_IMAGE As System.Windows.Forms.Label
    Friend WithEvents two_ref_M_HEADER_LOCATION As System.Windows.Forms.TextBox
    Friend WithEvents two_lbl_LOCATION As System.Windows.Forms.Label
    Friend WithEvents two_ref_M_HEADER_BASE As System.Windows.Forms.ComboBox
    Friend WithEvents two_lbl_BASE As System.Windows.Forms.Label
    Friend WithEvents two_ref_M_HEADER_HAICHI_KIJYUN As System.Windows.Forms.ComboBox
    Friend WithEvents two_lbl_HAICHI_KIJYUN As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_MFG_NO As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_MEISYOU As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_MEISYOU As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_MFG_NO As System.Windows.Forms.Label
    Friend WithEvents three_lbl_IMAGE As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_LOCATION As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_LOCATION As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_BASE As System.Windows.Forms.ComboBox
    Friend WithEvents three_lbl_BASE As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_UN_NO As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_UN_NO As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_MFG As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_MFG As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_KIKAKU As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_KIKAKU As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_HAICHI_KIJYUN As System.Windows.Forms.ComboBox
    Friend WithEvents three_lbl_HAICHI_KIJYUN As System.Windows.Forms.Label
    Friend WithEvents gpx_INSPECTION_PROGRAM As System.Windows.Forms.GroupBox
    Friend WithEvents three_lbl_BUNRUI_01 As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_BUNRUI_01 As System.Windows.Forms.TextBox
    Friend WithEvents three_ref_M_HEADER_SPL As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_SPL As System.Windows.Forms.Label
    Friend WithEvents M_HOZEN_CHECK_INTERVAL As System.Windows.Forms.ComboBox
    Friend WithEvents three_lbl_HOZEN_INTERVAL As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_CREATED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents four_lbl_CREATED_DATE As System.Windows.Forms.Label
    Friend WithEvents four_lbl_IMAGE As System.Windows.Forms.Label
    Friend WithEvents four_lbl_KOUSEI_INTERVAL As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_SPL As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_SPL As System.Windows.Forms.Label
    Friend WithEvents four_lbl_ATA As System.Windows.Forms.Label
    Friend WithEvents M_PME_ATA As System.Windows.Forms.TextBox
    Friend WithEvents four_ref_M_HEADER_MFG_NO As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_MEISYOU As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_MEISYOU As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_MFG_NO As System.Windows.Forms.Label
    Friend WithEvents M_PME_KOUSEI_KIKAN As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_KOUSEI_KIKAN As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_BASE As System.Windows.Forms.ComboBox
    Friend WithEvents four_lbl_BASE As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_UN_NO As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_UN_NO As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_MFG As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_MFG As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_HAICHI_KIJYUN As System.Windows.Forms.ComboBox
    Friend WithEvents four_lbl_HAICHI_KIJYUN As System.Windows.Forms.Label
    Friend WithEvents four_gpx_KOUSEI_GROUP As System.Windows.Forms.GroupBox
    Friend WithEvents three_ref_M_HEADER_CREATED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents three_lbl_CREATED_DATE As System.Windows.Forms.Label
    Friend WithEvents two_ref_M_HEADER_CREATED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents two_lbl_CREATED_DATE As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_MODIFIED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents four_lbl_MODIFIED_DATE As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_MODIFIED_BY As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_MODIFIED_BY As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_MODIFIED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents three_lbl_MODIFIED_DATE As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_MODIFIED_BY As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_MODIFIED_BY As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_DOUTOUSEI As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_DOUTOUSEI As System.Windows.Forms.Label
    Friend WithEvents M_RENTAL_TEKIGOUSEI As System.Windows.Forms.DateTimePicker
    Friend WithEvents six_lbl_TEKIGOUSEI As System.Windows.Forms.Label
    Friend WithEvents M_RENTAL_NOURYOKU As System.Windows.Forms.ComboBox
    Friend WithEvents six_lbl_NOURYOKU As System.Windows.Forms.Label
    Friend WithEvents M_RENTAL_LEASE_CONPANY As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_RENTAL_CONPANY As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_KUBUN_01 As System.Windows.Forms.ComboBox
    Friend WithEvents six_lbl_KUBUN_01 As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_KIKAKU As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_KIKAKU As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_MODIFIED_BY As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_MODIFIED_BY As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_CREATED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents six_lbl_CREATED_DATE As System.Windows.Forms.Label
    Friend WithEvents six_lbl_IMAGE As System.Windows.Forms.Label
    Friend WithEvents six_lbl_ATA As System.Windows.Forms.Label
    Friend WithEvents M_RENTAL_ATA As System.Windows.Forms.TextBox
    Friend WithEvents six_ref_M_HEADER_MFG_NO As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_MEISYOU As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_MEISYOU As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_MFG_NO As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_BASE As System.Windows.Forms.ComboBox
    Friend WithEvents six_lbl_BASE As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_UN_NO As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_UN_NO As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_MFG As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_MFG As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_HAICHI_KIJYUN As System.Windows.Forms.ComboBox
    Friend WithEvents six_lbl_HAICHI_KIJYUN As System.Windows.Forms.Label
    Friend WithEvents six_lbl_BUNRUI_01 As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_BUNRUI_01 As System.Windows.Forms.TextBox
    Friend WithEvents eight_ref_M_HEADER_KIKAKU As System.Windows.Forms.TextBox
    Friend WithEvents eight_lbl_KIKAKU As System.Windows.Forms.Label
    Friend WithEvents eight_ref_M_HEADER_MODIFIED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents eight_lbl_MODIFIED_DATE As System.Windows.Forms.Label
    Friend WithEvents eight_ref_M_HEADER_MODIFIED_BY As System.Windows.Forms.TextBox
    Friend WithEvents eight_lbl_MODIFIED_BY As System.Windows.Forms.Label
    Friend WithEvents eight_ref_M_HEADER_CREATED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents eight_lbl_CREATED_DATE As System.Windows.Forms.Label
    Friend WithEvents eight_lbl_IMAGE As System.Windows.Forms.Label
    Friend WithEvents eight_ref_M_HEADER_MFG_NO As System.Windows.Forms.TextBox
    Friend WithEvents eight_lbl_MEISYOU As System.Windows.Forms.Label
    Friend WithEvents eight_ref_M_HEADER_MEISYOU As System.Windows.Forms.TextBox
    Friend WithEvents eight_lbl_MFG_NO As System.Windows.Forms.Label
    Friend WithEvents eight_ref_M_HEADER_MFG As System.Windows.Forms.TextBox
    Friend WithEvents eight_lbl_MFG As System.Windows.Forms.Label
    Friend WithEvents eight_lbl_BUNRUI_01 As System.Windows.Forms.Label
    Friend WithEvents eight_ref_M_HEADER_BUNRUI_01 As System.Windows.Forms.TextBox
    Friend WithEvents btn_FileClear As System.Windows.Forms.Button
    Friend WithEvents M_HEADER_IMAGE As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents two_ref_M_HEADER_IMAGE As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents three_ref_M_HEADER_IMAGE As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents four_ref_M_HEADER_IMAGE As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents six_ref_M_HEADER_IMAGE As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents eight_ref_M_HEADER_IMAGE As System.Windows.Forms.PictureBox
    Friend WithEvents M_HEADER_HAIKI_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents gpx_RENTAL_GROUP As System.Windows.Forms.GroupBox
    Friend WithEvents gpx_CATEGORY_GROUP As System.Windows.Forms.GroupBox
    Friend WithEvents M_SHISAN_TAIYOU As System.Windows.Forms.TextBox
    Friend WithEvents eleven_ref_M_HEADER_CREATED_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents eleven_lbl_CREATED_DATE As System.Windows.Forms.Label
    Friend WithEvents eleven_ref_M_HEADER_KIKAKU As System.Windows.Forms.TextBox
    Friend WithEvents eleven_lbl_LOCATION As System.Windows.Forms.Label
    Friend WithEvents eleven_lbl_BASE As System.Windows.Forms.Label
    Friend WithEvents eleven_lbl_MFG As System.Windows.Forms.Label
    Friend WithEvents eleven_ref_M_HEADER_LOCATION As System.Windows.Forms.TextBox
    Friend WithEvents eleven_lbl_TAIYOU As System.Windows.Forms.Label
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents eleven_ref_M_HEADER_IMAGE As System.Windows.Forms.PictureBox
    Friend WithEvents eleven_lbl_IMAGE As System.Windows.Forms.Label
    Friend WithEvents M_SHISAN_SHISAN_NO As System.Windows.Forms.TextBox
    Friend WithEvents eleven_lbl_SHISAN_NO As System.Windows.Forms.Label
    Friend WithEvents eleven_ref_M_HEADER_BASE As System.Windows.Forms.ComboBox
    Friend WithEvents eleven_lbl_MFG_NO As System.Windows.Forms.Label
    Friend WithEvents eleven_lbl_KIKAKU As System.Windows.Forms.Label
    Friend WithEvents eleven_lbl_MEISYOU As System.Windows.Forms.Label
    Friend WithEvents eleven_ref_M_HEADER_MEISYOU As System.Windows.Forms.TextBox
    Friend WithEvents eleven_lbl_BUNRUI_01 As System.Windows.Forms.Label
    Friend WithEvents eleven_ref_M_HEADER_BUNRUI_01 As System.Windows.Forms.TextBox
    Friend WithEvents eleven_ref_M_HEADER_MFG As System.Windows.Forms.TextBox
    Friend WithEvents eleven_ref_M_HEADER_SPL As System.Windows.Forms.TextBox
    Friend WithEvents eleven_lbl_SPL As System.Windows.Forms.Label
    Friend WithEvents eleven_ref_M_HEADER_MFG_NO As System.Windows.Forms.TextBox

    Friend WithEvents M_HEADER_BUNRUI_01 As System.Windows.Forms.TextBox
    Friend WithEvents gpx_HOYU_GROUP As System.Windows.Forms.GroupBox
    Friend WithEvents lbl_BUNRUI_01 As System.Windows.Forms.Label
    Friend WithEvents one_lbl_KANRI_NO As System.Windows.Forms.Label
    Friend WithEvents M_HEADER_KANRI_NO As System.Windows.Forms.TextBox
    Friend WithEvents M_HEADER_MODEL As System.Windows.Forms.TextBox
    Friend WithEvents one_lbl_MODEL As System.Windows.Forms.Label
    Friend WithEvents gpx_ADDITIONAL_GROUP As System.Windows.Forms.GroupBox
    Friend WithEvents one_ref_STATUS As System.Windows.Forms.ComboBox
    Friend WithEvents three_ref_M_HEADER_KANRI_NO As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_KANRI_NO As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_MODEL As System.Windows.Forms.TextBox
    Friend WithEvents three_lbl_MODEL As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents M_HOZEN_ATA As System.Windows.Forms.TextBox
    Friend WithEvents M_HOZEN_END_CHECK_DATE As System.Windows.Forms.DateTimePicker
    Friend WithEvents three_lbl_END_CHECK_DATE As System.Windows.Forms.Label
    Friend WithEvents gpx_CHECK_RECORDS As System.Windows.Forms.GroupBox
    Friend WithEvents two_ref_M_HEADER_KANRI_NO As System.Windows.Forms.TextBox
    Friend WithEvents two_lbl_KANRI_NO As System.Windows.Forms.Label
    Friend WithEvents two_ref_M_HEADER_MODEL As System.Windows.Forms.TextBox
    Friend WithEvents two_lbl_MODEL As System.Windows.Forms.Label
    Friend WithEvents two_lbl_BUNRUI_01 As System.Windows.Forms.Label
    Friend WithEvents two_ref_M_HEADER_BUNRUI_01 As System.Windows.Forms.TextBox
    Friend WithEvents two_lbl_MEISYOU As System.Windows.Forms.Label
    Friend WithEvents two_ref_M_HEADER_MEISYOU As System.Windows.Forms.TextBox
    Friend WithEvents two_ref_M_HEADER_UN_NO As System.Windows.Forms.TextBox
    Friend WithEvents two_lbl_UN_NO As System.Windows.Forms.Label
    Friend WithEvents two_ref_M_HEADER_MFG As System.Windows.Forms.TextBox
    Friend WithEvents two_lbl_MFG As System.Windows.Forms.Label
    Friend WithEvents two_ref_M_HEADER_KIKAKU As System.Windows.Forms.TextBox
    Friend WithEvents two_lbl_KIKAKU As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_KANRI_NO As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_KANRI_NO As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_MODEL As System.Windows.Forms.TextBox
    Friend WithEvents six_lbl_MODEL As System.Windows.Forms.Label
    Friend WithEvents eleven_ref_M_HEADER_KANRI_NO As System.Windows.Forms.TextBox
    Friend WithEvents eleven_lbl_KANRI_NO As System.Windows.Forms.Label
    Friend WithEvents eleven_ref_M_HEADER_MODEL As System.Windows.Forms.TextBox
    Friend WithEvents elevent_lbl_MODEL As System.Windows.Forms.Label
    Friend WithEvents eight_ref_M_HEADER_MODEL As System.Windows.Forms.TextBox
    Friend WithEvents eightt_lbl_MODEL As System.Windows.Forms.Label
    Friend WithEvents eight_ref_M_HEADER_KANRI_NO As System.Windows.Forms.TextBox
    Friend WithEvents eight_lbl_KANRI_NO As System.Windows.Forms.Label
    Friend WithEvents btn_Kousei As System.Windows.Forms.Button
    Friend WithEvents four_ref_M_HEADER_KANRI_NO As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_KANRI_NO As System.Windows.Forms.Label
    Friend WithEvents four_lbl_BUNRUI_01 As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_BUNRUI_01 As System.Windows.Forms.TextBox
    Friend WithEvents pgs As System.Windows.Forms.ProgressBar
    Friend WithEvents four_ref_M_HEADER_MODEL As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_MODEL As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_KIKAKU As System.Windows.Forms.TextBox
    Friend WithEvents four_lbl_KIKAKU As System.Windows.Forms.Label
    Friend WithEvents M_PME_NOURYOKU As System.Windows.Forms.ComboBox
    Friend WithEvents four_lbl_M_PME_NOURYOKU As System.Windows.Forms.Label
    Friend WithEvents M_PME_KOUSEI_INTERVAL As System.Windows.Forms.ComboBox
    Friend WithEvents btn_Copy As System.Windows.Forms.Button
    Friend WithEvents btn_FileDownload As System.Windows.Forms.Button
    Friend WithEvents btn_ImageDownload As System.Windows.Forms.Button
    Friend WithEvents lbl_KEKKA As System.Windows.Forms.Label
    Friend WithEvents M_PME_KEKKA As System.Windows.Forms.TextBox
    Friend WithEvents M_HEADER_PARENT_NO As System.Windows.Forms.TextBox
    Friend WithEvents lbl_PARENT_NO As System.Windows.Forms.Label
    Friend WithEvents two_ref_M_HEADER_PARENT_NO As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents eleven_ref_M_HEADER_PARENT_NO As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents three_ref_M_HEADER_PARENT_NO As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents four_ref_M_HEADER_PARENT_NO As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents six_ref_M_HEADER_PARENT_NO As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents eight_ref_M_HEADER_PARENT_NO As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
End Class
