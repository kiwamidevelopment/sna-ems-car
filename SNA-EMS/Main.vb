﻿Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.IO

Imports Oracle.ManagedDataAccess.Client
Imports System.Text.RegularExpressions

'
'   クラス：Main
'   処理　：設備管理のメイン画面を構成し、検索から一覧表示、個別情報の編集まで対応する
'
Public Class Main
    Public kanriNo As String = ""
    Dim str As String = ""
    Dim search_model_text As String = ""
    Dim search_kanri_no_text As String = ""
    Dim search_name As String = ""
    Dim search_kubun_selectIndex = 0
    Private haita_flag As Boolean = False
    Private insert_flag As Boolean = False

    '2015-06-10 ADD ST 画面にロードしたデータが更新済みの場合　True
    Private IsUpdated_flag As Boolean = False
    '2015-06-10 ADD ED



    Public gmsValues As Dictionary(Of String, Dictionary(Of String, String))

    Public tabPageList As List(Of TabPage) = Nothing
    Private showTabList As List(Of String) = Nothing

    Public gid As String = Nothing
    Private kousei As Kousei

    '
    '   クラスの新規生成処理
    '   
    Public Sub New(ByVal kanriNo As String)
        InitializeComponent()
        Me.Text = Ini.ReadValue(Ini.Config.UX, "Setting", "SYSNAME")
        Me.kanriNo = kanriNo
    End Sub


    '
    '   画面情報の初期化処理
    '   
    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Helper.SetAllControlText(Me)
            'Me.TabControl1.TabPages.RemoveAt(7)
            tabPageList = New List(Of TabPage)()

            tabPageList.Add(Me.lbl_M_HEADER)
            tabPageList.Add(Me.lbl_M_PME)
            tabPageList.Add(Me.lbl_M_TOOL)
            tabPageList.Add(Me.lbl_M_HOZEN)

            tabPageList.Add(Me.lbl_M_RENTAL)
            tabPageList.Add(Me.lbl_M_SHISAN)
            tabPageList.Add(Me.lbl_M_LEASE)

            showTabList = New List(Of String)()
            ResetShowTab()

            ' set control status
            Status()
            If kanriNo Is Nothing Then
                btnNew_Click(Me, New EventArgs())
            Else
                ' search the kariNo data
                SearchSelectRowData()
            End If

            AddHandler M_HEADER_HAIKI_DATE.ValueChanged, AddressOf M_HEADER_HAIKI_DATE_ValueChanged '2015/09/11 taskplan ADD 自動廃棄チェック連動
            AddHandler M_HEADER_HAIKI_FLG.CheckedChanged, AddressOf M_HEADER_HAIKI_FLG_CheckedChanged '2015/09/11 taskplan ADD 自動廃棄チェック連動
        Catch ex As Exception
            Helper.Log("Main_Load", ex)
        End Try
    End Sub


    '
    '   システムの終了処理
    '   
    Private Sub Main_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.Dispose()
    End Sub


    '
    '   システムの終了前処理
    '   
    Private Sub Main_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        If Me.btn_Insert.Visible = True Then
            Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02003"), MsgBoxStyle.YesNo, "Warning")
            If Rtn = 6 Then
                e.Cancel = False
                Return
            End If
        Else
            Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02002"), MsgBoxStyle.YesNo, "Warning")
            If Rtn = 6 Then
                e.Cancel = False
                Return
            End If
        End If
        e.Cancel = True
        haita_flag = False
        Me.Cursor = Cursors.Default
    End Sub



    '
    '   画面の終了処理   
    '   
    Private Sub btn_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Close.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        If kousei IsNot Nothing Then
            kousei.Close()
            kousei.Dispose()
        End If

        If Me.btn_Insert.Visible = True Then
            Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02003"), MsgBoxStyle.YesNo, "Warning")
            If Rtn = 6 Then
                Me.Close()
                Me.Dispose()
                Return
            End If
        Else
            Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02002"), MsgBoxStyle.YesNo, "Warning")
            If Rtn = 6 Then
                Me.Close()
                Me.Dispose()
                Return
            End If
        End If
        haita_flag = False
        Me.Cursor = Cursors.Default
    End Sub


    Private Sub ATA_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If Helper.IsDecimal(Me.M_HOZEN_ATA.Text) Then
            str = Me.M_HOZEN_ATA.Text
        Else
            M_HOZEN_ATA.Text = str
            M_HOZEN_ATA.Focus()
        End If
    End Sub

    Private Sub M_PME_KOUSEI_SEIDO_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles M_PME_KOUSEI_SEIDO.KeyUp
        'Dim rex As Regex = New Regex("^([-+±]?){1}[A-Za-z0-9%,]*$", RegexOptions.IgnoreCase)
        'If rex.IsMatch(sender.Text) Then
        '    str = sender.Text
        'Else
        '    sender.Text = str
        '    sender.Focus()
        'End If
    End Sub


    Private Sub CancelSelectRowData()
        RefreshCacheDatas(kanriNo)
    End Sub

    '
    '   該当設備情報の取得処理
    '   
    Private Sub SearchSelectRowData()
        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor

            ShowAllTabs()
            clearCom()

            RefreshCacheDatas(kanriNo)

            InitMasterRefControlsValues()
            Me.btn_Kousei.Enabled = True
        Catch ex As Exception
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    '
    '   キャッシュ情報の更新処理（個別情報）
    '   
    Private Sub RefreshCacheDatasSelf()

        gmsValues = New Dictionary(Of String, Dictionary(Of String, String))

        For Each control As Control In Me.TabControl1.Controls
            Dim tableName As String = control.Tag
            If tableName Is Nothing Then
                Continue For
            End If
            If control.HasChildren Then
                'search data
                Dim dataReader As OracleDataReader = MasterRepository.GetHeaderDataReader(tableName, M_HEADER_KANRI_NO.Text)
                If dataReader.HasRows = False Then
                    Continue For
                End If
                dataReader.Read()
                'init value
                If "m_header".Equals(tableName.ToLower) Then
                    Helper.DataReaderToImageOrPDF(dataReader, Me, tableName)
                End If

                'get DataReader date to  Dictionary(Of String, Object)
                Dim data = Helper.GetDataReaderValues(dataReader)
                gmsValues.Add(tableName.ToLower, data)
                If "m_header".Equals(tableName.ToLower) Then
                    gid = data.Item("gid")
                End If

                dataReader.Close()
            End If
        Next
    End Sub

    '
    '   キャッシュ情報の更新処理
    '   
    Private Sub RefreshCacheDatas(ByVal kanriNo As String)

        gmsValues = New Dictionary(Of String, Dictionary(Of String, String))
        'get data
        MainHelper.ClearAllControlsValues(Me)
        For Each control As Control In Me.TabControl1.Controls
            Dim tableName As String = control.Tag
            If tableName Is Nothing Then
                Continue For
            End If
            If control.HasChildren Then
                'search data
                Dim dataReader As OracleDataReader = MasterRepository.GetHeaderDataReader(tableName, kanriNo)
                If dataReader.HasRows = False Then
                    Continue For
                End If
                dataReader.Read()
                'init value
                Helper.InitControlsData(dataReader, control, tableName)

                'get DataReader data to  Dictionary(Of String, Object)
                Dim data = Helper.GetDataReaderValues(dataReader)
                gmsValues.Add(tableName.ToLower, data)
                If "m_header".Equals(tableName.ToLower) Then
                    gid = data.Item("gid")
                End If

                dataReader.Close()
            End If
        Next
        If TabControl1.SelectedIndex > 0 Then
            Dim tabControlDict = New Dictionary(Of String, Object)
            Helper.GetAllControlsValues(Me.TabControl1.Controls.Item(0), tabControlDict, Me.TabControl1.Controls.Item(0).Tag)
            Helper.InitRefData(Me, tabControlDict, TabControl1.SelectedTab)
        End If

        SetTabByCheckFlg()
        ShowTabs()
    End Sub

    '
    '   パラメータの初期化処理
    '   
    Private Sub InitParameter(ByVal paramVal As String)
        M_HEADER_HAICHI_KIJYUN.DataSource = ParameterRepository.GetParameters("HAICHI_KIJYUN", paramVal)
        M_HEADER_HAICHI_KIJYUN.DisplayMember = "PARAMETER_VALUE"
        M_HEADER_HAICHI_KIJYUN.ValueMember = "PARAMETER_VALUE"

        '2018/09/11 設備区分は車両のみを固定表示
        M_HEADER_KUBUN_01.DataSource = ParameterRepository.GetParameters("KUBUN_03", paramVal)
        M_HEADER_KUBUN_01.DisplayMember = "PARAMETER_VALUE"
        M_HEADER_KUBUN_01.ValueMember = "PARAMETER_VALUE"

        M_HEADER_BASE.DataSource = ParameterRepository.GetParameters("BASE", paramVal)
        M_HEADER_BASE.DisplayMember = "PARAMETER_VALUE"
        M_HEADER_BASE.ValueMember = "PARAMETER_VALUE"

        M_HOZEN_CHECK_INTERVAL.DataSource = ParameterRepository.GetParameters("HOZEN_INTERVAL", paramVal)
        M_HOZEN_CHECK_INTERVAL.DisplayMember = "PARAMETER_VALUE"
        M_HOZEN_CHECK_INTERVAL.ValueMember = "PARAMETER_VALUE"


        one_ref_M_RENTAL_NOURYOKU.DataSource = ParameterRepository.GetParameters("NOURYOKU", paramVal)
        one_ref_M_RENTAL_NOURYOKU.DisplayMember = "PARAMETER_VALUE"
        one_ref_M_RENTAL_NOURYOKU.ValueMember = "PARAMETER_VALUE"

        M_RENTAL_NOURYOKU.DataSource = ParameterRepository.GetParameters("NOURYOKU", paramVal)
        M_RENTAL_NOURYOKU.DisplayMember = "PARAMETER_VALUE"
        M_RENTAL_NOURYOKU.ValueMember = "PARAMETER_VALUE"

        M_PME_NOURYOKU.DataSource = ParameterRepository.GetParameters("M_PME_NOURYOKU", paramVal)
        M_PME_NOURYOKU.DisplayMember = "PARAMETER_VALUE"
        M_PME_NOURYOKU.ValueMember = "PARAMETER_VALUE"


        M_TOOL_STATUS.DataSource = ParameterRepository.GetParameters("STATUS", paramVal)
        M_TOOL_STATUS.DisplayMember = "PARAMETER_VALUE"
        M_TOOL_STATUS.ValueMember = "PARAMETER_VALUE"

        M_HOZEN_STATUS.DataSource = ParameterRepository.GetParameters("STATUS", paramVal)
        M_HOZEN_STATUS.DisplayMember = "PARAMETER_VALUE"
        M_HOZEN_STATUS.ValueMember = "PARAMETER_VALUE"

        M_PME_STATUS.DataSource = ParameterRepository.GetParameters("STATUS", paramVal)
        M_PME_STATUS.DisplayMember = "PARAMETER_VALUE"
        M_PME_STATUS.ValueMember = "PARAMETER_VALUE"


        M_LEASE_FIRST_LOCATION.DataSource = ParameterRepository.GetParameters("FIRST_LOCATION", paramVal)
        M_LEASE_FIRST_LOCATION.DisplayMember = "PARAMETER_VALUE"
        M_LEASE_FIRST_LOCATION.ValueMember = "PARAMETER_VALUE"

        M_LEASE_LEASE_COUNT.DataSource = ParameterRepository.GetParameters("LEASE_COUNT", paramVal)
        M_LEASE_LEASE_COUNT.DisplayMember = "PARAMETER_VALUE"
        M_LEASE_LEASE_COUNT.ValueMember = "PARAMETER_VALUE"

        one_ref_STATUS.DataSource = ParameterRepository.GetParameters("STATUS", paramVal)
        one_ref_STATUS.DisplayMember = "PARAMETER_VALUE"
        one_ref_STATUS.ValueMember = "PARAMETER_VALUE"

        M_PME_KOUSEI_INTERVAL.DataSource = ParameterRepository.GetParameters("HOZEN_INTERVAL", paramVal)
        M_PME_KOUSEI_INTERVAL.DisplayMember = "PARAMETER_VALUE"
        M_PME_KOUSEI_INTERVAL.ValueMember = "PARAMETER_VALUE"
    End Sub


    '
    '   新規設備の生成処理
    '   
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_New.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor
        insert_flag = True
        Try
            M_HEADER_CREATED_BY.Text = LoginRoler.Employee.EMP_NAME
            M_HEADER_CREATED_DATE.Value = Date.Now
            'clear all components
            clearCom()
            'change toolbar(insert, cancel)
            changeBtnStatus_New()

        Catch ex As Exception
            Helper.Log("btnNew_Click", ex)
        Finally
            insert_flag = False
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub changeBtnStatus_New()
        M_HEADER_HOYU_FLG.Checked = True
        SetFLG(True, False)
        SetTabByCheckFlg()
        ShowTabs()

        Me.btn_New.Visible = False
        Me.btn_Update.Visible = False
        Me.btn_History.Visible = False
        Me.btn_Del.Visible = False
        Me.btn_Export.Visible = False
        Me.btn_Copy.Visible = False

        Me.btn_Kousei.Enabled = False

        Me.btn_Insert.Visible = True
        Me.btn_Cancel.Visible = True



    End Sub

    Private Sub changeBtnStatus_Copy()
        SetTabByCheckFlg()
        ShowTabs()
        Me.btn_New.Visible = False
        Me.btn_Update.Visible = False
        Me.btn_History.Visible = False
        Me.btn_Del.Visible = False
        Me.btn_Export.Visible = False
        Me.btn_Copy.Visible = False

        Me.btn_Kousei.Enabled = False

        Me.btn_Insert.Visible = True
        Me.btn_Cancel.Visible = True
    End Sub

    Private Sub changeBtnStatus_Insert()
        Me.btn_New.Visible = True
        Me.btn_Copy.Visible = True
        Me.btn_Update.Visible = True
        Me.btn_History.Visible = True
        Me.btn_Del.Visible = True
        '2018/09/13 一覧ボタンは非表示に
        Me.btn_Export.Visible = True
        Me.btn_Insert.Visible = False
        Me.btn_Cancel.Visible = False

        Me.btn_Kousei.Enabled = True
    End Sub

    '
    '   データのクリア処理
    '   
    Private Sub clearCom()
        InitParameter(Nothing)

        gmsValues = Nothing

        MainHelper.ClearAllControlsValues(Me)

        Me.btn_Kousei.Enabled = False

        Me.M_HOZEN_END_CHECK_DATE.Checked = False
        Me.M_RENTAL_TEKIGOUSEI.Checked = False

        Me.M_HEADER_HAIKI_DATE.Checked = False

        Me.M_HOZEN_CHECK_DATE.Checked = False
        Me.M_HOZEN_NEXT_CHECK_DATE.Checked = False
        Me.M_PME_KOUSEI_DATE.Checked = False
        Me.M_PME_DUE_DATE.Checked = False


        Me.M_RENTAL_LEASE_DATE.Checked = False
        Me.M_RENTAL_KAKUNIN_DATE.Checked = False
        Me.M_RENTAL_DUE_DATE.Checked = False

        Me.M_LEASE_LEASE_START_DATE.Checked = False
        Me.M_LEASE_LEASE_END_DATE.Checked = False

        Me.M_HOZEN_END_CHECK_DATE.Format = DateTimePickerFormat.Custom
        Me.M_HOZEN_END_CHECK_DATE.CustomFormat = "   "

        Me.M_RENTAL_TEKIGOUSEI.Format = DateTimePickerFormat.Custom
        Me.M_RENTAL_TEKIGOUSEI.CustomFormat = "   "
        M_RENTAL_TEKIGOUSEI.Text = ""


        Me.one_ref_M_RENTAL_TEKIGOUSEI.Format = DateTimePickerFormat.Custom
        Me.one_ref_M_RENTAL_TEKIGOUSEI.CustomFormat = "   "


        Me.M_HEADER_HAIKI_DATE.Format = DateTimePickerFormat.Short  '2015/09/11 taskplan MOD
        '        Me.M_HEADER_HAIKI_DATE.CustomFormat = "   "  '2015/09/11 taskplan DEL

        Me.M_HOZEN_CHECK_DATE.Format = DateTimePickerFormat.Custom
        Me.M_HOZEN_CHECK_DATE.CustomFormat = "   "
        Me.M_HOZEN_NEXT_CHECK_DATE.Format = DateTimePickerFormat.Custom
        Me.M_HOZEN_NEXT_CHECK_DATE.CustomFormat = "   "
        Me.M_PME_KOUSEI_DATE.Format = DateTimePickerFormat.Custom
        Me.M_PME_KOUSEI_DATE.CustomFormat = "   "
        Me.M_PME_DUE_DATE.Format = DateTimePickerFormat.Custom
        Me.M_PME_DUE_DATE.CustomFormat = "   "

        Me.M_RENTAL_LEASE_DATE.Format = DateTimePickerFormat.Custom
        Me.M_RENTAL_LEASE_DATE.CustomFormat = "   "
        Me.M_RENTAL_KAKUNIN_DATE.Format = DateTimePickerFormat.Custom
        Me.M_RENTAL_KAKUNIN_DATE.CustomFormat = "   "
        Me.M_RENTAL_DUE_DATE.Format = DateTimePickerFormat.Custom
        Me.M_RENTAL_DUE_DATE.CustomFormat = "   "

        Me.M_LEASE_LEASE_START_DATE.Format = DateTimePickerFormat.Custom
        Me.M_LEASE_LEASE_START_DATE.CustomFormat = "   "
        Me.M_LEASE_LEASE_END_DATE.Format = DateTimePickerFormat.Custom
        Me.M_LEASE_LEASE_END_DATE.CustomFormat = "   "

    End Sub

    '
    '   処理のキャンセル
    '   
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Cancel.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        Try
            Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "W02116"), MsgBoxStyle.YesNo, "Warning")
            If Rtn = 6 Then
                Me.Close()
                Me.Dispose()
            End If

        Catch ex As Exception
            Helper.Log("btnCancel_Click", ex)
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    '
    '   
    '   
    Private Sub btnInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Insert.Click
        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor

            If M_HEADER_KANRI_NO.Text = "" Then
                M_HEADER_KANRI_NO.BackColor = Color.Red
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02105"), MsgBoxStyle.Information, "Warning")
                Return
            End If
            If MasterRepository.ExistKanriNo(M_HEADER_KANRI_NO.Text) Then
                M_HEADER_KANRI_NO.BackColor = Color.Red
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02106"), MsgBoxStyle.Information, "Warning")
                Return
            End If

            If IsNoDataRepeat() Then
                TabControl1.SelectTab("lbl_M_HEADER")
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02114"), MsgBoxStyle.Information, "Information")
                Return
            End If
            M_HEADER_KANRI_NO.BackColor = Color.White

            Me.M_HEADER_CREATED_DATE.Text = DateTime.Now
            Me.M_HEADER_CREATED_BY.Text = LoginRoler.Employee.EMP_NAME
            'insert data to db
            MainHelper.AddTabControlData(Me)

            MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02108"), MsgBoxStyle.Information, "Information")
            'change status
            changeBtnStatus_Insert()
            'RefreshCacheDatas()
            RefreshCacheDatasSelf()

            Login.kensaku.SearchData(True)  '2015/09/11 taskplan ADD 検索表更新

        Catch ex As Exception
            If Not Helper.CheckContentLength(Me, Ini.ReadValue(Ini.Config.LANG, "System", "W02118"), ex) Then
                Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
            Else
                Helper.Log("btnInsert_Click", ex)
            End If

        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    '
    '   編集情報のDB更新処理
    '   
    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Update.Click
        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor

            If "".Equals(Me.M_HEADER_KANRI_NO.Text.Trim) Then
                M_HEADER_KANRI_NO.BackColor = Color.Red
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02131"), MsgBoxStyle.Information, "Information")    '対象データはありません。
                Return
            End If
            If MasterRepository.ExistKanriNo(M_HEADER_KANRI_NO.Text, gid) Then
                M_HEADER_KANRI_NO.BackColor = Color.Red
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02106"), MsgBoxStyle.Information, "Warning")
                Return
            End If
            If IsNoDataRepeat() Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02114"), MsgBoxStyle.Information, "Information")
                Return
            End If
            '2015-06-10 ADD ST '画面にロードされた更新日とDBの更新日が違っていたらエラー
            Dim MODIFY_DATE_STR As String = M_HEADER_MODIFIED_DATE.Value.ToString

            If MasterRepository.IsUpdated(M_HEADER_KANRI_NO.Text, gid, MODIFY_DATE_STR) Then
                M_HEADER_KANRI_NO.BackColor = Color.Red
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02130"), MsgBoxStyle.Information, "Warning")    'ほかのユーザーによる更新警告
                Return
            End If
            '2015-06-10 ADD ED
            M_HEADER_KANRI_NO.BackColor = Color.White

            Me.M_HEADER_MODIFIED_BY.Text = LoginRoler.Employee.EMP_NAME
            Me.M_HEADER_MODIFIED_DATE.Text = DateTime.Now

            Dim resCheckUp = MainHelper.UpdateTabControlData(Me)
            If resCheckUp Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02109"), MsgBoxStyle.Information, "Information")
                'RefreshCacheDatas()
                RefreshCacheDatasSelf()
            Else
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02111"), MsgBoxStyle.Information, "Information")
            End If

            Login.kensaku.SearchData(True, True)  '2015/09/11 taskplan ADD 検索表更新

        Catch ex As Exception
            If Not Helper.CheckContentLength(Me, Ini.ReadValue(Ini.Config.LANG, "System", "W02118"), ex) Then
                Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
            Else
                Helper.Log("btnUpdate_Click", ex)
            End If
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    '
    '   表示中の情報の出力処理
    '   
    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Export.Click
        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor

            Dim exp As New ExportFormExcel(Me)
            exp.Export()
        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub


    '
    '   表示情報の削除処理
    '   
    Private Sub btnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Del.Click
        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor

            If "".Equals(Me.M_HEADER_KANRI_NO.Text.Trim) Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02131"), MsgBoxStyle.Information, "Information")    '対象データはありません。
                Return
            End If

            If MessageBox.Show(Ini.ReadValue(Ini.Config.LANG, "Main", "M02118"), "yes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then '削除確認
                MainHelper.DelTabControlData(Me)
                Me.clearCom()
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02110"), MsgBoxStyle.Information, "Information")
                'change status
                changeBtnStatus_New()

                haita_flag = False

                Login.kensaku.SearchData(True)  '2015/09/11 taskplan ADD 検索表更新

                ' close the UI
                Me.Close()
                Me.Dispose()

            End If
        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    '
    '   外部写真情報の選択
    '   
    Private Sub IMAGESelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ImageSel.Click
        '
        Dim openFileDialog As New OpenFileDialog()
        'openFileDialog.Filter = "JPG(.jpg)|*.xls"
        openFileDialog.CheckFileExists = True
        If openFileDialog.ShowDialog() = DialogResult.OK Then
            Dim fname As String = openFileDialog.FileName
            Dim info As New System.IO.FileInfo(openFileDialog.FileName)
            Dim size As Long = info.Length
            If (size > Ini.ReadValue(Ini.Config.UX, "Setting", "FileMaxSize")) Then
                Call MsgBox("選択されたファイルのサイズがシステム制限（5M)を超えています。圧縮してから選択してください。")
            Else
                M_HEADER_IMAGE.ImageLocation = fname
            End If
        End If

    End Sub


    '
    '   各種詳細情報のタブ切り替え   
    '   
    Private Sub Status()
        Try
            btn_Copy.Visible = True
            btn_New.Visible = True
            '2018/09/11 MASTERしかないので一覧表示機能ボタンを非表示
            btn_Export.Visible = False
            btn_Update.Visible = True
            btn_History.Visible = True
            btn_Del.Visible = True

            SetUserRole()
        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
            haita_flag = False
        End Try
    End Sub


    '
    '   PDFファイルの選択処理
    '   
    Private Sub btn_File_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_File.Click
        Dim openFileDialog As New OpenFileDialog()
        openFileDialog.CheckFileExists = True
        openFileDialog.Filter = "PDF(.pdf)|*.pdf"
        If openFileDialog.ShowDialog() = DialogResult.OK Then
            Dim fname As String = openFileDialog.FileName
            Dim info As New System.IO.FileInfo(openFileDialog.FileName)
            Dim size As Long = info.Length
            If (size > Ini.ReadValue(Ini.Config.UX, "Setting", "FileMaxSize")) Then
                Call MsgBox("選択されたファイルのサイズがシステム制限（5M)を超えています。圧縮してから選択してください。")
            Else
                M_HEADER_CERTIFICATE_NAME.Text = fname
            End If
        End If
    End Sub

    Private Sub M_HEADER_HAIKI_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If M_HEADER_HAIKI_DATE.Checked = False Then
            Me.M_HEADER_HAIKI_DATE.Format = DateTimePickerFormat.Custom
            Me.M_HEADER_HAIKI_DATE.CustomFormat = "   "
            Me.M_HEADER_HAIKI_FLG.Checked = False   '2015/09/11 taskplan ADD 自動連動
            Return
        Else
            Me.M_HEADER_HAIKI_FLG.Checked = True    '2015/09/11 taskplan ADD 自動連動
        End If

        Me.M_HEADER_HAIKI_DATE.Format = DateTimePickerFormat.Short
        Me.M_HEADER_HAIKI_DATE.CustomFormat = vbNull
    End Sub

    Private Sub M_HEADER_TEKIGOUSEI_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_RENTAL_TEKIGOUSEI.ValueChanged
        If M_RENTAL_TEKIGOUSEI.Checked = False Then
            Me.M_RENTAL_TEKIGOUSEI.Format = DateTimePickerFormat.Custom
            Me.M_RENTAL_TEKIGOUSEI.CustomFormat = "   "
            Return
        End If
        Me.M_RENTAL_TEKIGOUSEI.Format = DateTimePickerFormat.Short
        Me.M_RENTAL_TEKIGOUSEI.CustomFormat = vbNull
    End Sub

    Private Sub M_HOZEN_CHECK_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HOZEN_CHECK_DATE.ValueChanged

        '2016
        Dim i As Integer

        If M_HOZEN_CHECK_DATE.Checked = False Then
            Me.M_HOZEN_CHECK_DATE.Format = DateTimePickerFormat.Custom
            Me.M_HOZEN_CHECK_DATE.CustomFormat = "   "

            GetEndCheckDate()

            Return
        End If
        Me.M_HOZEN_CHECK_DATE.Format = DateTimePickerFormat.Short
        Me.M_HOZEN_CHECK_DATE.CustomFormat = vbNull
        GetEndCheckDate()

        If IsDate(Me.M_HOZEN_CHECK_DATE.Value) = True Then
            i = 1
        End If

    End Sub

    Private Sub M_HOZEN_NEXT_CHECK_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HOZEN_NEXT_CHECK_DATE.ValueChanged
        If M_HOZEN_NEXT_CHECK_DATE.Checked = False Then
            Me.M_HOZEN_NEXT_CHECK_DATE.Format = DateTimePickerFormat.Custom
            Me.M_HOZEN_NEXT_CHECK_DATE.CustomFormat = "   "
            Return
        End If
        Me.M_HOZEN_NEXT_CHECK_DATE.Format = DateTimePickerFormat.Short
        Me.M_HOZEN_NEXT_CHECK_DATE.CustomFormat = vbNull
    End Sub

    Private Sub M_PME_KOUSEI_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_PME_KOUSEI_DATE.ValueChanged
        'If M_PME_KOUSEI_DATE.Checked = False Then
        '    Me.M_PME_KOUSEI_DATE.Format = DateTimePickerFormat.Custom
        '    Me.M_PME_KOUSEI_DATE.CustomFormat = "   "
        '    Return
        'End If
        Me.M_PME_KOUSEI_DATE.Format = DateTimePickerFormat.Short
        Me.M_PME_KOUSEI_DATE.CustomFormat = vbNull
    End Sub

    Private Sub M_PME_DUE_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_PME_DUE_DATE.ValueChanged
        'If M_PME_DUE_DATE.Checked = False Then
        '    Me.M_PME_DUE_DATE.Format = DateTimePickerFormat.Custom
        '    Me.M_PME_DUE_DATE.CustomFormat = "   "
        '    Return
        'End If
        Me.M_PME_DUE_DATE.Format = DateTimePickerFormat.Short
        Me.M_PME_DUE_DATE.CustomFormat = vbNull
    End Sub

    Private Sub M_RENTAL_LEASE_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_RENTAL_LEASE_DATE.ValueChanged
        If M_RENTAL_LEASE_DATE.Checked = False Then
            Me.M_RENTAL_LEASE_DATE.Format = DateTimePickerFormat.Custom
            Me.M_RENTAL_LEASE_DATE.CustomFormat = "   "
            Return
        End If
        Me.M_RENTAL_LEASE_DATE.Format = DateTimePickerFormat.Short
        Me.M_RENTAL_LEASE_DATE.CustomFormat = vbNull
    End Sub

    Private Sub M_RENTAL_KAKUNIN_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_RENTAL_KAKUNIN_DATE.ValueChanged
        If M_RENTAL_KAKUNIN_DATE.Checked = False Then
            Me.M_RENTAL_KAKUNIN_DATE.Format = DateTimePickerFormat.Custom
            Me.M_RENTAL_KAKUNIN_DATE.CustomFormat = "   "
            Return
        End If
        Me.M_RENTAL_KAKUNIN_DATE.Format = DateTimePickerFormat.Short
        Me.M_RENTAL_KAKUNIN_DATE.CustomFormat = vbNull
    End Sub

    Private Sub M_RENTAL_DUE_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_RENTAL_DUE_DATE.ValueChanged
        If M_RENTAL_DUE_DATE.Checked = False Then
            Me.M_RENTAL_DUE_DATE.Format = DateTimePickerFormat.Custom
            Me.M_RENTAL_DUE_DATE.CustomFormat = "   "
            Return
        End If
        Me.M_RENTAL_DUE_DATE.Format = DateTimePickerFormat.Short
        Me.M_RENTAL_DUE_DATE.CustomFormat = vbNull
    End Sub


    Private Sub M_LEASE_LEASE_START_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_LEASE_LEASE_START_DATE.ValueChanged
        If M_LEASE_LEASE_START_DATE.Checked = False Then
            Me.M_LEASE_LEASE_START_DATE.Format = DateTimePickerFormat.Custom
            Me.M_LEASE_LEASE_START_DATE.CustomFormat = "   "
            Return
        End If
        Me.M_LEASE_LEASE_START_DATE.Format = DateTimePickerFormat.Short
        Me.M_LEASE_LEASE_START_DATE.CustomFormat = vbNull
    End Sub

    Private Sub M_LEASE_LEASE_END_DATE_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_LEASE_LEASE_END_DATE.ValueChanged
        If M_LEASE_LEASE_END_DATE.Checked = False Then
            Me.M_LEASE_LEASE_END_DATE.Format = DateTimePickerFormat.Custom
            Me.M_LEASE_LEASE_END_DATE.CustomFormat = "   "
            Return
        End If
        Me.M_LEASE_LEASE_END_DATE.Format = DateTimePickerFormat.Short
        Me.M_LEASE_LEASE_END_DATE.CustomFormat = vbNull
    End Sub

    '
    '   更新履歴の呼び出し処理
    '   
    Private Sub btn_History_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_History.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor
        Try
            Dim history As History = New History()
            history.tabName = Me.TabControl1.SelectedTab.Tag
            history.gid = Me.gid
            history.Visible = True
            history.titleName = Me.TabControl1.SelectedTab.Text
            history.SearchHistoryData()

        Catch ex As Exception
            Helper.Log("btn_History_Click", ex)
        Finally
            Me.Cursor = Cursors.Default
            haita_flag = False
        End Try

    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl1.Selected
        If haita_flag Or Me.TabControl1.Controls.Count = 0 Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor
        Try

            Dim tabControlDict = New Dictionary(Of String, Object)
            Helper.GetAllControlsValues(Me.TabControl1.Controls.Item(0), tabControlDict, Me.TabControl1.Controls.Item(0).Tag)
            If TabControl1.SelectedIndex > 0 Then
                Helper.InitRefData(Me, tabControlDict, TabControl1.SelectedTab)
            End If

            InitMasterRefControlsValues()

        Catch ex As Exception
            Helper.Log("TabControl1_SelectedIndexChanged", ex)
        Finally
            Me.Cursor = Cursors.Default
            haita_flag = False
        End Try

    End Sub

    Private Sub InitMasterRefControlsValues()
        'ref child data
        If TabControl1.SelectedIndex = 0 Then
            one_ref_STATUS.SelectedIndex = 0
            one_ref_M_RENTAL_LEASE_CONPANY.Text = ""
            one_ref_M_RENTAL_NOURYOKU.SelectedIndex = 0
            Me.one_ref_M_RENTAL_TEKIGOUSEI.Format = DateTimePickerFormat.Custom
            one_ref_M_RENTAL_TEKIGOUSEI.CustomFormat = "   "


            If M_HEADER_RENTAL_FLG.Checked Then
                one_ref_M_RENTAL_LEASE_CONPANY.Text = M_RENTAL_LEASE_CONPANY.Text
                one_ref_M_RENTAL_NOURYOKU.SelectedIndex = M_RENTAL_NOURYOKU.SelectedIndex

                one_ref_M_RENTAL_TEKIGOUSEI.Value = M_RENTAL_TEKIGOUSEI.Value
                one_ref_M_RENTAL_TEKIGOUSEI.CustomFormat = M_RENTAL_TEKIGOUSEI.CustomFormat
                one_ref_M_RENTAL_TEKIGOUSEI.Checked = M_RENTAL_TEKIGOUSEI.Checked
                one_ref_M_RENTAL_TEKIGOUSEI.Format = M_RENTAL_TEKIGOUSEI.Format
            End If

            'status
            '汎用工具・器材
            If M_HEADER_EQUIPMENT_FLG.Checked Then
                one_ref_STATUS.SelectedIndex = M_TOOL_STATUS.SelectedIndex
            End If
            '保全設備
            If M_HEADER_HOZEN_FLG.Checked Then
                one_ref_STATUS.SelectedIndex = M_HOZEN_STATUS.SelectedIndex
            End If
            '定期校正計測器
            If M_HEADER_KOUSEI_FLG.Checked Then
                one_ref_STATUS.SelectedIndex = M_PME_STATUS.SelectedIndex
            End If
        End If

    End Sub

    '
    '   写真の削除処理   
    '   
    Private Sub btn_ImageClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ImageClear.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        M_HEADER_IMAGE.ImageLocation = ""

        Me.Cursor = Cursors.Default
        haita_flag = False
    End Sub


    '
    '   PDFファイルの削除処理
    '   
    Private Sub btn_FileClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_FileClear.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        M_HEADER_CERTIFICATE_NAME.Text = ""

        Me.Cursor = Cursors.Default
        haita_flag = False
    End Sub

    Private Function IsNoDataRepeat() As Boolean
        Dim tempList = New List(Of String)
        Dim valList = New List(Of TextBox)
        valList.Add(M_HEADER_BUNRUI_01)

        Dim IsRepeat = False
        For index = 0 To valList.Count - 1
            Dim text = valList.Item(index).Text
            If String.IsNullOrEmpty(text) Then
                Continue For
            End If
            If Not tempList.Exists(Function(val) val = text) Then
                tempList.Add(text)
            Else
                IsRepeat = True
                valList.Item(index).Focus()
            End If
        Next
        Return IsRepeat
    End Function


    ''' <summary>
    ''' 借用実績
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub M_HEADER_RENTAL_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HEADER_RENTAL_FLG.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor

        Try
            If M_HEADER_RENTAL_FLG.Checked Then
                M_HEADER_HOYU_FLG.Checked = False

                InitMasterRefControlsValues()

                SetFLG(False, False)
            Else
                M_HEADER_RENTAL_FLG.Checked = True
                ' 2016-04-07 YAMAOKA EDIT 関連フラグのブランクチェックを修正 START
                'SetFLG(True, False)
                SetFLG(False, False)
                ' 2016-04-07 YAMAOKA EDIT 関連フラグのブランクチェックを修正 END
            End If
            SetTabByCheckFlg()
            ShowTabs()

        Catch ex As Exception
            Helper.Log("M_HEADER_RENTAL_FLG_Check", ex)
        Finally
            Me.Cursor = Cursors.Default
            haita_flag = False
        End Try

    End Sub

    ''' <summary>
    ''' 保有
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub M_HEADER_HOYU_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HEADER_HOYU_FLG.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor
        Try
            If M_HEADER_HOYU_FLG.Checked Then
                M_HEADER_RENTAL_FLG.Checked = False

                InitMasterRefControlsValues()
                SetFLG(True, False)
            Else
                M_HEADER_HOYU_FLG.Checked = True
            End If

            SetTabByCheckFlg()
            ShowTabs()

        Catch ex As Exception
            Helper.Log("M_HEADER_HOYU_FLG_Check", ex)
        End Try

        Me.Cursor = Cursors.Default
        haita_flag = False
    End Sub

    ''' <summary>
    ''' リース
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub M_HEADER_LEASE_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HEADER_LEASE_FLG.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor
        Try
            If M_HEADER_LEASE_FLG.Checked Then
                M_HEADER_SHISAN_FLG.Checked = False
            End If

            SetTabByCheckFlg()
            ShowTabs()

        Catch ex As Exception
            Helper.Log("M_HEADER_LEASE_FLG_Check", ex)
        End Try

        Me.Cursor = Cursors.Default
        haita_flag = False
    End Sub

    ''' <summary>
    ''' 保全設備
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub M_HEADER_HOZEN_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HEADER_HOZEN_FLG.Click
        If M_HEADER_HOZEN_FLG.Checked Then
            '保全設備 Checked
            M_HEADER_EQUIPMENT_FLG.Checked = False
            M_HEADER_KOUSEI_FLG.Checked = False
            M_HEADER_NOT_KOUSEI_FLG.Checked = False

            InitMasterRefControlsValues()
        End If

        SetTabByCheckFlg()
        ShowTabs()
    End Sub

    Private Sub M_HEADER_CAR_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs)
        SetTabByCheckFlg()
        ShowTabs()
    End Sub

    ''' <summary>
    ''' 固定資産  CheckStateChanged
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub M_HEADER_SHISAN_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HEADER_SHISAN_FLG.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor
        Try
            If M_HEADER_SHISAN_FLG.Checked Then
                M_HEADER_LEASE_FLG.Checked = False
            End If
            SetTabByCheckFlg()
            ShowTabs()

        Catch ex As Exception
            Helper.Log("M_HEADER_SHISAN_FLG", ex)
        End Try

        Me.Cursor = Cursors.Default
        haita_flag = False

    End Sub

    Private Sub M_HEADER_KOUSEI_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HEADER_KOUSEI_FLG.Click
        Try
            If M_HEADER_KOUSEI_FLG.Checked Then
                '定期校正計測器 Checked
                M_HEADER_EQUIPMENT_FLG.Checked = False
                M_HEADER_HOZEN_FLG.Checked = False
                M_HEADER_NOT_KOUSEI_FLG.Checked = False

                InitMasterRefControlsValues()
            End If
            SetTabByCheckFlg()
            ShowTabs()
        Catch ex As Exception
            Helper.Log("M_HEADER_KOUSEI_FLG_Check", ex)
        End Try

    End Sub
    Private Sub M_HEADER_SHISETSU_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            SetTabByCheckFlg()
            ShowTabs()
        Catch ex As Exception
            Helper.Log("M_HEADER_SHISETSU_FLG_Check", ex)
        End Try

    End Sub

    Private Sub M_HEADER_EQUIPMENT_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HEADER_EQUIPMENT_FLG.Click
        Try
            If M_HEADER_EQUIPMENT_FLG.Checked Then
                '汎用工具・器材 Checked
                M_HEADER_HOZEN_FLG.Checked = False
                M_HEADER_KOUSEI_FLG.Checked = False
                M_HEADER_NOT_KOUSEI_FLG.Checked = False

                InitMasterRefControlsValues()
            End If
            SetTabByCheckFlg()
            ShowTabs()
        Catch ex As Exception
            Helper.Log("M_HEADER_EQUIPMENT_FLG_Check", ex)
        End Try

    End Sub

    Private Sub M_HEADER_NOT_KOUSEI_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HEADER_NOT_KOUSEI_FLG.Click
        Try
            If M_HEADER_NOT_KOUSEI_FLG.Checked Then
                '定期校正管理対象外計測器
                M_HEADER_EQUIPMENT_FLG.Checked = False
                M_HEADER_HOZEN_FLG.Checked = False
                M_HEADER_KOUSEI_FLG.Checked = False

                InitMasterRefControlsValues()
            End If
            SetTabByCheckFlg()
            ShowTabs()
        Catch ex As Exception
            Helper.Log("M_HEADER_NOT_KOUSEI_FLG_Check", ex)
        End Try
    End Sub

    Private Sub M_HEADER_OTHER_FLG_Check(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            SetTabByCheckFlg()
            ShowTabs()
        Catch ex As Exception
            Helper.Log("M_HEADER_OTHER_FLG_Check", ex)
        End Try

    End Sub

    Public Sub ClearFLG(ByVal enabled As Boolean, ByVal checked As Boolean)
        M_HEADER_HOYU_FLG.Checked = True
        SetFLG(enabled, checked)
    End Sub


    Public Sub SetFLG(ByVal enabled As Boolean, ByVal checked As Boolean)

        M_HEADER_LEASE_FLG.Checked = checked
        M_HEADER_LEASE_FLG.Enabled = enabled

        M_HEADER_SHISAN_FLG.Checked = checked
        M_HEADER_SHISAN_FLG.Enabled = enabled

    End Sub

    Public Sub SetTabByCheckFlg()
        showTabList.Clear()

        AddShowTab("lbl_M_HEADER")

        If M_HEADER_RENTAL_FLG.Checked Then
            AddShowTab("lbl_M_RENTAL")
        End If

        If M_HEADER_LEASE_FLG.Checked Then
            AddShowTab("lbl_M_LEASE")
        Else
            RemoveShowTab("lbl_M_LEASE")
        End If

        If M_HEADER_HOZEN_FLG.Checked Then
            AddShowTab("lbl_M_HOZEN")
        Else
            RemoveShowTab("lbl_M_HOZEN")
        End If


        If M_HEADER_SHISAN_FLG.Checked Then
            AddShowTab("lbl_M_SHISAN")
        Else
            RemoveShowTab("lbl_M_SHISAN")
        End If

        If M_HEADER_KOUSEI_FLG.Checked Then
            AddShowTab("lbl_M_PME")
        Else
            RemoveShowTab("lbl_M_PME")
        End If



        If M_HEADER_EQUIPMENT_FLG.Checked Then
            AddShowTab("lbl_M_TOOL")
        Else
            RemoveShowTab("lbl_M_TOOL")
        End If


    End Sub

    '
    '   タブ情報の表示処理（個別）
    '   
    Private Sub AddShowTab(ByVal tabName As String)
        If Not showTabList.Exists(Function(val) val = tabName) Then
            showTabList.Add(tabName)
        End If
    End Sub

    '
    '   タブ情報の非表示処理（個別）
    '   
    Private Sub RemoveShowTab(ByVal tabName As String)
        showTabList.Remove(tabName)
    End Sub

    '
    '   タブのリセット処理
    '   
    Private Sub ResetShowTab()
        M_HEADER_HOYU_FLG.Checked = True
        M_HEADER_EQUIPMENT_FLG.Checked = True
        SetFLG(True, False)
        SetTabByCheckFlg()
        ShowTabs()
    End Sub

    '
    '   タブ情報の表示処理
    '   
    Private Sub ShowTabs()
        Dim tabSel As String = Me.TabControl1.SelectedTab.Name
        Me.TabControl1.Controls.Clear()
        For Each tabPage As TabPage In tabPageList
            Dim tabName = tabPage.Name
            If showTabList.Exists(Function(val) val = tabName) Then
                Me.TabControl1.Controls.Add(tabPage)
                If tabSel.Equals(tabName) Then
                    If insert_flag Then
                    Else
                        Me.TabControl1.SelectTab(tabSel)
                    End If
                End If
            End If
        Next
        If insert_flag Then
            Me.TabControl1.SelectedIndex = 0
        End If
    End Sub

    Private Sub ShowAllTabs()
        Me.TabControl1.Controls.Clear()
        For Each tabPage As TabPage In tabPageList
            Me.TabControl1.Controls.Add(tabPage)
        Next
    End Sub

    Private Sub btn_Kousei_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Kousei.Click
        If kousei IsNot Nothing Then
            kousei.Dispose()
        End If
        kousei = New Kousei(Me)
        kousei.Show()
        Me.Hide()
    End Sub

    ''' <summary>
    ''' 点検間隔 SelectedIndexChanged
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub M_HOZEN_CHECK_INTERVAL_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M_HOZEN_CHECK_INTERVAL.SelectedIndexChanged
        GetEndCheckDate()
    End Sub


    Private Sub GetEndCheckDate()
        Try
            M_HOZEN_END_CHECK_DATE.Format = M_HOZEN_CHECK_DATE.Format
            M_HOZEN_END_CHECK_DATE.CustomFormat = M_HOZEN_CHECK_DATE.CustomFormat
            M_HOZEN_END_CHECK_DATE.Value = M_HOZEN_CHECK_DATE.Value

            If M_HOZEN_CHECK_INTERVAL.SelectedValue Is Nothing Or String.IsNullOrEmpty(M_HOZEN_CHECK_INTERVAL.SelectedValue) Or M_HOZEN_CHECK_DATE.Checked = False Then
                Return
            End If

            Dim monthStr = M_HOZEN_CHECK_INTERVAL.SelectedValue
            Dim reg As New Regex("(^[\d]*)")
            Dim m As Match = reg.Match(monthStr)
            If Not m.Success Or m.Groups.Count < 2 Then
                Return
            End If

            M_HOZEN_END_CHECK_DATE.Value = M_HOZEN_END_CHECK_DATE.Value.AddMonths(Integer.Parse(m.Groups(1).ToString()))
            '下記を追加 
            M_HOZEN_END_CHECK_DATE.Value = M_HOZEN_END_CHECK_DATE.Value.AddMonths(1)

            '月初日をセット
            Dim dt As Date = New Date(M_HOZEN_END_CHECK_DATE.Value.Year, M_HOZEN_END_CHECK_DATE.Value.Month, 1)
            M_HOZEN_END_CHECK_DATE.Value = dt.AddDays(-1)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub M_PME_ATA_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles M_PME_ATA.KeyUp
        If Helper.IsDecimal(Me.M_PME_ATA.Text) Then
            str = Me.M_PME_ATA.Text
        Else
            M_PME_ATA.Text = str
            M_PME_ATA.Focus()
        End If
    End Sub

    Private Sub M_HOZEN_ATA_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles M_HOZEN_ATA.KeyUp
        If Helper.IsDecimal(Me.M_HOZEN_ATA.Text) Then
            str = Me.M_HOZEN_ATA.Text
        Else
            M_HOZEN_ATA.Text = str
            M_HOZEN_ATA.Focus()
        End If
    End Sub

    Private Sub M_TOOL_ATA_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles M_TOOL_ATA.KeyUp
        If Helper.IsDecimal(Me.M_TOOL_ATA.Text) Then
            str = Me.M_TOOL_ATA.Text
        Else
            M_TOOL_ATA.Text = str
            M_TOOL_ATA.Focus()
        End If
    End Sub

    Private Sub M_PME_KOUSEI_KIKAN_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles M_PME_KOUSEI_KIKAN.KeyUp
        If Helper.IsDecimal(sender.Text) Then
            If sender.Text.ToString.IndexOf(".") > 0 Then
                Dim arr As String() = sender.Text.ToString.Split(".")
                If arr.GetValue(0).ToString.Length > 2 Or arr.GetValue(1).ToString.Length > 1 Then
                    sender.Text = str
                    sender.Focus()
                Else
                    str = sender.Text
                End If
            ElseIf sender.Text.ToString.Length > 2 Then
                sender.Text = str
                sender.Focus()
            Else
                str = sender.Text
            End If

        Else
            sender.Text = str
            sender.Focus()
        End If
    End Sub

    Private Sub SetUserRole()
        If LoginRoler.Employee.AX_MST <> "E" Then
            'View role, hide  create/update/delete
            btn_New.Visible = False
            btn_Insert.Visible = False
            btn_Update.Visible = False
            btn_Del.Visible = False
            btn_Cancel.Visible = False
            btn_Copy.Visible = False

            'btn_Kousei.Enabled = False
            M_HEADER_HOYU_FLG.Enabled = False
            M_HEADER_RENTAL_FLG.Enabled = False
            M_HEADER_SHISAN_FLG.Enabled = False
            M_HEADER_LEASE_FLG.Enabled = False
            M_HEADER_EQUIPMENT_FLG.Enabled = False
            M_HEADER_HOZEN_FLG.Enabled = False
            M_HEADER_KOUSEI_FLG.Enabled = False
            M_HEADER_NOT_KOUSEI_FLG.Enabled = False

        ElseIf LoginRoler.Employee.AX_MST = "E" Then
            'Edit
            If btn_Insert.Visible Then
                Return
            End If

            btn_New.Visible = True
            btn_Update.Visible = True
            btn_Del.Visible = True
            btn_Kousei.Enabled = True
            btn_Copy.Visible = True
        End If
    End Sub

    '
    '   複写設備の生成処理
    '   
    Private Sub btn_Copy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Copy.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor
        insert_flag = True
        Try
            M_HEADER_CREATED_BY.Text = LoginRoler.Employee.EMP_NAME
            M_HEADER_CREATED_DATE.Value = Date.Now
            '管理番号、名称、Certificate、写真、更新理由、備考、分類（全て未選択）
            Me.M_HEADER_KANRI_NO.Text = ""
            Me.M_HEADER_MEISYOU.Text = ""
            Me.M_HEADER_CERTIFICATE_NAME.Text = ""
            Me.M_HEADER_IMAGE.Image = Nothing
            Me.M_HEADER_IMAGE.ImageLocation = ""
            Me.M_HEADER_REASON.Text = ""
            Me.M_HEADER_REMARKS.Text = ""
            Me.M_HEADER_EQUIPMENT_FLG.Checked = False
            Me.M_HEADER_HOZEN_FLG.Checked = False
            Me.M_HEADER_KOUSEI_FLG.Checked = False
            Me.M_HEADER_NOT_KOUSEI_FLG.Checked = False

            Me.gmsValues = Nothing
            Me.gid = Nothing

            changeBtnStatus_Copy()

        Catch ex As Exception
            Helper.Log("btnCopy_Click", ex)
        Finally
            insert_flag = False
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btn_FileDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_FileDownload.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor
        Try
            Dim certificate_blob As Byte() = Nothing
            Dim certificate_name As String = ""
            Dim dt As DataTable = MasterRepository.GetBlobData(gid)
            If dt IsNot Nothing And dt.Rows.Count > 0 Then
                certificate_blob = dt.Rows.Item(0).Item("CERTIFICATE")
                certificate_name = dt.Rows.Item(0).Item("CERTIFICATE_NAME")
                If Helper.DownloadFile(certificate_blob, certificate_name) Then
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02117"), MsgBoxStyle.Information, "Information")
                Else
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02103"), MsgBoxStyle.Information, "Information")
                End If
            End If
        Catch ex As Exception
            Helper.Log("btn_FileDownload_Click", ex)
        Finally
            insert_flag = False
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btn_ImageDownload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ImageDownload.Click
        If haita_flag Then
            Return
        End If
        haita_flag = True
        Me.Cursor = Cursors.WaitCursor
        Try
            Dim certificate_blob As Byte() = Nothing
            Dim certificate_name As String = ""
            Dim dt As DataTable = MasterRepository.GetBlobData(gid)
            If dt IsNot Nothing And dt.Rows.Count > 0 Then
                certificate_blob = dt.Rows.Item(0).Item("IMAGE")
                certificate_name = dt.Rows.Item(0).Item("IMAGE_NAME")
                If Helper.DownloadFile(certificate_blob, certificate_name) Then
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02117"), MsgBoxStyle.Information, "Information")
                Else
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02103"), MsgBoxStyle.Information, "Information")
                End If
            End If
        Catch ex As Exception
            Helper.Log("btn_ImageDownload_Click", ex)
        Finally
            insert_flag = False
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    ''' <summary>
    ''' 2015/09/11 taskplan ADD 廃棄チェック連動
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub M_HEADER_HAIKI_FLG_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles M_HEADER_HAIKI_FLG.CheckedChanged
        Me.M_HEADER_HAIKI_DATE.Checked = M_HEADER_HAIKI_FLG.Checked
        If Me.M_HEADER_HAIKI_DATE.Checked Then Me.M_HEADER_HAIKI_DATE.Value = Now
    End Sub

End Class
