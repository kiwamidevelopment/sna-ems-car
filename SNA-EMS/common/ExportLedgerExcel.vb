﻿Imports System.Text
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Data
Imports System.Threading

'
'   クラス：ExportLedgerExcel
'   処理　：各種管理台帳出力フォーム案生成を行う
'
Public Class ExportLedgerExcel

    Private kensaku As Kensaku
    Private filePath As String
    Private template As String
    Private f_KEISOKKI_KANRI As String
    Private f_SETSUBI_KANRI As String
    Private f_SHISETSU_KIHON As String
    Private xlsFilePath As String
    Private isNoData As Boolean = True

    Private selected_KUBUN As String

    '
    '   クラスの生成処理
    '
    Public Sub New(ByVal kensaku As Kensaku)
        Me.kensaku = kensaku
        InitData()
    End Sub

    '
    '   情報の初期化処理
    '
    Private Sub InitData()
        filePath = Ini.ReadValue(Ini.Config.DB, "Setting", "TEMPPATH")
        f_KEISOKKI_KANRI = Ini.ReadValue(Ini.Config.DB, "Setting", "KEISOKKI_KANRI") '計測器管理台帳.xls
        f_SETSUBI_KANRI = Ini.ReadValue(Ini.Config.DB, "Setting", "SETSUBI_KANRI") '設備管理台帳.xls
        f_SHISETSU_KIHON = Ini.ReadValue(Ini.Config.DB, "Setting", "SHISETSU_KIHON") '施設設備基本台帳.xls
    End Sub

    '
    '   情報の出力処理
    '
    Public Sub Export()
        'write data into new file,save the file
        Dim xlApp As New Microsoft.Office.Interop.Excel.Application
        Dim xlBook As Microsoft.Office.Interop.Excel.Workbook = Nothing

        Try
            'choose path to save document
            Dim buffer As Byte() = Nothing
            Dim saveFileDialog As New SaveFileDialog()
            saveFileDialog.Filter = "Excel(.xls)|*.xls"
            If saveFileDialog.ShowDialog() = DialogResult.OK Then
                xlsFilePath = saveFileDialog.FileName
            End If
            If xlsFilePath = String.Empty Then
                Return
            End If

            kensaku.pgs.Location = New Point((kensaku.Width - kensaku.pgs.Width) / 2, kensaku.pgs.Location.Y)
            kensaku.pgs.Visible = True
            kensaku.pgs.Value = 0
            kensaku.pgs.Value += 5

            selected_KUBUN = kensaku.search_KUBUN.SelectedValue
            If String.IsNullOrEmpty(selected_KUBUN) Or "0" = selected_KUBUN Then
                'default 設備基本
                template = f_SHISETSU_KIHON
            ElseIf selected_KUBUN.Equals("1") Then
                '設備基本
                template = f_SHISETSU_KIHON
            ElseIf selected_KUBUN.Equals("2") Then
                '施設設備
                template = f_SETSUBI_KANRI
            ElseIf selected_KUBUN.Equals("3") Or selected_KUBUN.Equals("4") Then
                '計測器
                template = f_KEISOKKI_KANRI
            End If

            'copy template file to the choose path 
            FileCopy(String.Format("{0}\config\", Application.StartupPath()) + template, xlsFilePath)
            kensaku.pgs.Value += 5
            xlApp = CreateObject("Excel.Application")
            xlApp.Visible = False
            kensaku.pgs.Value += 5
            xlBook = xlApp.Workbooks.Open(xlsFilePath)    'open template file

            isNoData = True
            '===========
            kensaku.pgs.Value += 15
            kensaku.pgs.Value += 25
            kensaku.pgs.Value += 10
            kensaku.pgs.Value += 5
            kensaku.pgs.Value += 5
            If String.IsNullOrEmpty(selected_KUBUN) Or "0" = selected_KUBUN Then
                'default 設備基本
                Exp_SHISETSU_KIHON(xlApp)
            ElseIf selected_KUBUN.Equals("1") Then
                '設備基本
                Exp_SHISETSU_KIHON(xlApp)
            ElseIf selected_KUBUN.Equals("2") Then
                '施設設備
                Exp_SETSUBI_KANRI(xlApp)
            ElseIf selected_KUBUN.Equals("3") Or selected_KUBUN.Equals("4") Then
                '計測器
                'Exp_KEISOKKI_KANRI(xlApp)
                Exp_KOUSEI_KANRI(xlApp)
            End If
            kensaku.pgs.Value += 5
            kensaku.pgs.Value += 5
            kensaku.pgs.Value += 5
            kensaku.pgs.Value += 3
            kensaku.pgs.Value += 2
            kensaku.pgs.Value += 3
            kensaku.pgs.Value += 2
            '===========

            xlBook.Save()
            kensaku.pgs.Value = 100

            kensaku.pgs.Visible = False
            kensaku.pgs.Value = 0
            If isNoData = True Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02112"), MsgBoxStyle.Information, "Information")
                If Not xlBook Is Nothing Then
                    xlBook.Close()
                End If
                If Not xlApp Is Nothing Then
                    xlApp.Workbooks.Close()
                End If
                xlBook = Nothing
                xlApp = Nothing
                File.Delete(xlsFilePath)
            Else
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02107"), MsgBoxStyle.Information, "Information")
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If Not xlBook Is Nothing Then
                xlBook.Close()
            End If
            If Not xlApp Is Nothing Then
                xlApp.Workbooks.Close()
            End If
            xlBook = Nothing
            xlApp = Nothing
            kensaku.pgs.Visible = False
            kensaku.pgs.Value = 0
        End Try

    End Sub

    '
    '   情報の出力処理（施設設備基本台帳(案)）
    '
    Private Sub Exp_SHISETSU_KIHON(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Try
            Dim cnt As Integer = kensaku.M_HEADER_DataGridView.Rows.Count
            If cnt > 0 Then
                isNoData = False
                xlSheet = xlApp.ActiveSheet()

                Dim DataArray(65000, 28) As Object
                For index = 0 To cnt - 1

                    DataArray(index, 0) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KANRI_NO").Value) '管理番号
                    DataArray(index, 1) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_BUNRUI_01").Value) '分類番号
                    DataArray(index, 2) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MEISYOU").Value) '名称
                    DataArray(index, 3) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MODEL").Value) '型式
                    DataArray(index, 4) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KIKAKU").Value) '規格
                    DataArray(index, 5) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MFG_NO").Value) '製造番号
                    DataArray(index, 6) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_CREATED_DATE").Value) '登録日
                    DataArray(index, 7) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_BASE").Value) '設置場所
                    ' 2016-04-07 YAMAOKA EDIT エクセルレイアウト調整 START
                    'DataArray(index, 8) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_LOCATION").Value) 'ロケーション
                    DataArray(index, 8) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_LOCATION").Value) & Chr(10) & Chr(13) 'ロケーション
                    ' 2016-04-07 YAMAOKA EDIT エクセルレイアウト調整 END
                    DataArray(index, 9) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_HAICHI_KIJYUN").Value) '配置基準
                    DataArray(index, 10) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KUBUN_01").Value) '区分
                    DataArray(index, 11) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_STATUS").Value) 'STATUS
                    If Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_RENTAL_FLG").Value) = "Y" Then
                        DataArray(index, 12) = "○" '借用設備
                    Else
                        DataArray(index, 12) = "" '借用設備
                    End If

                    If Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_SHISAN_FLG").Value) = "Y" Then
                        DataArray(index, 13) = "○" '固定資産
                    Else
                        DataArray(index, 13) = "" '固定資産
                    End If

                    If Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_LEASE_FLG").Value) = "Y" Then
                        DataArray(index, 14) = "○" 'リース
                    Else
                        DataArray(index, 14) = "" 'リース
                    End If
                    DataArray(index, 15) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_UN_NO").Value) '危険物
                    DataArray(index, 16) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MFG").Value) '製造会社
                    DataArray(index, 17) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_SPL").Value) '購入会社
                Next
                xlSheet.Range("A2").Resize(cnt, 18).Value = DataArray
                xlSheet.PageSetup.RightFooter = "担当：" + LoginRoler.Employee.EMP_NAME
                xlSheet.PageSetup.LeftFooter = "様式 2-11-01" '2015/09/11 ADD taskplan

                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（設備管理台帳）
    '
    Private Sub Exp_SETSUBI_KANRI(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Try
            Dim cnt As Integer = kensaku.M_HEADER_DataGridView.Rows.Count
            If cnt > 0 Then
                isNoData = False
                xlSheet = xlApp.ActiveSheet
                Dim DataArray(65000, 28) As Object
                For index = 0 To cnt - 1
                    DataArray(index, 0) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KANRI_NO").Value) '管理番号
                    DataArray(index, 1) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_BUNRUI_01").Value) '分類番号
                    DataArray(index, 2) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MEISYOU").Value) '名称
                    DataArray(index, 3) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MODEL").Value) '型式
                    DataArray(index, 4) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KIKAKU").Value) '規格
                    DataArray(index, 5) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MFG_NO").Value) '製造番号
                    DataArray(index, 6) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_CREATED_DATE").Value) '登録日
                    DataArray(index, 7) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_BASE").Value) '設置場所
                    DataArray(index, 8) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_LOCATION").Value) 'ロケーション
                    DataArray(index, 9) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_HAICHI_KIJYUN").Value) '配置基準
                    DataArray(index, 10) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KUBUN_01").Value) '区分
                    DataArray(index, 11) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_STATUS").Value) 'STATUS
                    DataArray(index, 12) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_CHECK_INTERVAL").Value) '点検間隔
                    DataArray(index, 13) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_CHECK_DATE").Value) '点検実施日
                    DataArray(index, 14) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_NEXT_CHECK_DATE").Value) '次回点検月
                    DataArray(index, 15) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_END_CHECK_DATE").Value) '点検期限
                    DataArray(index, 16) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_CHECK_COMPANY").Value) '点検会社
                Next
                xlSheet.Range("A2").Resize(cnt, 17).Value = DataArray
                xlSheet.PageSetup.RightFooter = "担当：" + LoginRoler.Employee.EMP_NAME
                xlSheet.PageSetup.LeftFooter = "様式 5-11-01" '2015/09/11 ADD taskplan
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（計測器管理台帳）
    '
    Private Sub Exp_KEISOKKI_KANRI(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Try
            Dim cnt As Integer = kensaku.M_HEADER_DataGridView.Rows.Count
            If cnt > 0 Then
                isNoData = False
                xlSheet = xlApp.ActiveSheet
                Dim DataArray(65000, 28) As Object
                For index = 0 To cnt - 1
                    DataArray(index, 0) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KANRI_NO").Value) '管理番号
                    DataArray(index, 1) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_BUNRUI_01").Value) '分類番号
                    DataArray(index, 2) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MEISYOU").Value) '名称
                    DataArray(index, 3) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MODEL").Value) '型式
                    DataArray(index, 4) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KIKAKU").Value) '規格
                    DataArray(index, 5) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MFG_NO").Value) '製造番号
                    DataArray(index, 6) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_CREATED_DATE").Value) '登録日
                    DataArray(index, 7) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_BASE").Value) '設置場所
                    DataArray(index, 8) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_LOCATION").Value) 'ロケーション
                    DataArray(index, 9) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_HAICHI_KIJYUN").Value) '配置基準
                    DataArray(index, 10) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KUBUN_01").Value) '区分
                    DataArray(index, 11) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_STATUS").Value) 'STATUS
                    DataArray(index, 12) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KOUSEI_INTERVAL").Value) '校正間隔
                    DataArray(index, 13) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KOUSEI_COMPANY").Value) '校正会社
                    DataArray(index, 14) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KOUSEI_BY_COMPANY").Value) '校正仲介会社
                    DataArray(index, 15) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KOUSEI_DATE").Value) '最終校正日
                    DataArray(index, 16) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_DUE_DATE").Value) '有効期限
                Next
                xlSheet.Range("A2").Resize(cnt, 17).Value = DataArray
                xlSheet.PageSetup.RightFooter = "担当：" + LoginRoler.Employee.EMP_NAME
                '                xlSheet.PageSetup.LeftFooter = "" '2015/09/11 ADD taskplan(対象となる指定がない為、空欄)

                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（校正計測器管理台帳）
    '
    Private Sub Exp_KOUSEI_KANRI(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Try
            Dim cnt As Integer = kensaku.M_HEADER_DataGridView.Rows.Count
            If cnt > 0 Then
                isNoData = False
                xlSheet = xlApp.ActiveSheet
                 Dim DataArray(65000, 28) As Object
                For index = 0 To cnt - 1
                    DataArray(index, 0) = index + 1
                    DataArray(index, 1) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KANRI_NO").Value) '管理番号
                    DataArray(index, 2) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_BUNRUI_01").Value) '分類番号
                    DataArray(index, 3) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_HAICHI_KIJYUN").Value) '配置基準
                    DataArray(index, 4) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MEISYOU").Value) '名称
                    DataArray(index, 5) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MODEL").Value) '型式
                    DataArray(index, 6) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_MFG_NO").Value) 'シリアル番号
                    DataArray(index, 7) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_BASE").Value) '設置場所
                    DataArray(index, 8) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KOUSEI_COMPANY").Value) '校正会社
                    DataArray(index, 9) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KOUSEI_DATE").Value) '最終校正日
                    DataArray(index, 10) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_DUE_DATE").Value) '有効期限
                    DataArray(index, 11) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_KOUSEI_INTERVAL").Value) '校正間隔
                    DataArray(index, 12) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_STATUS").Value) 'STATUS
                    DataArray(index, 13) = Helper.DB_NullOrEmpty(kensaku.M_HEADER_DataGridView.Rows(index).Cells("DG_REMARKS").Value) '校正間隔
                Next
                xlSheet.Range("A2").Resize(cnt, 14).Value = DataArray
                xlSheet.PageSetup.RightFooter = "担当：" + LoginRoler.Employee.EMP_NAME
                xlSheet.PageSetup.LeftFooter = "様式 5-12-01" '2015/09/11 ADD taskplan

                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub
End Class