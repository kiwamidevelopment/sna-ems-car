Imports System.IO

'
'   クラス：Ini
'   処理　：システムの設定情報の取得を行う
'
Public Class Ini

    '
    '   設定情報のセクション情報
    '
    Public Enum Config
        DB
        LANG
        UX
    End Enum

    Private Shared res4lang As String = String.Format("{0}\config\res4lang.ini", Application.StartupPath())
    Private Shared res4db As String = String.Format("{0}\config\res4db.ini", Application.StartupPath())
    Private Shared res4ux As String = String.Format("{0}\config\res4ux.ini", Application.StartupPath())

    '
    '   設定情報の取得
    '
    Public Shared Function ReadValue(ByVal config As Config, ByVal section As String, ByVal key As String) As String
        Dim sPath As String = Nothing
        Select Case config
            Case SNA.SYARYO.Ini.Config.DB
                sPath = res4db
            Case SNA.SYARYO.Ini.Config.LANG
                sPath = res4lang
            Case SNA.SYARYO.Ini.Config.UX
                sPath = res4ux
        End Select
        section = "[" + section + "]"
        key = key + "="
        Dim fileContents As String() = File.ReadAllLines(sPath)
        Dim str As String = ""
        Dim isSection As Boolean = False
        For index = 0 To fileContents.Length - 1
            If fileContents(index) = section Then
                isSection = True
            End If
            If isSection And fileContents(index).StartsWith(key) Then
                str = fileContents(index).Substring(key.Length)
            End If
        Next

        If key = "TEMPPATH" Then
            Try
                If Not Directory.Exists(str) Then
                    System.IO.Directory.CreateDirectory(str)
                End If
            Catch ex As Exception
                str = Helper.GetUserTempPath()
                If Not Directory.Exists(str) Then
                    System.IO.Directory.CreateDirectory(str)
                End If
            End Try
        End If

        Return str
    End Function

End Class
