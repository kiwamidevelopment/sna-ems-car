﻿Imports System.Text
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Data
Imports System.Threading

'
'   クラス：ExportExcel
'   処理　：設備情報の帳票生成を行う
'
Public Class ExportFormExcel

    Private main As Main
    Private filePath As String
    Private template As String
    Private imageName As String
    Private combinationStatus As StringBuilder
    Private relativeImagePath As String
    Private relativePDFPath As String
    Private KANRI_NO As String
    Private xlsFilePath As String
    Private isNoData As Boolean = True

    Private LEASE_FLG As Boolean = True
    Private HOZEN_FLG As Boolean = True
    Private CAR_FLG As Boolean = True
    Private RENTAL_FLG As Boolean = True
    Private HAIKI_FLG As Boolean = True
    Private SHISAN_FLG As Boolean = True
    Private KOUSEI_FLG As Boolean = True
    Private SHISETSU_FLG As Boolean = True
    Private EQUIPMENT_FLG As Boolean = True
    Private OTHER_FLG As Boolean = True

    '
    '   クラスの生成処理
    '
    Public Sub New(ByVal main As Main)
        Me.main = main
        InitData()
    End Sub

    '
    '   情報の初期化処理
    '
    Private Sub InitData()
        filePath = Ini.ReadValue(Ini.Config.DB, "Setting", "TEMPPATH")
        template = Ini.ReadValue(Ini.Config.DB, "Setting", "FILENAME")
        imageName = Ini.ReadValue(Ini.Config.DB, "Setting", "IMAGENAME")
    End Sub

    '
    '   情報の出力処理
    '
    Public Sub Export()
        'write data into new file,save the file
        Dim xlApp As New Microsoft.Office.Interop.Excel.Application
        Dim xlBook As Microsoft.Office.Interop.Excel.Workbook = Nothing

        Try
            combinationStatus = New StringBuilder()
            relativeImagePath = ""
            'step1 choose path to save document
            Dim buffer As Byte() = Nothing
            Dim saveFileDialog As New SaveFileDialog()
            saveFileDialog.Filter = "Excel(.xls)|*.xls"
            If saveFileDialog.ShowDialog() = DialogResult.OK Then
                xlsFilePath = saveFileDialog.FileName
            End If
            If xlsFilePath = String.Empty Then
                Return
            End If

            main.pgs.Visible = True
            main.pgs.Value = 0
            main.pgs.Value += 5

            'step2 copy template file to the choose path 
            FileCopy(String.Format("{0}\config\", Application.StartupPath()) + template, xlsFilePath)
            main.pgs.Value += 5
            xlApp = CreateObject("Excel.Application")
            xlApp.Visible = False
            main.pgs.Value += 5
            xlBook = xlApp.Workbooks.Open(xlsFilePath)    'open template file

            isNoData = True
            '===========
            main.pgs.Value += 15
            Exp_M_HEADER(xlApp)
            main.pgs.Value += 25
            Exp_M_TOOL(xlApp)
            main.pgs.Value += 10
            Exp_M_HOZEN(xlApp)
            Exp_M_HOZEN_HISTORY(xlApp)
            main.pgs.Value += 5
            Exp_M_PME(xlApp)
            Exp_M_PME_HISTORY(xlApp)
            main.pgs.Value += 5
            'Exp_M_CAR(xlApp)
            'Exp_M_CAR_HISTORY(xlApp)
            main.pgs.Value += 5
            Exp_M_RENTAL(xlApp)
            Exp_M_RENTAL_HISTORY(xlApp)
            main.pgs.Value += 5
            'Exp_M_SHISETSU(xlApp)
            'Exp_M_SHISETSU_HISTORY(xlApp)
            main.pgs.Value += 5
            'Exp_M_OTHER(xlApp)
            main.pgs.Value += 3
            Exp_M_LEASE(xlApp)
            Exp_M_LEASE_HISTORY(xlApp)
            main.pgs.Value += 2
            Exp_VIEW_PAGE(xlApp)
            main.pgs.Value += 3
            'Exp_M_HAIKI(xlApp)
            main.pgs.Value += 2
            Exp_M_SHISAN(xlApp)
            '===========

            xlBook.Save()
            main.pgs.Value = 100
            'step5 ask user if he want to open file,yes to open file
            'If (MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02104"), MsgBoxStyle.YesNo, "Open File") = vbYes) Then
            '    xlApp.Visible = True
            'Else
            'End If
            main.pgs.Visible = False
            main.pgs.Value = 0
            If isNoData = True Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02112"), MsgBoxStyle.Information, "Information")
                If Not xlBook Is Nothing Then
                    xlBook.Close()
                End If
                If Not xlApp Is Nothing Then
                    xlApp.Workbooks.Close()
                End If
                xlBook = Nothing
                xlApp = Nothing
                File.Delete(xlsFilePath)
            Else
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02107"), MsgBoxStyle.Information, "Information")
            End If
        Catch ex As Exception
            Throw ex
        Finally
            If Not xlBook Is Nothing Then
                xlBook.Close()
            End If
            If Not xlApp Is Nothing Then
                xlApp.Workbooks.Close()
            End If
            xlBook = Nothing
            xlApp = Nothing
            main.pgs.Visible = False
            main.pgs.Value = 0
        End Try
    End Sub

    '
    '   情報の出力処理（ヘッダー情報）
    '
    Private Sub Exp_M_HEADER(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim no As New StringBuilder()
        Dim kubun As New StringBuilder()
        Dim dt As System.Data.DataTable

        Try
            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_HEADER", main.kanriNo)

            If dt.Rows.Count > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Sheets.Item(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_HEADER"))
                If xlSheet Is Nothing Then
                    Return
                End If
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_HEADER"))
                xlSheet.Cells(4, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("HOYU_FLG"))  '保有
                xlSheet.Cells(5, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("HAICHI_KIJYUN"))    '配置基準
                xlSheet.Cells(6, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KANRI_NO"))    '管理番号
                xlSheet.Cells(7, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("BUNRUI_01"))    '分類No.
                xlSheet.Cells(8, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MEISYOU"))    '名称
                xlSheet.Cells(9, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODEL"))    '型式
                xlSheet.Cells(10, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("Reference"))    'Reference
                xlSheet.Cells(11, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MFG_NO"))    '製造番号
                xlSheet.Cells(12, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KIKAKU"))    '規格
                'xlSheet.Cells(13, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ATA"))    'ATA
                xlSheet.Cells(13, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MFG"))    '製造会社
                xlSheet.Cells(14, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("SPL"))    '購入会社
                xlSheet.Cells(16, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("UN_NO"))    '危険物
                xlSheet.Cells(17, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KUBUN_01"))    '区分
                xlSheet.Cells(18, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("BASE"))    '配置基地
                xlSheet.Cells(19, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("LOCATION"))    'ロケーション
                xlSheet.Cells(20, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("EQUIPMENT_FLG")) '汎用工具・器材
                xlSheet.Cells(21, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("HOZEN_FLG"))    '保全設備
                'xlSheet.Cells(23, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("HOZEN_INTERVAL"))    '保全設備点検間隔
                xlSheet.Cells(22, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KOUSEI_FLG"))    '定期校正計測器
                'xlSheet.Cells(25, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KOUSEI_INTERVAL"))    '校正間隔
                xlSheet.Cells(23, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("NOT_KOUSEI_FLG"))    '定期校正管理対象外計測器
                'xlSheet.Cells(27, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CAR_FLG"))    '車両
                'xlSheet.Cells(28, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CAR_INTERVAL"))    '車両点検間隔
                xlSheet.Cells(24, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("RENTAL_FLG"))    '借用実績
                'xlSheet.Cells(30, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("RENTAL_CONPANY"))    '借用先
                'xlSheet.Cells(31, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("NOURYOKU"))    '能力審査
                'xlSheet.Cells(32, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("TEKIGOUSEI"))    '適合性確認
                xlSheet.Cells(25, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("DOUTOUSEI"))    '同等性確認
                xlSheet.Cells(26, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("SHISAN_FLG"))    '固定資産
                'xlSheet.Cells(35, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("TAIYOU"))    '耐用年数
                'xlSheet.Cells(36, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("SHISAN_NO"))    '固定資産番号
                xlSheet.Cells(27, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("LEASE_FLG"))    'リース
                'xlSheet.Cells(38, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("OTHER_FLG"))    'その他設備
                'xlSheet.Cells(39, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("SHISETSU_FLG"))    '施設・搬送器材
                'xlSheet.Cells(40, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("SHISETSU_INTERVAL"))    '施設・搬送機材点検間隔
                xlSheet.Cells(28, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("HAIKI_FLG"))    '廃棄
                xlSheet.Cells(29, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("HAIKI_DATE"))    '廃棄日
                xlSheet.Cells(30, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CREATED_BY"))    '登録者

                xlSheet.Cells(31, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CREATED_DATE"))     '登録日
                xlSheet.Cells(32, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REMARKS"))    '備考
                xlSheet.Cells(33, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REASON"))    '更新理由

                If Helper.DB_NullOrEmpty(dt.Rows(0)("LEASE_FLG")) = "Y" Then
                    LEASE_FLG = True
                Else
                    LEASE_FLG = False
                End If

                If Helper.DB_NullOrEmpty(dt.Rows(0)("HOZEN_FLG")) = "Y" Then
                    HOZEN_FLG = True
                Else
                    HOZEN_FLG = False
                End If

                'If Helper.DB_NullOrEmpty(dt.Rows(0)("CAR_FLG")) = "Y" Then
                '    CAR_FLG = True
                'Else
                '    CAR_FLG = False
                'End If

                If Helper.DB_NullOrEmpty(dt.Rows(0)("RENTAL_FLG")) = "Y" Then
                    RENTAL_FLG = True
                Else
                    RENTAL_FLG = False
                End If

                'If Helper.DB_NullOrEmpty(dt.Rows(0)("HAIKI_FLG")) = "Y" Then
                '    HAIKI_FLG = True
                'Else
                '    HAIKI_FLG = False
                'End If

                If Helper.DB_NullOrEmpty(dt.Rows(0)("SHISAN_FLG")) = "Y" Then
                    SHISAN_FLG = True
                Else
                    SHISAN_FLG = False
                End If

                If Helper.DB_NullOrEmpty(dt.Rows(0)("KOUSEI_FLG")) = "Y" Then
                    KOUSEI_FLG = True
                Else
                    KOUSEI_FLG = False
                End If

                'If Helper.DB_NullOrEmpty(dt.Rows(0)("SHISETSU_FLG")) = "Y" Then
                '    SHISETSU_FLG = True
                'Else
                '    SHISETSU_FLG = False
                'End If

                If Helper.DB_NullOrEmpty(dt.Rows(0)("EQUIPMENT_FLG")) = "Y" Then
                    EQUIPMENT_FLG = True
                Else
                    EQUIPMENT_FLG = False
                End If

                'If Helper.DB_NullOrEmpty(dt.Rows(0)("OTHER_FLG")) = "Y" Then
                '    OTHER_FLG = True
                'Else
                '    OTHER_FLG = False
                'End If

                Dim resultBasePath = Path.GetDirectoryName(xlsFilePath)
                Dim tFileName = Format(DateTime.Now, "yyyyMMddhhmmss")

                'If TypeOf dt.Rows(0)("image") Is DBNull Then
                '    relativeImagePath = ""
                'Else
                '    'If Not dt.Rows(0)("image") Is Nothing And dt.Rows(0)("image").Length > 1 Then
                '    Dim bytes As Byte() = dt.Rows(0)("image")
                '    Dim imageLocation = Path.Combine(filePath, imageName)
                '    relativeImagePath = String.Format("{0}{1}{2}", Path.GetFileNameWithoutExtension(imageName), tFileName, Path.GetExtension(imageName))
                '    Dim resultImagePath = Path.Combine(resultBasePath, relativeImagePath)
                '    FileCopy(imageLocation, resultImagePath)
                '    xlSheet.Hyperlinks.Add(xlSheet.Cells(34, 3), relativeImagePath) '写真
                'End If

                If Not dt.Rows(0)("image") Is DBNull.Value Then
                    If Not dt.Rows(0)("image") Is Nothing And dt.Rows(0)("image").Length > 1 Then
                        Dim bytes As Byte() = dt.Rows(0)("image")
                        Dim imageLocation = Path.Combine(filePath, imageName)
                        relativeImagePath = String.Format("{0}{1}{2}", Path.GetFileNameWithoutExtension(imageName), tFileName, Path.GetExtension(imageName))
                        Dim resultImagePath = Path.Combine(resultBasePath, relativeImagePath)
                        FileCopy(imageLocation, resultImagePath)
                        xlSheet.Hyperlinks.Add(xlSheet.Cells(34, 3), relativeImagePath) '写真
                    Else
                        relativeImagePath = ""
                    End If
                Else
                    relativeImagePath = ""
                End If

                Dim cerName = Ini.ReadValue(Ini.Config.DB, "Setting", "CERTIFICATE")
                If Not dt.Rows(0)("certificate") Is DBNull.Value Then
                    If Not dt.Rows(0)("certificate") Is Nothing And dt.Rows(0)("certificate").Length > 1 Then
                        Dim cerBytes As Byte() = dt.Rows(0)("certificate")
                        Dim cerLocation = Path.Combine(filePath, cerName)
                        relativePDFPath = String.Format("{0}{1}{2}", Path.GetFileNameWithoutExtension(cerName), tFileName, Path.GetExtension(cerName))
                        Dim resultcerPath = Path.Combine(resultBasePath, relativePDFPath)
                        FileCopy(cerLocation, resultcerPath)
                        xlSheet.Hyperlinks.Add(xlSheet.Cells(15, 3), relativePDFPath) 'Certificate
                    Else
                        relativePDFPath = ""
                    End If
                Else
                    relativePDFPath = ""
                End If



                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   シートの存在有無チェック
    '
    Private Shared Function Check_Sheet_Exist(ByRef xlApp As Microsoft.Office.Interop.Excel.Application, ByVal sheetName As String, ByVal isExist As Boolean) As Boolean
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        xlSheet = xlApp.Sheets.Item(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", sheetName))
        If xlSheet Is Nothing Then
            Return False
        End If
        If isExist = False Then
            xlSheet.Tab.ColorIndex = 1
            xlSheet = Nothing
            Return False
        End If
        Return True
    End Function

    '
    '   情報の出力処理（TOOL情報）
    '
    Private Sub Exp_M_TOOL(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable
        Dim temp As String = ""

        If Check_Sheet_Exist(xlApp, "M_TOOL", Me.EQUIPMENT_FLG) = False Then
            Return
        End If
        Try

            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_TOOL", main.kanriNo)
            'xlSheet = xlApp.ActiveSheet
            xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_TOOL"))
            If dt.Rows.Count > 0 Then
                isNoData = False
                temp = Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS"))
                xlSheet.Cells(16, 3).Value = temp
                xlSheet.Cells(11, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ATA"))
                If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
                Else
                    xlSheet.Hyperlinks.Add(xlSheet.Cells(19, 3), relativeImagePath) '写真
                End If

                xlSheet.Cells(20, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG"))
                If Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG")) = "Y" Then
                    combinationStatus.Append(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_TOOL") + ":" + temp)
                    combinationStatus.Append(vbCrLf)
                End If
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（保全情報）
    '
    Private Sub Exp_M_HOZEN(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable

        If Check_Sheet_Exist(xlApp, "M_HOZEN", Me.HOZEN_FLG) = False Then
            Return
        End If
        Try
            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_HOZEN", main.kanriNo)
            If dt.Rows.Count > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_HOZEN"))
                xlSheet.Cells(11, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ATA"))
                xlSheet.Cells(17, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CHECK_INTERVAL"))
                xlSheet.Cells(18, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CHECK_COMPANY"))
                xlSheet.Cells(19, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CHECK_BY"))
                xlSheet.Cells(20, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CHECK_DATE"))
                xlSheet.Cells(21, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("NEXT_CHECK_DATE"))
                xlSheet.Cells(22, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("END_CHECK_DATE"))


                xlSheet.Cells(23, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_DATE"))
                xlSheet.Cells(24, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_BY"))

                xlSheet.Cells(25, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS"))
                xlSheet.Cells(28, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REMARKS"))
                xlSheet.Cells(29, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REASON"))

                If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
                Else
                    xlSheet.Hyperlinks.Add(xlSheet.Cells(30, 3), relativeImagePath) '写真
                End If

                xlSheet.Cells(31, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG"))
                If Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG")) = "Y" Then
                    combinationStatus.Append(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_HOZEN") + ":" + Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS")))
                    combinationStatus.Append(vbCrLf)
                End If

                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（保全履歴）
    '
    Private Sub Exp_M_HOZEN_HISTORY(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable

        If Check_Sheet_Exist(xlApp, "M_HOZEN_HISTORY", Me.HOZEN_FLG) = False Then
            Return
        End If
        Try

            'KANRI_NO
            dt = MasterRepository.GetAllHistoryDataFromDB("M_HOZEN_HISTORY", main.gid)
            Dim cnt As Integer = dt.Rows.Count
            If cnt > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_HOZEN_HISTORY"))

                Dim DataArray(65000, 28) As Object
                For index = 0 To dt.Rows.Count - 1
                    DataArray(index, 0) = index + 1
                    DataArray(index, 1) = Helper.DB_NullOrEmpty(dt.Rows(index)("TRAN_CODE"))
                    DataArray(index, 2) = Helper.DB_NullOrEmptyHaveTime(dt.Rows(index)("TRAN_DATE"))
                    DataArray(index, 3) = Helper.DB_NullOrEmpty(dt.Rows(index)("CHECK_COMPANY"))
                    DataArray(index, 4) = Helper.DB_NullOrEmpty(dt.Rows(index)("CHECK_BY"))
                    DataArray(index, 5) = Helper.DB_NullOrEmpty(dt.Rows(index)("CHECK_DATE"))
                    DataArray(index, 6) = Helper.DB_NullOrEmpty(dt.Rows(index)("NEXT_CHECK_DATE"))
                    DataArray(index, 7) = Helper.DB_NullOrEmpty(dt.Rows(index)("CHECK_INTERVAL"))
                    DataArray(index, 8) = Helper.DB_NullOrEmpty(dt.Rows(index)("END_CHECK_DATE"))
                    DataArray(index, 9) = Helper.DB_NullOrEmpty(dt.Rows(index)("ATA"))
                    DataArray(index, 10) = Helper.DB_NullOrEmpty(dt.Rows(index)("STATUS"))
                    DataArray(index, 11) = Helper.DB_NullOrEmpty(dt.Rows(index)("ALLOW_FLG"))
                    DataArray(index, 12) = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_DATE"))
                    DataArray(index, 13) = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_BY"))
                    DataArray(index, 14) = Helper.DB_NullOrEmpty(dt.Rows(index)("REASON"))
                    DataArray(index, 15) = Helper.DB_NullOrEmpty(dt.Rows(index)("REMARKS"))
                Next
                xlSheet.Range("A4").Resize(cnt, 16).Value = DataArray
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（PME）
    '
    Private Sub Exp_M_PME(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim no As New StringBuilder()
        Dim dt As System.Data.DataTable

        If Check_Sheet_Exist(xlApp, "M_PME", Me.KOUSEI_FLG) = False Then
            Return
        End If
        Try

            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_PME", main.kanriNo)

            If dt.Rows.Count > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_PME"))
                xlSheet.Cells(10, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ATA"))
                xlSheet.Cells(16, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KOUSEI_COMPANY"))
                xlSheet.Cells(17, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KOUSEI_BY_COMPANY"))
                xlSheet.Cells(18, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KOUSEI_INTERVAL"))
                xlSheet.Cells(19, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KOUSEI_SEIDO"))
                xlSheet.Cells(20, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KOUSEI_DATE"))
                xlSheet.Cells(21, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KOUSEI_KIKAN"))
                xlSheet.Cells(22, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("NOURYOKU"))
                xlSheet.Cells(23, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KEKKA"))
                xlSheet.Cells(24, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("DUE_DATE"))
                xlSheet.Cells(25, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_DATE"))
                xlSheet.Cells(26, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_BY"))
                xlSheet.Cells(27, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS"))
                xlSheet.Cells(30, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REMARKS"))
                xlSheet.Cells(31, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REASON"))

                If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
                Else
                    xlSheet.Hyperlinks.Add(xlSheet.Cells(32, 3), relativeImagePath) '写真
                End If

                xlSheet.Cells(33, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG"))
                If Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG")) = "Y" Then
                    combinationStatus.Append(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_PME") + ":" + Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS")))
                    combinationStatus.Append(vbCrLf)
                End If
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（PME履歴）
    '
    Private Sub Exp_M_PME_HISTORY(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable
        Dim no As New StringBuilder()
        If Check_Sheet_Exist(xlApp, "M_PME_HISTORY", Me.KOUSEI_FLG) = False Then
            Return
        End If

        Try

            'KANRI_NO
            dt = MasterRepository.GetAllHistoryDataFromDB("M_PME_HISTORY", main.gid)
            Dim cnt As Integer = dt.Rows.Count
            If cnt > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_PME_HISTORY"))

                Dim DataArray(65000, 28) As Object
                For index = 0 To dt.Rows.Count - 1
                    DataArray(index, 0) = index + 1
                    DataArray(index, 1) = Helper.DB_NullOrEmpty(dt.Rows(index)("TRAN_CODE"))
                    DataArray(index, 2) = Helper.DB_NullOrEmptyHaveTime(dt.Rows(index)("TRAN_DATE"))
                    DataArray(index, 3) = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_COMPANY"))
                    DataArray(index, 4) = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_BY_COMPANY"))
                    DataArray(index, 5) = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_SEIDO"))
                    DataArray(index, 6) = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_INTERVAL"))
                    DataArray(index, 7) = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_DATE"))
                    DataArray(index, 8) = Helper.DB_NullOrEmpty(dt.Rows(index)("KEKKA"))
                    DataArray(index, 9) = Helper.DB_NullOrEmpty(dt.Rows(index)("DUE_DATE"))
                    DataArray(index, 10) = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_KIKAN"))
                    DataArray(index, 11) = Helper.DB_NullOrEmpty(dt.Rows(index)("NOURYOKU"))
                    DataArray(index, 12) = Helper.DB_NullOrEmpty(dt.Rows(index)("ATA"))
                    DataArray(index, 13) = Helper.DB_NullOrEmpty(dt.Rows(index)("STATUS"))
                    DataArray(index, 14) = Helper.DB_NullOrEmpty(dt.Rows(index)("ALLOW_FLG"))
                    DataArray(index, 15) = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_DATE"))
                    DataArray(index, 16) = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_BY"))
                    DataArray(index, 17) = Helper.DB_NullOrEmpty(dt.Rows(index)("REASON"))
                    DataArray(index, 18) = Helper.DB_NullOrEmpty(dt.Rows(index)("REMARKS"))

                    'xlSheet.Rows("4:4").Insert()

                    'xlSheet.Cells(4, 1).Value = cnt
                    'xlSheet.Cells(4, 2).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("TRAN_CODE"))
                    'xlSheet.Cells(4, 3).Value = Helper.DB_NullOrEmptyHaveTime(dt.Rows(index)("TRAN_DATE"))
                    'xlSheet.Cells(4, 4).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_COMPANY"))
                    'xlSheet.Cells(4, 5).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_BY_COMPANY"))
                    'xlSheet.Cells(4, 6).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_SEIDO"))
                    'xlSheet.Cells(4, 7).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_INTERVAL"))
                    'xlSheet.Cells(4, 8).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_DATE"))
                    'xlSheet.Cells(4, 9).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("KEKKA"))
                    'xlSheet.Cells(4, 10).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("DUE_DATE"))
                    'xlSheet.Cells(4, 11).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("KOUSEI_KIKAN"))
                    'xlSheet.Cells(4, 12).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("NOURYOKU"))
                    'xlSheet.Cells(4, 13).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("ATA"))
                    'xlSheet.Cells(4, 14).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("STATUS"))
                    'xlSheet.Cells(4, 15).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("ALLOW_FLG"))
                    'xlSheet.Cells(4, 16).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_DATE"))
                    'xlSheet.Cells(4, 17).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_BY"))
                    'xlSheet.Cells(4, 18).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("REASON"))
                    'xlSheet.Cells(4, 19).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("REMARKS"))



                    'cnt = cnt - 1
                Next
                xlSheet.Range("A4").Resize(cnt, 19).Value = DataArray
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（車）    
    '
    Private Sub Exp_M_CAR(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable

        If Check_Sheet_Exist(xlApp, "M_CAR", Me.CAR_FLG) = False Then
            Return
        End If
        Try

            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_CAR", main.kanriNo)

            If dt.Rows.Count > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_CAR"))
                xlSheet.Cells(12, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("LEASE_COMPANY"))
                xlSheet.Cells(15, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CHECK_COMPANY"))
                xlSheet.Cells(16, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CHECK_BY"))
                xlSheet.Cells(17, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CHECK_DATE"))
                xlSheet.Cells(18, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("NEXT_CHECK_DATE"))
                xlSheet.Cells(19, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("SYOUKAKI"))
                xlSheet.Cells(20, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("SYOUKAKI_DUE_DATE"))
                xlSheet.Cells(21, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_DATE"))
                xlSheet.Cells(22, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_BY"))

                xlSheet.Cells(23, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS"))
                xlSheet.Cells(26, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REMARKS"))
                xlSheet.Cells(27, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REASON"))

                If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
                Else
                    xlSheet.Hyperlinks.Add(xlSheet.Cells(28, 3), relativeImagePath) '写真
                End If

                xlSheet.Cells(29, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG"))
                If Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG")) = "Y" Then
                    combinationStatus.Append(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_CAR") + ":" + Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS")))
                    combinationStatus.Append(vbCrLf)
                End If
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（車履歴）
    '
    Private Sub Exp_M_CAR_HISTORY(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable
        If Check_Sheet_Exist(xlApp, "M_CAR_HISTORY", Me.CAR_FLG) = False Then
            Return
        End If

        Try

            'KANRI_NO
            dt = MasterRepository.GetAllHistoryDataFromDB("M_CAR_HISTORY", main.gid)
            Dim cnt As Integer = dt.Rows.Count
            If cnt > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_CAR_HISTORY"))
                For index = 0 To dt.Rows.Count - 1
                    xlSheet.Rows("4:4").Insert()

                    xlSheet.Cells(4, 1).Value = cnt
                    xlSheet.Cells(4, 2).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("TRAN_CODE"))
                    xlSheet.Cells(4, 3).Value = Helper.DB_NullOrEmptyHaveTime(dt.Rows(index)("TRAN_DATE"))
                    xlSheet.Cells(4, 4).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("CHECK_COMPANY"))
                    xlSheet.Cells(4, 5).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("CHECK_BY"))
                    xlSheet.Cells(4, 6).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("CHECK_DATE"))
                    xlSheet.Cells(4, 7).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("NEXT_CHECK_DATE"))
                    xlSheet.Cells(4, 8).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("SYOUKAKI"))
                    xlSheet.Cells(4, 9).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("SYOUKAKI_DUE_DATE"))
                    xlSheet.Cells(4, 10).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("STATUS"))
                    xlSheet.Cells(4, 11).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("ALLOW_FLG"))
                    xlSheet.Cells(4, 12).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_DATE"))
                    xlSheet.Cells(4, 13).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_BY"))
                    xlSheet.Cells(4, 14).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("REASON"))
                    xlSheet.Cells(4, 15).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("REMARKS"))

                    cnt = cnt - 1
                Next

                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（借用設備）
    '
    Private Sub Exp_M_RENTAL(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable

        If Check_Sheet_Exist(xlApp, "M_RENTAL", Me.RENTAL_FLG) = False Then
            Return
        End If
        Try

            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_RENTAL", main.kanriNo)

            If dt.Rows.Count > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_RENTAL"))
                xlSheet.Cells(10, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ATA"))
                xlSheet.Cells(15, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("LEASE_CONPANY"))
                xlSheet.Cells(16, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("NOURYOKU"))
                xlSheet.Cells(17, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("TEKIGOUSEI"))
                xlSheet.Cells(19, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("LEASE_DATE"))
                xlSheet.Cells(20, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("DUE_DATE"))
                xlSheet.Cells(21, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KAKUNIN_DATE"))
                xlSheet.Cells(22, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("KEKKA"))
                xlSheet.Cells(23, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_BY"))
                xlSheet.Cells(26, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REMARKS"))
                xlSheet.Cells(27, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REASON"))

                If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
                Else
                    xlSheet.Hyperlinks.Add(xlSheet.Cells(28, 3), relativeImagePath) '写真
                End If

                xlSheet.Cells(29, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG"))
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（RENTAL履歴）
    '
    Private Sub Exp_M_RENTAL_HISTORY(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable
        If Check_Sheet_Exist(xlApp, "M_RENTAL_HISTORY", Me.RENTAL_FLG) = False Then
            Return
        End If

        Try

            'KANRI_NO
            dt = MasterRepository.GetAllHistoryDataFromDB("M_RENTAL_HISTORY", main.gid)
            Dim cnt As Integer = dt.Rows.Count
            If cnt > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_RENTAL_HISTORY"))

                Dim DataArray(65000, 28) As Object
                For index = 0 To dt.Rows.Count - 1
                    DataArray(index, 0) = index + 1
                    DataArray(index, 1) = Helper.DB_NullOrEmpty(dt.Rows(index)("TRAN_CODE"))
                    DataArray(index, 2) = Helper.DB_NullOrEmptyHaveTime(dt.Rows(index)("TRAN_DATE"))
                    DataArray(index, 3) = Helper.DB_NullOrEmpty(dt.Rows(index)("LEASE_CONPANY"))
                    DataArray(index, 4) = Helper.DB_NullOrEmpty(dt.Rows(index)("NOURYOKU"))
                    DataArray(index, 5) = Helper.DB_NullOrEmpty(dt.Rows(index)("TEKIGOUSEI"))
                    DataArray(index, 6) = Helper.DB_NullOrEmpty(dt.Rows(index)("LEASE_DATE"))
                    DataArray(index, 7) = Helper.DB_NullOrEmpty(dt.Rows(index)("DUE_DATE"))
                    DataArray(index, 8) = Helper.DB_NullOrEmpty(dt.Rows(index)("KAKUNIN_DATE"))
                    DataArray(index, 9) = Helper.DB_NullOrEmpty(dt.Rows(index)("ATA"))
                    DataArray(index, 10) = Helper.DB_NullOrEmpty(dt.Rows(index)("KEKKA"))
                    DataArray(index, 11) = Helper.DB_NullOrEmpty(dt.Rows(index)("ALLOW_FLG"))
                    DataArray(index, 12) = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_BY"))
                    DataArray(index, 13) = Helper.DB_NullOrEmpty(dt.Rows(index)("REASON"))
                    DataArray(index, 14) = Helper.DB_NullOrEmpty(dt.Rows(index)("REMARKS"))

                Next
                xlSheet.Range("A4").Resize(cnt, 15).Value = DataArray
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（施設）
    '
    Private Sub Exp_M_SHISETSU(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable

        If Check_Sheet_Exist(xlApp, "M_SHISETSU", Me.SHISETSU_FLG) = False Then
            Return
        End If
        Try

            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_SHISETSU", main.kanriNo)

            If dt.Rows.Count > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_SHISETSU"))
                xlSheet.Cells(15, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CHECK_COMPANY"))
                xlSheet.Cells(16, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CHECK_BY"))
                xlSheet.Cells(17, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("CHECK_DATE"))
                xlSheet.Cells(18, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("NEXT_CHECK_DATE"))
                xlSheet.Cells(19, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_DATE"))
                xlSheet.Cells(20, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_BY"))
                If String.IsNullOrEmpty(Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS"))) = False Then
                    combinationStatus.Append(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_SHISETSU") + ":" + Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS")))
                    combinationStatus.Append(vbCrLf)
                End If
                xlSheet.Cells(21, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS"))
                xlSheet.Cells(24, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REMARKS"))
                xlSheet.Cells(25, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REASON"))

                If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
                Else
                    xlSheet.Hyperlinks.Add(xlSheet.Cells(26, 3), relativeImagePath) '写真
                End If
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（施設履歴）
    '
    Private Sub Exp_M_SHISETSU_HISTORY(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable
        If Check_Sheet_Exist(xlApp, "M_SHISETSU_HISTORY", Me.SHISETSU_FLG) = False Then
            Return
        End If
        Try

            'KANRI_NO
            dt = MasterRepository.GetAllHistoryDataFromDB("M_SHISETSU_HISTORY", main.gid)
            Dim cnt As Integer = dt.Rows.Count
            If cnt > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_SHISETSU_HISTORY"))
                For index = 0 To dt.Rows.Count - 1
                    xlSheet.Rows("4:4").Insert()

                    xlSheet.Cells(4, 1).Value = cnt
                    xlSheet.Cells(4, 2).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("TRAN_CODE"))
                    xlSheet.Cells(4, 3).Value = Helper.DB_NullOrEmptyHaveTime(dt.Rows(index)("TRAN_DATE"))
                    xlSheet.Cells(4, 4).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("CHECK_COMPANY"))
                    xlSheet.Cells(4, 5).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("CHECK_BY"))
                    xlSheet.Cells(4, 6).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("CHECK_DATE"))
                    xlSheet.Cells(4, 7).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("NEXT_CHECK_DATE"))
                    xlSheet.Cells(4, 8).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("STATUS"))
                    xlSheet.Cells(4, 9).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_DATE"))
                    xlSheet.Cells(4, 10).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_BY"))
                    xlSheet.Cells(4, 11).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("REASON"))
                    xlSheet.Cells(4, 12).Value = Helper.DB_NullOrEmpty(dt.Rows(index)("REMARKS"))

                    cnt = cnt - 1
                Next

                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（その他）
    '
    Private Sub Exp_M_OTHER(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable

        If Check_Sheet_Exist(xlApp, "M_OTHER", Me.OTHER_FLG) = False Then
            Return
        End If

        Try

            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_OTHER", main.kanriNo)

            If dt.Rows.Count > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_OTHER"))
                xlSheet.Cells(15, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS"))
                xlSheet.Cells(18, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REASON"))

                If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
                Else
                    xlSheet.Hyperlinks.Add(xlSheet.Cells(19, 3), relativeImagePath) '写真
                End If

                xlSheet.Cells(20, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG"))
                If Helper.DB_NullOrEmpty(dt.Rows(0)("ALLOW_FLG")) = "Y" Then
                    combinationStatus.Append(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_OTHER") + ":" + Helper.DB_NullOrEmpty(dt.Rows(0)("STATUS")))
                End If
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（リース）
    '
    Private Sub Exp_M_LEASE(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable

        If Check_Sheet_Exist(xlApp, "M_LEASE", Me.LEASE_FLG) = False Then
            Return
        End If
        Try

            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_LEASE", main.kanriNo)

            If dt.Rows.Count > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_LEASE"))
                xlSheet.Cells(11, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("LEASE_COMPANY"))
                xlSheet.Cells(12, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("LEASE_NO"))
                xlSheet.Cells(13, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("FIRST_LOCATION"))
                xlSheet.Cells(14, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("LEASE_COUNT"))
                xlSheet.Cells(15, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("LEASE_START_DATE"))
                xlSheet.Cells(16, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("LEASE_END_DATE"))
                xlSheet.Cells(17, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_DATE"))
                xlSheet.Cells(18, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("MODIFIED_BY"))
                xlSheet.Cells(21, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REMARKS"))
                xlSheet.Cells(22, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REASON"))
                If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
                Else
                    xlSheet.Hyperlinks.Add(xlSheet.Cells(23, 3), relativeImagePath) '写真
                End If


                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（リース履歴）
    '
    Private Sub Exp_M_LEASE_HISTORY(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable
        If Check_Sheet_Exist(xlApp, "M_LEASE_HISTORY", Me.LEASE_FLG) = False Then
            Return
        End If

        Try

            'KANRI_NO
            dt = MasterRepository.GetAllHistoryDataFromDB("M_LEASE_HISTORY", main.gid)
            Dim cnt As Integer = dt.Rows.Count
            If cnt > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_LEASE_HISTORY"))

                Dim DataArray(65000, 28) As Object
                For index = 0 To dt.Rows.Count - 1
                    DataArray(index, 0) = index + 1
                    DataArray(index, 1) = Helper.DB_NullOrEmpty(dt.Rows(index)("TRAN_CODE"))
                    DataArray(index, 2) = Helper.DB_NullOrEmptyHaveTime(dt.Rows(index)("TRAN_DATE"))
                    DataArray(index, 3) = Helper.DB_NullOrEmpty(dt.Rows(index)("LEASE_COMPANY"))
                    DataArray(index, 4) = Helper.DB_NullOrEmpty(dt.Rows(index)("LEASE_NO"))
                    DataArray(index, 5) = Helper.DB_NullOrEmpty(dt.Rows(index)("FIRST_LOCATION"))
                    DataArray(index, 6) = Helper.DB_NullOrEmpty(dt.Rows(index)("LEASE_COUNT"))
                    DataArray(index, 7) = Helper.DB_NullOrEmpty(dt.Rows(index)("LEASE_START_DATE"))
                    DataArray(index, 8) = Helper.DB_NullOrEmpty(dt.Rows(index)("LEASE_END_DATE"))
                    DataArray(index, 9) = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_DATE"))
                    DataArray(index, 10) = Helper.DB_NullOrEmpty(dt.Rows(index)("MODIFIED_BY"))
                    DataArray(index, 11) = Helper.DB_NullOrEmpty(dt.Rows(index)("REASON"))
                    DataArray(index, 12) = Helper.DB_NullOrEmpty(dt.Rows(index)("REMARKS"))
                Next
                xlSheet.Range("A4").Resize(cnt, 13).Value = DataArray
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（廃棄）
    '
    Private Sub Exp_M_HAIKI(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable

        If Check_Sheet_Exist(xlApp, "M_HAIKI", Me.HAIKI_FLG) = False Then
            Return
        End If

        Try

            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_HAIKI", main.kanriNo)

            If dt.Rows.Count > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_HAIKI"))
                xlSheet.Cells(14, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("REMARKS"))
                If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
                Else
                    xlSheet.Hyperlinks.Add(xlSheet.Cells(15, 3), relativeImagePath) '写真
                End If
                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    '
    '   情報の出力処理（資産）
    '
    Private Sub Exp_M_SHISAN(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Dim dt As System.Data.DataTable

        If Check_Sheet_Exist(xlApp, "M_SHISAN", Me.SHISAN_FLG) = False Then
            Return
        End If

        Try

            'KANRI_NO
            dt = MasterRepository.GetAllDataFromDB("M_SHISAN", main.kanriNo)

            If dt.Rows.Count > 0 Then
                isNoData = False
                'xlSheet = xlApp.ActiveSheet
                xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "M_SHISAN"))

                xlSheet.Cells(12, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("TAIYOU"))
                xlSheet.Cells(13, 3).Value = Helper.DB_NullOrEmpty(dt.Rows(0)("SHISAN_NO"))
                If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
                Else
                    xlSheet.Hyperlinks.Add(xlSheet.Cells(18, 3), relativeImagePath) '写真
                End If

                xlSheet = Nothing
            End If
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub

    Private Sub Exp_VIEW_PAGE(ByRef xlApp As Microsoft.Office.Interop.Excel.Application)
        Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet
        Try

            xlSheet = xlApp.Sheets.Item(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "VIEW_PAGE"))
            If xlSheet Is Nothing Then
                Return
            End If
            'xlSheet = xlApp.ActiveSheet
            xlSheet = xlApp.Worksheets(Ini.ReadValue(Ini.Config.DB, "Excel_Sheet_Name", "VIEW_PAGE"))
            xlSheet.Cells(23, 3).Value = combinationStatus.ToString

            If relativePDFPath Is Nothing Or "".Equals(relativePDFPath) Then
            Else
                xlSheet.Hyperlinks.Add(xlSheet.Cells(14, 3), relativePDFPath) 'Certificate
            End If

            If relativeImagePath Is Nothing Or "".Equals(relativeImagePath) Then
            Else
                xlSheet.Hyperlinks.Add(xlSheet.Cells(26, 3), relativeImagePath) '写真
            End If
            xlSheet = Nothing
        Catch ex As Exception
            Throw ex
        Finally
        End Try
    End Sub
End Class
