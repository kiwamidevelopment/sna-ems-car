﻿Imports System.Data

Public Class Kousei
    Public meisyou As String
    Public mainForm As Main
    Public gid As String
    Private haita_flag As Boolean = False
    Private kUpdate As KouseiUpdate
    Public txt_remark As String = ""
    Public txt_nouryoku As String = ""
    Public txt_kousei_company As String = ""

    '
    '   クラスの新規生成処理
    '   
    Public Sub New(ByVal main As Main)
        mainForm = main
        InitializeComponent()
        M_KOUSEI_KANRI_DataGridView.AutoGenerateColumns = False
        '   Verify the user's access permission
        If LoginRoler.Employee Is Nothing Then
            Me.Close()
            Application.ExitThread()
            Return
        End If
    End Sub

    '
    '   初期化処理
    '
    Private Sub Kousei_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Text = Ini.ReadValue(Ini.Config.UX, "Setting", "Kousei")
        Helper.SetAllControlText(Me)
        gid = mainForm.gid
        M_HEADER_MEISYOU.Text = mainForm.M_HEADER_MEISYOU.Text
        M_HEADER_BUNRUI_01.Text = mainForm.M_HEADER_BUNRUI_01.Text
        M_HEADER_MODEL.Text = mainForm.M_HEADER_MODEL.Text
        M_HEADER_MFG.Text = mainForm.M_HEADER_MFG.Text
        M_HEADER_MFG_NO.Text = mainForm.M_HEADER_MFG_NO.Text
        M_HEADER_BASE.Text = mainForm.M_HEADER_BASE.Text
        M_PME_ATA.Text = mainForm.M_PME_ATA.Text
        M_HEADER_CREATED_DATE.Text = mainForm.M_HEADER_CREATED_DATE.Text
        M_PME_KOUSEI_SEIDO.Text = mainForm.M_PME_KOUSEI_SEIDO.Text
        M_PME_KOUSEI_INTERVAL.Text = mainForm.M_PME_KOUSEI_INTERVAL.Text
        M_HEADER_KANRI_NO.Text = mainForm.M_HEADER_KANRI_NO.Text
        txt_remark = mainForm.M_PME_REMARKS.Text
        txt_nouryoku = mainForm.M_PME_NOURYOKU.Text
        txt_kousei_company = mainForm.M_PME_KOUSEI_COMPANY.Text

        InitDataGrid(True)

        SetUserRole()
    End Sub

    Private Sub btn_New_Click(sender As System.Object, e As System.EventArgs) Handles btn_New.Click
        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor

            If kUpdate IsNot Nothing Then
                kUpdate.Dispose()
                kUpdate = Nothing
            End If
            kUpdate = New KouseiUpdate(Me, gid, 0, M_HEADER_KANRI_NO.Text, M_HEADER_BUNRUI_01.Text)
            kUpdate.Show()
        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btn_Update_Click(sender As System.Object, e As System.EventArgs) Handles btn_Update.Click
        If M_KOUSEI_KANRI_DataGridView.SelectedRows.Count = 0 Then
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "Main", "M02113"))   'データは選択されていません
            Return
        End If
        OpenKouseiUpdate()
    End Sub

    Private Sub btn_Del_Click(sender As System.Object, e As System.EventArgs) Handles btn_Del.Click
        If M_KOUSEI_KANRI_DataGridView.SelectedRows.Count = 0 Then
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "Main", "M02113"))  'データは選択されていません
            Return
        End If
        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor


               Dim str As String = String.Empty
                For Each dr As DataGridViewRow In Me.M_KOUSEI_KANRI_DataGridView.Rows
                    Try
                        Dim cbx As DataGridViewCheckBoxCell = DirectCast(dr.Cells("CHK"), DataGridViewCheckBoxCell)
                        If CBool(cbx.FormattedValue) Then
                            str = String.Format("{0}, {1}", str, dr.Cells("SID").Value)
                        End If
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                Next
            If str IsNot String.Empty And str.Length > 0 Then
                If MessageBox.Show(Ini.ReadValue(Ini.Config.LANG, "Main", "M02118"), "yes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then '削除確認
                    KouseiRepository.Delete("M_KOUSEI_KANRI", String.Format("SID IN ({0})", str.Substring(1)))
                    InitDataGrid(False)
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02110"), MsgBoxStyle.Information, "Information")
                End If
            Else
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02115"), MsgBoxStyle.Information, "Information")
            End If
        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub



    Public Sub InitDataGrid(useHaita As Boolean)
        If useHaita Then
            If haita_flag Then
                Return
            End If
            haita_flag = True
        End If

        Try
            If useHaita Then
                Me.Cursor = Cursors.WaitCursor
            End If
            M_KOUSEI_KANRI_DataGridView.DataSource = KouseiRepository.GetAllDataFromDB(gid)
        Catch ex As Exception
            Helper.Log("Kousei_Load", ex)
        Finally
            If useHaita Then
                Me.Cursor = Cursors.Default
                haita_flag = False
            End If
        End Try
    End Sub

    Private Sub M_KOUSEI_KANRI_DataGridView_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles M_KOUSEI_KANRI_DataGridView.CellClick
        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor
            If e.RowIndex > -1 Then
                Me.M_KOUSEI_KANRI_DataGridView.Rows(e.RowIndex).Cells(0).Value = Not Me.M_KOUSEI_KANRI_DataGridView.Rows(e.RowIndex).Cells(0).Value
                If M_KOUSEI_KANRI_DataGridView.Columns(e.ColumnIndex).Name = "DG_CHECK_FILE_NAME" Then
                    ' 2016-04-07 YAMAOKA EDIT セルの指定番号を修正 START
                    'If Not M_KOUSEI_KANRI_DataGridView.Rows(e.RowIndex).Cells(12).Value Is DBNull.Value Then
                    '    Dim ch_file_name As String = M_KOUSEI_KANRI_DataGridView.Rows(e.RowIndex).Cells(12).Value
                    '    If Not String.IsNullOrEmpty(ch_file_name) Then
                    'Dim ch_file As Object = M_KOUSEI_KANRI_DataGridView.Rows(e.RowIndex).Cells(16).Value
                    If Not M_KOUSEI_KANRI_DataGridView.Rows(e.RowIndex).Cells(11).Value Is DBNull.Value Then
                        Dim ch_file_name As String = M_KOUSEI_KANRI_DataGridView.Rows(e.RowIndex).Cells(11).Value
                        If Not String.IsNullOrEmpty(ch_file_name) Then
                            Dim ch_file As Object = M_KOUSEI_KANRI_DataGridView.Rows(e.RowIndex).Cells(15).Value
                            ' 2016-04-07 YAMAOKA EDIT セルの指定番号を修正 END
                            Dim blob As Byte() = Nothing
                            If TypeOf (ch_file) Is Byte() Then
                                blob = CType(ch_file, Byte())
                            End If
                            If Helper.DownloadFile(blob, ch_file_name) Then
                                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02117"), MsgBoxStyle.Information, "Information")
                            Else
                                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02103"), MsgBoxStyle.Information, "Information")
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Helper.Log("M_KOUSEI_KANRI_DataGridView_CellClick", ex)
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    '
    '   一覧中の情報の出力処理
    ' 
    Private Sub btn_Export_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Export.Click
        Try
            If haita_flag Then
                Return
            End If
            haita_flag = True
            Me.Cursor = Cursors.WaitCursor

            Dim exp As New ExportKouseiExcel(Me)
            exp.Export()
        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
            haita_flag = False
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub Kousei_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        If btn_New.Visible Then
            UpdateMainPME()
        End If
        mainForm.Show()
    End Sub

    'Private Sub M_KOUSEI_KANRI_DataGridView_CellContentDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles M_KOUSEI_KANRI_DataGridView.CellDoubleClick
    '    If (e.RowIndex = -1) Then
    '        Return
    '    End If

    '    OpenKouseiUpdate()
    'End Sub

    Private Sub OpenKouseiUpdate()
        If kUpdate IsNot Nothing Then
            kUpdate.Dispose()
            kUpdate = Nothing
        End If
        Dim sid As Integer = Integer.Parse(M_KOUSEI_KANRI_DataGridView.SelectedRows(0).Cells("SID").Value)
        kUpdate = New KouseiUpdate(Me, gid, sid, M_HEADER_KANRI_NO.Text, M_HEADER_BUNRUI_01.Text)
        kUpdate.Show()

    End Sub




    Private Sub UpdateMainPME()
        Dim dt As DataTable = KouseiRepository.GetTopDataByGID(gid)
        If dt Is Nothing Or dt.Rows.Count = 0 Then
            Return
        End If

        If Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("KOUSEI_DATE")) IsNot "" Then
            mainForm.M_PME_KOUSEI_DATE.Value = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("KOUSEI_DATE"))
        End If
        If Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("DUE_DATE")) IsNot "" Then
            mainForm.M_PME_DUE_DATE.Value = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("DUE_DATE"))
        End If

        If Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("TENKEN_KEKKA")) IsNot "" Then
            mainForm.M_PME_KEKKA.Text = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("TENKEN_KEKKA"))
        Else
            mainForm.M_PME_KEKKA.Text = Helper.DB_NullOrEmpty(dt.Rows.Item(0).Item("JURYOU_KEKKA"))
        End If

    End Sub

    Private Sub btn_GoBack_Click(sender As System.Object, e As System.EventArgs) Handles btn_GoBack.Click
        Me.Close()
    End Sub

    Private Sub SetUserRole()
        'If LoginRoler.Employee.AX_VAD <> "E" Then
        '    btn_New.Visible = False
        '    btn_Update.Visible = False
        '    btn_Del.Visible = False

        'ElseIf LoginRoler.Employee.AX_VAD = "E" Then
        '    btn_New.Visible = True
        '    btn_Update.Visible = True
        '    btn_Del.Visible = True

        'End If

        If LoginRoler.Employee.AX_KOSE = "N" Then
            If LoginRoler.Employee.ROLE = "1" Or LoginRoler.Employee.ROLE = "4" Then
                btn_New.Visible = False
                btn_Update.Visible = False
                btn_Del.Visible = False
            End If

        ElseIf LoginRoler.Employee.AX_KOSE = "R" Then
            If LoginRoler.Employee.ROLE = "1" Or LoginRoler.Employee.ROLE = "2" Then
                btn_New.Visible = True
                btn_Update.Visible = True
                btn_Del.Visible = True
            End If
        ElseIf LoginRoler.Employee.AX_KOSE = "T" Then
            If LoginRoler.Employee.ROLE = "1" Or LoginRoler.Employee.ROLE = "3" Then
                btn_New.Visible = True
                btn_Update.Visible = True
                btn_Del.Visible = True
            End If
        ElseIf LoginRoler.Employee.AX_KOSE = "B" Then
            If LoginRoler.Employee.ROLE = "1" Or LoginRoler.Employee.ROLE = "2" Or LoginRoler.Employee.ROLE = "3" Then
                btn_New.Visible = True
                btn_Update.Visible = True
                btn_Del.Visible = True
            End If
        Else
            btn_New.Visible = False
            btn_Update.Visible = False
            btn_Del.Visible = False
        End If
    End Sub


End Class