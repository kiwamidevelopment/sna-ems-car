﻿Imports System.IO
Imports System.Data
Imports System.ComponentModel


'
'   クラス：Login
'   処理　：ログイン画面での入力情報の認証処理を行う
'
Public Class Login
    Public kensaku As Kensaku   '2015/09/15 taskplan 広域化

    '   
    '   ログインのキャンセル処理
    '
    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Close.Click
        Dim Rtn = MsgBox(Ini.ReadValue(Ini.Config.LANG, "Login", "W02001"), MsgBoxStyle.YesNo, "Warning")
        If Rtn = 6 Then
            Me.Close()
            Application.ExitThread()
        End If
    End Sub

    '
    '   承認処理の呼び出し
    '   
    Private Sub Login_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Login.Click
        Me.backgroundWorker.RunWorkerAsync()
    End Sub

    Private Sub Password_TextBox_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Password_TextBox.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            Me.backgroundWorker.RunWorkerAsync()
        End If
    End Sub

    Private Sub backgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As RunWorkerCompletedEventArgs) Handles backgroundWorker.RunWorkerCompleted
        Login()
    End Sub

    '
    '   入力情報の認証処理
    '   
    Private Sub Login()
        If String.IsNullOrEmpty(UserName_TextBox.Text) Then
            MsgBox(Ini.ReadValue(Ini.Config.LANG, "Login", "LoginName_Is_Null"), MsgBoxStyle.Information, "Warning")
            Return
        End If

        If String.IsNullOrEmpty(Password_TextBox.Text) Then
            MsgBox(Ini.ReadValue(Ini.Config.LANG, "Login", "LoginPwd_Is_Null"), MsgBoxStyle.Information, "Warning")
            Return
        End If
        Dim employee As M_EMPLOYEE
        Try
            Dim role As String = AX_KOSE_Combo.SelectedValue
            If role = "0" Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Login", "E02002"), MsgBoxStyle.Critical, "Error")
                Return
            End If

            ' 2016-04-08 YAMAOKA EDIT 役割とユーザーの関連付け START
            'employee = EmployeeRepository.GetEmployee(UserName_TextBox.Text, Password_TextBox.Text)
            employee = EmployeeRepository.GetEmployee(UserName_TextBox.Text, Password_TextBox.Text, AX_KOSE_Combo.SelectedIndex)
            ' 2016-04-08 YAMAOKA EDIT 役割とユーザーの関連付け END
            'If employee Is Nothing Or employee.EMP_PSWD <> Password_TextBox.Text Then
            If employee Is Nothing Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Login", "E02001"), MsgBoxStyle.Critical, "Error")
                Return
            End If

            ' validate user purview
            Dim kose As String = employee.AX_KOSE
            If kose = "N" Then 'N:view E:edit
                If role = "2" Or role = "3" Then
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Login", "E02002"), MsgBoxStyle.Critical, "Error")
                    Return
                End If
            ElseIf kose = "R" Then
                If role = "3" Then
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Login", "E02002"), MsgBoxStyle.Critical, "Error")
                    Return
                End If
            ElseIf kose = "T" Then
                If role = "2" Then
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Login", "E02002"), MsgBoxStyle.Critical, "Error")
                    Return
                End If
            ElseIf kose = "B" Then
                If role = "1" Then
                    MsgBox(Ini.ReadValue(Ini.Config.LANG, "Login", "E02002"), MsgBoxStyle.Critical, "Error")
                    Return
                End If
            End If

            employee.ROLE = role
        Catch ex As Exception
            Dim msg As String = Ini.ReadValue(Ini.Config.LANG, "System", "E00001")
            Helper.Log(msg, ex)
            MsgBox(msg & Environment.NewLine & Environment.NewLine & "詳細：" & ex.Message, MsgBoxStyle.Critical, "Error")
            Return
        End Try

        LoginRoler.Employee = employee

        Kensaku = New Kensaku(Me)
        kensaku.Show()

        'Login Log
        EmployeeRepository.AddLoginLog(LoginLog.LOGIN_IN)
    End Sub

    '
    '   初期化処理
    '   
    Private Sub LoginForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text = Ini.ReadValue(Ini.Config.UX, "Setting", "SYSNAME")
            Helper.SetAllControlText(Me)
            AX_KOSE_Combo.DataSource = ParameterRepository.GetParameters("AX_KOSE", Nothing)
            AX_KOSE_Combo.DisplayMember = "PARAMETER_VALUE"
            AX_KOSE_Combo.ValueMember = "SEQ_NO"

            AX_KOSE_Combo.SelectedIndex = 1
        Catch ex As Exception

        End Try
    End Sub
End Class
