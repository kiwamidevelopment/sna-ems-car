﻿Imports System.Data

'
'   クラス：History
'   処理　：設備の各種情報の更新履歴を取得表示する
'
Public Class History
    Public tabName As String = Nothing
    Public titleName As String = Nothing
    Public gid As String = Nothing

    '
    '   クラスの新規生成処理
    '   
    Public Sub New()
        InitializeComponent()
        '   Verify the user's access permission
        If LoginRoler.Employee Is Nothing Then
            Me.Close()
            Application.ExitThread()
            Return
        End If
    End Sub

    '
    '   初期化処理
    '
    Private Sub History_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = Ini.ReadValue(Ini.Config.UX, "Setting", "History")
    End Sub

    '
    '   更新履歴情報の取得
    '
    Public Sub SearchHistoryData()
        Try
            Me.Text = Ini.ReadValue(Ini.Config.UX, "Setting", "History") + "-" + titleName

            Me.Cursor = Cursors.WaitCursor
            historyDataGridView.DataSource = Nothing
            Dim historyTable As DataTable = MasterRepository.GetHistoryDataTable(tabName, gid)
            If historyTable.Columns.Contains("image") Then
                historyTable.Columns.Remove("image")
            End If
            If historyTable.Columns.Contains("certificate") Then
                historyTable.Columns.Remove("certificate")
            End If
            If historyTable.Columns.Contains("gid") Then
                historyTable.Columns.Remove("gid")
            End If

            For index = 1 To historyTable.Columns.Count
                Dim colName = historyTable.Columns.Item(index - 1).ColumnName
                Dim str = Ini.ReadValue(Ini.Config.UX, "Setting", colName)
                If Not String.IsNullOrEmpty(str) Then
                    historyTable.Columns.Item(index - 1).ColumnName = str
                End If
            Next

            historyDataGridView.DataSource = historyTable
            If historyTable Is Nothing Or historyTable.Rows.Count = 0 Then
                MsgBox(Ini.ReadValue(Ini.Config.LANG, "Main", "M02103"), MsgBoxStyle.Information, "Warning")
            End If

            For index = 1 To historyTable.Columns.Count
                Dim str = historyTable.Columns.Item(index - 1).ColumnName
                '   日付フォーマットの対応 2015/04/10
                If str = "廃棄日" Or str = "更新日" Or str = "登録日" Or str = "点検期限" Or str = "点検実施日" Or str = "次回点検日" Or str = "有効期限" Or str = "校正実施日" Or str = "借用日" Or str = "校正結果確認日" Or str = "リース開始日" Or str = "リース満了日" Then
                    historyDataGridView.Columns.Item(index - 1).DefaultCellStyle.Format = "yyyy/MM/dd"
                End If

            Next


        Catch ex As Exception
            Helper.MsgBoxAndLog(Ini.ReadValue(Ini.Config.LANG, "System", "E00001"), ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    '
    '   画面の終了処理
    '
    Private Sub btn_Close_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Close.Click
        Me.Close()
        Me.Dispose(True)
    End Sub

    Private Sub History_Resize(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Resize

    End Sub
End Class